﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Line : MonoBehaviour
{

    public Vector3 p0, p1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    
}

[CustomEditor(typeof(Line))]
public class LineInspector : Editor {
    private void OnSceneGUI() {
        Line line = target as Line;
        Transform hTransform = line.transform;
        Quaternion hRotation = Tools.pivotRotation == PivotRotation.Local ? hTransform.rotation : Quaternion.identity;

        Vector3 p0 = hTransform.TransformPoint(line.p0);
		Vector3 p1 = hTransform.TransformPoint(line.p1);

        Handles.color = Color.white;
        Handles.DrawLine(line.p0, line.p1);

        EditorGUI.BeginChangeCheck();
		p0 = Handles.DoPositionHandle(p0, hRotation);
		if (EditorGUI.EndChangeCheck()) {
            Undo.RecordObject(line, "Move Point");
			EditorUtility.SetDirty(line);
			line.p0 = hTransform.InverseTransformPoint(p0);
		}
		EditorGUI.BeginChangeCheck();
		p1 = Handles.DoPositionHandle(p1, hRotation);
		if (EditorGUI.EndChangeCheck()) {
            Undo.RecordObject(line, "Move Point");
			EditorUtility.SetDirty(line);
			line.p1 = hTransform.InverseTransformPoint(p1);
		}
    }
}
