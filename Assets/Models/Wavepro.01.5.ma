//Maya ASCII 2019 scene
//Name: Wavepro.01.5.ma
//Last modified: Wed, Apr 03, 2019 01:21:51 PM
//Codeset: 1252
requires maya "2019";
requires "stereoCamera" "10.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2019";
fileInfo "version" "2019";
fileInfo "cutIdentifier" "201812112215-434d8d9c04";
fileInfo "osv" "Microsoft Windows 10 Technical Preview  (Build 17134)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "5A317293-406C-9185-5492-AEA5E46A18ED";
	setAttr ".t" -type "double3" 178.27273851928669 121.67633595161934 174.39078140730305 ;
	setAttr ".r" -type "double3" -28.538352729655799 41.800000000001603 2.1332374770057372e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "9885D28F-48A8-B7A9-3E8A-09A5273F396A";
	setAttr -k off ".v";
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 264.71653055828182;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "E9AB178A-4C66-E1BB-DA1F-D192F0C9C3D1";
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "E7837EBF-42C2-4CA6-36E0-03BDA3827FA4";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "3F3AC9A4-43FF-B0C3-E4CA-70B5C2B12141";
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "11B2ED60-484F-0FF3-30A4-A58D4B4D2A39";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "CFC5DDD2-4048-BE42-8719-B9B1236E7B83";
	setAttr ".t" -type "double3" 100.10744832501547 -0.71582556787689633 3.8095592119926671 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "866D3FE7-423E-C74B-3928-E6A2EB18F615";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 28.824992960749661;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "waveGRP_01";
	rename -uid "3A52668A-469C-664F-E909-ECA4C216215B";
	setAttr ".rp" -type "double3" 0 0.66297004715381647 -3.0485750753698451 ;
	setAttr ".sp" -type "double3" 0 0.66297004715381647 -3.0485750753698451 ;
createNode transform -n "CRV" -p "waveGRP_01";
	rename -uid "E7F61DA6-4074-ECBE-1AFD-6198690D1CFA";
	setAttr ".it" no;
createNode nurbsCurve -n "CRVShape" -p "|waveGRP_01|CRV";
	rename -uid "5204692E-49C8-C2BD-A10F-C0BD9DB585C2";
	setAttr -k off ".v";
	setAttr ".tw" yes;
	setAttr -s 51 ".cp[0:50]" -type "double3" 0 0 13.105178918401744 0 
		0 8.79706842012191 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode transform -n "guts_GRP" -p "waveGRP_01";
	rename -uid "6F65FD92-4491-7ED6-9802-5EA92E44D0DE";
	setAttr ".rp" -type "double3" 0 3.086054304693814 5.1025990357395044 ;
	setAttr ".sp" -type "double3" 0 3.086054304693814 5.1025990357395044 ;
createNode transform -n "curve2" -p "|waveGRP_01|guts_GRP";
	rename -uid "3631FA1F-4EDA-4AAF-961B-B4BE18711C93";
	addAttr -ci true -k true -sn "handleSize" -ln "handleSize" -dv 1 -at "float";
	setAttr ".rp" -type "double3" 0 -178.71178233931573 -1.4586794008110158 ;
	setAttr ".sp" -type "double3" 0 -178.71178233931573 -1.4586794008110158 ;
createNode nurbsCurve -n "curveShape2" -p "|waveGRP_01|guts_GRP|curve2";
	rename -uid "5342DCBE-4697-6F88-7E39-5E9578B175EB";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve4" -p "|waveGRP_01|guts_GRP";
	rename -uid "02DE5EA4-4B28-0D74-AA51-47888CB8D58A";
	addAttr -ci true -k true -sn "handleSize" -ln "handleSize" -dv 1 -at "float";
	setAttr ".rp" -type "double3" 0 -49.027239636955784 2.1534198201671906 ;
	setAttr ".sp" -type "double3" 0 -49.027239636955784 2.1534198201671906 ;
createNode nurbsCurve -n "curveShape4" -p "|waveGRP_01|guts_GRP|curve4";
	rename -uid "90838937-4BC0-2751-FDED-D69CADD1E53B";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve2attachedCurve1" -p "|waveGRP_01|guts_GRP";
	rename -uid "D13D1E7C-4314-1EBE-6279-E2B43359C2C9";
	setAttr ".it" no;
createNode nurbsCurve -n "curve2attachedCurveShape1" -p "|waveGRP_01|guts_GRP|curve2attachedCurve1";
	rename -uid "B805D8AF-4D38-9F6F-20FB-10B96971968C";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve2attachedCurve1attachedCurve1" -p "|waveGRP_01|guts_GRP";
	rename -uid "BD673B75-4C87-93AD-6256-F3830F603389";
createNode nurbsCurve -n "curve2attachedCurve1attachedCurveShape1" -p "|waveGRP_01|guts_GRP|curve2attachedCurve1attachedCurve1";
	rename -uid "5E08DA85-4F5E-7095-B58A-1BA4FE51A3A6";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "waveCtrlGRP_01Buf" -p "waveGRP_01";
	rename -uid "FE1A69ED-4BA3-1093-7A4C-94BB08799BC7";
createNode parentConstraint -n "waveCtrlGRP_01Buf_parentConstraint1" -p "waveCtrlGRP_01Buf";
	rename -uid "5B2EC2DC-404D-D739-70AB-C982F37CCA8C";
	addAttr -ci true -k true -sn "w0" -ln "wave01_CtrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 0 33.587504550141148 ;
	setAttr -k on ".w0";
createNode transform -n "waveCtrlGRP_01" -p "waveCtrlGRP_01Buf";
	rename -uid "0888951B-4644-2C59-2809-B1B09096E0EF";
	setAttr ".t" -type "double3" 0 0 -33.587504550141148 ;
createNode transform -n "waveHandleBuf" -p "waveCtrlGRP_01";
	rename -uid "2B3AF61E-4EE1-7925-C061-F9BD7CDFEDD0";
createNode transform -n "waveHandle" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandleBuf";
	rename -uid "BF644385-45B7-0761-9DA2-528F8B42B4E7";
createNode nurbsCurve -n "nurbsCircleShape2" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandleBuf|waveHandle";
	rename -uid "789922EF-4330-D3FE-6FF1-EF9F66E04B0D";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandleLoc" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandleBuf|waveHandle";
	rename -uid "EE6D486F-433E-0D21-9B8B-ED8E186D6FB8";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandleLocShape" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandleBuf|waveHandle|waveHandleLoc";
	rename -uid "1D840984-48AE-CFDB-357C-4DB655103001";
	setAttr -k off ".v";
createNode transform -n "waveHandle1Buf" -p "waveCtrlGRP_01";
	rename -uid "85DDFD1C-49B6-7AAF-C5A2-F8A6EE0FF6C8";
createNode transform -n "waveHandle1" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle1Buf";
	rename -uid "C05376C6-4B93-BE4F-F71D-05B86C99418E";
createNode nurbsCurve -n "nurbsCircleShape3" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle1Buf|waveHandle1";
	rename -uid "A5A9341A-4B40-0B15-1DF1-D39E98E91470";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle1Loc" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle1Buf|waveHandle1";
	rename -uid "B2A2AD16-4FC4-96DA-0C49-60A1A41AF6E9";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle1LocShape" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle1Buf|waveHandle1|waveHandle1Loc";
	rename -uid "2FFA1867-41A1-BBC2-C862-BCA5ACADF7B2";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" 0 0 -8.8817841970012523e-16 ;
createNode transform -n "waveHandle2Buf" -p "waveCtrlGRP_01";
	rename -uid "414CCC5C-483D-64C5-37D8-768C32C72393";
createNode transform -n "waveHandle2" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle2Buf";
	rename -uid "9598C432-4892-C70C-5870-0CA5AB9C3C70";
createNode nurbsCurve -n "nurbsCircleShape4" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle2Buf|waveHandle2";
	rename -uid "DA6BD81B-40BF-6D89-B26F-6389BCE8283E";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle2Loc" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle2Buf|waveHandle2";
	rename -uid "8CEF8228-48D4-5D61-EDD5-3382FBB692AF";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle2LocShape" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle2Buf|waveHandle2|waveHandle2Loc";
	rename -uid "E41D7681-4AD5-D399-2E8A-2281330337FA";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" 0 0 -8.8817841970012523e-16 ;
createNode transform -n "waveHandle3Buf" -p "waveCtrlGRP_01";
	rename -uid "A5591DB7-405C-6948-4883-2FBDFDF6C0DD";
createNode transform -n "waveHandle3" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle3Buf";
	rename -uid "E388DBC7-40C4-1286-8206-20A9FBFAA14C";
createNode nurbsCurve -n "nurbsCircleShape5" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle3Buf|waveHandle3";
	rename -uid "3E6F1902-4D4B-106E-2448-FDBCB16DB259";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle3Loc" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle3Buf|waveHandle3";
	rename -uid "5566817C-47E4-58DF-BAC9-FD9C132AD3AF";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle3LocShape" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle3Buf|waveHandle3|waveHandle3Loc";
	rename -uid "E0F2BA2A-4F03-9F34-1BB5-B589D61CAAD5";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" 0 -4.4408920985006262e-16 0 ;
createNode transform -n "waveHandle4Buf" -p "waveCtrlGRP_01";
	rename -uid "3B9462D7-4D37-7C63-A048-A7B055C24A8B";
createNode transform -n "waveHandle4" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle4Buf";
	rename -uid "5C1D1210-449A-F727-B0AC-AB995D809E94";
createNode nurbsCurve -n "nurbsCircleShape6" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle4Buf|waveHandle4";
	rename -uid "1D4749BA-4690-B1BF-A624-C086E03D3BE6";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840296
		-2.1558421030922106e-33 0.3086000292377824 0
		-1.3361703213645398e-17 0.21821317334840273 0.21821317334840273
		-1.8896301901141494e-17 1.1102230246251565e-16 0.3086000292377824
		-1.3361703213645401e-17 -0.21821317334840273 0.21821317334840273
		-5.6938245452781404e-33 -0.30860002923778251 1.1102230246251565e-16
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.1102230246251565e-16 -0.3086000292377824
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840296
		-2.1558421030922106e-33 0.3086000292377824 0
		-1.3361703213645398e-17 0.21821317334840273 0.21821317334840273
		;
createNode transform -n "waveHandle4Loc" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle4Buf|waveHandle4";
	rename -uid "AFEA0EDE-419D-4699-6482-78A9F60C45F0";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle4LocShape" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle4Buf|waveHandle4|waveHandle4Loc";
	rename -uid "2BA7595C-47A6-D5F4-D3A3-F2AFD7AF14BA";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" 0 -4.4408920985006262e-16 8.8817841970012523e-16 ;
createNode transform -n "waveHandle5Buf" -p "waveCtrlGRP_01";
	rename -uid "76A78A20-4112-DCD8-22DE-22B0987AF3E3";
createNode transform -n "waveHandle5" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle5Buf";
	rename -uid "87F8E07D-41CB-BD5B-38F2-3BA4E5556575";
createNode nurbsCurve -n "nurbsCircleShape1" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle5Buf|waveHandle5";
	rename -uid "41570C2D-483D-8CEC-F91E-969592ED65B5";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.1026781894481244e-16 0.21821317334840271 -0.21821317334840304
		-1.7791145552058667e-32 0.30860002923778257 3.5207573393294986e-17
		-1.1026781894481232e-16 0.21821317334840284 0.21821317334840282
		-1.5594224504505452e-16 8.9424567909960508e-17 0.30860002923778251
		-1.1026781894481236e-16 -0.21821317334840279 0.21821317334840287
		-4.6988441819384398e-32 -0.30860002923778262 9.2987211484036115e-17
		1.1026781894481226e-16 -0.21821317334840287 -0.21821317334840268
		1.5594224504505452e-16 -1.6574967770032629e-16 -0.30860002923778251
		1.1026781894481244e-16 0.21821317334840271 -0.21821317334840304
		-1.7791145552058667e-32 0.30860002923778257 3.5207573393294986e-17
		-1.1026781894481232e-16 0.21821317334840284 0.21821317334840282
		;
createNode transform -n "waveHandle5Loc" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle5Buf|waveHandle5";
	rename -uid "E958B9D0-4571-16AE-51AB-B4AB813918DA";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle5LocShape" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle5Buf|waveHandle5|waveHandle5Loc";
	rename -uid "DAB44747-4D37-D57F-F7BE-1DA28056321B";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" 0 5.5511151231257827e-17 0 ;
createNode transform -n "Anchorcrv1" -p "waveCtrlGRP_01";
	rename -uid "16EF1995-4027-55BB-53AE-E3B939B32070";
	setAttr ".t" -type "double3" 0 0 7.3378464335338505 ;
	setAttr ".rp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
	setAttr ".sp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
createNode nurbsCurve -n "AnchorcrvShape1" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|Anchorcrv1";
	rename -uid "D68DC6C4-46CC-EBE7-9116-BC9B95CC714F";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "Anchorcrv2" -p "waveCtrlGRP_01";
	rename -uid "166BE6E8-4524-B6AB-F1BF-A8905ABF0152";
	setAttr ".t" -type "double3" 0 0 -29.031414968196096 ;
	setAttr ".rp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
	setAttr ".sp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
createNode nurbsCurve -n "AnchorcrvShape2" -p "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|Anchorcrv2";
	rename -uid "530C8FEA-4EFE-1DDD-9A4E-B992F0FED3B1";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 12 0 no 3
		17 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 12 12
		15
		0 0.31649962882926275 3.473816476486677
		0 0.32032474979003439 3.7140401444416846
		0 0.32733752875043509 4.1944978750028756
		0 0.33594421944510944 4.9152096511274133
		0 0.34263837575037198 5.6359417812960499
		0 0.34741994946530685 6.3566891235950846
		0 0.35028890720410627 7.0774466205211217
		0 0.35124522868645058 7.7982091919612788
		0 0.35028890720413208 8.5189717634014333
		0 0.34741994946520338 9.2397292603274774
		0 0.342638375750419 9.9604766026265015
		0 0.33594421944502451 10.681208732795145
		0 0.32733752875038624 11.401920508919678
		0 0.32032474978997755 11.882378239480872
		0 0.3164996288292059 12.122601907435879
		;
createNode transform -n "waveGRP_02";
	rename -uid "8352EF88-4DBC-DC61-0ECE-5D90EE82882D";
	setAttr ".rp" -type "double3" 0 0.66297004715381647 -3.0485750753698451 ;
	setAttr ".sp" -type "double3" 0 0.66297004715381647 -3.0485750753698451 ;
createNode transform -n "CRV" -p "waveGRP_02";
	rename -uid "467DB13F-49B5-8AC3-64E2-A7961679AACF";
	setAttr ".it" no;
createNode nurbsCurve -n "CRVShape" -p "|waveGRP_02|CRV";
	rename -uid "B60456BB-4F55-3A34-1FC7-08A3FB6E3F43";
	setAttr -k off ".v";
	setAttr ".tw" yes;
	setAttr -s 51 ".cp[0:50]" -type "double3" 0 0 13.223500623101277 0 
		0 10.416129550418244 0 0 9.5959410029541807 0 0 8.3656151965416363 0 0 7.1352546437762001 
		0 0 5.9048681224097272 0 0 4.6744642660972673 0 0 3.4440517470907075 0 0 2.2136392280841743 
		0 0 0.98323537177174636 0 0 -0.24715114959476736 0 0 -1.4775117023602142 0 0 -2.7078375087727409 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode transform -n "guts_GRP" -p "waveGRP_02";
	rename -uid "728FBADD-45EB-9BBC-D6FF-6987F87F4B7F";
	setAttr ".rp" -type "double3" 0 3.086054304693814 5.1025990357395044 ;
	setAttr ".sp" -type "double3" 0 3.086054304693814 5.1025990357395044 ;
createNode transform -n "curve2" -p "|waveGRP_02|guts_GRP";
	rename -uid "255E7E03-4BEF-99DA-2B8A-E997152A13D1";
	addAttr -ci true -k true -sn "handleSize" -ln "handleSize" -dv 1 -at "float";
	setAttr ".rp" -type "double3" 0 -178.71178233931573 -1.4586794008110158 ;
	setAttr ".sp" -type "double3" 0 -178.71178233931573 -1.4586794008110158 ;
createNode nurbsCurve -n "curveShape2" -p "|waveGRP_02|guts_GRP|curve2";
	rename -uid "9E93587D-4458-F841-92E8-D0A0AA46C285";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve4" -p "|waveGRP_02|guts_GRP";
	rename -uid "76C8A7E9-4FBC-BF33-D914-2097F4E3F8B8";
	addAttr -ci true -k true -sn "handleSize" -ln "handleSize" -dv 1 -at "float";
	setAttr ".rp" -type "double3" 0 -49.027239636955784 2.1534198201671906 ;
	setAttr ".sp" -type "double3" 0 -49.027239636955784 2.1534198201671906 ;
createNode nurbsCurve -n "curveShape4" -p "|waveGRP_02|guts_GRP|curve4";
	rename -uid "69C86BEB-46CC-DB4A-5452-9CA2574E38A8";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve2attachedCurve1" -p "|waveGRP_02|guts_GRP";
	rename -uid "32CFD90B-46BA-3988-EAC4-E7BBF0758291";
	setAttr ".it" no;
createNode nurbsCurve -n "curve2attachedCurveShape1" -p "|waveGRP_02|guts_GRP|curve2attachedCurve1";
	rename -uid "22F683D8-4154-6B84-E031-B89B9FA8BD25";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve2attachedCurve1attachedCurve1" -p "|waveGRP_02|guts_GRP";
	rename -uid "B09B30A4-409A-704D-656A-AFA2F636287B";
createNode nurbsCurve -n "curve2attachedCurve1attachedCurveShape1" -p "|waveGRP_02|guts_GRP|curve2attachedCurve1attachedCurve1";
	rename -uid "1210461C-4DF8-0994-436C-FD8AA306D0AC";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "waveCtrlGRP_02Buf" -p "waveGRP_02";
	rename -uid "56D08989-4B91-563A-3BDA-6EAA91D0C2B8";
createNode parentConstraint -n "waveCtrlGRP_02Buf_parentConstraint1" -p "waveCtrlGRP_02Buf";
	rename -uid "1A18BB6E-48BC-9BDD-B0CA-6F9C02D1E479";
	addAttr -ci true -k true -sn "w0" -ln "wave02_CtrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 4 0 33.587504550141148 ;
	setAttr -k on ".w0";
createNode transform -n "waveCtrlGRP_02" -p "waveCtrlGRP_02Buf";
	rename -uid "E9F01728-42FF-B020-0DDC-45819D273762";
	setAttr ".t" -type "double3" -4 0 -33.587504550141148 ;
createNode transform -n "waveHandleBuf" -p "waveCtrlGRP_02";
	rename -uid "D6687A3D-4E01-A68E-9FA2-949560744D06";
	setAttr ".t" -type "double3" 3.8960116037826449 1.1869772150908968 4.6245948976369542 ;
createNode transform -n "waveHandle" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandleBuf";
	rename -uid "24EA297D-46E6-0F92-24E7-A285A731DD40";
createNode nurbsCurve -n "nurbsCircleShape2" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandleBuf|waveHandle";
	rename -uid "7FC70D44-4F7F-7315-8476-4C9B9B20976D";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandleLoc" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandleBuf|waveHandle";
	rename -uid "8CCCE04A-4F04-E642-15D9-78A34826F45E";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandleLocShape" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandleBuf|waveHandle|waveHandleLoc";
	rename -uid "A4067124-47AD-E112-4578-B9971D346DA6";
	setAttr -k off ".v";
createNode transform -n "waveHandle1Buf" -p "waveCtrlGRP_02";
	rename -uid "70B47C51-4605-58F1-022C-118E8BAAD4DB";
	setAttr ".t" -type "double3" 4 1.6320714382695214 2.434548151301736 ;
createNode transform -n "waveHandle1" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle1Buf";
	rename -uid "320A776C-409F-4B42-08E2-BFA99E0025D8";
createNode nurbsCurve -n "nurbsCircleShape3" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle1Buf|waveHandle1";
	rename -uid "29689430-4447-3D0A-4D40-4EB902641BF1";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle1Loc" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle1Buf|waveHandle1";
	rename -uid "87C1DFDE-4CC9-C24C-63BE-0688C7311D91";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle1LocShape" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle1Buf|waveHandle1|waveHandle1Loc";
	rename -uid "299786FB-426D-D106-6793-ABAE2B6671A0";
	setAttr -k off ".v";
createNode transform -n "waveHandle2Buf" -p "waveCtrlGRP_02";
	rename -uid "345F1059-4762-EADA-3D87-02A135745B4F";
	setAttr ".t" -type "double3" 4 2.2774736510295717 -2.0854817607654041 ;
createNode transform -n "waveHandle2" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle2Buf";
	rename -uid "1E5C2F2C-4C67-938E-0A9F-24A7FC38F3DF";
createNode nurbsCurve -n "nurbsCircleShape4" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle2Buf|waveHandle2";
	rename -uid "606A2A9E-4008-376A-A6BE-6280B60CAACB";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle2Loc" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle2Buf|waveHandle2";
	rename -uid "1FB49618-438F-C47F-3419-D696481764B9";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle2LocShape" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle2Buf|waveHandle2|waveHandle2Loc";
	rename -uid "7F56A4CD-4733-ED5C-2F19-8EA0BC3D1853";
	setAttr -k off ".v";
createNode transform -n "waveHandle3Buf" -p "waveCtrlGRP_02";
	rename -uid "42D12A93-4276-C352-C1D4-DEB2ADC63B6C";
	setAttr ".t" -type "double3" 4 0.50869703665481225 7.3588098778470217 ;
createNode transform -n "waveHandle3" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle3Buf";
	rename -uid "1CF97D2B-44F8-5D0E-D2C1-5ABFA72B80AE";
createNode nurbsCurve -n "nurbsCircleShape5" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle3Buf|waveHandle3";
	rename -uid "576C5FC1-418A-6D45-5FA3-08A11EF9E6E2";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle3Loc" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle3Buf|waveHandle3";
	rename -uid "DEDCE5BF-4087-6588-130D-59941C66E88A";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle3LocShape" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle3Buf|waveHandle3|waveHandle3Loc";
	rename -uid "5FB59C85-4FE4-56E0-74BF-B4996A7A546E";
	setAttr -k off ".v";
createNode transform -n "waveHandle4Buf" -p "waveCtrlGRP_02";
	rename -uid "F03BE7FF-4CD7-521C-F892-43BB22FAF45E";
	setAttr ".t" -type "double3" 4 0.26078214136999278 9.2075939093074641 ;
createNode transform -n "waveHandle4" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle4Buf";
	rename -uid "71F44D1D-4287-0152-A4E2-04A14E4759B1";
createNode nurbsCurve -n "nurbsCircleShape6" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle4Buf|waveHandle4";
	rename -uid "7A6A8EC3-439B-93B0-8E35-4C983EC6E1F9";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840296
		-2.1558421030922106e-33 0.3086000292377824 0
		-1.3361703213645398e-17 0.21821317334840273 0.21821317334840273
		-1.8896301901141494e-17 1.1102230246251565e-16 0.3086000292377824
		-1.3361703213645401e-17 -0.21821317334840273 0.21821317334840273
		-5.6938245452781404e-33 -0.30860002923778251 1.1102230246251565e-16
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.1102230246251565e-16 -0.3086000292377824
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840296
		-2.1558421030922106e-33 0.3086000292377824 0
		-1.3361703213645398e-17 0.21821317334840273 0.21821317334840273
		;
createNode transform -n "waveHandle4Loc" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle4Buf|waveHandle4";
	rename -uid "ADBEA569-4359-C211-16A7-64998C81814D";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle4LocShape" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle4Buf|waveHandle4|waveHandle4Loc";
	rename -uid "BF053732-4262-7D86-8C4B-CC83A682B0AE";
	setAttr -k off ".v";
createNode transform -n "waveHandle5Buf" -p "waveCtrlGRP_02";
	rename -uid "C8C49120-4889-90AE-8E9B-389A9B834BA0";
	setAttr ".t" -type "double3" 4 0.94341839696076146 6.0938718639812013 ;
createNode transform -n "waveHandle5" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle5Buf";
	rename -uid "190F4DA1-4B0A-36F3-43E3-09B71DDA7A70";
createNode nurbsCurve -n "nurbsCircleShape1" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle5Buf|waveHandle5";
	rename -uid "1FACA983-4E94-3032-D6DA-549FBB6D683D";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.1026781894481244e-16 0.21821317334840271 -0.21821317334840304
		-1.7791145552058667e-32 0.30860002923778257 3.5207573393294986e-17
		-1.1026781894481232e-16 0.21821317334840284 0.21821317334840282
		-1.5594224504505452e-16 8.9424567909960508e-17 0.30860002923778251
		-1.1026781894481236e-16 -0.21821317334840279 0.21821317334840287
		-4.6988441819384398e-32 -0.30860002923778262 9.2987211484036115e-17
		1.1026781894481226e-16 -0.21821317334840287 -0.21821317334840268
		1.5594224504505452e-16 -1.6574967770032629e-16 -0.30860002923778251
		1.1026781894481244e-16 0.21821317334840271 -0.21821317334840304
		-1.7791145552058667e-32 0.30860002923778257 3.5207573393294986e-17
		-1.1026781894481232e-16 0.21821317334840284 0.21821317334840282
		;
createNode transform -n "waveHandle5Loc" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle5Buf|waveHandle5";
	rename -uid "D6325EC8-4B0F-FF81-2905-58AEB2393C0F";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle5LocShape" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle5Buf|waveHandle5|waveHandle5Loc";
	rename -uid "531F3BFE-4820-9FDC-13F3-E8B18EA899B4";
	setAttr -k off ".v";
createNode transform -n "Anchorcrv1" -p "waveCtrlGRP_02";
	rename -uid "3AABAE31-4998-57FE-60BF-A9B150FD5D20";
	setAttr ".t" -type "double3" 4 0 7.3378464335338505 ;
	setAttr ".rp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
	setAttr ".sp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
createNode nurbsCurve -n "AnchorcrvShape1" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|Anchorcrv1";
	rename -uid "C1A62D83-4007-EFDC-8644-01923B89E09B";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "Anchorcrv2" -p "waveCtrlGRP_02";
	rename -uid "2CEBF0AC-49E7-E6AA-0C66-B5AE4FF445E7";
	setAttr ".t" -type "double3" 4 0 -29.031414968196096 ;
	setAttr ".rp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
	setAttr ".sp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
createNode nurbsCurve -n "AnchorcrvShape2" -p "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|Anchorcrv2";
	rename -uid "66896F94-46C3-DE75-1E47-5E9DE23ED061";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 12 0 no 3
		17 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 12 12
		15
		0 0.31649962882926275 3.473816476486677
		0 0.32032474979003439 3.7140401444416846
		0 0.32733752875043509 4.1944978750028756
		0 0.33594421944510944 4.9152096511274133
		0 0.34263837575037198 5.6359417812960499
		0 0.34741994946530685 6.3566891235950846
		0 0.35028890720410627 7.0774466205211217
		0 0.35124522868645058 7.7982091919612788
		0 0.35028890720413208 8.5189717634014333
		0 0.34741994946520338 9.2397292603274774
		0 0.342638375750419 9.9604766026265015
		0 0.33594421944502451 10.681208732795145
		0 0.32733752875038624 11.401920508919678
		0 0.32032474978997755 11.882378239480872
		0 0.3164996288292059 12.122601907435879
		;
createNode transform -n "waveGRP_03";
	rename -uid "4C2E8889-4B20-0AC9-CF19-D39E0962E12D";
	setAttr ".rp" -type "double3" 0 0.66297004715381647 -3.0485750753698451 ;
	setAttr ".sp" -type "double3" 0 0.66297004715381647 -3.0485750753698451 ;
createNode transform -n "CRV" -p "waveGRP_03";
	rename -uid "79CC806C-4F90-A00E-92ED-C6B1B8AB4506";
	setAttr ".it" no;
createNode nurbsCurve -n "CRVShape" -p "|waveGRP_03|CRV";
	rename -uid "45279BDD-4F97-9EAF-97D3-448A67BEFC8A";
	setAttr -k off ".v";
	setAttr ".tw" yes;
	setAttr -s 51 ".cp[0:50]" -type "double3" 0 0 13.395690640824522 0 
		0 12.985605239437884 0 0 12.165416691973821 0 0 10.935090885561277 0 0 9.7047303327958403 
		0 0 8.4743438114293674 0 0 7.2439399551169075 0 0 6.0135274361103477 0 0 4.7831149171038145 
		0 0 3.5527110607913865 0 0 2.3223245394248728 0 0 1.091963986659426 0 0 -0.13836181975310069 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode transform -n "guts_GRP" -p "waveGRP_03";
	rename -uid "F1055C0F-4FD4-A4D2-2BD7-A4A14180BAAC";
	setAttr ".rp" -type "double3" 0 3.086054304693814 5.1025990357395044 ;
	setAttr ".sp" -type "double3" 0 3.086054304693814 5.1025990357395044 ;
createNode transform -n "curve2" -p "|waveGRP_03|guts_GRP";
	rename -uid "ED4B18F5-4F87-485C-9560-55B0C86A549C";
	addAttr -ci true -k true -sn "handleSize" -ln "handleSize" -dv 1 -at "float";
	setAttr ".rp" -type "double3" 0 -178.71178233931573 -1.4586794008110158 ;
	setAttr ".sp" -type "double3" 0 -178.71178233931573 -1.4586794008110158 ;
createNode nurbsCurve -n "curveShape2" -p "|waveGRP_03|guts_GRP|curve2";
	rename -uid "9FDCAB5C-47C5-1213-C880-1596BDA255ED";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve4" -p "|waveGRP_03|guts_GRP";
	rename -uid "ADE44FA8-43FB-853A-48B4-9F9B2BCD76DF";
	addAttr -ci true -k true -sn "handleSize" -ln "handleSize" -dv 1 -at "float";
	setAttr ".rp" -type "double3" 0 -49.027239636955784 2.1534198201671906 ;
	setAttr ".sp" -type "double3" 0 -49.027239636955784 2.1534198201671906 ;
createNode nurbsCurve -n "curveShape4" -p "|waveGRP_03|guts_GRP|curve4";
	rename -uid "9C7CDF4D-40D8-D984-31F0-E7A37E84C369";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve2attachedCurve1" -p "|waveGRP_03|guts_GRP";
	rename -uid "E599F9D9-46D0-3CF1-9D95-7294FF58CFC4";
	setAttr ".it" no;
createNode nurbsCurve -n "curve2attachedCurveShape1" -p "|waveGRP_03|guts_GRP|curve2attachedCurve1";
	rename -uid "FB8E29C8-4AE1-1AA2-A787-C3A4F3F46343";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve2attachedCurve1attachedCurve1" -p "|waveGRP_03|guts_GRP";
	rename -uid "68211F8B-4632-C7FF-19FA-9CBFEB0EF39E";
createNode nurbsCurve -n "curve2attachedCurve1attachedCurveShape1" -p "|waveGRP_03|guts_GRP|curve2attachedCurve1attachedCurve1";
	rename -uid "0DBDE543-4EE0-54AF-864E-AC8157DF1ACE";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "waveCtrlGRP_03Buf" -p "waveGRP_03";
	rename -uid "8100999F-48AF-6083-3261-22AE1D9F1696";
createNode parentConstraint -n "waveCtrlGRP_03Buf_parentConstraint1" -p "waveCtrlGRP_03Buf";
	rename -uid "D0C60BAB-4596-30CA-F670-65B45F48E6BB";
	addAttr -ci true -k true -sn "w0" -ln "wave03_CtrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 8 0 33.587504550141148 ;
	setAttr -k on ".w0";
createNode transform -n "waveCtrlGRP_03" -p "waveCtrlGRP_03Buf";
	rename -uid "0FB6D4C2-4974-CA8C-9CC0-DE8720FF7C4F";
	setAttr ".t" -type "double3" -8 0 -33.587504550141148 ;
createNode transform -n "waveHandleBuf" -p "waveCtrlGRP_03";
	rename -uid "79631657-491D-C40E-6FA7-3FA05CAD1DF8";
	setAttr ".t" -type "double3" 8 1.3718397623342908 6.7024878757741719 ;
createNode transform -n "waveHandle" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandleBuf";
	rename -uid "CF85EC9A-4B3F-C453-BEC8-31ABBA631FB2";
createNode nurbsCurve -n "nurbsCircleShape2" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandleBuf|waveHandle";
	rename -uid "7FD6A041-4A1F-BE4E-5BCE-50B25BEDD0E3";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandleLoc" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandleBuf|waveHandle";
	rename -uid "563657EE-4667-4A10-CB5D-6BB72D110EC6";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandleLocShape" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandleBuf|waveHandle|waveHandleLoc";
	rename -uid "D00FFD60-4554-667C-BD36-E1A792A3B2D8";
	setAttr -k off ".v";
createNode transform -n "waveHandle1Buf" -p "waveCtrlGRP_03";
	rename -uid "2C919407-4474-5CA9-1E77-A78A3F74D7DE";
	setAttr ".t" -type "double3" 8 2.1250110017806891 4.6478288581995475 ;
createNode transform -n "waveHandle1" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle1Buf";
	rename -uid "A6117ECE-4F86-FB44-78F2-5699D432CA1C";
createNode nurbsCurve -n "nurbsCircleShape3" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle1Buf|waveHandle1";
	rename -uid "D0A31F09-4B1D-60E4-9767-A284174CF51E";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle1Loc" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle1Buf|waveHandle1";
	rename -uid "3ACFE168-40D4-2219-7BC2-49BE1C232B78";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle1LocShape" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle1Buf|waveHandle1|waveHandle1Loc";
	rename -uid "EABC319B-4F00-64ED-501D-ABB93C2377F0";
	setAttr -k off ".v";
createNode transform -n "waveHandle2Buf" -p "waveCtrlGRP_03";
	rename -uid "F61C9AF2-49BD-FCFA-A430-CB81B6FDD88A";
	setAttr ".t" -type "double3" 8 2.870181255155503 -0.8041998786472444 ;
createNode transform -n "waveHandle2" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle2Buf";
	rename -uid "EADAB0B9-4C96-0300-97BD-88B291474565";
createNode nurbsCurve -n "nurbsCircleShape4" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle2Buf|waveHandle2";
	rename -uid "E69ABBCC-4544-4A5E-0ECF-ED8316D9B016";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle2Loc" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle2Buf|waveHandle2";
	rename -uid "2C0B52C2-478B-7860-F3CD-299E093ED818";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle2LocShape" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle2Buf|waveHandle2|waveHandle2Loc";
	rename -uid "8A8EA96D-4102-73D2-AF07-7C893036A5E7";
	setAttr -k off ".v";
createNode transform -n "waveHandle3Buf" -p "waveCtrlGRP_03";
	rename -uid "77A6B80D-4D06-11C4-77DC-BDBDAE30ACAA";
	setAttr ".t" -type "double3" 8 0.36091069680121179 8.1185027431620291 ;
createNode transform -n "waveHandle3" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle3Buf";
	rename -uid "68EFC67B-411F-0091-90F3-629CC433F7E4";
createNode nurbsCurve -n "nurbsCircleShape5" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle3Buf|waveHandle3";
	rename -uid "E512B96F-4457-97DA-62DE-CAA5E0771531";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle3Loc" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle3Buf|waveHandle3";
	rename -uid "B3A23CDB-4254-3E5C-1BAB-B085D767C612";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle3LocShape" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle3Buf|waveHandle3|waveHandle3Loc";
	rename -uid "4FDDDE0D-4926-F59B-5D9E-62968918CF06";
	setAttr -k off ".v";
createNode transform -n "waveHandle4Buf" -p "waveCtrlGRP_03";
	rename -uid "99DED028-48B5-4D3A-522D-519AA76AC858";
	setAttr ".t" -type "double3" 8 0.12711409167058763 9.1376832100003398 ;
createNode transform -n "waveHandle4" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle4Buf";
	rename -uid "A1247E13-4E4D-5137-FBA7-5988B34ABF61";
createNode nurbsCurve -n "nurbsCircleShape6" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle4Buf|waveHandle4";
	rename -uid "CAF9AB19-48C1-4967-AE2C-49829049F326";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840296
		-2.1558421030922106e-33 0.3086000292377824 0
		-1.3361703213645398e-17 0.21821317334840273 0.21821317334840273
		-1.8896301901141494e-17 1.1102230246251565e-16 0.3086000292377824
		-1.3361703213645401e-17 -0.21821317334840273 0.21821317334840273
		-5.6938245452781404e-33 -0.30860002923778251 1.1102230246251565e-16
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.1102230246251565e-16 -0.3086000292377824
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840296
		-2.1558421030922106e-33 0.3086000292377824 0
		-1.3361703213645398e-17 0.21821317334840273 0.21821317334840273
		;
createNode transform -n "waveHandle4Loc" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle4Buf|waveHandle4";
	rename -uid "9F8C6340-453B-D554-29A0-63B48EEACFE3";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle4LocShape" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle4Buf|waveHandle4|waveHandle4Loc";
	rename -uid "795D4A50-4050-AB83-E4F7-2A84FBA3D037";
	setAttr -k off ".v";
createNode transform -n "waveHandle5Buf" -p "waveCtrlGRP_03";
	rename -uid "4F3F8494-4FEB-EDBF-0326-24A2E2B759D5";
	setAttr ".t" -type "double3" 8 0.96934497388218832 7.2736013965968604 ;
createNode transform -n "waveHandle5" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle5Buf";
	rename -uid "C190F61B-44E3-6052-222F-7796C4341A6F";
createNode nurbsCurve -n "nurbsCircleShape1" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle5Buf|waveHandle5";
	rename -uid "05A33F7A-4199-AFB2-5D33-7893D649B1E2";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.1026781894481244e-16 0.21821317334840271 -0.21821317334840304
		-1.7791145552058667e-32 0.30860002923778257 3.5207573393294986e-17
		-1.1026781894481232e-16 0.21821317334840284 0.21821317334840282
		-1.5594224504505452e-16 8.9424567909960508e-17 0.30860002923778251
		-1.1026781894481236e-16 -0.21821317334840279 0.21821317334840287
		-4.6988441819384398e-32 -0.30860002923778262 9.2987211484036115e-17
		1.1026781894481226e-16 -0.21821317334840287 -0.21821317334840268
		1.5594224504505452e-16 -1.6574967770032629e-16 -0.30860002923778251
		1.1026781894481244e-16 0.21821317334840271 -0.21821317334840304
		-1.7791145552058667e-32 0.30860002923778257 3.5207573393294986e-17
		-1.1026781894481232e-16 0.21821317334840284 0.21821317334840282
		;
createNode transform -n "waveHandle5Loc" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle5Buf|waveHandle5";
	rename -uid "4EE0B410-4541-9C78-97E5-E988F0CC4C99";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle5LocShape" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle5Buf|waveHandle5|waveHandle5Loc";
	rename -uid "89E545F5-4383-863B-08E8-E1B464685468";
	setAttr -k off ".v";
createNode transform -n "Anchorcrv1" -p "waveCtrlGRP_03";
	rename -uid "8B192080-42CB-E0BB-172A-1DA3405F4AD9";
	setAttr ".t" -type "double3" 8 0 7.3378464335338505 ;
	setAttr ".rp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
	setAttr ".sp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
createNode nurbsCurve -n "AnchorcrvShape1" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|Anchorcrv1";
	rename -uid "CE357AB1-4D8B-999E-D15F-BF8D804BDC52";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "Anchorcrv2" -p "waveCtrlGRP_03";
	rename -uid "8799279F-4295-16CF-1E6C-1DB6EB895715";
	setAttr ".t" -type "double3" 8 0 -29.031414968196096 ;
	setAttr ".rp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
	setAttr ".sp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
createNode nurbsCurve -n "AnchorcrvShape2" -p "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|Anchorcrv2";
	rename -uid "45A8B16D-4FEB-B7E2-C6C1-F19D1613CAA4";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 12 0 no 3
		17 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 12 12
		15
		0 0.31649962882926275 3.473816476486677
		0 0.32032474979003439 3.7140401444416846
		0 0.32733752875043509 4.1944978750028756
		0 0.33594421944510944 4.9152096511274133
		0 0.34263837575037198 5.6359417812960499
		0 0.34741994946530685 6.3566891235950846
		0 0.35028890720410627 7.0774466205211217
		0 0.35124522868645058 7.7982091919612788
		0 0.35028890720413208 8.5189717634014333
		0 0.34741994946520338 9.2397292603274774
		0 0.342638375750419 9.9604766026265015
		0 0.33594421944502451 10.681208732795145
		0 0.32733752875038624 11.401920508919678
		0 0.32032474978997755 11.882378239480872
		0 0.3164996288292059 12.122601907435879
		;
createNode transform -n "waveGRP_04";
	rename -uid "7DAB8C59-4995-6BEF-61E6-7D90DD4B26E2";
	setAttr ".rp" -type "double3" 0 0.66297004715381647 -3.0485750753698451 ;
	setAttr ".sp" -type "double3" 0 0.66297004715381647 -3.0485750753698451 ;
createNode transform -n "CRV" -p "waveGRP_04";
	rename -uid "E7E44EC6-455D-C7E6-65DA-DB88218B7D69";
	setAttr ".it" no;
createNode nurbsCurve -n "CRVShape" -p "|waveGRP_04|CRV";
	rename -uid "BAA7573A-4BD1-C1B8-D802-20871998AE60";
	setAttr -k off ".v";
	setAttr ".tw" yes;
	setAttr -s 51 ".cp[0:50]" -type "double3" 0 0 13.395690640824522 0 
		0 12.985605239437884 0 0 12.165416691973821 0 0 10.935090885561277 0 0 9.7047303327958403 
		0 0 8.4743438114293674 0 0 7.2439399551169075 0 0 6.0135274361103477 0 0 4.7831149171038145 
		0 0 3.5527110607913865 0 0 2.3223245394248728 0 0 1.091963986659426 0 0 -0.13836181975310069 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode transform -n "guts_GRP" -p "waveGRP_04";
	rename -uid "0F2B1E65-4C6C-81DA-C3A7-3EAB67063F75";
	setAttr ".rp" -type "double3" 0 3.086054304693814 5.1025990357395044 ;
	setAttr ".sp" -type "double3" 0 3.086054304693814 5.1025990357395044 ;
createNode transform -n "curve2" -p "|waveGRP_04|guts_GRP";
	rename -uid "02864002-4BF9-E88E-619B-58A2DCADEBA6";
	addAttr -ci true -k true -sn "handleSize" -ln "handleSize" -dv 1 -at "float";
	setAttr ".rp" -type "double3" 0 -178.71178233931573 -1.4586794008110158 ;
	setAttr ".sp" -type "double3" 0 -178.71178233931573 -1.4586794008110158 ;
createNode nurbsCurve -n "curveShape2" -p "|waveGRP_04|guts_GRP|curve2";
	rename -uid "F260C503-455F-6273-3793-108DE3D3A9C3";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve4" -p "|waveGRP_04|guts_GRP";
	rename -uid "708CDCC1-42F1-2680-75F1-2D8FBD7BDF2F";
	addAttr -ci true -k true -sn "handleSize" -ln "handleSize" -dv 1 -at "float";
	setAttr ".rp" -type "double3" 0 -49.027239636955784 2.1534198201671906 ;
	setAttr ".sp" -type "double3" 0 -49.027239636955784 2.1534198201671906 ;
createNode nurbsCurve -n "curveShape4" -p "|waveGRP_04|guts_GRP|curve4";
	rename -uid "E16462D8-484F-A0A0-F731-D8BDA983523B";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve2attachedCurve1" -p "|waveGRP_04|guts_GRP";
	rename -uid "6326556B-4BFE-F787-CD8B-2689D8DACC9F";
	setAttr ".it" no;
createNode nurbsCurve -n "curve2attachedCurveShape1" -p "|waveGRP_04|guts_GRP|curve2attachedCurve1";
	rename -uid "7ACA3CF0-4E7E-3653-EE9A-62877B4FC084";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve2attachedCurve1attachedCurve1" -p "|waveGRP_04|guts_GRP";
	rename -uid "9FA30BED-447F-258B-ADD8-769E6F59716D";
createNode nurbsCurve -n "curve2attachedCurve1attachedCurveShape1" -p "|waveGRP_04|guts_GRP|curve2attachedCurve1attachedCurve1";
	rename -uid "2555BBE0-413D-C206-E37D-E9BD4CC6C714";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "waveCtrlGRP_04Buf" -p "waveGRP_04";
	rename -uid "334F9186-4CBA-DD47-766E-97BC392DF1DB";
createNode parentConstraint -n "waveCtrlGRP_04Buf_parentConstraint1" -p "waveCtrlGRP_04Buf";
	rename -uid "426C60F7-4FA5-B352-7390-4C9D0026017D";
	addAttr -ci true -k true -sn "w0" -ln "wave04_CtrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 12 0 33.587504550141148 ;
	setAttr -k on ".w0";
createNode transform -n "waveCtrlGRP_04" -p "waveCtrlGRP_04Buf";
	rename -uid "5E1F6D35-4E0D-37BC-B7EA-2D82AEBFC8D5";
	setAttr ".t" -type "double3" -12 0 -33.587504550141148 ;
createNode transform -n "waveHandleBuf" -p "waveCtrlGRP_04";
	rename -uid "FD2B398D-407E-B6C4-F2A7-F29F83F818D7";
	setAttr ".t" -type "double3" 12 0.89975617195766888 10.026563129058689 ;
createNode transform -n "waveHandle" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandleBuf";
	rename -uid "FAF02D52-432C-4ADF-DAF6-A6A32E071B56";
createNode nurbsCurve -n "nurbsCircleShape2" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandleBuf|waveHandle";
	rename -uid "D63424EA-406A-6B80-C417-1FA011808159";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandleLoc" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandleBuf|waveHandle";
	rename -uid "DBFA3D2A-4357-5ED3-3F07-ED9FF3E00800";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandleLocShape" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandleBuf|waveHandle|waveHandleLoc";
	rename -uid "BDD15339-4F8F-47B1-E935-428E2F9952F6";
	setAttr -k off ".v";
createNode transform -n "waveHandle1Buf" -p "waveCtrlGRP_04";
	rename -uid "4D3DA53B-4D5F-9283-77C8-6AA73B398EE3";
	setAttr ".t" -type "double3" 12 3.0669523265466223 7.4768131235155986 ;
createNode transform -n "waveHandle1" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle1Buf";
	rename -uid "51595BA4-4E9F-7FEE-72CD-2AA9C3975E8A";
createNode nurbsCurve -n "nurbsCircleShape3" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle1Buf|waveHandle1";
	rename -uid "2261A52B-4F9D-0C8A-19A4-EB9BB06748C0";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle1Loc" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle1Buf|waveHandle1";
	rename -uid "A61A97FC-4168-92EB-ED68-7DBF2A8C9678";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle1LocShape" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle1Buf|waveHandle1|waveHandle1Loc";
	rename -uid "0D6760D1-44BA-C9DB-2179-FC9FC724556B";
	setAttr -k off ".v";
createNode transform -n "waveHandle2Buf" -p "waveCtrlGRP_04";
	rename -uid "A2B6A684-4B40-21DF-194A-A8850DB8CC66";
	setAttr ".t" -type "double3" 12 4.1848589707308488 -2.4950121003622749 ;
createNode transform -n "waveHandle2" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle2Buf";
	rename -uid "66EF5D42-4D51-7533-BD15-C1B8AC60285F";
createNode nurbsCurve -n "nurbsCircleShape4" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle2Buf|waveHandle2";
	rename -uid "48B2A587-4510-B133-1F8C-A6B06744C78E";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle2Loc" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle2Buf|waveHandle2";
	rename -uid "8EC724B4-43DC-A528-3EA4-AA9D684220F3";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle2LocShape" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle2Buf|waveHandle2|waveHandle2Loc";
	rename -uid "4FF6B633-405A-8692-CFCB-ECBCF6D5A907";
	setAttr -k off ".v";
createNode transform -n "waveHandle3Buf" -p "waveCtrlGRP_04";
	rename -uid "FF333888-4B0B-2A08-0DE7-4498D3E111F7";
	setAttr ".t" -type "double3" 12 1.0002000587216409 6.641675563074652 ;
createNode transform -n "waveHandle3" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle3Buf";
	rename -uid "134D6A5C-4F3D-926C-006C-D195E7E82A90";
createNode nurbsCurve -n "nurbsCircleShape5" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle3Buf|waveHandle3";
	rename -uid "7C2D2525-4EF4-E46C-44DA-5A8B0EB3F2F2";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle3Loc" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle3Buf|waveHandle3";
	rename -uid "6E063252-4EB8-79B3-B6DB-09B4AA48432C";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle3LocShape" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle3Buf|waveHandle3|waveHandle3Loc";
	rename -uid "94DA284E-4F50-7335-4826-C399F477DFC7";
	setAttr -k off ".v";
createNode transform -n "waveHandle4Buf" -p "waveCtrlGRP_04";
	rename -uid "E0EB365B-48F1-2BAC-15A6-ED901563170A";
	setAttr ".t" -type "double3" 12 -0.50191275117254508 8.6359770063094778 ;
createNode transform -n "waveHandle4" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle4Buf";
	rename -uid "E2D00044-44B5-B9A2-BD08-16868CA07C40";
createNode nurbsCurve -n "nurbsCircleShape6" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle4Buf|waveHandle4";
	rename -uid "A8F3F1BE-4121-CB02-7CFB-E0A72789C5A3";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840296
		-2.1558421030922106e-33 0.3086000292377824 0
		-1.3361703213645398e-17 0.21821317334840273 0.21821317334840273
		-1.8896301901141494e-17 1.1102230246251565e-16 0.3086000292377824
		-1.3361703213645401e-17 -0.21821317334840273 0.21821317334840273
		-5.6938245452781404e-33 -0.30860002923778251 1.1102230246251565e-16
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.1102230246251565e-16 -0.3086000292377824
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840296
		-2.1558421030922106e-33 0.3086000292377824 0
		-1.3361703213645398e-17 0.21821317334840273 0.21821317334840273
		;
createNode transform -n "waveHandle4Loc" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle4Buf|waveHandle4";
	rename -uid "EC04706C-41CD-13AF-2E75-359529ACDB96";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle4LocShape" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle4Buf|waveHandle4|waveHandle4Loc";
	rename -uid "1398F033-4AD6-FAC7-58D6-E395A6EB6BB3";
	setAttr -k off ".v";
createNode transform -n "waveHandle5Buf" -p "waveCtrlGRP_04";
	rename -uid "42082DE0-481A-BDCC-618C-29BDBC22DB65";
	setAttr ".t" -type "double3" 12 0.98347141621187706 8.4365353356778368 ;
createNode transform -n "waveHandle5" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle5Buf";
	rename -uid "9C27C6C2-4C16-D888-A6FA-E096B40D23D5";
createNode nurbsCurve -n "nurbsCircleShape1" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle5Buf|waveHandle5";
	rename -uid "F603BC19-4297-F320-2300-1FA5AD2B880E";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.1026781894481244e-16 0.21821317334840271 -0.21821317334840304
		-1.7791145552058667e-32 0.30860002923778257 3.5207573393294986e-17
		-1.1026781894481232e-16 0.21821317334840284 0.21821317334840282
		-1.5594224504505452e-16 8.9424567909960508e-17 0.30860002923778251
		-1.1026781894481236e-16 -0.21821317334840279 0.21821317334840287
		-4.6988441819384398e-32 -0.30860002923778262 9.2987211484036115e-17
		1.1026781894481226e-16 -0.21821317334840287 -0.21821317334840268
		1.5594224504505452e-16 -1.6574967770032629e-16 -0.30860002923778251
		1.1026781894481244e-16 0.21821317334840271 -0.21821317334840304
		-1.7791145552058667e-32 0.30860002923778257 3.5207573393294986e-17
		-1.1026781894481232e-16 0.21821317334840284 0.21821317334840282
		;
createNode transform -n "waveHandle5Loc" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle5Buf|waveHandle5";
	rename -uid "95A3E113-4F97-E786-EEEE-E791D68F55FC";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle5LocShape" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle5Buf|waveHandle5|waveHandle5Loc";
	rename -uid "60D0A64C-4C45-405A-6E3B-E2B63764F029";
	setAttr -k off ".v";
createNode transform -n "Anchorcrv1" -p "waveCtrlGRP_04";
	rename -uid "F6F03400-45B8-AA68-3D10-EDA7B4AB1CA2";
	setAttr ".t" -type "double3" 12 0 7.3378464335338505 ;
	setAttr ".rp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
	setAttr ".sp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
createNode nurbsCurve -n "AnchorcrvShape1" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|Anchorcrv1";
	rename -uid "370A7226-4473-38EB-68F4-D28117B0F508";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "Anchorcrv2" -p "waveCtrlGRP_04";
	rename -uid "56540FAA-45CC-09AC-E1AD-B1815E2F0FF9";
	setAttr ".t" -type "double3" 12 0 -29.031414968196096 ;
	setAttr ".rp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
	setAttr ".sp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
createNode nurbsCurve -n "AnchorcrvShape2" -p "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|Anchorcrv2";
	rename -uid "7556DE69-4CAC-41B1-5858-3689D0C25C44";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 12 0 no 3
		17 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 12 12
		15
		0 0.31649962882926275 3.473816476486677
		0 0.32032474979003439 3.7140401444416846
		0 0.32733752875043509 4.1944978750028756
		0 0.33594421944510944 4.9152096511274133
		0 0.34263837575037198 5.6359417812960499
		0 0.34741994946530685 6.3566891235950846
		0 0.35028890720410627 7.0774466205211217
		0 0.35124522868645058 7.7982091919612788
		0 0.35028890720413208 8.5189717634014333
		0 0.34741994946520338 9.2397292603274774
		0 0.342638375750419 9.9604766026265015
		0 0.33594421944502451 10.681208732795145
		0 0.32733752875038624 11.401920508919678
		0 0.32032474978997755 11.882378239480872
		0 0.3164996288292059 12.122601907435879
		;
createNode transform -n "waveGRP_05";
	rename -uid "7A6C50BE-4E92-959A-C0B3-85BD497018A8";
	setAttr ".rp" -type "double3" 0 0.66297004715381647 -3.0485750753698451 ;
	setAttr ".sp" -type "double3" 0 0.66297004715381647 -3.0485750753698451 ;
createNode transform -n "CRV" -p "waveGRP_05";
	rename -uid "D1E34DC3-4CDB-A348-E9F0-4B922BACD8A9";
	setAttr ".it" no;
createNode nurbsCurve -n "CRVShape" -p "|waveGRP_05|CRV";
	rename -uid "F55A7B59-453F-EA9C-01A5-2085B5F3EF75";
	setAttr -k off ".v";
	setAttr ".tw" yes;
	setAttr -s 51 ".cp[0:50]" -type "double3" 0 0 13.395690640824522 0 
		0 12.985605239437884 0 0 12.165416691973821 0 0 10.935090885561277 0 0 9.7047303327958403 
		0 0 8.4743438114293674 0 0 7.2439399551169075 0 0 6.0135274361103477 0 0 4.7831149171038145 
		0 0 3.5527110607913865 0 0 2.3223245394248728 0 0 1.091963986659426 0 0 -0.13836181975310069 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode transform -n "guts_GRP" -p "waveGRP_05";
	rename -uid "214DCD11-4D38-B2ED-27A1-E6B82023D021";
	setAttr ".rp" -type "double3" 0 3.086054304693814 5.1025990357395044 ;
	setAttr ".sp" -type "double3" 0 3.086054304693814 5.1025990357395044 ;
createNode transform -n "curve2" -p "|waveGRP_05|guts_GRP";
	rename -uid "504F1B8C-4C85-8908-5141-97B1F1BEB399";
	addAttr -ci true -k true -sn "handleSize" -ln "handleSize" -dv 1 -at "float";
	setAttr ".rp" -type "double3" 0 -178.71178233931573 -1.4586794008110158 ;
	setAttr ".sp" -type "double3" 0 -178.71178233931573 -1.4586794008110158 ;
createNode nurbsCurve -n "curveShape2" -p "|waveGRP_05|guts_GRP|curve2";
	rename -uid "8506763C-48BD-6746-C013-23BF1FB9ABEF";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve4" -p "|waveGRP_05|guts_GRP";
	rename -uid "557C3FA9-4C8D-A93A-DF27-5B92CCC9C2AB";
	addAttr -ci true -k true -sn "handleSize" -ln "handleSize" -dv 1 -at "float";
	setAttr ".rp" -type "double3" 0 -49.027239636955784 2.1534198201671906 ;
	setAttr ".sp" -type "double3" 0 -49.027239636955784 2.1534198201671906 ;
createNode nurbsCurve -n "curveShape4" -p "|waveGRP_05|guts_GRP|curve4";
	rename -uid "6F9594ED-4B6D-22E7-56E9-9DA721B5E246";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve2attachedCurve1" -p "|waveGRP_05|guts_GRP";
	rename -uid "6270ECF4-415E-6BC7-2D4A-618793F9F7D7";
	setAttr ".it" no;
createNode nurbsCurve -n "curve2attachedCurveShape1" -p "|waveGRP_05|guts_GRP|curve2attachedCurve1";
	rename -uid "D5412870-47EC-C486-D467-82B8BBCA2F6B";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve2attachedCurve1attachedCurve1" -p "|waveGRP_05|guts_GRP";
	rename -uid "641198B3-4ADD-73BF-48B1-7199D13D182E";
createNode nurbsCurve -n "curve2attachedCurve1attachedCurveShape1" -p "|waveGRP_05|guts_GRP|curve2attachedCurve1attachedCurve1";
	rename -uid "A7125E2F-49B2-5B70-E4AB-73B123912E31";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "waveCtrlGRP_05Buf" -p "waveGRP_05";
	rename -uid "7BC63B01-4CFA-71C2-306D-67A7B095C387";
createNode parentConstraint -n "waveCtrlGRP_05Buf_parentConstraint1" -p "waveCtrlGRP_05Buf";
	rename -uid "1148F884-4708-19C9-90FC-98A59ED2930D";
	addAttr -ci true -k true -sn "w0" -ln "wave05_CtrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 16 0 33.587504550141148 ;
	setAttr -k on ".w0";
createNode transform -n "waveCtrlGRP_05" -p "waveCtrlGRP_05Buf";
	rename -uid "16755510-4C76-BF4E-0DF8-CEA085DCF01C";
	setAttr ".t" -type "double3" -16 0 -33.587504550141148 ;
createNode transform -n "waveHandleBuf" -p "waveCtrlGRP_05";
	rename -uid "2CC7C6D6-4ABA-DC60-9BC6-C8944B9E10B8";
	setAttr ".t" -type "double3" 16 0.68943596235980087 10.599276112676959 ;
createNode transform -n "waveHandle" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandleBuf";
	rename -uid "2F3B45ED-49E9-31F5-E8EB-11AB3DC473D4";
createNode nurbsCurve -n "nurbsCircleShape2" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandleBuf|waveHandle";
	rename -uid "4A1D6A3A-4735-947A-1C13-8B9B4A32D7C9";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandleLoc" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandleBuf|waveHandle";
	rename -uid "B9EAA065-41E1-1B17-FF20-8EB7F13E28A1";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandleLocShape" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandleBuf|waveHandle|waveHandleLoc";
	rename -uid "837083E0-4CA5-55AD-AA0F-7BB14ABD4562";
	setAttr -k off ".v";
createNode transform -n "waveHandle1Buf" -p "waveCtrlGRP_05";
	rename -uid "B835A9A8-40E9-AA89-0E0F-4F82D202481B";
	setAttr ".t" -type "double3" 16 3.9218197427552295 7.6439275960906006 ;
createNode transform -n "waveHandle1" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle1Buf";
	rename -uid "39DCA9D9-46CA-D0EA-73A4-C3A0280E3778";
createNode nurbsCurve -n "nurbsCircleShape3" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle1Buf|waveHandle1";
	rename -uid "C7877CD8-4588-7362-A800-92B27E92974F";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle1Loc" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle1Buf|waveHandle1";
	rename -uid "FF5EE813-4934-26DC-0619-86A2D713948C";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle1LocShape" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle1Buf|waveHandle1|waveHandle1Loc";
	rename -uid "154D6771-4868-C9E0-0134-A4A49668DC9B";
	setAttr -k off ".v";
createNode transform -n "waveHandle2Buf" -p "waveCtrlGRP_05";
	rename -uid "F1ACEDC6-4528-B599-1EA7-0487B4FCD364";
	setAttr ".t" -type "double3" 15.999999999999993 4.4776362348262104 -3.8462899103579509 ;
createNode transform -n "waveHandle2" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle2Buf";
	rename -uid "7485EE49-4CF9-84DB-C387-46B5857FC431";
createNode nurbsCurve -n "nurbsCircleShape4" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle2Buf|waveHandle2";
	rename -uid "7CCAB342-443C-10CD-EB8D-57A189159CA6";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle2Loc" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle2Buf|waveHandle2";
	rename -uid "816359D6-4D4C-5C52-37DE-D696F39A785D";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle2LocShape" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle2Buf|waveHandle2|waveHandle2Loc";
	rename -uid "BA80E61F-4647-8E43-E6DE-65BDF1178AD8";
	setAttr -k off ".v";
createNode transform -n "waveHandle3Buf" -p "waveCtrlGRP_05";
	rename -uid "A6476551-49B6-DEF4-7A87-8FBFF624EBC6";
	setAttr ".t" -type "double3" 16 2.6412466419730745 5.0054692727698678 ;
createNode transform -n "waveHandle3" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle3Buf";
	rename -uid "0B6D165B-4FCC-BEE0-C577-C0BE4AD23BD4";
createNode nurbsCurve -n "nurbsCircleShape5" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle3Buf|waveHandle3";
	rename -uid "FA341B73-4D6F-2545-4646-749A0DC0E361";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle3Loc" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle3Buf|waveHandle3";
	rename -uid "DCBA899A-4CEE-7279-06F0-829B371E8E47";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle3LocShape" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle3Buf|waveHandle3|waveHandle3Loc";
	rename -uid "554ABA91-48F5-00C0-EF31-8A844B0B5E20";
	setAttr -k off ".v";
createNode transform -n "waveHandle4Buf" -p "waveCtrlGRP_05";
	rename -uid "AB168CD0-4307-A29E-53FB-28BAE2E12F75";
	setAttr ".t" -type "double3" 16 -1.1091307374897426 7.8961728065985337 ;
createNode transform -n "waveHandle4" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle4Buf";
	rename -uid "5C80372F-4C8B-0655-63DE-2EA6137121F5";
createNode nurbsCurve -n "nurbsCircleShape6" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle4Buf|waveHandle4";
	rename -uid "3560C0FC-4FCC-76B0-B36F-859FDA38F102";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840296
		-2.1558421030922106e-33 0.3086000292377824 0
		-1.3361703213645398e-17 0.21821317334840273 0.21821317334840273
		-1.8896301901141494e-17 1.1102230246251565e-16 0.3086000292377824
		-1.3361703213645401e-17 -0.21821317334840273 0.21821317334840273
		-5.6938245452781404e-33 -0.30860002923778251 1.1102230246251565e-16
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.1102230246251565e-16 -0.3086000292377824
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840296
		-2.1558421030922106e-33 0.3086000292377824 0
		-1.3361703213645398e-17 0.21821317334840273 0.21821317334840273
		;
createNode transform -n "waveHandle4Loc" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle4Buf|waveHandle4";
	rename -uid "6D3811B8-47DE-22AA-8168-5F86CB9C605D";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle4LocShape" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle4Buf|waveHandle4|waveHandle4Loc";
	rename -uid "D9676F71-44C7-28DD-671E-01BF9F06CFEE";
	setAttr -k off ".v";
createNode transform -n "waveHandle5Buf" -p "waveCtrlGRP_05";
	rename -uid "061B450F-4C99-9E54-E255-00888A61AA60";
	setAttr ".t" -type "double3" 16 0.82319183683428698 8.624519722799187 ;
createNode transform -n "waveHandle5" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle5Buf";
	rename -uid "A5DCC37C-4BE4-C85E-BDBC-A987273AE4F2";
createNode nurbsCurve -n "nurbsCircleShape1" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle5Buf|waveHandle5";
	rename -uid "ADDB0C43-4E14-D6E5-46FC-669AABAA0B86";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.1026781894481244e-16 0.21821317334840271 -0.21821317334840304
		-1.7791145552058667e-32 0.30860002923778257 3.5207573393294986e-17
		-1.1026781894481232e-16 0.21821317334840284 0.21821317334840282
		-1.5594224504505452e-16 8.9424567909960508e-17 0.30860002923778251
		-1.1026781894481236e-16 -0.21821317334840279 0.21821317334840287
		-4.6988441819384398e-32 -0.30860002923778262 9.2987211484036115e-17
		1.1026781894481226e-16 -0.21821317334840287 -0.21821317334840268
		1.5594224504505452e-16 -1.6574967770032629e-16 -0.30860002923778251
		1.1026781894481244e-16 0.21821317334840271 -0.21821317334840304
		-1.7791145552058667e-32 0.30860002923778257 3.5207573393294986e-17
		-1.1026781894481232e-16 0.21821317334840284 0.21821317334840282
		;
createNode transform -n "waveHandle5Loc" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle5Buf|waveHandle5";
	rename -uid "73F0B58E-4712-122D-499B-0EA87D724D73";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle5LocShape" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle5Buf|waveHandle5|waveHandle5Loc";
	rename -uid "B19C748C-4F83-D7A0-D8EB-81AC5EE63B57";
	setAttr -k off ".v";
createNode transform -n "Anchorcrv1" -p "waveCtrlGRP_05";
	rename -uid "64390647-485B-05FC-AFE2-4CA29A20F548";
	setAttr ".t" -type "double3" 16 0 7.3378464335338505 ;
	setAttr ".rp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
	setAttr ".sp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
createNode nurbsCurve -n "AnchorcrvShape1" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|Anchorcrv1";
	rename -uid "0865421A-4EF8-8063-3CB9-52A5BB6F4E2B";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "Anchorcrv2" -p "waveCtrlGRP_05";
	rename -uid "A3F9E13B-4014-DEF6-AE84-B591CE3D7BE1";
	setAttr ".t" -type "double3" 16 0 -29.031414968196096 ;
	setAttr ".rp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
	setAttr ".sp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
createNode nurbsCurve -n "AnchorcrvShape2" -p "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|Anchorcrv2";
	rename -uid "C6D25249-4234-D86E-1B3B-1C87FC4F70C7";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 12 0 no 3
		17 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 12 12
		15
		0 0.31649962882926275 3.473816476486677
		0 0.32032474979003439 3.7140401444416846
		0 0.32733752875043509 4.1944978750028756
		0 0.33594421944510944 4.9152096511274133
		0 0.34263837575037198 5.6359417812960499
		0 0.34741994946530685 6.3566891235950846
		0 0.35028890720410627 7.0774466205211217
		0 0.35124522868645058 7.7982091919612788
		0 0.35028890720413208 8.5189717634014333
		0 0.34741994946520338 9.2397292603274774
		0 0.342638375750419 9.9604766026265015
		0 0.33594421944502451 10.681208732795145
		0 0.32733752875038624 11.401920508919678
		0 0.32032474978997755 11.882378239480872
		0 0.3164996288292059 12.122601907435879
		;
createNode transform -n "waveGRP_06";
	rename -uid "FED8E579-4882-8CD4-BE47-01AFB9FBCDCA";
	setAttr ".rp" -type "double3" 0 0.66297004715381647 -3.0485750753698451 ;
	setAttr ".sp" -type "double3" 0 0.66297004715381647 -3.0485750753698451 ;
createNode transform -n "CRV" -p "waveGRP_06";
	rename -uid "6926FCE9-4287-0461-E1E2-98BF5ECADE59";
	setAttr ".it" no;
createNode nurbsCurve -n "CRVShape" -p "|waveGRP_06|CRV";
	rename -uid "D984C33C-4A6E-9BBB-C1F0-B599A68AD889";
	setAttr -k off ".v";
	setAttr ".tw" yes;
	setAttr -s 51 ".cp[0:50]" -type "double3" 0 0 13.395690640824522 0 
		0 12.985605239437884 0 0 12.165416691973821 0 0 10.935090885561277 0 0 9.7047303327958403 
		0 0 8.4743438114293674 0 0 7.2439399551169075 0 0 6.0135274361103477 0 0 4.7831149171038145 
		0 0 3.5527110607913865 0 0 2.3223245394248728 0 0 1.091963986659426 0 0 -0.13836181975310069 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode transform -n "guts_GRP" -p "waveGRP_06";
	rename -uid "6365834A-49BC-CE64-AFAB-2AB3834E0559";
	setAttr ".rp" -type "double3" 0 3.086054304693814 5.1025990357395044 ;
	setAttr ".sp" -type "double3" 0 3.086054304693814 5.1025990357395044 ;
createNode transform -n "curve2" -p "|waveGRP_06|guts_GRP";
	rename -uid "8B90C224-4240-1E71-574D-3FB4FA93A6C4";
	addAttr -ci true -k true -sn "handleSize" -ln "handleSize" -dv 1 -at "float";
	setAttr ".rp" -type "double3" 0 -178.71178233931573 -1.4586794008110158 ;
	setAttr ".sp" -type "double3" 0 -178.71178233931573 -1.4586794008110158 ;
createNode nurbsCurve -n "curveShape2" -p "|waveGRP_06|guts_GRP|curve2";
	rename -uid "97A5D433-4D5F-5809-041E-FF935D575C37";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve4" -p "|waveGRP_06|guts_GRP";
	rename -uid "8D9FE957-406C-8304-EBAF-A38D97A3B8D4";
	addAttr -ci true -k true -sn "handleSize" -ln "handleSize" -dv 1 -at "float";
	setAttr ".rp" -type "double3" 0 -49.027239636955784 2.1534198201671906 ;
	setAttr ".sp" -type "double3" 0 -49.027239636955784 2.1534198201671906 ;
createNode nurbsCurve -n "curveShape4" -p "|waveGRP_06|guts_GRP|curve4";
	rename -uid "AF0C09D2-4E6E-729F-83FE-91BB72B1C7F7";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve2attachedCurve1" -p "|waveGRP_06|guts_GRP";
	rename -uid "EAC4EE87-4E7F-1C70-552C-3DB4B66522E8";
	setAttr ".it" no;
createNode nurbsCurve -n "curve2attachedCurveShape1" -p "|waveGRP_06|guts_GRP|curve2attachedCurve1";
	rename -uid "2D59F468-40B9-6B9C-ECC7-5796D28C7D81";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve2attachedCurve1attachedCurve1" -p "|waveGRP_06|guts_GRP";
	rename -uid "B7E92698-4A63-4A58-913A-C3B0C28B295C";
createNode nurbsCurve -n "curve2attachedCurve1attachedCurveShape1" -p "|waveGRP_06|guts_GRP|curve2attachedCurve1attachedCurve1";
	rename -uid "8E712580-478B-862A-47F4-F099AEA4031D";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "waveCtrlGRP_06Buf" -p "waveGRP_06";
	rename -uid "66512AB7-4EF8-EBA7-3414-85B1A06127DD";
createNode parentConstraint -n "waveCtrlGRP_06Buf_parentConstraint1" -p "waveCtrlGRP_06Buf";
	rename -uid "65189546-4AF9-446F-0749-C8B196B151E0";
	addAttr -ci true -k true -sn "w0" -ln "wave06_CtrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 20 0 33.587504550141148 ;
	setAttr -k on ".w0";
createNode transform -n "waveCtrlGRP_06" -p "waveCtrlGRP_06Buf";
	rename -uid "566121E0-49D5-B673-D347-74BA0269D910";
	setAttr ".t" -type "double3" -20 0 -33.587504550141148 ;
createNode transform -n "waveHandleBuf" -p "waveCtrlGRP_06";
	rename -uid "7DFEEE30-4BAC-8DDD-FF87-61849FACCB93";
	setAttr ".t" -type "double3" 20 0.50821627763029364 10.103029348960874 ;
createNode transform -n "waveHandle" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandleBuf";
	rename -uid "D3900D98-45C8-A149-F908-5EA07559560E";
createNode nurbsCurve -n "nurbsCircleShape2" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandleBuf|waveHandle";
	rename -uid "E1A4E9C5-4173-AC65-2523-08A573562992";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandleLoc" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandleBuf|waveHandle";
	rename -uid "9A962C9B-4C37-A8F9-7405-B4BB587D327A";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandleLocShape" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandleBuf|waveHandle|waveHandleLoc";
	rename -uid "5263B17A-42AF-DA1B-7F22-ACB543F0DDBA";
	setAttr -k off ".v";
createNode transform -n "waveHandle1Buf" -p "waveCtrlGRP_06";
	rename -uid "7B81AF56-485F-D01D-B455-1FAC019246FA";
	setAttr ".t" -type "double3" 20 5.2031263186663121 6.8575776119283525 ;
createNode transform -n "waveHandle1" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle1Buf";
	rename -uid "B5AFC951-4423-C4AB-50A4-7E8D753C7AEC";
createNode nurbsCurve -n "nurbsCircleShape3" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle1Buf|waveHandle1";
	rename -uid "B0245D55-4127-853B-E3D7-2BA276587D39";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle1Loc" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle1Buf|waveHandle1";
	rename -uid "7C4434CA-4238-BEA3-2935-3FBEC4537AD5";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle1LocShape" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle1Buf|waveHandle1|waveHandle1Loc";
	rename -uid "AEEFA0A2-452C-FE30-517D-9E944F5B0A00";
	setAttr -k off ".v";
createNode transform -n "waveHandle2Buf" -p "waveCtrlGRP_06";
	rename -uid "A05550F6-4821-EB90-DB68-3CAAC1CEC26D";
	setAttr ".t" -type "double3" 20.000000000000004 4.3491097965320966 -5.2297361656268135 ;
createNode transform -n "waveHandle2" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle2Buf";
	rename -uid "FDC86536-430E-3B1A-1DCC-80BC0BEDFA30";
createNode nurbsCurve -n "nurbsCircleShape4" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle2Buf|waveHandle2";
	rename -uid "A6096008-4515-85BE-374A-058CC41B65DF";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle2Loc" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle2Buf|waveHandle2";
	rename -uid "0C6EA30D-4AE7-6559-CD50-9FB0F1706666";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle2LocShape" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle2Buf|waveHandle2|waveHandle2Loc";
	rename -uid "B2319050-44E3-05E4-1C7A-E6833CCF6249";
	setAttr -k off ".v";
createNode transform -n "waveHandle3Buf" -p "waveCtrlGRP_06";
	rename -uid "806D0741-4187-817B-945F-F8B4C0EC51C1";
	setAttr ".t" -type "double3" 20 3.6984086292116576 3.4200393925428987 ;
createNode transform -n "waveHandle3" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle3Buf";
	rename -uid "F18E7B1B-4287-A85D-1B0E-E78BECAFBFA3";
createNode nurbsCurve -n "nurbsCircleShape5" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle3Buf|waveHandle3";
	rename -uid "807A882C-4D34-DC3F-0D4A-67A9D4D38A42";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle3Loc" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle3Buf|waveHandle3";
	rename -uid "72282A88-409A-1707-D058-67BFDB88849D";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle3LocShape" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle3Buf|waveHandle3|waveHandle3Loc";
	rename -uid "F950B0FD-47D2-A245-BD88-C3AB4F455D14";
	setAttr -k off ".v";
createNode transform -n "waveHandle4Buf" -p "waveCtrlGRP_06";
	rename -uid "783B23C8-4252-390B-3F56-20B6192672C6";
	setAttr ".t" -type "double3" 20 -2.1449080269354992 6.4924181839363815 ;
createNode transform -n "waveHandle4" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle4Buf";
	rename -uid "A56DB8E9-4FA5-6D1F-869E-36B7C4C5CB8D";
createNode nurbsCurve -n "nurbsCircleShape6" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle4Buf|waveHandle4";
	rename -uid "20F8E5E9-4E3A-FEAC-AEC7-35A4BAC72897";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840296
		-2.1558421030922106e-33 0.3086000292377824 0
		-1.3361703213645398e-17 0.21821317334840273 0.21821317334840273
		-1.8896301901141494e-17 1.1102230246251565e-16 0.3086000292377824
		-1.3361703213645401e-17 -0.21821317334840273 0.21821317334840273
		-5.6938245452781404e-33 -0.30860002923778251 1.1102230246251565e-16
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.1102230246251565e-16 -0.3086000292377824
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840296
		-2.1558421030922106e-33 0.3086000292377824 0
		-1.3361703213645398e-17 0.21821317334840273 0.21821317334840273
		;
createNode transform -n "waveHandle4Loc" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle4Buf|waveHandle4";
	rename -uid "67F90C1D-4207-2415-5419-529B0AEDB089";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle4LocShape" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle4Buf|waveHandle4|waveHandle4Loc";
	rename -uid "770393EF-449D-E8D8-C369-008C14843A5D";
	setAttr -k off ".v";
createNode transform -n "waveHandle5Buf" -p "waveCtrlGRP_06";
	rename -uid "28E6042A-4F28-03F0-F1F1-62853F14C6B4";
	setAttr ".t" -type "double3" 20 0.31172194095803035 8.5674029310542483 ;
createNode transform -n "waveHandle5" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle5Buf";
	rename -uid "C5C209A9-4891-2364-D4EB-7A983727F5E6";
createNode nurbsCurve -n "nurbsCircleShape1" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle5Buf|waveHandle5";
	rename -uid "D93D1B65-40C3-70E3-9DEE-76969E82FC9F";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.1026781894481244e-16 0.21821317334840271 -0.21821317334840304
		-1.7791145552058667e-32 0.30860002923778257 3.5207573393294986e-17
		-1.1026781894481232e-16 0.21821317334840284 0.21821317334840282
		-1.5594224504505452e-16 8.9424567909960508e-17 0.30860002923778251
		-1.1026781894481236e-16 -0.21821317334840279 0.21821317334840287
		-4.6988441819384398e-32 -0.30860002923778262 9.2987211484036115e-17
		1.1026781894481226e-16 -0.21821317334840287 -0.21821317334840268
		1.5594224504505452e-16 -1.6574967770032629e-16 -0.30860002923778251
		1.1026781894481244e-16 0.21821317334840271 -0.21821317334840304
		-1.7791145552058667e-32 0.30860002923778257 3.5207573393294986e-17
		-1.1026781894481232e-16 0.21821317334840284 0.21821317334840282
		;
createNode transform -n "waveHandle5Loc" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle5Buf|waveHandle5";
	rename -uid "C668F9C6-4C17-DC7D-A8E6-DB8A7CE9B0B6";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle5LocShape" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle5Buf|waveHandle5|waveHandle5Loc";
	rename -uid "DEE9250F-4955-0517-B138-35AE50DAAF48";
	setAttr -k off ".v";
createNode transform -n "Anchorcrv1" -p "waveCtrlGRP_06";
	rename -uid "0939FF99-4AA2-7D66-EAEF-37B526A78097";
	setAttr ".t" -type "double3" 20 0 7.3378464335338505 ;
	setAttr ".rp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
	setAttr ".sp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
createNode nurbsCurve -n "AnchorcrvShape1" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|Anchorcrv1";
	rename -uid "8969D527-4F21-9B45-5789-108D03AEE9A5";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "Anchorcrv2" -p "waveCtrlGRP_06";
	rename -uid "9E0B094A-45A4-FB32-2A9D-768B4C9F8E0E";
	setAttr ".t" -type "double3" 20 0 -29.031414968196096 ;
	setAttr ".rp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
	setAttr ".sp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
createNode nurbsCurve -n "AnchorcrvShape2" -p "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|Anchorcrv2";
	rename -uid "1522ECBF-4CAB-DFF1-3566-13BBC2765DEE";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 12 0 no 3
		17 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 12 12
		15
		0 0.31649962882926275 3.473816476486677
		0 0.32032474979003439 3.7140401444416846
		0 0.32733752875043509 4.1944978750028756
		0 0.33594421944510944 4.9152096511274133
		0 0.34263837575037198 5.6359417812960499
		0 0.34741994946530685 6.3566891235950846
		0 0.35028890720410627 7.0774466205211217
		0 0.35124522868645058 7.7982091919612788
		0 0.35028890720413208 8.5189717634014333
		0 0.34741994946520338 9.2397292603274774
		0 0.342638375750419 9.9604766026265015
		0 0.33594421944502451 10.681208732795145
		0 0.32733752875038624 11.401920508919678
		0 0.32032474978997755 11.882378239480872
		0 0.3164996288292059 12.122601907435879
		;
createNode transform -n "waveGRP_07";
	rename -uid "4E3193E5-4C14-DACD-5671-A8ABA36B1FC2";
	setAttr ".rp" -type "double3" 0 0.66297004715381647 -3.0485750753698451 ;
	setAttr ".sp" -type "double3" 0 0.66297004715381647 -3.0485750753698451 ;
createNode transform -n "CRV" -p "waveGRP_07";
	rename -uid "79B68440-409A-77DC-776F-3E98BC38AA01";
	setAttr ".it" no;
createNode nurbsCurve -n "CRVShape" -p "|waveGRP_07|CRV";
	rename -uid "08C8BEDC-4882-EEA2-E7BB-70989A7AD175";
	setAttr -k off ".v";
	setAttr ".tw" yes;
	setAttr -s 51 ".cp[0:50]" -type "double3" 0 0 13.395690640824522 0 
		0 12.985605239437884 0 0 12.165416691973821 0 0 10.935090885561277 0 0 9.7047303327958403 
		0 0 8.4743438114293674 0 0 7.2439399551169075 0 0 6.0135274361103477 0 0 4.7831149171038145 
		0 0 3.5527110607913865 0 0 2.3223245394248728 0 0 1.091963986659426 0 0 -0.13836181975310069 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode transform -n "guts_GRP" -p "waveGRP_07";
	rename -uid "6DA82714-452A-F7CB-04B6-7E92EDF368BA";
	setAttr ".rp" -type "double3" 0 3.086054304693814 5.1025990357395044 ;
	setAttr ".sp" -type "double3" 0 3.086054304693814 5.1025990357395044 ;
createNode transform -n "curve2" -p "|waveGRP_07|guts_GRP";
	rename -uid "8A51E8AE-4D0A-C45A-3CB2-2D9CAE0A1F45";
	addAttr -ci true -k true -sn "handleSize" -ln "handleSize" -dv 1 -at "float";
	setAttr ".rp" -type "double3" 0 -178.71178233931573 -1.4586794008110158 ;
	setAttr ".sp" -type "double3" 0 -178.71178233931573 -1.4586794008110158 ;
createNode nurbsCurve -n "curveShape2" -p "|waveGRP_07|guts_GRP|curve2";
	rename -uid "9F242770-42A4-FEAF-D184-5AB15EC8BF1F";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve4" -p "|waveGRP_07|guts_GRP";
	rename -uid "443D6C92-4B46-A53B-F4C6-81B18FF89CC3";
	addAttr -ci true -k true -sn "handleSize" -ln "handleSize" -dv 1 -at "float";
	setAttr ".rp" -type "double3" 0 -49.027239636955784 2.1534198201671906 ;
	setAttr ".sp" -type "double3" 0 -49.027239636955784 2.1534198201671906 ;
createNode nurbsCurve -n "curveShape4" -p "|waveGRP_07|guts_GRP|curve4";
	rename -uid "8221D08A-43E8-6259-9B89-93B66610C95A";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve2attachedCurve1" -p "|waveGRP_07|guts_GRP";
	rename -uid "8D906AA3-4673-B5AF-4C8E-75A5C3EDD6FD";
	setAttr ".it" no;
createNode nurbsCurve -n "curve2attachedCurveShape1" -p "|waveGRP_07|guts_GRP|curve2attachedCurve1";
	rename -uid "BF5119F0-4633-0F86-8A35-B1B4BCE63C01";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve2attachedCurve1attachedCurve1" -p "|waveGRP_07|guts_GRP";
	rename -uid "D03EB5E7-4E91-B54B-6046-72B52EA0757D";
createNode nurbsCurve -n "curve2attachedCurve1attachedCurveShape1" -p "|waveGRP_07|guts_GRP|curve2attachedCurve1attachedCurve1";
	rename -uid "8CBCB10A-40F9-1B9E-4A33-B58292A58D28";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "waveCtrlGRP_07Buf" -p "waveGRP_07";
	rename -uid "AF639EFC-4A52-C4F5-765A-A18B1C4D0328";
createNode parentConstraint -n "waveCtrlGRP_07Buf_parentConstraint1" -p "waveCtrlGRP_07Buf";
	rename -uid "412F4C76-4C46-E355-40C7-D2A0A1163A25";
	addAttr -ci true -k true -sn "w0" -ln "wave07_CtrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 24 0 33.587504550141148 ;
	setAttr -k on ".w0";
createNode transform -n "waveCtrlGRP_07" -p "waveCtrlGRP_07Buf";
	rename -uid "DF71A368-4903-8832-D868-A28237249545";
	setAttr ".t" -type "double3" -24 0 -33.587504550141148 ;
createNode transform -n "waveHandleBuf" -p "waveCtrlGRP_07";
	rename -uid "B23084DC-4F52-DFC0-C65A-63A96FE096F3";
	setAttr ".t" -type "double3" 24 0.54087181781586069 9.444425917163219 ;
createNode transform -n "waveHandle" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandleBuf";
	rename -uid "8B3A3BB4-4CD4-3313-D375-20925215CE30";
createNode nurbsCurve -n "nurbsCircleShape2" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandleBuf|waveHandle";
	rename -uid "F8DB2EA5-44D8-4DE5-32DE-58B0B81C63F9";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandleLoc" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandleBuf|waveHandle";
	rename -uid "1FB71819-4F55-14EB-91F9-1EA8BC16685C";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandleLocShape" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandleBuf|waveHandle|waveHandleLoc";
	rename -uid "9B7451BB-4BC4-BF3D-FF08-2A86232EBAAA";
	setAttr -k off ".v";
createNode transform -n "waveHandle1Buf" -p "waveCtrlGRP_07";
	rename -uid "921D1851-443D-07B5-3D22-068FA4EA830C";
	setAttr ".t" -type "double3" 24.0423658644597 5.9822231664132017 5.8191499743864927 ;
createNode transform -n "waveHandle1" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle1Buf";
	rename -uid "B9F41A7E-434B-6543-4807-C58C433EC698";
createNode nurbsCurve -n "nurbsCircleShape3" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle1Buf|waveHandle1";
	rename -uid "07B078C3-42E7-5375-3E95-F6891E75BB46";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle1Loc" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle1Buf|waveHandle1";
	rename -uid "2D8C93BB-474A-454B-1FDC-389DE83F0E49";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle1LocShape" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle1Buf|waveHandle1|waveHandle1Loc";
	rename -uid "8788A893-40E3-AC13-0CA7-1F827E0BA053";
	setAttr -k off ".v";
createNode transform -n "waveHandle2Buf" -p "waveCtrlGRP_07";
	rename -uid "09AD7A7A-4690-9BDF-8504-E8A30059181C";
	setAttr ".t" -type "double3" 24 4.1547093470710132 -6.0473194096341176 ;
createNode transform -n "waveHandle2" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle2Buf";
	rename -uid "9143BD61-4ED3-C4C1-AE9A-60B0D6B3447A";
createNode nurbsCurve -n "nurbsCircleShape4" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle2Buf|waveHandle2";
	rename -uid "CF6B2C1F-4A19-D792-D148-A49F7252DA80";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle2Loc" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle2Buf|waveHandle2";
	rename -uid "F8C588BC-4894-C094-BCCD-76A3BD5E80C4";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle2LocShape" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle2Buf|waveHandle2|waveHandle2Loc";
	rename -uid "7D9D1C15-4A62-F6B6-F2C0-23B6467B5B66";
	setAttr -k off ".v";
createNode transform -n "waveHandle3Buf" -p "waveCtrlGRP_07";
	rename -uid "EE6AC0F6-4058-53B0-4368-44885E153EA6";
	setAttr ".t" -type "double3" 24 4.1736610134949403 2.3629492718926874 ;
createNode transform -n "waveHandle3" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle3Buf";
	rename -uid "64A0F0EE-4407-F938-9373-819CCF480F12";
createNode nurbsCurve -n "nurbsCircleShape5" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle3Buf|waveHandle3";
	rename -uid "07854254-4BBD-A5CD-BAAA-CFBA29AB0131";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle3Loc" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle3Buf|waveHandle3";
	rename -uid "037D197A-4A45-9BD5-2EAF-0C8741D22E5D";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle3LocShape" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle3Buf|waveHandle3|waveHandle3Loc";
	rename -uid "26BD47D2-4C75-C596-D38A-8CAA425B49C6";
	setAttr -k off ".v";
createNode transform -n "waveHandle4Buf" -p "waveCtrlGRP_07";
	rename -uid "9F274BB7-479D-07EB-D123-01AC633452F5";
	setAttr ".t" -type "double3" 24 -2.6318658672626594 5.4532668927044003 ;
createNode transform -n "waveHandle4" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle4Buf";
	rename -uid "1BB73307-4280-B683-96EB-FAA787F296A9";
createNode nurbsCurve -n "nurbsCircleShape6" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle4Buf|waveHandle4";
	rename -uid "B9F57292-4F3B-AFA5-5254-669B3503C42A";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840296
		-2.1558421030922106e-33 0.3086000292377824 0
		-1.3361703213645398e-17 0.21821317334840273 0.21821317334840273
		-1.8896301901141494e-17 1.1102230246251565e-16 0.3086000292377824
		-1.3361703213645401e-17 -0.21821317334840273 0.21821317334840273
		-5.6938245452781404e-33 -0.30860002923778251 1.1102230246251565e-16
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.1102230246251565e-16 -0.3086000292377824
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840296
		-2.1558421030922106e-33 0.3086000292377824 0
		-1.3361703213645398e-17 0.21821317334840273 0.21821317334840273
		;
createNode transform -n "waveHandle4Loc" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle4Buf|waveHandle4";
	rename -uid "0D4F4471-4E31-9B42-A49A-2DBA105DE6C0";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle4LocShape" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle4Buf|waveHandle4|waveHandle4Loc";
	rename -uid "E195238C-426A-D5FE-57E7-61A6B2959FA5";
	setAttr -k off ".v";
createNode transform -n "waveHandle5Buf" -p "waveCtrlGRP_07";
	rename -uid "85FEE320-4550-C764-5E65-6AAAA6D669B8";
	setAttr ".t" -type "double3" 24 0.17385057775449689 8.239479375103155 ;
createNode transform -n "waveHandle5" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle5Buf";
	rename -uid "C2628F1E-4DA1-8A8B-06B2-D9B012CC04E1";
createNode nurbsCurve -n "nurbsCircleShape1" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle5Buf|waveHandle5";
	rename -uid "22AECD42-4806-F595-2607-DCA2EEF2BBDA";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.1026781894481244e-16 0.21821317334840271 -0.21821317334840304
		-1.7791145552058667e-32 0.30860002923778257 3.5207573393294986e-17
		-1.1026781894481232e-16 0.21821317334840284 0.21821317334840282
		-1.5594224504505452e-16 8.9424567909960508e-17 0.30860002923778251
		-1.1026781894481236e-16 -0.21821317334840279 0.21821317334840287
		-4.6988441819384398e-32 -0.30860002923778262 9.2987211484036115e-17
		1.1026781894481226e-16 -0.21821317334840287 -0.21821317334840268
		1.5594224504505452e-16 -1.6574967770032629e-16 -0.30860002923778251
		1.1026781894481244e-16 0.21821317334840271 -0.21821317334840304
		-1.7791145552058667e-32 0.30860002923778257 3.5207573393294986e-17
		-1.1026781894481232e-16 0.21821317334840284 0.21821317334840282
		;
createNode transform -n "waveHandle5Loc" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle5Buf|waveHandle5";
	rename -uid "C5630C5B-48DA-CAAD-4462-29B8F24B0785";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle5LocShape" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle5Buf|waveHandle5|waveHandle5Loc";
	rename -uid "FD91988F-4D2D-A7CA-219C-EBAF8BA4292F";
	setAttr -k off ".v";
createNode transform -n "Anchorcrv1" -p "waveCtrlGRP_07";
	rename -uid "13C2FC90-4C9D-ED39-1746-B593F340E44F";
	setAttr ".t" -type "double3" 24 0 7.3378464335338505 ;
	setAttr ".rp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
	setAttr ".sp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
createNode nurbsCurve -n "AnchorcrvShape1" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|Anchorcrv1";
	rename -uid "8A2D3DE5-40C8-D4DF-80CF-0CB768D5D10E";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "Anchorcrv2" -p "waveCtrlGRP_07";
	rename -uid "90193154-4BF4-B0EE-4CDD-81836D41816A";
	setAttr ".t" -type "double3" 24 0 -29.031414968196096 ;
	setAttr ".rp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
	setAttr ".sp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
createNode nurbsCurve -n "AnchorcrvShape2" -p "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|Anchorcrv2";
	rename -uid "9CEAF800-4F4A-B5D7-8F32-4787ED83C526";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 12 0 no 3
		17 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 12 12
		15
		0 0.31649962882926275 3.473816476486677
		0 0.32032474979003439 3.7140401444416846
		0 0.32733752875043509 4.1944978750028756
		0 0.33594421944510944 4.9152096511274133
		0 0.34263837575037198 5.6359417812960499
		0 0.34741994946530685 6.3566891235950846
		0 0.35028890720410627 7.0774466205211217
		0 0.35124522868645058 7.7982091919612788
		0 0.35028890720413208 8.5189717634014333
		0 0.34741994946520338 9.2397292603274774
		0 0.342638375750419 9.9604766026265015
		0 0.33594421944502451 10.681208732795145
		0 0.32733752875038624 11.401920508919678
		0 0.32032474978997755 11.882378239480872
		0 0.3164996288292059 12.122601907435879
		;
createNode transform -n "waveGRP_08";
	rename -uid "F7266BF1-4D83-411B-C457-77ABBC7EF487";
	setAttr ".rp" -type "double3" 0 0.66297004715381647 -3.0485750753698451 ;
	setAttr ".sp" -type "double3" 0 0.66297004715381647 -3.0485750753698451 ;
createNode transform -n "CRV" -p "waveGRP_08";
	rename -uid "AF822F6D-4553-5939-5B2D-0F8C5A7AC0CA";
	setAttr ".it" no;
createNode nurbsCurve -n "CRVShape" -p "|waveGRP_08|CRV";
	rename -uid "CFDBE7AA-45BE-21BA-6DA7-D4AA3D6C5340";
	setAttr -k off ".v";
	setAttr ".tw" yes;
	setAttr -s 51 ".cp[0:50]" -type "double3" 0 0 13.395690640824522 0 
		0 12.985605239437884 0 0 12.165416691973821 0 0 10.935090885561277 0 0 9.7047303327958403 
		0 0 8.4743438114293674 0 0 7.2439399551169075 0 0 6.0135274361103477 0 0 4.7831149171038145 
		0 0 3.5527110607913865 0 0 2.3223245394248728 0 0 1.091963986659426 0 0 -0.13836181975310069 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode transform -n "guts_GRP" -p "waveGRP_08";
	rename -uid "D9EF6A08-4FC0-65D4-FC54-D592267D3907";
	setAttr ".rp" -type "double3" 0 3.086054304693814 5.1025990357395044 ;
	setAttr ".sp" -type "double3" 0 3.086054304693814 5.1025990357395044 ;
createNode transform -n "curve2" -p "|waveGRP_08|guts_GRP";
	rename -uid "CA0BD49C-4A6B-4398-F301-9E9021AF358E";
	addAttr -ci true -k true -sn "handleSize" -ln "handleSize" -dv 1 -at "float";
	setAttr ".rp" -type "double3" 0 -178.71178233931573 -1.4586794008110158 ;
	setAttr ".sp" -type "double3" 0 -178.71178233931573 -1.4586794008110158 ;
createNode nurbsCurve -n "curveShape2" -p "|waveGRP_08|guts_GRP|curve2";
	rename -uid "43F92A9D-4A8F-0026-73E6-6DAE2C026B8D";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve4" -p "|waveGRP_08|guts_GRP";
	rename -uid "F00CE97D-4EED-DAD1-9BBD-0B9F83373583";
	addAttr -ci true -k true -sn "handleSize" -ln "handleSize" -dv 1 -at "float";
	setAttr ".rp" -type "double3" 0 -49.027239636955784 2.1534198201671906 ;
	setAttr ".sp" -type "double3" 0 -49.027239636955784 2.1534198201671906 ;
createNode nurbsCurve -n "curveShape4" -p "|waveGRP_08|guts_GRP|curve4";
	rename -uid "2C033EDD-4B38-4394-D2D7-EABCF5192772";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve2attachedCurve1" -p "|waveGRP_08|guts_GRP";
	rename -uid "A555080E-45F2-B6AE-10BE-3086A06FE12D";
	setAttr ".it" no;
createNode nurbsCurve -n "curve2attachedCurveShape1" -p "|waveGRP_08|guts_GRP|curve2attachedCurve1";
	rename -uid "CB6BF02E-4137-EC87-440E-50B1838FC057";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve2attachedCurve1attachedCurve1" -p "|waveGRP_08|guts_GRP";
	rename -uid "F3FEC304-4B6B-D155-2C51-87B282E128B9";
createNode nurbsCurve -n "curve2attachedCurve1attachedCurveShape1" -p "|waveGRP_08|guts_GRP|curve2attachedCurve1attachedCurve1";
	rename -uid "7E8B9B10-48ED-8321-4646-DC8CB06772CE";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "waveCtrlGRP_08Buf" -p "waveGRP_08";
	rename -uid "A5D97663-44C7-783A-A4C3-6E8EA6F8212C";
createNode parentConstraint -n "waveCtrlGRP_08Buf_parentConstraint1" -p "waveCtrlGRP_08Buf";
	rename -uid "EEF99D5D-42DD-C33C-2D38-FABA0B8FCC2E";
	addAttr -ci true -k true -sn "w0" -ln "wave08_CtrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 29 0 33.587504550141148 ;
	setAttr -k on ".w0";
createNode transform -n "waveCtrlGRP_08" -p "waveCtrlGRP_08Buf";
	rename -uid "41752816-48F9-1D61-B2F3-D6B1AA1CE526";
	setAttr ".t" -type "double3" -29 0 -33.587504550141148 ;
createNode transform -n "waveHandleBuf" -p "waveCtrlGRP_08";
	rename -uid "A9877548-4B99-AF9E-FF43-E3A126122D6D";
	setAttr ".t" -type "double3" 29 0.69095329004538764 8.9835135559553692 ;
createNode transform -n "waveHandle" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandleBuf";
	rename -uid "6BA1C9BB-4B58-A852-95EA-55BE8D34224C";
createNode nurbsCurve -n "nurbsCircleShape2" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandleBuf|waveHandle";
	rename -uid "8E54FF02-4940-138D-FB04-098273BA0A9B";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandleLoc" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandleBuf|waveHandle";
	rename -uid "D683DB92-4390-E7BD-7BE0-1FA71DDEC5F9";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandleLocShape" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandleBuf|waveHandle|waveHandleLoc";
	rename -uid "5D531888-48CC-4353-ADCB-F592BE6ACEF0";
	setAttr -k off ".v";
createNode transform -n "waveHandle1Buf" -p "waveCtrlGRP_08";
	rename -uid "D670F207-4BA5-1956-4DEB-838DC5246D35";
	setAttr ".t" -type "double3" 29 6.4299485191042685 4.8589665869423397 ;
createNode transform -n "waveHandle1" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle1Buf";
	rename -uid "6F6574E4-40B6-C2FC-BF11-B88353B1287D";
createNode nurbsCurve -n "nurbsCircleShape3" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle1Buf|waveHandle1";
	rename -uid "A6EFA41C-497E-1E28-C077-0FBA324BE6DB";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle1Loc" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle1Buf|waveHandle1";
	rename -uid "85301864-471D-1830-E362-769AE125A6C7";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle1LocShape" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle1Buf|waveHandle1|waveHandle1Loc";
	rename -uid "BC9CC9E2-4A4A-FC6D-7BFA-EBA54C0EF686";
	setAttr -k off ".v";
createNode transform -n "waveHandle2Buf" -p "waveCtrlGRP_08";
	rename -uid "C35AD76E-4C40-A193-2E70-B2B74C031022";
	setAttr ".t" -type "double3" 29 4.0364712603314592 -6.5673513434678279 ;
createNode transform -n "waveHandle2" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle2Buf";
	rename -uid "5D135612-4D4C-5D0A-46FB-0896EE6DFAFC";
createNode nurbsCurve -n "nurbsCircleShape4" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle2Buf|waveHandle2";
	rename -uid "7662BDC8-40D6-77AE-55D1-B0A67E210E00";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle2Loc" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle2Buf|waveHandle2";
	rename -uid "C92273FC-47A9-DC79-925C-089AEA7990E5";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle2LocShape" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle2Buf|waveHandle2|waveHandle2Loc";
	rename -uid "E174A897-43A5-1368-2AA1-E79656B81EF1";
	setAttr -k off ".v";
createNode transform -n "waveHandle3Buf" -p "waveCtrlGRP_08";
	rename -uid "6635A9DC-43D6-5C74-063A-5D8F8B833C58";
	setAttr ".t" -type "double3" 29 4.3869989222895214 1.5717099099171878 ;
createNode transform -n "waveHandle3" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle3Buf";
	rename -uid "7C64E37A-4846-33B3-32CE-188E43B20BD1";
createNode nurbsCurve -n "nurbsCircleShape5" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle3Buf|waveHandle3";
	rename -uid "20015A8D-4EAC-D725-FB0C-7BA3E341136B";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle3Loc" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle3Buf|waveHandle3";
	rename -uid "E5280883-4BF4-76E9-2F09-A29885B8C7C2";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle3LocShape" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle3Buf|waveHandle3|waveHandle3Loc";
	rename -uid "0C931228-41AF-6FB5-E582-D5B2ABE425C9";
	setAttr -k off ".v";
createNode transform -n "waveHandle4Buf" -p "waveCtrlGRP_08";
	rename -uid "2EC4A267-4497-2F26-919B-4E845659E8E8";
	setAttr ".t" -type "double3" 29 -2.7337751601157345 4.7514520026059079 ;
createNode transform -n "waveHandle4" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle4Buf";
	rename -uid "09CC3C9B-444E-55FF-CA37-AC904EDFF9CF";
createNode nurbsCurve -n "nurbsCircleShape6" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle4Buf|waveHandle4";
	rename -uid "073A6384-48E7-7AB4-53CB-F18000620BEA";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840296
		-2.1558421030922106e-33 0.3086000292377824 0
		-1.3361703213645398e-17 0.21821317334840273 0.21821317334840273
		-1.8896301901141494e-17 1.1102230246251565e-16 0.3086000292377824
		-1.3361703213645401e-17 -0.21821317334840273 0.21821317334840273
		-5.6938245452781404e-33 -0.30860002923778251 1.1102230246251565e-16
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.1102230246251565e-16 -0.3086000292377824
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840296
		-2.1558421030922106e-33 0.3086000292377824 0
		-1.3361703213645398e-17 0.21821317334840273 0.21821317334840273
		;
createNode transform -n "waveHandle4Loc" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle4Buf|waveHandle4";
	rename -uid "BCFCD4BA-407D-5591-B106-B0BDB41E6C22";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle4LocShape" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle4Buf|waveHandle4|waveHandle4Loc";
	rename -uid "73CA1646-4473-2BA0-AAEC-9C973276375C";
	setAttr -k off ".v";
createNode transform -n "waveHandle5Buf" -p "waveCtrlGRP_08";
	rename -uid "33C49AEB-4C6B-DADF-88F2-17B7E0C7F5AE";
	setAttr ".t" -type "double3" 29 0.33140318601652774 7.8405295235879171 ;
createNode transform -n "waveHandle5" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle5Buf";
	rename -uid "359F7C0A-4DA4-3027-B9DF-2196A4E24CF3";
createNode nurbsCurve -n "nurbsCircleShape1" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle5Buf|waveHandle5";
	rename -uid "538E16FC-43F7-8917-00E9-6AB0E1ACB28C";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.1026781894481244e-16 0.21821317334840271 -0.21821317334840304
		-1.7791145552058667e-32 0.30860002923778257 3.5207573393294986e-17
		-1.1026781894481232e-16 0.21821317334840284 0.21821317334840282
		-1.5594224504505452e-16 8.9424567909960508e-17 0.30860002923778251
		-1.1026781894481236e-16 -0.21821317334840279 0.21821317334840287
		-4.6988441819384398e-32 -0.30860002923778262 9.2987211484036115e-17
		1.1026781894481226e-16 -0.21821317334840287 -0.21821317334840268
		1.5594224504505452e-16 -1.6574967770032629e-16 -0.30860002923778251
		1.1026781894481244e-16 0.21821317334840271 -0.21821317334840304
		-1.7791145552058667e-32 0.30860002923778257 3.5207573393294986e-17
		-1.1026781894481232e-16 0.21821317334840284 0.21821317334840282
		;
createNode transform -n "waveHandle5Loc" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle5Buf|waveHandle5";
	rename -uid "D25121E7-429C-BE4D-3B80-0F8095ED30B6";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle5LocShape" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle5Buf|waveHandle5|waveHandle5Loc";
	rename -uid "7211050B-4727-2B38-BED0-2C9D30883CAE";
	setAttr -k off ".v";
createNode transform -n "Anchorcrv1" -p "waveCtrlGRP_08";
	rename -uid "6D8FC117-43FE-5A8F-AE5F-309B70C68F72";
	setAttr ".t" -type "double3" 29 0 7.3378464335338505 ;
	setAttr ".rp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
	setAttr ".sp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
createNode nurbsCurve -n "AnchorcrvShape1" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|Anchorcrv1";
	rename -uid "3FF34BC0-4F06-4E72-AD4A-37AEA17DAD96";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "Anchorcrv2" -p "waveCtrlGRP_08";
	rename -uid "B1E09C8E-4794-3C4F-D433-B08EC80BC800";
	setAttr ".t" -type "double3" 29 0 -29.031414968196096 ;
	setAttr ".rp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
	setAttr ".sp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
createNode nurbsCurve -n "AnchorcrvShape2" -p "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|Anchorcrv2";
	rename -uid "788A8385-4039-F61E-5CD9-3BBBBB52E7D6";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 12 0 no 3
		17 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 12 12
		15
		0 0.31649962882926275 3.473816476486677
		0 0.32032474979003439 3.7140401444416846
		0 0.32733752875043509 4.1944978750028756
		0 0.33594421944510944 4.9152096511274133
		0 0.34263837575037198 5.6359417812960499
		0 0.34741994946530685 6.3566891235950846
		0 0.35028890720410627 7.0774466205211217
		0 0.35124522868645058 7.7982091919612788
		0 0.35028890720413208 8.5189717634014333
		0 0.34741994946520338 9.2397292603274774
		0 0.342638375750419 9.9604766026265015
		0 0.33594421944502451 10.681208732795145
		0 0.32733752875038624 11.401920508919678
		0 0.32032474978997755 11.882378239480872
		0 0.3164996288292059 12.122601907435879
		;
createNode transform -n "waveGRP_09";
	rename -uid "B7EB8DE4-435D-AD87-A7D3-C5A38B72028E";
	setAttr ".rp" -type "double3" 0 0.66297004715381647 -3.0485750753698451 ;
	setAttr ".sp" -type "double3" 0 0.66297004715381647 -3.0485750753698451 ;
createNode transform -n "CRV" -p "waveGRP_09";
	rename -uid "DCB0A64D-4A65-C488-F8DC-BCAEA32C6F2F";
	setAttr ".it" no;
createNode nurbsCurve -n "CRVShape" -p "|waveGRP_09|CRV";
	rename -uid "E31E291F-45FD-ADB3-5AAE-83B4A8F8642E";
	setAttr -k off ".v";
	setAttr ".tw" yes;
	setAttr -s 51 ".cp[0:50]" -type "double3" 0 0 10.670439451139341 0 
		0 10.26035404975271 0 0 9.4401655022886466 0 0 8.2098396958761022 0 0 6.979479143110666 
		0 0 5.7490926217441931 0 0 4.5186887654317331 0 0 3.2882762464251734 0 0 2.0578637274186402 
		0 0 0.82745987110621222 0 0 -0.4029266502603015 0 0 -1.6332872030257484 0 0 -2.863613009438275 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode transform -n "guts_GRP" -p "waveGRP_09";
	rename -uid "FA1B1DCC-4685-B1D9-35BA-E8A333B3A4CD";
	setAttr ".rp" -type "double3" 0 3.086054304693814 5.1025990357395044 ;
	setAttr ".sp" -type "double3" 0 3.086054304693814 5.1025990357395044 ;
createNode transform -n "curve2" -p "|waveGRP_09|guts_GRP";
	rename -uid "34B4A679-483D-D88D-0F02-DAB04129D3F3";
	addAttr -ci true -k true -sn "handleSize" -ln "handleSize" -dv 1 -at "float";
	setAttr ".rp" -type "double3" 0 -178.71178233931573 -1.4586794008110158 ;
	setAttr ".sp" -type "double3" 0 -178.71178233931573 -1.4586794008110158 ;
createNode nurbsCurve -n "curveShape2" -p "|waveGRP_09|guts_GRP|curve2";
	rename -uid "A06C8164-476C-4EFD-A033-B4A33D414656";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve4" -p "|waveGRP_09|guts_GRP";
	rename -uid "97C3F4D0-441F-747C-CA69-F8A029509BBF";
	addAttr -ci true -k true -sn "handleSize" -ln "handleSize" -dv 1 -at "float";
	setAttr ".rp" -type "double3" 0 -49.027239636955784 2.1534198201671906 ;
	setAttr ".sp" -type "double3" 0 -49.027239636955784 2.1534198201671906 ;
createNode nurbsCurve -n "curveShape4" -p "|waveGRP_09|guts_GRP|curve4";
	rename -uid "FA8F25BC-443D-AC48-AD78-65B872B17A6C";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve2attachedCurve1" -p "|waveGRP_09|guts_GRP";
	rename -uid "CC2A3456-45BF-1AE5-C33B-A2A2EA8A1D61";
	setAttr ".it" no;
createNode nurbsCurve -n "curve2attachedCurveShape1" -p "|waveGRP_09|guts_GRP|curve2attachedCurve1";
	rename -uid "CA3E938D-4545-66BC-FE6F-4CB743251E77";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve2attachedCurve1attachedCurve1" -p "|waveGRP_09|guts_GRP";
	rename -uid "5CE7CBB9-4278-A53C-B502-769CDDD15E96";
createNode nurbsCurve -n "curve2attachedCurve1attachedCurveShape1" -p "|waveGRP_09|guts_GRP|curve2attachedCurve1attachedCurve1";
	rename -uid "285F2C91-44D6-8C15-F393-94B7B10AEB0B";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "waveCtrlGRP_09Buf" -p "waveGRP_09";
	rename -uid "9461888B-4BC9-75F6-6CD6-3ABD1ECD6EA6";
createNode parentConstraint -n "waveCtrlGRP_09Buf_parentConstraint1" -p "waveCtrlGRP_09Buf";
	rename -uid "AA26A85E-44F9-9E11-CC00-538A810A4539";
	addAttr -ci true -k true -sn "w0" -ln "wave09_CtrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 34 0 33.587504550141148 ;
	setAttr -k on ".w0";
createNode transform -n "waveCtrlGRP_09" -p "waveCtrlGRP_09Buf";
	rename -uid "E68C9734-45EC-607B-2632-BCA91C3C2216";
	setAttr ".t" -type "double3" -34 0 -33.587504550141148 ;
createNode transform -n "waveHandleBuf" -p "waveCtrlGRP_09";
	rename -uid "D9B62288-4DAD-7236-BB89-1683D8B32BAC";
	setAttr ".t" -type "double3" 34 2.714353948651544 7.1544525654640907 ;
createNode transform -n "waveHandle" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandleBuf";
	rename -uid "798700F5-4F62-F71A-3B6C-6CAEE22CFF5F";
createNode nurbsCurve -n "nurbsCircleShape2" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandleBuf|waveHandle";
	rename -uid "FDFE81E2-4412-22A1-3DFE-B1B9B78B8505";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandleLoc" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandleBuf|waveHandle";
	rename -uid "AFCFC732-4F47-8025-AFEC-58A13092D26B";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandleLocShape" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandleBuf|waveHandle|waveHandleLoc";
	rename -uid "5F2DDA16-4B01-3574-DDA1-86BB84BB4CC9";
	setAttr -k off ".v";
createNode transform -n "waveHandle1Buf" -p "waveCtrlGRP_09";
	rename -uid "B30C7398-4C7F-F8D9-6C85-4EA18917B4E6";
	setAttr ".t" -type "double3" 34 7.0729627346965627 2.2743920627571779 ;
createNode transform -n "waveHandle1" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle1Buf";
	rename -uid "1B83917F-45B1-9A81-2EB0-66B4C89842A2";
createNode nurbsCurve -n "nurbsCircleShape3" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle1Buf|waveHandle1";
	rename -uid "7F63660F-4729-9145-C8AE-6CB888553DB6";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle1Loc" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle1Buf|waveHandle1";
	rename -uid "5A05F09C-4ADC-DF99-F85D-27B979914D77";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle1LocShape" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle1Buf|waveHandle1|waveHandle1Loc";
	rename -uid "DB964EBA-4BF6-60D1-B23D-988127CB2E77";
	setAttr -k off ".v";
createNode transform -n "waveHandle2Buf" -p "waveCtrlGRP_09";
	rename -uid "302A1388-415C-2380-8806-C2BBB97269E8";
	setAttr ".t" -type "double3" 34 3.9101502602709211 -7.5953845191390483 ;
createNode transform -n "waveHandle2" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle2Buf";
	rename -uid "5512AFEB-4A96-662F-E507-EF83F685892F";
createNode nurbsCurve -n "nurbsCircleShape4" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle2Buf|waveHandle2";
	rename -uid "3AB57E21-438E-0D07-C19D-41AC74B9E64C";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle2Loc" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle2Buf|waveHandle2";
	rename -uid "BE39E7DC-4C3F-5F82-7248-7DB7E2AB63C8";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle2LocShape" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle2Buf|waveHandle2|waveHandle2Loc";
	rename -uid "29D553FF-44BB-19CB-5443-3B957F2280D4";
	setAttr -k off ".v";
createNode transform -n "waveHandle3Buf" -p "waveCtrlGRP_09";
	rename -uid "5C7612C8-4A25-4B55-AB72-919094C249D9";
	setAttr ".t" -type "double3" 34 3.6294228321518114 -0.76826533990863322 ;
createNode transform -n "waveHandle3" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle3Buf";
	rename -uid "725CEE71-4161-0240-0FC6-6C8A5E567A26";
createNode nurbsCurve -n "nurbsCircleShape5" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle3Buf|waveHandle3";
	rename -uid "6CD8ACF6-4D08-38C6-AA98-4597209E048E";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle3Loc" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle3Buf|waveHandle3";
	rename -uid "9D5D62AC-4A8A-F67C-BCCC-F5B3553C2CE9";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle3LocShape" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle3Buf|waveHandle3|waveHandle3Loc";
	rename -uid "D7D8126D-46F0-5B89-8050-168860C2340E";
	setAttr -k off ".v";
createNode transform -n "waveHandle4Buf" -p "waveCtrlGRP_09";
	rename -uid "5F1358AA-47BE-5AA8-2999-0AB2B8989A0A";
	setAttr ".t" -type "double3" 34 -2.327380265128733 3.4167839011593566 ;
createNode transform -n "waveHandle4" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle4Buf";
	rename -uid "972A9E0E-4DE2-49E8-06D2-399DB882889B";
createNode nurbsCurve -n "nurbsCircleShape6" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle4Buf|waveHandle4";
	rename -uid "16A3E8E5-46D9-A1EE-9729-A8AC509C275B";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840296
		-2.1558421030922106e-33 0.3086000292377824 0
		-1.3361703213645398e-17 0.21821317334840273 0.21821317334840273
		-1.8896301901141494e-17 1.1102230246251565e-16 0.3086000292377824
		-1.3361703213645401e-17 -0.21821317334840273 0.21821317334840273
		-5.6938245452781404e-33 -0.30860002923778251 1.1102230246251565e-16
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.1102230246251565e-16 -0.3086000292377824
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840296
		-2.1558421030922106e-33 0.3086000292377824 0
		-1.3361703213645398e-17 0.21821317334840273 0.21821317334840273
		;
createNode transform -n "waveHandle4Loc" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle4Buf|waveHandle4";
	rename -uid "E6672D1B-4B4C-3680-383F-9FA63D378560";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle4LocShape" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle4Buf|waveHandle4|waveHandle4Loc";
	rename -uid "4AA13374-47E5-B12A-73B4-B989E3AB4323";
	setAttr -k off ".v";
createNode transform -n "waveHandle5Buf" -p "waveCtrlGRP_09";
	rename -uid "288AD169-4E51-AA68-7979-BB89D599707F";
	setAttr ".t" -type "double3" 34 1.7951865375475275 6.1676326343188306 ;
createNode transform -n "waveHandle5" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle5Buf";
	rename -uid "A305E663-4052-8E40-A55F-24A659D27C0E";
createNode nurbsCurve -n "nurbsCircleShape1" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle5Buf|waveHandle5";
	rename -uid "F1F3452D-4F3A-04EE-032C-868E5A920B1C";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.1026781894481244e-16 0.21821317334840271 -0.21821317334840304
		-1.7791145552058667e-32 0.30860002923778257 3.5207573393294986e-17
		-1.1026781894481232e-16 0.21821317334840284 0.21821317334840282
		-1.5594224504505452e-16 8.9424567909960508e-17 0.30860002923778251
		-1.1026781894481236e-16 -0.21821317334840279 0.21821317334840287
		-4.6988441819384398e-32 -0.30860002923778262 9.2987211484036115e-17
		1.1026781894481226e-16 -0.21821317334840287 -0.21821317334840268
		1.5594224504505452e-16 -1.6574967770032629e-16 -0.30860002923778251
		1.1026781894481244e-16 0.21821317334840271 -0.21821317334840304
		-1.7791145552058667e-32 0.30860002923778257 3.5207573393294986e-17
		-1.1026781894481232e-16 0.21821317334840284 0.21821317334840282
		;
createNode transform -n "waveHandle5Loc" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle5Buf|waveHandle5";
	rename -uid "80FDB42C-4C88-1A5E-8A8F-E89BA8E554B5";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle5LocShape" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle5Buf|waveHandle5|waveHandle5Loc";
	rename -uid "EB687C5D-4522-2252-E871-E3ADD5BAC974";
	setAttr -k off ".v";
createNode transform -n "Anchorcrv1" -p "waveCtrlGRP_09";
	rename -uid "8EC8A9CC-4177-5F2A-003F-90A06A4B61C7";
	setAttr ".t" -type "double3" 34 0 7.3378464335338505 ;
	setAttr ".rp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
	setAttr ".sp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
createNode nurbsCurve -n "AnchorcrvShape1" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|Anchorcrv1";
	rename -uid "D8F56457-4A3B-37FB-4055-A0A7202F0120";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "Anchorcrv2" -p "waveCtrlGRP_09";
	rename -uid "E77CBD32-49F6-F3FD-7906-0BB9FB716B5E";
	setAttr ".t" -type "double3" 34 0 -29.031414968196096 ;
	setAttr ".rp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
	setAttr ".sp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
createNode nurbsCurve -n "AnchorcrvShape2" -p "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|Anchorcrv2";
	rename -uid "6C3A4ED9-47AE-A118-700F-F8AF067341CB";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 12 0 no 3
		17 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 12 12
		15
		0 0.31649962882926275 3.473816476486677
		0 0.32032474979003439 3.7140401444416846
		0 0.32733752875043509 4.1944978750028756
		0 0.33594421944510944 4.9152096511274133
		0 0.34263837575037198 5.6359417812960499
		0 0.34741994946530685 6.3566891235950846
		0 0.35028890720410627 7.0774466205211217
		0 0.35124522868645058 7.7982091919612788
		0 0.35028890720413208 8.5189717634014333
		0 0.34741994946520338 9.2397292603274774
		0 0.342638375750419 9.9604766026265015
		0 0.33594421944502451 10.681208732795145
		0 0.32733752875038624 11.401920508919678
		0 0.32032474978997755 11.882378239480872
		0 0.3164996288292059 12.122601907435879
		;
createNode transform -n "waveGRP_10";
	rename -uid "91618993-4ED1-20CC-689C-D995FC0FDB40";
	setAttr ".rp" -type "double3" 0 0.66297004715381647 -3.0485750753698451 ;
	setAttr ".sp" -type "double3" 0 0.66297004715381647 -3.0485750753698451 ;
createNode transform -n "CRV" -p "waveGRP_10";
	rename -uid "A5232B50-43A8-46A6-8EAE-0AB6FBBC13AE";
	setAttr ".rp" -type "double3" 40 0.54602515079087233 -3.0485750753698451 ;
	setAttr ".sp" -type "double3" 40 0.54602515079087233 -3.0485750753698451 ;
	setAttr ".it" no;
createNode nurbsCurve -n "CRVShape" -p "|waveGRP_10|CRV";
	rename -uid "E8DC859B-496E-6D36-26D8-02B82AD9F4B5";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "guts_GRP" -p "waveGRP_10";
	rename -uid "5A65E5BC-4687-4557-68AD-E6B98610CBCE";
	setAttr ".rp" -type "double3" 0 3.086054304693814 5.1025990357395044 ;
	setAttr ".sp" -type "double3" 0 3.086054304693814 5.1025990357395044 ;
createNode transform -n "curve2" -p "|waveGRP_10|guts_GRP";
	rename -uid "43B9C80E-4DA4-A0DE-1110-C7BA0817B940";
	addAttr -ci true -k true -sn "handleSize" -ln "handleSize" -dv 1 -at "float";
	setAttr ".rp" -type "double3" 0 -178.71178233931573 -1.4586794008110158 ;
	setAttr ".sp" -type "double3" 0 -178.71178233931573 -1.4586794008110158 ;
createNode nurbsCurve -n "curveShape2" -p "|waveGRP_10|guts_GRP|curve2";
	rename -uid "B9C3BE6A-4296-E4D6-6432-98A18E282BAC";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve4" -p "|waveGRP_10|guts_GRP";
	rename -uid "71D973F8-47C8-B99A-6432-CBB943E73804";
	addAttr -ci true -k true -sn "handleSize" -ln "handleSize" -dv 1 -at "float";
	setAttr ".rp" -type "double3" 0 -49.027239636955784 2.1534198201671906 ;
	setAttr ".sp" -type "double3" 0 -49.027239636955784 2.1534198201671906 ;
createNode nurbsCurve -n "curveShape4" -p "|waveGRP_10|guts_GRP|curve4";
	rename -uid "C21EC66F-413C-DCDD-F905-9082E993B0DE";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve2attachedCurve1" -p "|waveGRP_10|guts_GRP";
	rename -uid "70E1E6D3-4850-0BA6-6033-B4BEFF68F48C";
	setAttr ".it" no;
createNode nurbsCurve -n "curve2attachedCurveShape1" -p "|waveGRP_10|guts_GRP|curve2attachedCurve1";
	rename -uid "1EA4AA31-4A8A-C78D-5DAB-6B9A242E9C33";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve2attachedCurve1attachedCurve1" -p "|waveGRP_10|guts_GRP";
	rename -uid "96F19CA1-4EEE-D8B4-E971-308D843726B5";
createNode nurbsCurve -n "curve2attachedCurve1attachedCurveShape1" -p "|waveGRP_10|guts_GRP|curve2attachedCurve1attachedCurve1";
	rename -uid "6A60BD3B-4306-E3C4-3B16-43A3ACFB5BF0";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "waveCtrlGRP_10Buf" -p "waveGRP_10";
	rename -uid "52381643-45B2-5DB1-7CE3-43B4FAA6EFAF";
createNode parentConstraint -n "waveCtrlGRP_10Buf_parentConstraint1" -p "waveCtrlGRP_10Buf";
	rename -uid "01EFD96C-48C3-8187-1D2E-2F9F8B0D4CB2";
	addAttr -ci true -k true -sn "w0" -ln "wave10_CtrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 38 0 33.587504550141148 ;
	setAttr -k on ".w0";
createNode transform -n "waveCtrlGRP_10" -p "waveCtrlGRP_10Buf";
	rename -uid "5907A896-43C7-3578-1F3E-01A5056A012D";
	setAttr ".t" -type "double3" -38 0 -33.587504550141148 ;
createNode transform -n "waveHandleBuf" -p "waveCtrlGRP_10";
	rename -uid "ECF8541A-49F9-80D2-84ED-598FF27A31A7";
	setAttr ".t" -type "double3" 40 5.8939687288825739 3.9231598768422629 ;
createNode transform -n "waveHandle" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandleBuf";
	rename -uid "E03AF53B-496F-4C2C-4552-C3B79D9BABC7";
createNode nurbsCurve -n "nurbsCircleShape2" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandleBuf|waveHandle";
	rename -uid "3CAE4E1E-4548-8A39-3D79-7B8C7F9F85DC";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandleLoc" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandleBuf|waveHandle";
	rename -uid "55C99572-4B9E-1563-5D58-1E9CB9DCB911";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandleLocShape" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandleBuf|waveHandle|waveHandleLoc";
	rename -uid "62D9CB3A-455F-E0D8-31F0-3FB877F17CC6";
	setAttr -k off ".v";
createNode transform -n "waveHandle1Buf" -p "waveCtrlGRP_10";
	rename -uid "1F4D32CD-46FE-B681-087D-3AADB26E57C0";
	setAttr ".t" -type "double3" 40 7.3604653063224985 -1.2060768472190402 ;
createNode transform -n "waveHandle1" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle1Buf";
	rename -uid "3466B4F1-4C27-F012-3403-B3987FE85041";
createNode nurbsCurve -n "nurbsCircleShape3" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle1Buf|waveHandle1";
	rename -uid "EA9F401A-4200-2255-19C1-D7BA83F31A79";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle1Loc" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle1Buf|waveHandle1";
	rename -uid "5F7FA982-491E-5C4D-C4EA-8392F5917E2E";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle1LocShape" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle1Buf|waveHandle1|waveHandle1Loc";
	rename -uid "BCB2EFA2-494A-859D-0E98-A28B3342C378";
	setAttr -k off ".v";
createNode transform -n "waveHandle2Buf" -p "waveCtrlGRP_10";
	rename -uid "67859664-4C9A-5F87-B8D6-45B400AB0294";
	setAttr ".t" -type "double3" 40 3.841029130272319 -8.8754358817576602 ;
createNode transform -n "waveHandle2" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle2Buf";
	rename -uid "71060DB4-475F-74CE-804E-98916AF765EF";
createNode nurbsCurve -n "nurbsCircleShape4" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle2Buf|waveHandle2";
	rename -uid "F4453390-4CB3-A2EF-C2EE-A481B87742AC";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle2Loc" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle2Buf|waveHandle2";
	rename -uid "7DFDC927-403C-28C8-5A9F-3D8FF73598D3";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle2LocShape" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle2Buf|waveHandle2|waveHandle2Loc";
	rename -uid "0BB4382E-4436-AD27-2171-43BCC60C985A";
	setAttr -k off ".v";
createNode transform -n "waveHandle3Buf" -p "waveCtrlGRP_10";
	rename -uid "5B3EB781-405C-C912-24AB-96ADEB002A2A";
	setAttr ".t" -type "double3" 40 3.9032325193538053 -2.6420679886592597 ;
createNode transform -n "waveHandle3" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle3Buf";
	rename -uid "99156EF0-452D-FB12-1D66-E89B65B7CB96";
createNode nurbsCurve -n "nurbsCircleShape5" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle3Buf|waveHandle3";
	rename -uid "A97B2E37-421B-9D52-73C4-9FBCC0F18897";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle3Loc" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle3Buf|waveHandle3";
	rename -uid "B0475370-4908-C558-7D99-3999B0C16211";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle3LocShape" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle3Buf|waveHandle3|waveHandle3Loc";
	rename -uid "84A0A771-4143-28FD-DB9E-73B9E7F3AF52";
	setAttr -k off ".v";
createNode transform -n "waveHandle4Buf" -p "waveCtrlGRP_10";
	rename -uid "565A9360-4871-B266-5E85-408748CF79C9";
	setAttr ".t" -type "double3" 40 -1.1500057053067401 2.1416293534837139 ;
createNode transform -n "waveHandle4" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle4Buf";
	rename -uid "E92033F4-407B-C506-19B0-7F934A4A08BC";
createNode nurbsCurve -n "nurbsCircleShape6" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle4Buf|waveHandle4";
	rename -uid "7966FD50-469E-BA6A-D18C-AFB4C68E275A";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840296
		-2.1558421030922106e-33 0.3086000292377824 0
		-1.3361703213645398e-17 0.21821317334840273 0.21821317334840273
		-1.8896301901141494e-17 1.1102230246251565e-16 0.3086000292377824
		-1.3361703213645401e-17 -0.21821317334840273 0.21821317334840273
		-5.6938245452781404e-33 -0.30860002923778251 1.1102230246251565e-16
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.1102230246251565e-16 -0.3086000292377824
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840296
		-2.1558421030922106e-33 0.3086000292377824 0
		-1.3361703213645398e-17 0.21821317334840273 0.21821317334840273
		;
createNode transform -n "waveHandle4Loc" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle4Buf|waveHandle4";
	rename -uid "5337CD87-4D66-5406-9F68-BB833D8897C0";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle4LocShape" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle4Buf|waveHandle4|waveHandle4Loc";
	rename -uid "989D4688-40DB-56DA-BBDA-9FA64EDA425D";
	setAttr -k off ".v";
createNode transform -n "waveHandle5Buf" -p "waveCtrlGRP_10";
	rename -uid "EC9E959A-4DFA-299A-E5FA-A1AA8E3B8767";
	setAttr ".t" -type "double3" 40 4.7198074969768227 3.1538049360824103 ;
createNode transform -n "waveHandle5" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle5Buf";
	rename -uid "F858F022-408A-1F10-02CC-9E94BF73F48C";
createNode nurbsCurve -n "nurbsCircleShape1" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle5Buf|waveHandle5";
	rename -uid "4D460E40-45F5-C886-A27F-65BE09D694C3";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.1026781894481244e-16 0.21821317334840271 -0.21821317334840304
		-1.7791145552058667e-32 0.30860002923778257 3.5207573393294986e-17
		-1.1026781894481232e-16 0.21821317334840284 0.21821317334840282
		-1.5594224504505452e-16 8.9424567909960508e-17 0.30860002923778251
		-1.1026781894481236e-16 -0.21821317334840279 0.21821317334840287
		-4.6988441819384398e-32 -0.30860002923778262 9.2987211484036115e-17
		1.1026781894481226e-16 -0.21821317334840287 -0.21821317334840268
		1.5594224504505452e-16 -1.6574967770032629e-16 -0.30860002923778251
		1.1026781894481244e-16 0.21821317334840271 -0.21821317334840304
		-1.7791145552058667e-32 0.30860002923778257 3.5207573393294986e-17
		-1.1026781894481232e-16 0.21821317334840284 0.21821317334840282
		;
createNode transform -n "waveHandle5Loc" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle5Buf|waveHandle5";
	rename -uid "2E024907-4692-8D84-60C5-92B7ED3A6005";
	setAttr -l on ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode locator -n "waveHandle5LocShape" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle5Buf|waveHandle5|waveHandle5Loc";
	rename -uid "DC10C7F7-4FA6-3D84-8320-5798CBA3A51F";
	setAttr -k off ".v";
createNode transform -n "Anchorcrv1" -p "waveCtrlGRP_10";
	rename -uid "3B3A50BF-41E9-25AF-9578-C9A6E943EA23";
	setAttr ".t" -type "double3" 40 0 7.3378464335338505 ;
	setAttr ".rp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
	setAttr ".sp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
createNode nurbsCurve -n "AnchorcrvShape1" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|Anchorcrv1";
	rename -uid "A20732CA-4463-92F0-D74D-0A9C27B3C805";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "Anchorcrv2" -p "waveCtrlGRP_10";
	rename -uid "A8469951-4552-F01E-685D-B69E00099788";
	setAttr ".t" -type "double3" 40 0 -29.031414968196096 ;
	setAttr ".rp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
	setAttr ".sp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
createNode nurbsCurve -n "AnchorcrvShape2" -p "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|Anchorcrv2";
	rename -uid "AF8F6C15-4C8E-3901-C388-0BABBB49158A";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 12 0 no 3
		17 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 12 12
		15
		0 0.31649962882926275 3.473816476486677
		0 0.32032474979003439 3.7140401444416846
		0 0.32733752875043509 4.1944978750028756
		0 0.33594421944510944 4.9152096511274133
		0 0.34263837575037198 5.6359417812960499
		0 0.34741994946530685 6.3566891235950846
		0 0.35028890720410627 7.0774466205211217
		0 0.35124522868645058 7.7982091919612788
		0 0.35028890720413208 8.5189717634014333
		0 0.34741994946520338 9.2397292603274774
		0 0.342638375750419 9.9604766026265015
		0 0.33594421944502451 10.681208732795145
		0 0.32733752875038624 11.401920508919678
		0 0.32032474978997755 11.882378239480872
		0 0.3164996288292059 12.122601907435879
		;
createNode transform -n "loftedSurface1";
	rename -uid "47594EB6-4CE8-78CE-0F3C-8B9724CD578E";
createNode mesh -n "loftedSurfaceShape1" -p "loftedSurface1";
	rename -uid "7A1137AB-477E-3258-733A-379E4B0BBC46";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ugsdt" no;
	setAttr ".bnr" 0;
	setAttr ".vnm" 0;
createNode mesh -n "loftedSurfaceShape1Orig" -p "loftedSurface1";
	rename -uid "BB71EC56-4123-E027-EC39-AFAF119CBBC0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ugsdt" no;
	setAttr ".bnr" 0;
	setAttr ".vnm" 0;
createNode transform -n "wave1Handle";
	rename -uid "B31664BF-42F0-0FE8-AABF-078EF79DF5A4";
	setAttr ".t" -type "double3" 20 1.5976322144269943 -26.749565667423052 ;
	setAttr ".s" -type "double3" 22.509023666381836 22.509023666381836 22.509023666381836 ;
	setAttr ".smd" 7;
createNode deformWave -n "wave1HandleShape" -p "wave1Handle";
	rename -uid "CB4188EE-45A9-870D-ED7C-2BB4AF58F54A";
	setAttr -k off ".v";
	setAttr ".dd" -type "doubleArray" 7 0 2.4000000000000004 0 0.009999999999999995
		 0.55999999999999994 -1.4729096989966557 0 ;
	setAttr ".hw" 22;
createNode transform -n "wave2Handle";
	rename -uid "06E4B7D9-4B82-D58A-C578-29AC2DF62DBC";
	setAttr ".t" -type "double3" 20 2.5987142324447632 27.599711354061942 ;
	setAttr ".s" -type "double3" 22.509023666381836 22.509023666381836 22.509023666381836 ;
	setAttr ".smd" 7;
createNode deformWave -n "wave2HandleShape" -p "wave2Handle";
	rename -uid "7DE809D0-47FC-9636-3BA1-78BDB51F46D7";
	setAttr -k off ".v";
	setAttr ".dd" -type "doubleArray" 7 0 2.2200000000000002 0 0.010000000000000009
		 0.43999999999999995 0.65819397993311046 0 ;
	setAttr ".hw" 22;
createNode transform -n "locator1";
	rename -uid "470B920F-4307-BA82-7633-43ACFE7A20AF";
createNode locator -n "locatorShape1" -p "locator1";
	rename -uid "064E843A-474E-497E-7CB9-0CBF927E2A40";
	setAttr -k off ".v";
createNode transform -n "waveMasterGRP";
	rename -uid "2093CC96-4670-EC86-4190-FFA715E1C2D6";
	setAttr ".rp" -type "double3" 0 0.66297004715381647 -3.0485750753698451 ;
	setAttr ".sp" -type "double3" 0 0.66297004715381647 -3.0485750753698451 ;
createNode transform -n "waveHandle" -p "waveMasterGRP";
	rename -uid "7C4E6365-4E88-067B-E123-66ACDC3F027A";
createNode nurbsCurve -n "nurbsCircleShape2" -p "|waveMasterGRP|waveHandle";
	rename -uid "793AD158-4CF3-0C1A-0DC9-1AA253F4A67A";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle1" -p "waveMasterGRP";
	rename -uid "EBBF1E6F-472A-7768-3747-98B18050763C";
createNode nurbsCurve -n "nurbsCircleShape3" -p "|waveMasterGRP|waveHandle1";
	rename -uid "E43536FE-4AE3-7670-A8D9-DE8C155A6D82";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle2" -p "waveMasterGRP";
	rename -uid "3088E69B-4345-8C4F-10B0-4AA1ADD6B304";
createNode nurbsCurve -n "nurbsCircleShape4" -p "|waveMasterGRP|waveHandle2";
	rename -uid "0A0098CA-4EF9-C8EF-4D6A-FDBA083A0B35";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle3" -p "waveMasterGRP";
	rename -uid "53CAB348-4E30-05EF-6ABE-2C8D90EE5EF3";
createNode nurbsCurve -n "nurbsCircleShape5" -p "|waveMasterGRP|waveHandle3";
	rename -uid "6D00ADC1-4665-329B-D91C-6D945F41D34C";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		-1.8896301901141494e-17 8.9424567909960471e-17 0.30860002923778246
		-1.3361703213645401e-17 -0.21821317334840271 0.21821317334840282
		-5.6938245452781404e-33 -0.30860002923778251 9.2987211484036091e-17
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.6574967770032621e-16 -0.30860002923778246
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840298
		-2.1558421030922106e-33 0.30860002923778246 3.520757339329498e-17
		-1.3361703213645398e-17 0.21821317334840276 0.21821317334840276
		;
createNode transform -n "waveHandle4" -p "waveMasterGRP";
	rename -uid "A07A9B1E-4B12-5C6D-4FCB-E4A36A2E35E2";
createNode nurbsCurve -n "nurbsCircleShape6" -p "|waveMasterGRP|waveHandle4";
	rename -uid "EF63BA97-4245-383E-E02F-E2910F5952B9";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840296
		-2.1558421030922106e-33 0.3086000292377824 0
		-1.3361703213645398e-17 0.21821317334840273 0.21821317334840273
		-1.8896301901141494e-17 1.1102230246251565e-16 0.3086000292377824
		-1.3361703213645401e-17 -0.21821317334840273 0.21821317334840273
		-5.6938245452781404e-33 -0.30860002923778251 1.1102230246251565e-16
		1.3361703213645389e-17 -0.21821317334840279 -0.21821317334840262
		1.8896301901141494e-17 -1.1102230246251565e-16 -0.3086000292377824
		1.3361703213645412e-17 0.21821317334840262 -0.21821317334840296
		-2.1558421030922106e-33 0.3086000292377824 0
		-1.3361703213645398e-17 0.21821317334840273 0.21821317334840273
		;
createNode transform -n "waveHandle5" -p "waveMasterGRP";
	rename -uid "CFF9B56A-44F4-86AF-E51A-B9B33C885C6F";
createNode nurbsCurve -n "nurbsCircleShape1" -p "|waveMasterGRP|waveHandle5";
	rename -uid "87B34DE8-4F29-E532-7E14-7BBD305689B6";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "Anchorcrv1" -p "waveMasterGRP";
	rename -uid "79D867F2-4658-79DD-4A4C-FE8BB5FB1A6D";
	setAttr ".t" -type "double3" 55 0 7.3378464335338469 ;
	setAttr ".rp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
	setAttr ".sp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
createNode nurbsCurve -n "AnchorcrvShape1" -p "|waveMasterGRP|Anchorcrv1";
	rename -uid "14BB41F1-47C1-0768-57CF-84B7CBF56DF6";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "Anchorcrv2" -p "waveMasterGRP";
	rename -uid "A9B9861E-4CED-AB7E-22DF-E095F4F8A481";
	setAttr ".t" -type "double3" 55 0 -29.031414968196099 ;
	setAttr ".rp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
	setAttr ".sp" -type "double3" 0 0.33387242875782824 7.7982091919612779 ;
createNode nurbsCurve -n "AnchorcrvShape2" -p "|waveMasterGRP|Anchorcrv2";
	rename -uid "4877FA10-4EBF-E998-E5DE-7ABB31CC0887";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 12 0 no 3
		17 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 12 12
		15
		0 0.31649962882926275 3.473816476486677
		0 0.32032474979003439 3.7140401444416846
		0 0.32733752875043509 4.1944978750028756
		0 0.33594421944510944 4.9152096511274133
		0 0.34263837575037198 5.6359417812960499
		0 0.34741994946530685 6.3566891235950846
		0 0.35028890720410627 7.0774466205211217
		0 0.35124522868645058 7.7982091919612788
		0 0.35028890720413208 8.5189717634014333
		0 0.34741994946520338 9.2397292603274774
		0 0.342638375750419 9.9604766026265015
		0 0.33594421944502451 10.681208732795145
		0 0.32733752875038624 11.401920508919678
		0 0.32032474978997755 11.882378239480872
		0 0.3164996288292059 12.122601907435879
		;
createNode transform -n "CRV" -p "waveMasterGRP";
	rename -uid "586235C3-4BC2-CFE1-300F-9FA3730B8F83";
	setAttr ".it" no;
createNode nurbsCurve -n "CRVShape" -p "|waveMasterGRP|CRV";
	rename -uid "AFF7C352-4F6B-2BD8-9DAD-62B321D6D2B9";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "guts_GRP" -p "waveMasterGRP";
	rename -uid "0EE30951-4016-CF0A-7EBE-488F500F699A";
	setAttr ".rp" -type "double3" 0 3.086054304693814 5.1025990357395044 ;
	setAttr ".sp" -type "double3" 0 3.086054304693814 5.1025990357395044 ;
createNode transform -n "curve2" -p "|waveMasterGRP|guts_GRP";
	rename -uid "484C86D0-4E5A-7AEC-BA42-A8817C2D47DE";
	addAttr -ci true -k true -sn "handleSize" -ln "handleSize" -dv 1 -at "float";
	setAttr ".ove" yes;
	setAttr ".rp" -type "double3" 0 -178.71178233931573 -1.4586794008110158 ;
	setAttr ".sp" -type "double3" 0 -178.71178233931573 -1.4586794008110158 ;
createNode nurbsCurve -n "curveShape2" -p "|waveMasterGRP|guts_GRP|curve2";
	rename -uid "3E7B3083-46A4-6F7C-F79A-0C9EBBD88F69";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve4" -p "|waveMasterGRP|guts_GRP";
	rename -uid "3AA6CF7A-4EF4-79D0-6792-00A430A64349";
	addAttr -ci true -k true -sn "handleSize" -ln "handleSize" -dv 1 -at "float";
	setAttr ".ove" yes;
	setAttr ".rp" -type "double3" 0 -49.027239636955784 2.1534198201671906 ;
	setAttr ".sp" -type "double3" 0 -49.027239636955784 2.1534198201671906 ;
createNode nurbsCurve -n "curveShape4" -p "|waveMasterGRP|guts_GRP|curve4";
	rename -uid "FE0A8DD9-4DC4-1E86-E130-8DB69724D97E";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve2attachedCurve1" -p "|waveMasterGRP|guts_GRP";
	rename -uid "ED59A351-4B3D-399C-0397-5C944A6EF391";
	setAttr ".ove" yes;
	setAttr ".it" no;
createNode nurbsCurve -n "curve2attachedCurveShape1" -p "|waveMasterGRP|guts_GRP|curve2attachedCurve1";
	rename -uid "DA94C74D-480D-96AC-856F-F08B2F77EE6C";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "curve2attachedCurve1attachedCurve1" -p "|waveMasterGRP|guts_GRP";
	rename -uid "A18B5372-4A78-77E1-3FD4-32B6C177B98F";
	setAttr ".ove" yes;
createNode nurbsCurve -n "curve2attachedCurve1attachedCurveShape1" -p "|waveMasterGRP|guts_GRP|curve2attachedCurve1attachedCurve1";
	rename -uid "082B25EA-48EA-FEF2-81B0-E7A5C508698C";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode transform -n "waveOffsetGRP";
	rename -uid "EC808099-447D-63AF-AB11-6CBB821B681B";
createNode transform -n "wave01_CtrlBuf" -p "waveOffsetGRP";
	rename -uid "1164A631-4F1D-90D7-722A-57853114F0B3";
	addAttr -ci true -sn "offset" -ln "offset" -at "double";
	setAttr ".t" -type "double3" 0 0 33.587504550141148 ;
	setAttr -k on ".offset";
createNode transform -n "wave01_Ctrl" -p "wave01_CtrlBuf";
	rename -uid "6421A68E-4CED-5342-794A-20BAA3A2363D";
	addAttr -ci true -sn "offset" -ln "offset" -at "double";
	setAttr -k on ".offset";
createNode nurbsCurve -n "wave01_CtrlShape" -p "wave01_Ctrl";
	rename -uid "88377463-4EA5-A3E5-A3DD-60B7CE98BA04";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.78361162489122504 4.7982373409884682e-17 -0.78361162489122382
		-1.2643170607829326e-16 6.7857323231109134e-17 -1.1081941875543879
		-0.78361162489122427 4.7982373409884713e-17 -0.78361162489122427
		-1.1081941875543879 1.9663354616187859e-32 -3.2112695072372299e-16
		-0.78361162489122449 -4.7982373409884694e-17 0.78361162489122405
		-3.3392053635905195e-16 -6.7857323231109146e-17 1.1081941875543881
		0.78361162489122382 -4.7982373409884719e-17 0.78361162489122438
		1.1081941875543879 -3.6446300679047921e-32 5.9521325992805852e-16
		0.78361162489122504 4.7982373409884682e-17 -0.78361162489122382
		-1.2643170607829326e-16 6.7857323231109134e-17 -1.1081941875543879
		-0.78361162489122427 4.7982373409884713e-17 -0.78361162489122427
		;
createNode transform -n "wave02_CtrlBuf" -p "waveOffsetGRP";
	rename -uid "D9D9118B-46C6-5596-65F5-94AB211E2BC5";
	addAttr -ci true -sn "offset" -ln "offset" -at "double";
	setAttr ".t" -type "double3" 4 0 33.587504550141148 ;
	setAttr -k on ".offset" 2;
createNode transform -n "wave02_Ctrl" -p "wave02_CtrlBuf";
	rename -uid "93A11AE0-4C38-CD42-B023-3A812C8BCED4";
	addAttr -ci true -sn "offset" -ln "offset" -at "double";
	setAttr -k on ".offset" 2;
createNode nurbsCurve -n "wave02_CtrlShape" -p "wave02_Ctrl";
	rename -uid "21132C5B-4677-1C6C-E2C6-4493ED6FD472";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.78361162489122504 4.7982373409884682e-17 -0.78361162489122382
		-1.2643170607829326e-16 6.7857323231109134e-17 -1.1081941875543879
		-0.78361162489122427 4.7982373409884713e-17 -0.78361162489122427
		-1.1081941875543879 1.9663354616187859e-32 -3.2112695072372299e-16
		-0.78361162489122449 -4.7982373409884694e-17 0.78361162489122405
		-3.3392053635905195e-16 -6.7857323231109146e-17 1.1081941875543881
		0.78361162489122382 -4.7982373409884719e-17 0.78361162489122438
		1.1081941875543879 -3.6446300679047921e-32 5.9521325992805852e-16
		0.78361162489122504 4.7982373409884682e-17 -0.78361162489122382
		-1.2643170607829326e-16 6.7857323231109134e-17 -1.1081941875543879
		-0.78361162489122427 4.7982373409884713e-17 -0.78361162489122427
		;
createNode transform -n "wave03_CtrlBuf" -p "waveOffsetGRP";
	rename -uid "51689064-4382-547E-CFCA-6C8F82327A5A";
	addAttr -ci true -sn "offset" -ln "offset" -at "double";
	setAttr ".t" -type "double3" 8 0 33.587504550141148 ;
	setAttr -k on ".offset" 5;
createNode transform -n "wave03_Ctrl" -p "wave03_CtrlBuf";
	rename -uid "90685D0D-4506-4942-3A16-9F915897EC92";
	addAttr -ci true -sn "offset" -ln "offset" -at "double";
	setAttr -k on ".offset" 5;
createNode nurbsCurve -n "wave03_CtrlShape" -p "wave03_Ctrl";
	rename -uid "99D5ED47-4D91-A09E-DD1D-81BFF9C005EF";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.78361162489122504 4.7982373409884682e-17 -0.78361162489122382
		-1.2643170607829326e-16 6.7857323231109134e-17 -1.1081941875543879
		-0.78361162489122427 4.7982373409884713e-17 -0.78361162489122427
		-1.1081941875543879 1.9663354616187859e-32 -3.2112695072372299e-16
		-0.78361162489122449 -4.7982373409884694e-17 0.78361162489122405
		-3.3392053635905195e-16 -6.7857323231109146e-17 1.1081941875543881
		0.78361162489122382 -4.7982373409884719e-17 0.78361162489122438
		1.1081941875543879 -3.6446300679047921e-32 5.9521325992805852e-16
		0.78361162489122504 4.7982373409884682e-17 -0.78361162489122382
		-1.2643170607829326e-16 6.7857323231109134e-17 -1.1081941875543879
		-0.78361162489122427 4.7982373409884713e-17 -0.78361162489122427
		;
createNode transform -n "wave04_CtrlBuf" -p "waveOffsetGRP";
	rename -uid "F9C9AEA3-42DF-FFB9-4A68-7CBBAC89F343";
	addAttr -ci true -sn "offset" -ln "offset" -at "double";
	setAttr ".t" -type "double3" 12 0 33.587504550141148 ;
	setAttr -k on ".offset" 11;
createNode transform -n "wave04_Ctrl" -p "wave04_CtrlBuf";
	rename -uid "4B25A554-4608-9036-8CB7-5AB54C6AB189";
	addAttr -ci true -sn "offset" -ln "offset" -at "double";
	setAttr -k on ".offset" 11;
createNode nurbsCurve -n "wave04_CtrlShape" -p "wave04_Ctrl";
	rename -uid "C7C224A2-4157-7D70-88CF-43A32592EB50";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.78361162489122504 4.7982373409884682e-17 -0.78361162489122382
		-1.2643170607829326e-16 6.7857323231109134e-17 -1.1081941875543879
		-0.78361162489122427 4.7982373409884713e-17 -0.78361162489122427
		-1.1081941875543879 1.9663354616187859e-32 -3.2112695072372299e-16
		-0.78361162489122449 -4.7982373409884694e-17 0.78361162489122405
		-3.3392053635905195e-16 -6.7857323231109146e-17 1.1081941875543881
		0.78361162489122382 -4.7982373409884719e-17 0.78361162489122438
		1.1081941875543879 -3.6446300679047921e-32 5.9521325992805852e-16
		0.78361162489122504 4.7982373409884682e-17 -0.78361162489122382
		-1.2643170607829326e-16 6.7857323231109134e-17 -1.1081941875543879
		-0.78361162489122427 4.7982373409884713e-17 -0.78361162489122427
		;
createNode transform -n "wave05_CtrlBuf" -p "waveOffsetGRP";
	rename -uid "0A33F3FF-4922-32ED-B940-9DB085F87E11";
	addAttr -ci true -sn "offset" -ln "offset" -at "double";
	setAttr ".t" -type "double3" 16 0 33.587504550141148 ;
	setAttr -k on ".offset" 17;
createNode transform -n "wave05_Ctrl" -p "wave05_CtrlBuf";
	rename -uid "88E9C940-4BBB-6E9A-A3AD-708F0B92FAC1";
	addAttr -ci true -sn "offset" -ln "offset" -at "double";
	setAttr -k on ".offset" 17;
createNode nurbsCurve -n "wave05_CtrlShape" -p "wave05_Ctrl";
	rename -uid "1C793331-4603-1B38-542D-A2863263AD37";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.78361162489122504 4.7982373409884682e-17 -0.78361162489122382
		-1.2643170607829326e-16 6.7857323231109134e-17 -1.1081941875543879
		-0.78361162489122427 4.7982373409884713e-17 -0.78361162489122427
		-1.1081941875543879 1.9663354616187859e-32 -3.2112695072372299e-16
		-0.78361162489122449 -4.7982373409884694e-17 0.78361162489122405
		-3.3392053635905195e-16 -6.7857323231109146e-17 1.1081941875543881
		0.78361162489122382 -4.7982373409884719e-17 0.78361162489122438
		1.1081941875543879 -3.6446300679047921e-32 5.9521325992805852e-16
		0.78361162489122504 4.7982373409884682e-17 -0.78361162489122382
		-1.2643170607829326e-16 6.7857323231109134e-17 -1.1081941875543879
		-0.78361162489122427 4.7982373409884713e-17 -0.78361162489122427
		;
createNode transform -n "wave06_CtrlBuf" -p "waveOffsetGRP";
	rename -uid "1BE42ABB-4B93-1343-260C-EE9B30289C79";
	addAttr -ci true -sn "offset" -ln "offset" -at "double";
	setAttr ".t" -type "double3" 20 0 33.587504550141148 ;
	setAttr -k on ".offset" 26;
createNode transform -n "wave06_Ctrl" -p "wave06_CtrlBuf";
	rename -uid "3D454DF3-447A-E32B-0000-3E92C10AB784";
	addAttr -ci true -sn "offset" -ln "offset" -at "double";
	setAttr -k on ".offset" 26;
createNode nurbsCurve -n "wave06_CtrlShape" -p "wave06_Ctrl";
	rename -uid "998E1185-4837-11FC-820E-449986CD2728";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.78361162489122504 4.7982373409884682e-17 -0.78361162489122382
		-1.2643170607829326e-16 6.7857323231109134e-17 -1.1081941875543879
		-0.78361162489122427 4.7982373409884713e-17 -0.78361162489122427
		-1.1081941875543879 1.9663354616187859e-32 -3.2112695072372299e-16
		-0.78361162489122449 -4.7982373409884694e-17 0.78361162489122405
		-3.3392053635905195e-16 -6.7857323231109146e-17 1.1081941875543881
		0.78361162489122382 -4.7982373409884719e-17 0.78361162489122438
		1.1081941875543879 -3.6446300679047921e-32 5.9521325992805852e-16
		0.78361162489122504 4.7982373409884682e-17 -0.78361162489122382
		-1.2643170607829326e-16 6.7857323231109134e-17 -1.1081941875543879
		-0.78361162489122427 4.7982373409884713e-17 -0.78361162489122427
		;
createNode transform -n "wave07_CtrlBuf" -p "waveOffsetGRP";
	rename -uid "8615B3CD-46DC-B595-C323-BD9F298B8FD0";
	addAttr -ci true -sn "offset" -ln "offset" -at "double";
	setAttr ".t" -type "double3" 24 0 33.587504550141148 ;
	setAttr -k on ".offset" 32;
createNode transform -n "wave07_Ctrl" -p "wave07_CtrlBuf";
	rename -uid "129DC628-48C0-B298-44A2-579FB5F1AFDC";
	addAttr -ci true -sn "offset" -ln "offset" -at "double";
	setAttr -k on ".offset" 32;
createNode nurbsCurve -n "wave07_CtrlShape" -p "wave07_Ctrl";
	rename -uid "60170008-4214-63CF-B810-57919E82C2D9";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.78361162489122504 4.7982373409884682e-17 -0.78361162489122382
		-1.2643170607829326e-16 6.7857323231109134e-17 -1.1081941875543879
		-0.78361162489122427 4.7982373409884713e-17 -0.78361162489122427
		-1.1081941875543879 1.9663354616187859e-32 -3.2112695072372299e-16
		-0.78361162489122449 -4.7982373409884694e-17 0.78361162489122405
		-3.3392053635905195e-16 -6.7857323231109146e-17 1.1081941875543881
		0.78361162489122382 -4.7982373409884719e-17 0.78361162489122438
		1.1081941875543879 -3.6446300679047921e-32 5.9521325992805852e-16
		0.78361162489122504 4.7982373409884682e-17 -0.78361162489122382
		-1.2643170607829326e-16 6.7857323231109134e-17 -1.1081941875543879
		-0.78361162489122427 4.7982373409884713e-17 -0.78361162489122427
		;
createNode transform -n "wave08_CtrlBuf" -p "waveOffsetGRP";
	rename -uid "DE58EC8F-4128-9DB6-58AD-F79CBC6B0053";
	addAttr -ci true -sn "offset" -ln "offset" -at "double";
	setAttr ".t" -type "double3" 29 0 33.587504550141148 ;
	setAttr -k on ".offset" 36;
createNode transform -n "wave08_Ctrl" -p "wave08_CtrlBuf";
	rename -uid "ADA530B1-41E6-5887-C59B-D6A874239DA3";
	addAttr -ci true -sn "offset" -ln "offset" -at "double";
	setAttr -k on ".offset" 36;
createNode nurbsCurve -n "wave08_CtrlShape" -p "wave08_Ctrl";
	rename -uid "7A9C9633-4AC8-9EAC-BAA7-2F9400B1AEBB";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.78361162489122504 4.7982373409884682e-17 -0.78361162489122382
		-1.2643170607829326e-16 6.7857323231109134e-17 -1.1081941875543879
		-0.78361162489122427 4.7982373409884713e-17 -0.78361162489122427
		-1.1081941875543879 1.9663354616187859e-32 -3.2112695072372299e-16
		-0.78361162489122449 -4.7982373409884694e-17 0.78361162489122405
		-3.3392053635905195e-16 -6.7857323231109146e-17 1.1081941875543881
		0.78361162489122382 -4.7982373409884719e-17 0.78361162489122438
		1.1081941875543879 -3.6446300679047921e-32 5.9521325992805852e-16
		0.78361162489122504 4.7982373409884682e-17 -0.78361162489122382
		-1.2643170607829326e-16 6.7857323231109134e-17 -1.1081941875543879
		-0.78361162489122427 4.7982373409884713e-17 -0.78361162489122427
		;
createNode transform -n "wave09_CtrlBuf" -p "waveOffsetGRP";
	rename -uid "755D26B4-44B3-B261-13CD-BAB9832D2C22";
	addAttr -ci true -sn "offset" -ln "offset" -at "double";
	setAttr ".t" -type "double3" 34 0 33.587504550141148 ;
	setAttr -k on ".offset" 44;
createNode transform -n "wave09_Ctrl" -p "wave09_CtrlBuf";
	rename -uid "E2124AE0-4E54-228B-7ADB-92BC95AD738C";
	addAttr -ci true -sn "offset" -ln "offset" -at "double";
	setAttr ".t" -type "double3" 3.374312501603093 0 -7.1054273576010019e-15 ;
	setAttr -k on ".offset" 44;
createNode nurbsCurve -n "wave09_CtrlShape" -p "wave09_Ctrl";
	rename -uid "C8C90B8F-4DC2-AFE7-E67B-9BB43A81BDCF";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.78361162489122504 4.7982373409884682e-17 -0.78361162489122382
		-1.2643170607829326e-16 6.7857323231109134e-17 -1.1081941875543879
		-0.78361162489122427 4.7982373409884713e-17 -0.78361162489122427
		-1.1081941875543879 1.9663354616187859e-32 -3.2112695072372299e-16
		-0.78361162489122449 -4.7982373409884694e-17 0.78361162489122405
		-3.3392053635905195e-16 -6.7857323231109146e-17 1.1081941875543881
		0.78361162489122382 -4.7982373409884719e-17 0.78361162489122438
		1.1081941875543879 -3.6446300679047921e-32 5.9521325992805852e-16
		0.78361162489122504 4.7982373409884682e-17 -0.78361162489122382
		-1.2643170607829326e-16 6.7857323231109134e-17 -1.1081941875543879
		-0.78361162489122427 4.7982373409884713e-17 -0.78361162489122427
		;
createNode transform -n "wave10_CtrlBuf" -p "waveOffsetGRP";
	rename -uid "5F1F9444-460C-BA79-D3E9-148EC5B71820";
	addAttr -ci true -sn "offset" -ln "offset" -at "double";
	setAttr ".t" -type "double3" 38 0 33.587504550141148 ;
	setAttr -k on ".offset" 54;
createNode transform -n "wave10_Ctrl" -p "wave10_CtrlBuf";
	rename -uid "044465A5-4F7A-A0E6-D4D4-85BEC4550ACA";
	addAttr -ci true -sn "offset" -ln "offset" -at "double";
	setAttr ".t" -type "double3" 6.7414243110722651 0 -7.1054273576010019e-15 ;
	setAttr -k on ".offset" 54;
createNode nurbsCurve -n "wave10_CtrlShape" -p "wave10_Ctrl";
	rename -uid "F6C99FBF-47FE-8D26-B47E-D59B84EFD965";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.78361162489122504 4.7982373409884682e-17 -0.78361162489122382
		-1.2643170607829326e-16 6.7857323231109134e-17 -1.1081941875543879
		-0.78361162489122427 4.7982373409884713e-17 -0.78361162489122427
		-1.1081941875543879 1.9663354616187859e-32 -3.2112695072372299e-16
		-0.78361162489122449 -4.7982373409884694e-17 0.78361162489122405
		-3.3392053635905195e-16 -6.7857323231109146e-17 1.1081941875543881
		0.78361162489122382 -4.7982373409884719e-17 0.78361162489122438
		1.1081941875543879 -3.6446300679047921e-32 5.9521325992805852e-16
		0.78361162489122504 4.7982373409884682e-17 -0.78361162489122382
		-1.2643170607829326e-16 6.7857323231109134e-17 -1.1081941875543879
		-0.78361162489122427 4.7982373409884713e-17 -0.78361162489122427
		;
createNode transform -n "CRVBaseWire";
	rename -uid "FF6F04F8-4D24-66C1-2374-2C8DF85A0458";
	setAttr ".rp" -type "double3" 40 0.54602515079087233 -3.0485750753698451 ;
	setAttr ".sp" -type "double3" 40 0.54602515079087233 -3.0485750753698451 ;
	setAttr ".it" no;
createNode nurbsCurve -n "CRVBaseWireShape" -p "CRVBaseWire";
	rename -uid "1C8E1C44-4469-EF06-5986-6DA050880C82";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 48 0 no 3
		53 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25
		 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 48 48
		51
		40 0.3164996288292059 19.46044834096973
		40 0.32032474978997755 19.220224673014723
		40 0.32733752875038624 18.739766942453528
		40 0.33594421944502451 18.019055166328997
		40 0.342638375750419 17.298323036160351
		40 0.34741994946520338 16.577575693861327
		40 0.35028890720413208 15.856818196935283
		40 0.35124522868645058 15.136055625495128
		40 0.35028890720410627 14.415293054054972
		40 0.34741994946530685 13.694535557128935
		40 0.34263837575037198 12.9737882148299
		40 0.33594421944510944 12.253056084661264
		40 0.32733752875043509 11.532344308536725
		40 0.36044179083358585 5.7095981968098526
		40 0.41167564351813413 0.50008186224666273
		40 0.42091111066801223 0.00013495965320700631
		40 0.42891532298503593 -0.49983320261309366
		40.000000000000007 0.4356882310406397 -0.99981955241976106
		40 0.44122979399405354 -1.4998210680188429
		40.000000000000007 0.44553997817192936 -1.9998347141051314
		40 0.44861875745162744 -2.4998574586661162
		40 0.45046611315014556 -2.99988626963431
		40 0.45108203408562964 -3.4999181115383307
		40 0.45046651644342472 -3.999949962356649
		40.000000000000007 0.44861956425104266 -4.4999787495618087
		40 0.44533595399886994 -6.5600000000001355
		40 0.38289828303690238 -8.3927356811467746
		40 0.34142499665278792 -8.7882806367278246
		40 0.31802178549837284 -9.1853132857172071
		40.000000000000007 0.31273915883913189 -9.5829977313087156
		40 0.32558759456858205 -9.9805102741430076
		40.000000000000007 0.35654052758745414 -10.377023957280352
		40 0.40553356792533624 -10.771714807156634
		40 0.47246497278270772 -11.163762624463526
		40 0.55719534224146128 -11.552351853471782
		40 0.65954963380692266 -11.936677540427514
		40.000000000000007 0.77931114274261282 -12.315930788728121
		40 0.62982649808504432 -14.714113894211096
		40 0.32733752875038624 -17.629494459276415
		40 0.33594421944502451 -18.35020623540095
		40 0.342638375750419 -19.070938365569596
		40 0.34741994946520338 -19.791685707868616
		40 0.35028890720413208 -20.512443204794664
		40 0.35124522868645058 -21.233205776234819
		40 0.35028890720410627 -21.953968347674973
		40 0.34741994946530685 -22.67472584460101
		40 0.34263837575037198 -23.395473186900045
		40 0.33594421944510944 -24.116205317068683
		40 0.32733752875043509 -24.836917093193222
		40 0.32032474979003439 -25.31737482375441
		40 0.31649962882926275 -25.55759849170942
		;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "A5F6F6C5-4407-354D-56E0-159F77D19123";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "BF0AD145-445E-FFBB-C52B-6EA6FCF7B865";
	setAttr ".cdl" 5;
	setAttr -s 6 ".dli[1:5]"  1 2 3 4 5;
	setAttr -s 4 ".dli";
createNode displayLayer -n "defaultLayer";
	rename -uid "8B0C8D9A-4830-B165-2D53-4A9CD3A86DC9";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "2F431B08-4111-495D-E2E5-01B2A4FC352D";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "3293624F-4A04-037D-11EB-DB9048791967";
	setAttr ".g" yes;
createNode makeThreePointCircularArc -n "threePointCircularArc1_waveGRP_01";
	rename -uid "E95DD887-4E52-4DCF-1D5F-A5AFDC531D6F";
	setAttr ".s" 12;
createNode makeThreePointCircularArc -n "threePointCircularArc2_waveGRP_01";
	rename -uid "0DAF702B-4BAA-B754-D603-4AAB5D9B4084";
	setAttr ".s" 12;
createNode attachCurve -n "attachCurve1";
	rename -uid "2A0B3EFC-46DA-39AB-F9F2-9EA680355A5F";
	setAttr ".rv1" yes;
	setAttr ".rv2" yes;
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode makeThreePointCircularArc -n "makeThreePointCircularArc3";
	rename -uid "CACA9ED6-429C-021F-2C7E-4DB2A870382B";
	setAttr ".s" 12;
	setAttr ".pt1" -type "double3" 0 0.31649962882933713 3.4738164764866752 ;
	setAttr ".pt2" -type "double3" 0 0.35082020593628394 8.0384532317106761 ;
	setAttr ".pt3" -type "double3" 0 0.31649962882933713 12.122601907437414 ;
createNode attachCurve -n "attachCurve2";
	rename -uid "EE2A2600-4560-8599-9859-3F8F157E59B1";
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode attachCurve -n "attachCurve3";
	rename -uid "E18BC07B-41DA-E73A-9AC2-2AB8FFDFB679";
	setAttr ".rv1" yes;
	setAttr ".rv2" yes;
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "AF051173-44B8-DDE7-4CB0-0A8F01490311";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n"
		+ "            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1381\n            -height 214\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n"
		+ "            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n"
		+ "                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n"
		+ "            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n"
		+ "                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n"
		+ "                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n"
		+ "\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n"
		+ "                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n"
		+ "                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n{ string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n"
		+ "                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n"
		+ "                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n"
		+ "                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName; };\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n"
		+ "\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xpm\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1381\\n    -height 214\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1381\\n    -height 214\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels yes -displayOrthographicLabels yes -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "79EF3963-43C6-67A8-59D5-41B912B29BB2";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 300 -ast 1 -aet 300 ";
	setAttr ".st" 6;
createNode displayLayer -n "BaseArcs";
	rename -uid "EC1C59F5-48DD-25EC-4756-4C9553450A95";
	setAttr ".dt" 2;
	setAttr ".do" 1;
createNode displayLayer -n "loftCurves";
	rename -uid "280AA957-40A1-F6C2-4E3D-509534A55E0F";
	setAttr ".dt" 2;
	setAttr ".do" 1;
createNode makeThreePointCircularArc -n "makeThreePointCircularArc4";
	rename -uid "CD53191B-409B-D380-60B6-0CA499B79F03";
	setAttr ".s" 12;
	setAttr ".pt1" -type "double3" 0 0.31649962882933713 3.4738164764866752 ;
	setAttr ".pt2" -type "double3" 0 0.35082020593628394 8.0384532317106761 ;
	setAttr ".pt3" -type "double3" 0 0.31649962882933713 12.122601907437414 ;
createNode attachCurve -n "attachCurve4";
	rename -uid "15E416DC-426B-B40D-6011-9C9BEF958293";
	setAttr ".rv1" yes;
	setAttr ".rv2" yes;
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode attachCurve -n "attachCurve5";
	rename -uid "C4D03011-4176-CFEC-9404-EC8E975F982A";
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode attachCurve -n "attachCurve6";
	rename -uid "8705EC71-49B4-4A54-AE29-F7BE6E2CF1D6";
	setAttr ".rv1" yes;
	setAttr ".rv2" yes;
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode makeThreePointCircularArc -n "threePointCircularArc1_waveGRP_02";
	rename -uid "5E6E04D6-4009-CBFC-0D08-B59B7C4F7CA6";
	setAttr ".s" 12;
createNode makeThreePointCircularArc -n "threePointCircularArc2_waveGRP_02";
	rename -uid "EA33542B-4321-95A8-5EA2-838005ACF5F1";
	setAttr ".s" 12;
createNode makeThreePointCircularArc -n "makeThreePointCircularArc7";
	rename -uid "F31590DE-43A3-E3F3-E04A-1C9807BA23F9";
	setAttr ".s" 12;
	setAttr ".pt1" -type "double3" 0 0.31649962882933713 3.4738164764866752 ;
	setAttr ".pt2" -type "double3" 0 0.35082020593628394 8.0384532317106761 ;
	setAttr ".pt3" -type "double3" 0 0.31649962882933713 12.122601907437414 ;
createNode attachCurve -n "attachCurve7";
	rename -uid "4276D90F-4D50-9A3B-E607-6881D51721C4";
	setAttr ".rv1" yes;
	setAttr ".rv2" yes;
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode attachCurve -n "attachCurve8";
	rename -uid "ABE866D2-4282-ABFB-31F1-6987B8BE4FEF";
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode attachCurve -n "attachCurve9";
	rename -uid "4F9342D1-4BFF-AB78-DE22-3ABD4880A5D0";
	setAttr ".rv1" yes;
	setAttr ".rv2" yes;
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode makeThreePointCircularArc -n "threePointCircularArc1_waveGRP_03";
	rename -uid "56588BA2-4505-82C4-9E54-5885321F8827";
	setAttr ".s" 12;
createNode makeThreePointCircularArc -n "threePointCircularArc2_waveGRP_03";
	rename -uid "8A317ED2-42DD-F05D-6794-3388B97744D9";
	setAttr ".s" 12;
createNode makeThreePointCircularArc -n "makeThreePointCircularArc10";
	rename -uid "9B0BBD1A-4588-6927-6C38-FCA90640B5AB";
	setAttr ".s" 12;
	setAttr ".pt1" -type "double3" 0 0.31649962882933713 3.4738164764866752 ;
	setAttr ".pt2" -type "double3" 0 0.35082020593628394 8.0384532317106761 ;
	setAttr ".pt3" -type "double3" 0 0.31649962882933713 12.122601907437414 ;
createNode attachCurve -n "attachCurve10";
	rename -uid "BB379BB9-4E30-0AFC-649C-EEA01F759E39";
	setAttr ".rv1" yes;
	setAttr ".rv2" yes;
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode attachCurve -n "attachCurve11";
	rename -uid "C89C1298-476A-A3D6-50E4-1DA6C48466E5";
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode attachCurve -n "attachCurve12";
	rename -uid "24FDA547-44E0-59D6-DD31-48B45AF9A65D";
	setAttr ".rv1" yes;
	setAttr ".rv2" yes;
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode makeThreePointCircularArc -n "threePointCircularArc1_waveGRP_04";
	rename -uid "BCD94D67-4EEE-743D-E407-7EBF61DA27F2";
	setAttr ".s" 12;
createNode makeThreePointCircularArc -n "threePointCircularArc2_waveGRP_04";
	rename -uid "B036920C-4FA8-6190-044A-57A451E9A94F";
	setAttr ".s" 12;
createNode makeThreePointCircularArc -n "makeThreePointCircularArc13";
	rename -uid "16DA3831-4CA9-0E71-C083-2EB613FCD48C";
	setAttr ".s" 12;
	setAttr ".pt1" -type "double3" 0 0.31649962882933713 3.4738164764866752 ;
	setAttr ".pt2" -type "double3" 0 0.35082020593628394 8.0384532317106761 ;
	setAttr ".pt3" -type "double3" 0 0.31649962882933713 12.122601907437414 ;
createNode attachCurve -n "attachCurve13";
	rename -uid "F3EF38DC-4AB3-B8AC-39C1-41A1F070D22E";
	setAttr ".rv1" yes;
	setAttr ".rv2" yes;
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode attachCurve -n "attachCurve14";
	rename -uid "E960F162-4760-DD40-1DD3-D69927594B49";
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode attachCurve -n "attachCurve15";
	rename -uid "A5BC77DB-490B-38CB-B4E0-228DBC59F1AB";
	setAttr ".rv1" yes;
	setAttr ".rv2" yes;
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode makeThreePointCircularArc -n "threePointCircularArc1_waveGRP_05";
	rename -uid "59CBA0D3-4811-2830-320D-7BB61F491CC2";
	setAttr ".s" 12;
createNode makeThreePointCircularArc -n "threePointCircularArc2_waveGRP_05";
	rename -uid "0F91D4E8-4021-C0A1-5A65-B9BA52639CA1";
	setAttr ".s" 12;
createNode makeThreePointCircularArc -n "makeThreePointCircularArc16";
	rename -uid "E2EDFE99-43D0-EE2A-373A-56A252AEABEA";
	setAttr ".s" 12;
	setAttr ".pt1" -type "double3" 0 0.31649962882933713 3.4738164764866752 ;
	setAttr ".pt2" -type "double3" 0 0.35082020593628394 8.0384532317106761 ;
	setAttr ".pt3" -type "double3" 0 0.31649962882933713 12.122601907437414 ;
createNode attachCurve -n "attachCurve16";
	rename -uid "56CBCFA2-4841-1F5B-A895-5E921AE5088F";
	setAttr ".rv1" yes;
	setAttr ".rv2" yes;
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode attachCurve -n "attachCurve17";
	rename -uid "4D88563C-49C1-144C-B282-AC875BE043B8";
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode attachCurve -n "attachCurve18";
	rename -uid "D088C5DA-410C-3FB8-8B9D-53A4014D0B1C";
	setAttr ".rv1" yes;
	setAttr ".rv2" yes;
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode makeThreePointCircularArc -n "threePointCircularArc1_waveGRP_06";
	rename -uid "48FAEAD2-4881-6FD3-4648-F6A39756EF73";
	setAttr ".s" 12;
createNode makeThreePointCircularArc -n "threePointCircularArc2_waveGRP_06";
	rename -uid "A3447B92-47E5-DFED-0EA2-06ACF146B9C6";
	setAttr ".s" 12;
createNode makeThreePointCircularArc -n "makeThreePointCircularArc19";
	rename -uid "CA761041-4C5D-7AEC-4640-38B61A3A9A74";
	setAttr ".s" 12;
	setAttr ".pt1" -type "double3" 0 0.31649962882933713 3.4738164764866752 ;
	setAttr ".pt2" -type "double3" 0 0.35082020593628394 8.0384532317106761 ;
	setAttr ".pt3" -type "double3" 0 0.31649962882933713 12.122601907437414 ;
createNode attachCurve -n "attachCurve19";
	rename -uid "AEB02EBE-4C0B-7461-F939-048E1F51F313";
	setAttr ".rv1" yes;
	setAttr ".rv2" yes;
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode attachCurve -n "attachCurve20";
	rename -uid "CDA96CC5-4571-DB2B-BFBA-F8904F38B890";
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode attachCurve -n "attachCurve21";
	rename -uid "1A8EDF9F-4065-C91B-7861-B4B817D3063F";
	setAttr ".rv1" yes;
	setAttr ".rv2" yes;
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode makeThreePointCircularArc -n "threePointCircularArc1_waveGRP_07";
	rename -uid "7693AEA0-430C-3AFD-8B98-44886D760D16";
	setAttr ".s" 12;
createNode makeThreePointCircularArc -n "threePointCircularArc2_waveGRP_07";
	rename -uid "96BB01D2-4B69-C90E-9C96-29B96FF51E59";
	setAttr ".s" 12;
createNode makeThreePointCircularArc -n "makeThreePointCircularArc22";
	rename -uid "E86D659F-41F1-7C07-2DE3-37AF9787A139";
	setAttr ".s" 12;
	setAttr ".pt1" -type "double3" 0 0.31649962882933713 3.4738164764866752 ;
	setAttr ".pt2" -type "double3" 0 0.35082020593628394 8.0384532317106761 ;
	setAttr ".pt3" -type "double3" 0 0.31649962882933713 12.122601907437414 ;
createNode attachCurve -n "attachCurve22";
	rename -uid "2C462CB9-4A10-202B-FB6F-A1BC5C7B9376";
	setAttr ".rv1" yes;
	setAttr ".rv2" yes;
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode attachCurve -n "attachCurve23";
	rename -uid "3490A660-4705-B70E-E786-A0BF1FD5C707";
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode attachCurve -n "attachCurve24";
	rename -uid "5E0F4AB5-42BD-1350-9879-56986BEA611D";
	setAttr ".rv1" yes;
	setAttr ".rv2" yes;
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode makeThreePointCircularArc -n "threePointCircularArc1_waveGRP_08";
	rename -uid "C63B7A8D-4E76-6089-9AF6-09805077550A";
	setAttr ".s" 12;
createNode makeThreePointCircularArc -n "threePointCircularArc2_waveGRP_08";
	rename -uid "D2E2711F-4C01-9220-7D81-4E8737B9F5A3";
	setAttr ".s" 12;
createNode makeThreePointCircularArc -n "makeThreePointCircularArc25";
	rename -uid "A261CA5D-4CAD-31A5-A359-15A2621DE545";
	setAttr ".s" 12;
	setAttr ".pt1" -type "double3" 0 0.31649962882933713 3.4738164764866752 ;
	setAttr ".pt2" -type "double3" 0 0.35082020593628394 8.0384532317106761 ;
	setAttr ".pt3" -type "double3" 0 0.31649962882933713 12.122601907437414 ;
createNode attachCurve -n "attachCurve25";
	rename -uid "34DA5339-41E2-713F-093E-F8BCF5B7A825";
	setAttr ".rv1" yes;
	setAttr ".rv2" yes;
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode attachCurve -n "attachCurve26";
	rename -uid "42BFA48D-4C5E-12CB-BAE3-52920279A0FD";
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode attachCurve -n "attachCurve27";
	rename -uid "DF15DEB3-4323-5CB1-111C-3D99B175D503";
	setAttr ".rv1" yes;
	setAttr ".rv2" yes;
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode makeThreePointCircularArc -n "threePointCircularArc1_waveGRP_09";
	rename -uid "326A75ED-47F3-9FF9-F5AF-D7875FA03EF5";
	setAttr ".s" 12;
createNode makeThreePointCircularArc -n "threePointCircularArc2_waveGRP_09";
	rename -uid "BA295394-4B7C-FE15-E008-EB9FA4E9DA79";
	setAttr ".s" 12;
createNode makeThreePointCircularArc -n "makeThreePointCircularArc28";
	rename -uid "73A5E85D-4B05-0EE2-F529-7AB2F328789A";
	setAttr ".s" 12;
	setAttr ".pt1" -type "double3" 0 0.31649962882933713 3.4738164764866752 ;
	setAttr ".pt2" -type "double3" 0 0.35082020593628394 8.0384532317106761 ;
	setAttr ".pt3" -type "double3" 0 0.31649962882933713 12.122601907437414 ;
createNode attachCurve -n "attachCurve28";
	rename -uid "E3E790FA-4186-3E47-49C0-65BF7CADB1E5";
	setAttr ".rv1" yes;
	setAttr ".rv2" yes;
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode attachCurve -n "attachCurve29";
	rename -uid "572B135E-4B84-CD26-5718-839705310EE6";
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode attachCurve -n "attachCurve30";
	rename -uid "36B985D9-4E66-6AD5-183F-4DA11A14D384";
	setAttr ".rv1" yes;
	setAttr ".rv2" yes;
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode lambert -n "lambert2";
	rename -uid "EA689B0E-4C61-A851-C13C-E08742E83FB3";
	setAttr ".ic" -type "float3" 0.39669999 0.39669999 0.39669999 ;
createNode shadingEngine -n "lambert2SG";
	rename -uid "3A4456F2-4D81-84D5-BB9A-3B8FFA89A841";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "FC6CB694-4751-53BD-6201-AE9FD41817A0";
	setAttr ".tmip" 5;
createNode ramp -n "ramp1";
	rename -uid "73F8CA91-42F6-25C9-B4F3-658F2AC27B30";
	addAttr -ci true -sn "resolution" -ln "resolution" -dv 32 -at "long";
	setAttr ".t" 1;
	setAttr ".in" 0;
	setAttr -s 4 ".cel";
	setAttr ".cel[0].ep" 0;
	setAttr ".cel[0].ec" -type "float3" 0.296 0.91880536 1 ;
	setAttr ".cel[1].ep" 0.31000000238418579;
	setAttr ".cel[1].ec" -type "float3" 0 0.90344661 0.91736001 ;
	setAttr ".cel[2].ep" 0.76499998569488525;
	setAttr ".cel[2].ec" -type "float3" 0.36199999 0.93162775 1 ;
	setAttr ".cel[5].ep" 0.18999999761581421;
	setAttr ".cel[5].ec" -type "float3" 0.11295498 0.77899998 0.77433771 ;
	setAttr ".uw" 0.30577999353408813;
	setAttr ".n" 0.45456001162528992;
	setAttr ".nf" 0.39669999480247498;
	setAttr ".resolution" 256;
createNode place2dTexture -n "place2dTexture1";
	rename -uid "E4CB53D0-4D3A-1C2A-7974-2D9356B6867A";
	setAttr ".re" -type "float2" 10 2 ;
createNode loft -n "loft1";
	rename -uid "16DB7083-4A29-195B-90BD-A5AC01A870C9";
	setAttr -s 10 ".ic";
	setAttr ".u" yes;
	setAttr ".ar" no;
	setAttr ".rsn" yes;
createNode nurbsTessellate -n "nurbsTessellate1";
	rename -uid "BFE1AEC0-4E8E-F521-FE60-BE88C899285E";
	setAttr ".f" 2;
	setAttr ".pt" 1;
	setAttr ".chr" 0.1;
	setAttr ".ut" 1;
	setAttr ".un" 100;
	setAttr ".vt" 1;
	setAttr ".vn" 31;
	setAttr ".ucr" no;
	setAttr ".cht" 0.2;
createNode nonLinear -n "wave1";
	rename -uid "22D8D2F0-4722-6044-4429-FCBAA6A2305A";
	addAttr -is true -ci true -k true -sn "amp" -ln "amplitude" -smn -5 -smx 5 -at "double";
	addAttr -is true -ci true -k true -sn "wav" -ln "wavelength" -dv 1 -min 0.1 -smn 
		0.1 -smx 10 -at "double";
	addAttr -is true -ci true -k true -sn "off" -ln "offset" -smn -10 -smx 10 -at "double";
	addAttr -is true -ci true -k true -sn "dr" -ln "dropoff" -min -1 -max 1 -at "double";
	addAttr -is true -ci true -k true -sn "dp" -ln "dropoffPosition" -min 0 -max 1 -at "double";
	addAttr -is true -ci true -k true -sn "mnr" -ln "minRadius" -min 0 -smn 0 -smx 10 
		-at "double";
	addAttr -is true -ci true -k true -sn "mxr" -ln "maxRadius" -dv 1 -min 0 -smn 0 
		-smx 10 -at "double";
	setAttr -k on ".amp" 0.009999999999999995;
	setAttr -k on ".wav" 0.55999999999999994;
	setAttr -k on ".off";
	setAttr -k on ".dr";
	setAttr -k on ".dp";
	setAttr -k on ".mnr";
	setAttr -k on ".mxr" 2.4000000000000004;
createNode tweak -n "tweak1";
	rename -uid "524817D8-45E6-8E37-79FF-8BA3B50FE106";
createNode objectSet -n "wave1Set";
	rename -uid "75479D67-4EBF-079A-31A5-69B85EED5590";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "wave1GroupId";
	rename -uid "2DA5E892-4237-38E3-17FF-CABDE575C479";
	setAttr ".ihi" 0;
createNode groupParts -n "wave1GroupParts";
	rename -uid "9A90895A-4232-B6B1-475C-54A5F56B2420";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet1";
	rename -uid "FBD73A78-4B23-2E57-E9F7-7687AAE71FDC";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId2";
	rename -uid "44DBD81C-4071-C7DC-BB15-30B2E978E98E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "A7543809-4985-14DC-A74D-C5B8F06B5FF6";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode animCurveTU -n "wave1_offset";
	rename -uid "A99747C1-4736-D4A1-B987-158593673251";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  1 0.4 300 -3.1000000000000005;
	setAttr -s 2 ".kwl[0:1]" yes yes;
createNode nonLinear -n "wave2";
	rename -uid "980CB05A-4534-8A8D-0B50-D2A4C08759B1";
	addAttr -is true -ci true -k true -sn "amp" -ln "amplitude" -smn -5 -smx 5 -at "double";
	addAttr -is true -ci true -k true -sn "wav" -ln "wavelength" -dv 1 -min 0.1 -smn 
		0.1 -smx 10 -at "double";
	addAttr -is true -ci true -k true -sn "off" -ln "offset" -smn -10 -smx 10 -at "double";
	addAttr -is true -ci true -k true -sn "dr" -ln "dropoff" -min -1 -max 1 -at "double";
	addAttr -is true -ci true -k true -sn "dp" -ln "dropoffPosition" -min 0 -max 1 -at "double";
	addAttr -is true -ci true -k true -sn "mnr" -ln "minRadius" -min 0 -smn 0 -smx 10 
		-at "double";
	addAttr -is true -ci true -k true -sn "mxr" -ln "maxRadius" -dv 1 -min 0 -smn 0 
		-smx 10 -at "double";
	setAttr -k on ".amp" 0.010000000000000009;
	setAttr -k on ".wav" 0.43999999999999995;
	setAttr -k on ".off";
	setAttr -k on ".dr";
	setAttr -k on ".dp";
	setAttr -k on ".mnr";
	setAttr -k on ".mxr" 2.22;
createNode objectSet -n "wave2Set";
	rename -uid "55D8B131-4A50-4205-71A1-188A66F89705";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "wave2GroupId";
	rename -uid "4261887F-48F6-D8EE-B0D5-CB800FE399E5";
	setAttr ".ihi" 0;
createNode groupParts -n "wave2GroupParts";
	rename -uid "4941CD66-48C6-A834-FA49-23ACD19822BF";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode animCurveTU -n "wave2_offset";
	rename -uid "267E25A0-45CC-C5C0-E59F-48A8599C9572";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  1 0 300 1.2300000000000002;
	setAttr -s 2 ".kwl[0:1]" yes yes;
createNode animCurveTU -n "wave1_envelope";
	rename -uid "3DDB5D3C-4EC7-DB51-330D-B285FFC2F160";
	setAttr ".tan" 2;
	setAttr -s 4 ".ktv[0:3]"  1 0.23000000417232513 147 0.24000000953674316
		 198 0.90000003576278687 300 0.11000003665685654;
	setAttr -s 4 ".kwl[0:3]" yes yes yes yes;
createNode animCurveTU -n "wave2_envelope";
	rename -uid "ACC64768-4CEB-2833-95BA-119B02B90A78";
	setAttr ".tan" 2;
	setAttr -s 4 ".ktv[0:3]"  1 0.23000000417232513 147 0.24000000953674316
		 198 0.90000003576278687 300 0.11000003665685654;
	setAttr -s 4 ".kwl[0:3]" yes yes yes yes;
createNode animCurveTL -n "locator1_translateX";
	rename -uid "DCFA5C21-49F6-16A9-205C-F3A86019079F";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 22.63607432754317;
createNode animCurveTL -n "locator1_translateY";
	rename -uid "257F93AE-4B7B-4600-429A-FD831FC1290D";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 8.2338255549919506;
createNode animCurveTL -n "locator1_translateZ";
	rename -uid "70A4F118-4046-E93F-C03B-1ABDA29CF66C";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  1 -2.9172957267112203 300 -0.70876878063514814;
	setAttr -s 2 ".kwl[0:1]" yes yes;
createNode polyMirror -n "polyMirror1";
	rename -uid "AF641EC1-4596-4B44-A372-8CA79AF83059";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".p" -type "double3" 0 2.6503432989120483 3.6829147338867188 ;
	setAttr ".d" 1;
	setAttr ".ma" 0;
	setAttr ".mm" 3;
	setAttr ".mtt" 1;
	setAttr ".fnf" 2970;
	setAttr ".lnf" 5939;
	setAttr ".pc" -type "double3" 0 2.6503432989120483 3.6829147338867188 ;
	setAttr ".kv" no;
createNode polyTweakUV -n "polyTweakUV1";
	rename -uid "1BA2D4E2-46FE-3638-C276-799E38C679F9";
	setAttr ".uopa" yes;
	setAttr -s 2881 ".uvtk";
	setAttr ".uvtk[6]" -type "float2" 0.088339686 -0.032478333 ;
	setAttr ".uvtk[9]" -type "float2" 0.0080622286 -0.026978731 ;
	setAttr ".uvtk[10]" -type "float2" 0.0038489252 -0.00081767142 ;
	setAttr ".uvtk[13]" -type "float2" 0.0012438074 0.0014844984 ;
	setAttr ".uvtk[14]" -type "float2" 0.00053761154 0.00077791512 ;
	setAttr ".uvtk[17]" -type "float2" 0.00036499649 0.00071017444 ;
	setAttr ".uvtk[18]" -type "float2" 0.00011718273 -5.6624413e-06 ;
	setAttr ".uvtk[21]" -type "float2" 5.3885393e-05 -3.7655234e-05 ;
	setAttr ".uvtk[23]" -type "float2" 9.1053545e-05 -2.5387853e-05 ;
	setAttr ".uvtk[24]" -type "float2" 0.00017608609 0.00029044598 ;
	setAttr ".uvtk[26]" -type "float2" 0.00010691583 8.5264444e-05 ;
	setAttr ".uvtk[27]" -type "float2" 0.00022619218 0.0002643913 ;
	setAttr ".uvtk[28]" -type "float2" 0.000190394 0.00018888712 ;
	setAttr ".uvtk[29]" -type "float2" 0.00028375722 0.00054132938 ;
	setAttr ".uvtk[30]" -type "float2" 0.00018405914 -2.1569431e-05 ;
	setAttr ".uvtk[32]" -type "float2" 0.00011880696 9.033829e-06 ;
	setAttr ".uvtk[34]" -type "float2" 0.00014467537 -1.0631979e-05 ;
	setAttr ".uvtk[35]" -type "float2" 0.00039538741 0.00079610199 ;
	setAttr ".uvtk[36]" -type "float2" 0.00025738403 0.00029646605 ;
	setAttr ".uvtk[37]" -type "float2" 0.0003650263 0.0002553761 ;
	setAttr ".uvtk[38]" -type "float2" 0.00028257817 0.00029115379 ;
	setAttr ".uvtk[39]" -type "float2" 0.00043280423 0.000819318 ;
	setAttr ".uvtk[40]" -type "float2" 0.00087277591 0.0019854456 ;
	setAttr ".uvtk[42]" -type "float2" 0.00064533576 0.0018630624 ;
	setAttr ".uvtk[43]" -type "float2" 0.00029803719 0.00090348721 ;
	setAttr ".uvtk[45]" -type "float2" 0.00023030117 0.00054235756 ;
	setAttr ".uvtk[46]" -type "float2" 0.00050295144 0.0012459904 ;
	setAttr ".uvtk[47]" -type "float2" 0.00040671229 0.00096030533 ;
	setAttr ".uvtk[48]" -type "float2" 0.00051218271 0.0014915317 ;
	setAttr ".uvtk[49]" -type "float2" 0.0003905734 0.0012129396 ;
	setAttr ".uvtk[51]" -type "float2" 0.00034913141 0.0012286156 ;
	setAttr ".uvtk[52]" -type "float2" 0.00076484308 0.0022601485 ;
	setAttr ".uvtk[53]" -type "float2" 0.00060979463 0.0019187182 ;
	setAttr ".uvtk[54]" -type "float2" 0.0006776154 0.0017932206 ;
	setAttr ".uvtk[55]" -type "float2" 0.00089792162 0.0020048618 ;
	setAttr ".uvtk[56]" -type "float2" 0.00069005787 0.0020434111 ;
	setAttr ".uvtk[57]" -type "float2" 0.00054655597 0.001393646 ;
	setAttr ".uvtk[58]" -type "float2" 0.0007288456 0.001398325 ;
	setAttr ".uvtk[59]" -type "float2" 0.00057950616 0.0014396012 ;
	setAttr ".uvtk[60]" -type "float2" 0.00074308366 0.0020682067 ;
	setAttr ".uvtk[61]" -type "float2" 0.00097760931 0.0019624531 ;
	setAttr ".uvtk[62]" -type "float2" 0.0008379519 0.0023983717 ;
	setAttr ".uvtk[63]" -type "float2" 0.001074262 0.002209425 ;
	setAttr ".uvtk[64]" -type "float2" 0.00089667737 0.0023556948 ;
	setAttr ".uvtk[65]" -type "float2" 0.0010389835 0.0017776489 ;
	setAttr ".uvtk[66]" -type "float2" 0.00085698068 0.000172019 ;
	setAttr ".uvtk[68]" -type "float2" 0.0010270476 0.0005043596 ;
	setAttr ".uvtk[69]" -type "float2" 0.00032575428 -0.00011239946 ;
	setAttr ".uvtk[71]" -type "float2" 0.00024340302 -6.1001629e-05 ;
	setAttr ".uvtk[73]" -type "float2" 0.00029668212 -9.7688287e-05 ;
	setAttr ".uvtk[74]" -type "float2" 0.00071880221 0.00070634484 ;
	setAttr ".uvtk[75]" -type "float2" 0.00047452748 0.00020427257 ;
	setAttr ".uvtk[76]" -type "float2" 0.00066557527 7.7702105e-05 ;
	setAttr ".uvtk[77]" -type "float2" 0.00058892369 0.00013326108 ;
	setAttr ".uvtk[78]" -type "float2" 0.0008725822 0.00061471015 ;
	setAttr ".uvtk[79]" -type "float2" 0.00014702976 -0.00017797202 ;
	setAttr ".uvtk[81]" -type "float2" 0.00031378865 -0.00013809651 ;
	setAttr ".uvtk[83]" -type "float2" 0.0002464205 -0.00015520677 ;
	setAttr ".uvtk[84]" -type "float2" 0.0010650009 0.0004036203 ;
	setAttr ".uvtk[85]" -type "float2" 0.00066515803 2.143532e-05 ;
	setAttr ".uvtk[86]" -type "float2" 0.00043551624 -8.8468194e-05 ;
	setAttr ".uvtk[87]" -type "float2" 0.00055496395 -3.2283366e-05 ;
	setAttr ".uvtk[88]" -type "float2" 0.00097110868 0.00028286129 ;
	setAttr ".uvtk[89]" -type "float2" 0.002709657 0.00036050379 ;
	setAttr ".uvtk[90]" -type "float2" 0.0018208623 0.001435861 ;
	setAttr ".uvtk[91]" -type "float2" 0.0011758432 0.0018580556 ;
	setAttr ".uvtk[92]" -type "float2" 0.0009368211 0.0013029426 ;
	setAttr ".uvtk[93]" -type "float2" 0.0014014542 0.0010019243 ;
	setAttr ".uvtk[94]" -type "float2" 0.0011868104 0.0011659563 ;
	setAttr ".uvtk[95]" -type "float2" 0.0014745668 0.0016621947 ;
	setAttr ".uvtk[96]" -type "float2" 0.0016558841 0.0011494458 ;
	setAttr ".uvtk[97]" -type "float2" 0.0013882518 0.0019851625 ;
	setAttr ".uvtk[98]" -type "float2" 0.0022113174 0.0014061332 ;
	setAttr ".uvtk[99]" -type "float2" 0.0018187538 0.0017061383 ;
	setAttr ".uvtk[100]" -type "float2" 0.0020986572 0.00076746941 ;
	setAttr ".uvtk[101]" -type "float2" 0.0021766871 0.00068846345 ;
	setAttr ".uvtk[102]" -type "float2" 0.0020128936 0.0011921823 ;
	setAttr ".uvtk[103]" -type "float2" 0.0015150607 0.00083808601 ;
	setAttr ".uvtk[104]" -type "float2" 0.0014683455 0.00046506524 ;
	setAttr ".uvtk[105]" -type "float2" 0.0014793575 0.0006531328 ;
	setAttr ".uvtk[106]" -type "float2" 0.002080068 0.00094507635 ;
	setAttr ".uvtk[107]" -type "float2" 0.0031511486 -3.6671758e-05 ;
	setAttr ".uvtk[108]" -type "float2" 0.0025915504 0.0010790378 ;
	setAttr ".uvtk[109]" -type "float2" 0.0029909015 0.00043845177 ;
	setAttr ".uvtk[110]" -type "float2" 0.0027540624 0.00076261163 ;
	setAttr ".uvtk[111]" -type "float2" 0.0034508109 -0.00043742359 ;
	setAttr ".uvtk[112]" -type "float2" 0.0025462806 -0.017428875 ;
	setAttr ".uvtk[114]" -type "float2" 0.0020987466 -0.0077355504 ;
	setAttr ".uvtk[115]" -type "float2" 0.0011466555 -0.0040359199 ;
	setAttr ".uvtk[117]" -type "float2" 0.0010258108 -0.00093874335 ;
	setAttr ".uvtk[118]" -type "float2" 0.00042335875 -0.00014537573 ;
	setAttr ".uvtk[120]" -type "float2" 0.00041157473 0.00064817071 ;
	setAttr ".uvtk[121]" -type "float2" 0.00095874071 0.00075316429 ;
	setAttr ".uvtk[122]" -type "float2" 0.00073384494 0.00083601475 ;
	setAttr ".uvtk[123]" -type "float2" 0.00075664185 -0.00047644973 ;
	setAttr ".uvtk[124]" -type "float2" 0.00040456094 -0.0014156103 ;
	setAttr ".uvtk[126]" -type "float2" 0.00040728878 -0.00079548359 ;
	setAttr ".uvtk[127]" -type "float2" 0.0010745712 -0.0024936795 ;
	setAttr ".uvtk[128]" -type "float2" 0.00077654421 -0.0016225874 ;
	setAttr ".uvtk[129]" -type "float2" 0.00076494738 -0.0027271211 ;
	setAttr ".uvtk[130]" -type "float2" 0.001654692 -0.0028943717 ;
	setAttr ".uvtk[131]" -type "float2" 0.001201313 -0.0015245974 ;
	setAttr ".uvtk[132]" -type "float2" 0.0010852143 0.00046944618 ;
	setAttr ".uvtk[133]" -type "float2" 0.0014627352 -0.00042125583 ;
	setAttr ".uvtk[134]" -type "float2" 0.0011956319 7.2747469e-05 ;
	setAttr ".uvtk[135]" -type "float2" 0.0013623163 -0.0021930039 ;
	setAttr ".uvtk[136]" -type "float2" 0.0014272444 -0.0052978396 ;
	setAttr ".uvtk[137]" -type "float2" 0.0013271719 -0.0034192801 ;
	setAttr ".uvtk[138]" -type "float2" 0.0018752441 -0.0053084195 ;
	setAttr ".uvtk[139]" -type "float2" 0.0015293434 -0.0043723583 ;
	setAttr ".uvtk[140]" -type "float2" 0.0016999319 -0.0065456927 ;
	setAttr ".uvtk[141]" -type "float2" 0.0012355447 -0.0094938278 ;
	setAttr ".uvtk[143]" -type "float2" 0.0012196079 -0.0066837072 ;
	setAttr ".uvtk[144]" -type "float2" 0.00038758945 -0.0023389459 ;
	setAttr ".uvtk[146]" -type "float2" 0.00037998985 -0.0019882321 ;
	setAttr ".uvtk[147]" -type "float2" 0.0011680312 -0.0054950416 ;
	setAttr ".uvtk[148]" -type "float2" 0.00078514591 -0.0037835538 ;
	setAttr ".uvtk[149]" -type "float2" 0.00075806491 -0.0045771003 ;
	setAttr ".uvtk[150]" -type "float2" 0.00038132537 -0.002995193 ;
	setAttr ".uvtk[152]" -type "float2" 0.0003718771 -0.0025311112 ;
	setAttr ".uvtk[153]" -type "float2" 0.0012140609 -0.0078159571 ;
	setAttr ".uvtk[154]" -type "float2" 0.00077883154 -0.0052194595 ;
	setAttr ".uvtk[155]" -type "float2" 0.00076255947 -0.0062792301 ;
	setAttr ".uvtk[156]" -type "float2" 0.0024179518 -0.012331873 ;
	setAttr ".uvtk[157]" -type "float2" 0.0016203262 -0.0087099075 ;
	setAttr ".uvtk[158]" -type "float2" 0.0015522242 -0.0071241558 ;
	setAttr ".uvtk[159]" -type "float2" 0.0022629052 -0.010135561 ;
	setAttr ".uvtk[160]" -type "float2" 0.0018530414 -0.008671999 ;
	setAttr ".uvtk[161]" -type "float2" 0.0019899309 -0.010591596 ;
	setAttr ".uvtk[162]" -type "float2" 0.001722984 -0.012415528 ;
	setAttr ".uvtk[163]" -type "float2" 0.0016903281 -0.010258466 ;
	setAttr ".uvtk[164]" -type "float2" 0.0025023147 -0.01455608 ;
	setAttr ".uvtk[165]" -type "float2" 0.0020884201 -0.012502253 ;
	setAttr ".uvtk[166]" -type "float2" 0.0021363646 -0.015057743 ;
	setAttr ".uvtk[167]" -type "float2" 0.0066251755 -0.01334092 ;
	setAttr ".uvtk[168]" -type "float2" 0.0041833073 -0.010928214 ;
	setAttr ".uvtk[169]" -type "float2" 0.0035503209 -0.004948765 ;
	setAttr ".uvtk[170]" -type "float2" 0.0021584183 -0.0036228299 ;
	setAttr ".uvtk[171]" -type "float2" 0.0018620864 -0.00092381239 ;
	setAttr ".uvtk[172]" -type "float2" 0.0031222701 -0.0019578934 ;
	setAttr ".uvtk[173]" -type "float2" 0.0024669915 -0.0014415383 ;
	setAttr ".uvtk[174]" -type "float2" 0.002763249 -0.0043046474 ;
	setAttr ".uvtk[175]" -type "float2" 0.002641663 -0.0088646114 ;
	setAttr ".uvtk[176]" -type "float2" 0.0024116337 -0.0062342882 ;
	setAttr ".uvtk[177]" -type "float2" 0.0038592666 -0.0079204738 ;
	setAttr ".uvtk[178]" -type "float2" 0.0030870885 -0.0071212649 ;
	setAttr ".uvtk[179]" -type "float2" 0.003297925 -0.0099409223 ;
	setAttr ".uvtk[180]" -type "float2" 0.0053975731 -0.0066444576 ;
	setAttr ".uvtk[181]" -type "float2" 0.0042315125 -0.0055500567 ;
	setAttr ".uvtk[182]" -type "float2" 0.0037492365 -0.0024527609 ;
	setAttr ".uvtk[183]" -type "float2" 0.0046433806 -0.0033767819 ;
	setAttr ".uvtk[184]" -type "float2" 0.0041346103 -0.0029264092 ;
	setAttr ".uvtk[185]" -type "float2" 0.00480555 -0.006123364 ;
	setAttr ".uvtk[186]" -type "float2" 0.0050733984 -0.011804163 ;
	setAttr ".uvtk[187]" -type "float2" 0.0047322363 -0.0086542368 ;
	setAttr ".uvtk[188]" -type "float2" 0.0060520321 -0.0099566579 ;
	setAttr ".uvtk[189]" -type "float2" 0.0053444058 -0.0093272924 ;
	setAttr ".uvtk[190]" -type "float2" 0.005823046 -0.012607843 ;
	setAttr ".uvtk[191]" -type "float2" 0.0045852065 -0.023071885 ;
	setAttr ".uvtk[192]" -type "float2" 0.0044725984 -0.016719431 ;
	setAttr ".uvtk[193]" -type "float2" 0.0029460266 -0.013930708 ;
	setAttr ".uvtk[194]" -type "float2" 0.0028386489 -0.011486292 ;
	setAttr ".uvtk[195]" -type "float2" 0.0043606013 -0.013911158 ;
	setAttr ".uvtk[196]" -type "float2" 0.0034896582 -0.012752891 ;
	setAttr ".uvtk[197]" -type "float2" 0.0036415011 -0.015383005 ;
	setAttr ".uvtk[198]" -type "float2" 0.0030510202 -0.019541383 ;
	setAttr ".uvtk[199]" -type "float2" 0.0030424744 -0.016405344 ;
	setAttr ".uvtk[200]" -type "float2" 0.0045509189 -0.019554943 ;
	setAttr ".uvtk[201]" -type "float2" 0.0036727414 -0.01806137 ;
	setAttr ".uvtk[202]" -type "float2" 0.0037094578 -0.021397531 ;
	setAttr ".uvtk[203]" -type "float2" 0.0075222254 -0.019927949 ;
	setAttr ".uvtk[204]" -type "float2" 0.0054635108 -0.017916769 ;
	setAttr ".uvtk[205]" -type "float2" 0.0053338557 -0.014954776 ;
	setAttr ".uvtk[206]" -type "float2" 0.0071892142 -0.016733795 ;
	setAttr ".uvtk[207]" -type "float2" 0.0061675757 -0.01588884 ;
	setAttr ".uvtk[208]" -type "float2" 0.0065123737 -0.018984526 ;
	setAttr ".uvtk[209]" -type "float2" 0.005627349 -0.024554849 ;
	setAttr ".uvtk[210]" -type "float2" 0.0056089461 -0.020912319 ;
	setAttr ".uvtk[211]" -type "float2" 0.0078765005 -0.023141414 ;
	setAttr ".uvtk[212]" -type "float2" 0.0066321343 -0.022096157 ;
	setAttr ".uvtk[213]" -type "float2" 0.0067503899 -0.025856733 ;
	setAttr ".uvtk[214]" -type "float2" 0.071926087 -0.0030213743 ;
	setAttr ".uvtk[216]" -type "float2" 0.024802625 -0.0024404526 ;
	setAttr ".uvtk[217]" -type "float2" 0.011222869 -0.00041352957 ;
	setAttr ".uvtk[219]" -type "float2" 0.00094628334 -0.00014390796 ;
	setAttr ".uvtk[220]" -type "float2" -0.00060999393 -0.00026671588 ;
	setAttr ".uvtk[222]" -type "float2" -1.2040138e-05 -0.00020455196 ;
	setAttr ".uvtk[224]" -type "float2" -0.00041036308 -0.00023014471 ;
	setAttr ".uvtk[225]" -type "float2" 0.00076778233 7.2561204e-05 ;
	setAttr ".uvtk[226]" -type "float2" 0.00023049116 -0.00014719367 ;
	setAttr ".uvtk[227]" -type "float2" -0.00016874075 -0.00027567148 ;
	setAttr ".uvtk[228]" -type "float2" -0.00013247132 -0.00021054596 ;
	setAttr ".uvtk[229]" -type "float2" 0.00052478909 -3.606081e-05 ;
	setAttr ".uvtk[230]" -type "float2" 0.0034498274 -0.00035785884 ;
	setAttr ".uvtk[232]" -type "float2" -4.3272972e-05 -0.00029350817 ;
	setAttr ".uvtk[234]" -type "float2" 0.0014310181 -0.00031942874 ;
	setAttr ".uvtk[235]" -type "float2" 0.0028165281 -0.00023953617 ;
	setAttr ".uvtk[236]" -type "float2" 0.0010972321 -0.00033706427 ;
	setAttr ".uvtk[237]" -type "float2" 0.0072797239 -0.00046376884 ;
	setAttr ".uvtk[238]" -type "float2" 0.0037434697 -0.00039759278 ;
	setAttr ".uvtk[239]" -type "float2" 0.0063917041 -0.00033067912 ;
	setAttr ".uvtk[240]" -type "float2" 0.0071266592 -0.0018025637 ;
	setAttr ".uvtk[241]" -type "float2" 0.003932476 2.9921532e-05 ;
	setAttr ".uvtk[242]" -type "float2" 0.0024312735 0.00043292344 ;
	setAttr ".uvtk[243]" -type "float2" 0.0014712811 0.00029741228 ;
	setAttr ".uvtk[244]" -type "float2" 0.0023051947 -1.0386109e-05 ;
	setAttr ".uvtk[245]" -type "float2" 0.0015449971 0.00013546646 ;
	setAttr ".uvtk[246]" -type "float2" 0.0026798099 0.00021889806 ;
	setAttr ".uvtk[247]" -type "float2" 0.0044146329 -0.0011809468 ;
	setAttr ".uvtk[248]" -type "float2" 0.0033963025 0.00012783706 ;
	setAttr ".uvtk[249]" -type "float2" 0.0055089742 -0.00038652122 ;
	setAttr ".uvtk[250]" -type "float2" 0.0039767921 -0.00015294552 ;
	setAttr ".uvtk[251]" -type "float2" 0.0051599741 -0.0015127808 ;
	setAttr ".uvtk[252]" -type "float2" 0.018558919 -0.00040343404 ;
	setAttr ".uvtk[253]" -type "float2" 0.0069899708 -0.00013296306 ;
	setAttr ".uvtk[254]" -type "float2" 0.0048775524 -0.00014421344 ;
	setAttr ".uvtk[255]" -type "float2" 0.014959991 -0.00036200881 ;
	setAttr ".uvtk[256]" -type "float2" 0.0092095733 -0.00025714934 ;
	setAttr ".uvtk[257]" -type "float2" 0.012041718 -0.00027827919 ;
	setAttr ".uvtk[258]" -type "float2" 0.011094868 -0.0020409971 ;
	setAttr ".uvtk[259]" -type "float2" 0.0091763288 -0.00058464706 ;
	setAttr ".uvtk[260]" -type "float2" 0.02184388 -0.0009162724 ;
	setAttr ".uvtk[261]" -type "float2" 0.014706254 -0.00075888634 ;
	setAttr ".uvtk[262]" -type "float2" 0.017269224 -0.0022544712 ;
	setAttr ".uvtk[263]" -type "float2" 0.044218689 -0.00063422322 ;
	setAttr ".uvtk[265]" -type "float2" 0.030029267 -0.00059971213 ;
	setAttr ".uvtk[266]" -type "float2" 0.011644751 -0.00042361766 ;
	setAttr ".uvtk[268]" -type "float2" 0.0059393793 -0.00039311126 ;
	setAttr ".uvtk[270]" -type "float2" 0.0089119375 -0.00041215122 ;
	setAttr ".uvtk[271]" -type "float2" 0.017027244 -0.00049269199 ;
	setAttr ".uvtk[272]" -type "float2" 0.011639789 -0.00052122772 ;
	setAttr ".uvtk[273]" -type "float2" 0.021469742 -0.00059081614 ;
	setAttr ".uvtk[274]" -type "float2" 0.016810149 -0.00056361407 ;
	setAttr ".uvtk[275]" -type "float2" 0.023673743 -0.0005556196 ;
	setAttr ".uvtk[276]" -type "float2" 0.018420905 -0.00043263659 ;
	setAttr ".uvtk[278]" -type "float2" 0.013506591 -0.00043253973 ;
	setAttr ".uvtk[280]" -type "float2" 0.016363233 -0.00043498725 ;
	setAttr ".uvtk[281]" -type "float2" 0.034912676 -0.0006249994 ;
	setAttr ".uvtk[282]" -type "float2" 0.025233835 -0.0006069541 ;
	setAttr ".uvtk[283]" -type "float2" 0.032791674 -0.00060943514 ;
	setAttr ".uvtk[284]" -type "float2" 0.029639453 -0.00061193109 ;
	setAttr ".uvtk[285]" -type "float2" 0.040807605 -0.0006339848 ;
	setAttr ".uvtk[286]" -type "float2" 0.052636206 -0.0028615594 ;
	setAttr ".uvtk[287]" -type "float2" 0.0432657 -0.00069977343 ;
	setAttr ".uvtk[288]" -type "float2" 0.026402429 -0.00051677227 ;
	setAttr ".uvtk[289]" -type "float2" 0.021994278 -0.00045631826 ;
	setAttr ".uvtk[290]" -type "float2" 0.037172467 -0.00060121715 ;
	setAttr ".uvtk[291]" -type "float2" 0.029748678 -0.00053961575 ;
	setAttr ".uvtk[292]" -type "float2" 0.035042614 -0.00061716139 ;
	setAttr ".uvtk[293]" -type "float2" 0.033805683 -0.0026038289 ;
	setAttr ".uvtk[294]" -type "float2" 0.030378446 -0.0010491759 ;
	setAttr ".uvtk[295]" -type "float2" 0.048303336 -0.0012684315 ;
	setAttr ".uvtk[296]" -type "float2" 0.039649934 -0.0011718571 ;
	setAttr ".uvtk[297]" -type "float2" 0.043656588 -0.0027426183 ;
	setAttr ".uvtk[298]" -type "float2" 0.061044514 -0.00078879297 ;
	setAttr ".uvtk[299]" -type "float2" 0.049988002 -0.00074733794 ;
	setAttr ".uvtk[300]" -type "float2" 0.043235004 -0.0006377548 ;
	setAttr ".uvtk[301]" -type "float2" 0.053627998 -0.0006596446 ;
	setAttr ".uvtk[302]" -type "float2" 0.049695611 -0.00065355003 ;
	setAttr ".uvtk[303]" -type "float2" 0.056961685 -0.00077325106 ;
	setAttr ".uvtk[304]" -type "float2" 0.060183346 -0.0029449761 ;
	setAttr ".uvtk[305]" -type "float2" 0.055695325 -0.0013343543 ;
	setAttr ".uvtk[306]" -type "float2" 0.06703797 -0.0013953596 ;
	setAttr ".uvtk[307]" -type "float2" 0.062853545 -0.0013735592 ;
	setAttr ".uvtk[308]" -type "float2" 0.067841321 -0.0029971749 ;
	setAttr ".uvtk[309]" -type "float2" 0.035840303 -0.031200588 ;
	setAttr ".uvtk[310]" -type "float2" 0.0331209 -0.016291589 ;
	setAttr ".uvtk[311]" -type "float2" 0.011865541 -0.01510945 ;
	setAttr ".uvtk[312]" -type "float2" 0.009806335 -0.0079743564 ;
	setAttr ".uvtk[313]" -type "float2" 0.0062881112 -0.0071282387 ;
	setAttr ".uvtk[314]" -type "float2" 0.0054133981 -0.0037875175 ;
	setAttr ".uvtk[315]" -type "float2" 0.0084907711 -0.0045188665 ;
	setAttr ".uvtk[316]" -type "float2" 0.006373927 -0.0041783452 ;
	setAttr ".uvtk[317]" -type "float2" 0.0074569732 -0.0075685382 ;
	setAttr ".uvtk[318]" -type "float2" 0.0077821016 -0.01399821 ;
	setAttr ".uvtk[319]" -type "float2" 0.0070901215 -0.010515273 ;
	setAttr ".uvtk[320]" -type "float2" 0.010879397 -0.011487722 ;
	setAttr ".uvtk[321]" -type "float2" 0.0083740652 -0.011026293 ;
	setAttr ".uvtk[322]" -type "float2" 0.0091996789 -0.014577925 ;
	setAttr ".uvtk[323]" -type "float2" 0.029708326 -0.0088739991 ;
	setAttr ".uvtk[324]" -type "float2" 0.014486402 -0.0083191693 ;
	setAttr ".uvtk[325]" -type "float2" 0.012994602 -0.004812181 ;
	setAttr ".uvtk[326]" -type "float2" 0.027545631 -0.0052858293 ;
	setAttr ".uvtk[327]" -type "float2" 0.019411847 -0.0050621927 ;
	setAttr ".uvtk[328]" -type "float2" 0.021405384 -0.0086148679 ;
	setAttr ".uvtk[329]" -type "float2" 0.016862243 -0.015565395 ;
	setAttr ".uvtk[330]" -type "float2" 0.015825748 -0.011894286 ;
	setAttr ".uvtk[331]" -type "float2" 0.031623453 -0.012535423 ;
	setAttr ".uvtk[332]" -type "float2" 0.022933647 -0.0122343 ;
	setAttr ".uvtk[333]" -type "float2" 0.024286404 -0.015961796 ;
	setAttr ".uvtk[334]" -type "float2" 0.014272839 -0.02955538 ;
	setAttr ".uvtk[335]" -type "float2" 0.013378814 -0.022154003 ;
	setAttr ".uvtk[336]" -type "float2" 0.0089834481 -0.020774215 ;
	setAttr ".uvtk[337]" -type "float2" 0.008361131 -0.01748094 ;
	setAttr ".uvtk[338]" -type "float2" 0.012753904 -0.018731803 ;
	setAttr ".uvtk[339]" -type "float2" 0.0099585205 -0.018149346 ;
	setAttr ".uvtk[340]" -type "float2" 0.010598391 -0.021508038 ;
	setAttr ".uvtk[341]" -type "float2" 0.0095833838 -0.027962744 ;
	setAttr ".uvtk[342]" -type "float2" 0.0093729645 -0.024058282 ;
	setAttr ".uvtk[343]" -type "float2" 0.013871923 -0.025548786 ;
	setAttr ".uvtk[344]" -type "float2" 0.011110082 -0.024860173 ;
	setAttr ".uvtk[345]" -type "float2" 0.0113139 -0.028814197 ;
	setAttr ".uvtk[346]" -type "float2" 0.035057485 -0.023590326 ;
	setAttr ".uvtk[347]" -type "float2" 0.018589199 -0.022712916 ;
	setAttr ".uvtk[348]" -type "float2" 0.017794818 -0.019251466 ;
	setAttr ".uvtk[349]" -type "float2" 0.034325123 -0.020053446 ;
	setAttr ".uvtk[350]" -type "float2" 0.025225893 -0.019685417 ;
	setAttr ".uvtk[351]" -type "float2" 0.025924876 -0.023196638 ;
	setAttr ".uvtk[352]" -type "float2" 0.019352302 -0.030188859 ;
	setAttr ".uvtk[353]" -type "float2" 0.019072443 -0.026156008 ;
	setAttr ".uvtk[354]" -type "float2" 0.035511136 -0.027104586 ;
	setAttr ".uvtk[355]" -type "float2" 0.026506737 -0.026669532 ;
	setAttr ".uvtk[356]" -type "float2" 0.026752725 -0.030743003 ;
	setAttr ".uvtk[357]" -type "float2" 0.084200621 -0.017227471 ;
	setAttr ".uvtk[358]" -type "float2" 0.064134628 -0.016970575 ;
	setAttr ".uvtk[359]" -type "float2" 0.059419543 -0.0094360411 ;
	setAttr ".uvtk[360]" -type "float2" 0.03954713 -0.0091017485 ;
	setAttr ".uvtk[361]" -type "float2" 0.036793247 -0.0054771602 ;
	setAttr ".uvtk[362]" -type "float2" 0.05637002 -0.0057760477 ;
	setAttr ".uvtk[363]" -type "float2" 0.046966553 -0.0056422949 ;
	setAttr ".uvtk[364]" -type "float2" 0.049768597 -0.0092844069 ;
	setAttr ".uvtk[365]" -type "float2" 0.043269977 -0.016571522 ;
	setAttr ".uvtk[366]" -type "float2" 0.04160662 -0.012787282 ;
	setAttr ".uvtk[367]" -type "float2" 0.061965227 -0.013154477 ;
	setAttr ".uvtk[368]" -type "float2" 0.052301049 -0.012994677 ;
	setAttr ".uvtk[369]" -type "float2" 0.054179251 -0.016795069 ;
	setAttr ".uvtk[370]" -type "float2" 0.07919839 -0.0096499026 ;
	setAttr ".uvtk[371]" -type "float2" 0.067202866 -0.009547323 ;
	setAttr ".uvtk[372]" -type "float2" 0.063930362 -0.0058779418 ;
	setAttr ".uvtk[373]" -type "float2" 0.076050401 -0.0059670508 ;
	setAttr ".uvtk[374]" -type "float2" 0.071622491 -0.0059379637 ;
	setAttr ".uvtk[375]" -type "float2" 0.074833751 -0.0096186697 ;
	setAttr ".uvtk[376]" -type "float2" 0.072128892 -0.017098337 ;
	setAttr ".uvtk[377]" -type "float2" 0.069945902 -0.013276696 ;
	setAttr ".uvtk[378]" -type "float2" 0.081915915 -0.013392657 ;
	setAttr ".uvtk[379]" -type "float2" 0.077661812 -0.013354748 ;
	setAttr ".uvtk[380]" -type "float2" 0.079946965 -0.01718536 ;
	setAttr ".uvtk[381]" -type "float2" 0.067936063 -0.032110572 ;
	setAttr ".uvtk[382]" -type "float2" 0.066918612 -0.024387389 ;
	setAttr ".uvtk[383]" -type "float2" 0.045576766 -0.023919523 ;
	setAttr ".uvtk[384]" -type "float2" 0.044578359 -0.020353407 ;
	setAttr ".uvtk[385]" -type "float2" 0.065695077 -0.020785362 ;
	setAttr ".uvtk[386]" -type "float2" 0.055609733 -0.020598024 ;
	setAttr ".uvtk[387]" -type "float2" 0.056612313 -0.024180859 ;
	setAttr ".uvtk[388]" -type "float2" 0.046309277 -0.031577885 ;
	setAttr ".uvtk[389]" -type "float2" 0.046087131 -0.027452201 ;
	setAttr ".uvtk[390]" -type "float2" 0.067614377 -0.027955592 ;
	setAttr ".uvtk[391]" -type "float2" 0.057327867 -0.027735263 ;
	setAttr ".uvtk[392]" -type "float2" 0.057589978 -0.031874895 ;
	setAttr ".uvtk[393]" -type "float2" 0.087196052 -0.024690241 ;
	setAttr ".uvtk[394]" -type "float2" 0.075140566 -0.024537295 ;
	setAttr ".uvtk[395]" -type "float2" 0.073910236 -0.020926625 ;
	setAttr ".uvtk[396]" -type "float2" 0.085950881 -0.021064848 ;
	setAttr ".uvtk[397]" -type "float2" 0.081668079 -0.021018445 ;
	setAttr ".uvtk[398]" -type "float2" 0.083018601 -0.02463907 ;
	setAttr ".uvtk[399]" -type "float2" 0.07630676 -0.032289088 ;
	setAttr ".uvtk[400]" -type "float2" 0.076001406 -0.028122663 ;
	setAttr ".uvtk[401]" -type "float2" 0.088059127 -0.028291225 ;
	setAttr ".uvtk[402]" -type "float2" 0.083795905 -0.02823326 ;
	setAttr ".uvtk[403]" -type "float2" 0.084279656 -0.032415867 ;
	setAttr ".uvtk[406]" -type "float2" 0.0046433806 -0.038254023 ;
	setAttr ".uvtk[407]" -type "float2" 0.0014627352 -0.026902556 ;
	setAttr ".uvtk[409]" -type "float2" 0.0022629052 -0.031863987 ;
	setAttr ".uvtk[410]" -type "float2" 0.0011680312 -0.020137668 ;
	setAttr ".uvtk[412]" -type "float2" 0.0012140609 -0.01672256 ;
	setAttr ".uvtk[413]" -type "float2" 0.0003718771 -0.0066409111 ;
	setAttr ".uvtk[415]" -type "float2" 0.00038132537 -0.0044124722 ;
	setAttr ".uvtk[416]" -type "float2" 0.0012355447 -0.012573361 ;
	setAttr ".uvtk[417]" -type "float2" 0.00076255947 -0.0086597204 ;
	setAttr ".uvtk[418]" -type "float2" 0.00077883154 -0.012095869 ;
	setAttr ".uvtk[419]" -type "float2" 0.00037998985 -0.0082490444 ;
	setAttr ".uvtk[421]" -type "float2" 0.00038758945 -0.0082095265 ;
	setAttr ".uvtk[422]" -type "float2" 0.0012196079 -0.019682884 ;
	setAttr ".uvtk[423]" -type "float2" 0.00075806491 -0.014544427 ;
	setAttr ".uvtk[424]" -type "float2" 0.00078514591 -0.014755309 ;
	setAttr ".uvtk[425]" -type "float2" 0.0025023147 -0.027091801 ;
	setAttr ".uvtk[426]" -type "float2" 0.0016903281 -0.020702839 ;
	setAttr ".uvtk[427]" -type "float2" 0.001722984 -0.016042352 ;
	setAttr ".uvtk[428]" -type "float2" 0.0025462806 -0.021779358 ;
	setAttr ".uvtk[429]" -type "float2" 0.0021363646 -0.019092023 ;
	setAttr ".uvtk[430]" -type "float2" 0.0020884201 -0.024130344 ;
	setAttr ".uvtk[431]" -type "float2" 0.0015522242 -0.024707198 ;
	setAttr ".uvtk[432]" -type "float2" 0.0016203262 -0.024060488 ;
	setAttr ".uvtk[433]" -type "float2" 0.0024179518 -0.030946791 ;
	setAttr ".uvtk[434]" -type "float2" 0.0019899309 -0.027769566 ;
	setAttr ".uvtk[435]" -type "float2" 0.0018530414 -0.02856499 ;
	setAttr ".uvtk[436]" -type "float2" 0.00095874071 -0.016024411 ;
	setAttr ".uvtk[438]" -type "float2" 0.0010745712 -0.017226636 ;
	setAttr ".uvtk[439]" -type "float2" 0.00040728878 -0.0062920451 ;
	setAttr ".uvtk[441]" -type "float2" 0.00040456094 -0.0072749257 ;
	setAttr ".uvtk[442]" -type "float2" 0.0011466555 -0.018816173 ;
	setAttr ".uvtk[443]" -type "float2" 0.00076494738 -0.01347363 ;
	setAttr ".uvtk[444]" -type "float2" 0.00077654421 -0.01207298 ;
	setAttr ".uvtk[445]" -type "float2" 0.00041157473 -0.0058831573 ;
	setAttr ".uvtk[447]" -type "float2" 0.00042335875 -0.0058117509 ;
	setAttr ".uvtk[448]" -type "float2" 0.0010258108 -0.016233861 ;
	setAttr ".uvtk[449]" -type "float2" 0.00075664185 -0.011304379 ;
	setAttr ".uvtk[450]" -type "float2" 0.00073384494 -0.011255562 ;
	setAttr ".uvtk[451]" -type "float2" 0.0018752441 -0.028834224 ;
	setAttr ".uvtk[452]" -type "float2" 0.0013271719 -0.021682203 ;
	setAttr ".uvtk[453]" -type "float2" 0.0014272444 -0.023380876 ;
	setAttr ".uvtk[454]" -type "float2" 0.0020987466 -0.030615151 ;
	setAttr ".uvtk[455]" -type "float2" 0.0016999319 -0.027274132 ;
	setAttr ".uvtk[456]" -type "float2" 0.0015293434 -0.025530279 ;
	setAttr ".uvtk[457]" -type "float2" 0.0010852143 -0.020201921 ;
	setAttr ".uvtk[458]" -type "float2" 0.001201313 -0.020565927 ;
	setAttr ".uvtk[459]" -type "float2" 0.001654692 -0.027535141 ;
	setAttr ".uvtk[460]" -type "float2" 0.0013623163 -0.0243029 ;
	setAttr ".uvtk[461]" -type "float2" 0.0011956319 -0.023791075 ;
	setAttr ".uvtk[462]" -type "float2" 0.0071892142 -0.044105649 ;
	setAttr ".uvtk[463]" -type "float2" 0.0043606013 -0.039309621 ;
	setAttr ".uvtk[464]" -type "float2" 0.0045509189 -0.03389883 ;
	setAttr ".uvtk[465]" -type "float2" 0.0030424744 -0.029685795 ;
	setAttr ".uvtk[466]" -type "float2" 0.0030510202 -0.024137795 ;
	setAttr ".uvtk[467]" -type "float2" 0.0045852065 -0.028034687 ;
	setAttr ".uvtk[468]" -type "float2" 0.0037094578 -0.026218355 ;
	setAttr ".uvtk[469]" -type "float2" 0.0036727414 -0.03193444 ;
	setAttr ".uvtk[470]" -type "float2" 0.0028386489 -0.034708977 ;
	setAttr ".uvtk[471]" -type "float2" 0.0029460266 -0.033701062 ;
	setAttr ".uvtk[472]" -type "float2" 0.0044725984 -0.038148105 ;
	setAttr ".uvtk[473]" -type "float2" 0.0036415011 -0.036081314 ;
	setAttr ".uvtk[474]" -type "float2" 0.0034896582 -0.037171185 ;
	setAttr ".uvtk[475]" -type "float2" 0.0078765005 -0.038361251 ;
	setAttr ".uvtk[476]" -type "float2" 0.0056089461 -0.035611033 ;
	setAttr ".uvtk[477]" -type "float2" 0.005627349 -0.029649317 ;
	setAttr ".uvtk[478]" -type "float2" 0.0080622286 -0.032246053 ;
	setAttr ".uvtk[479]" -type "float2" 0.0067503899 -0.03104192 ;
	setAttr ".uvtk[480]" -type "float2" 0.0066321343 -0.037098348 ;
	setAttr ".uvtk[481]" -type "float2" 0.0053338557 -0.041153669 ;
	setAttr ".uvtk[482]" -type "float2" 0.0054635108 -0.039952099 ;
	setAttr ".uvtk[483]" -type "float2" 0.0075222254 -0.042828918 ;
	setAttr ".uvtk[484]" -type "float2" 0.0065123737 -0.041497707 ;
	setAttr ".uvtk[485]" -type "float2" 0.0061675757 -0.042745948 ;
	setAttr ".uvtk[486]" -type "float2" 0.0031222701 -0.033855975 ;
	setAttr ".uvtk[487]" -type "float2" 0.0038592666 -0.036269248 ;
	setAttr ".uvtk[488]" -type "float2" 0.0024116337 -0.031692088 ;
	setAttr ".uvtk[489]" -type "float2" 0.002641663 -0.033491611 ;
	setAttr ".uvtk[490]" -type "float2" 0.0041833073 -0.038119793 ;
	setAttr ".uvtk[491]" -type "float2" 0.003297925 -0.035974741 ;
	setAttr ".uvtk[492]" -type "float2" 0.0030870885 -0.034142554 ;
	setAttr ".uvtk[493]" -type "float2" 0.0018620864 -0.029585004 ;
	setAttr ".uvtk[494]" -type "float2" 0.0021584183 -0.030321121 ;
	setAttr ".uvtk[495]" -type "float2" 0.0035503209 -0.034772217 ;
	setAttr ".uvtk[496]" -type "float2" 0.002763249 -0.032706082 ;
	setAttr ".uvtk[497]" -type "float2" 0.0024669915 -0.031854868 ;
	setAttr ".uvtk[498]" -type "float2" 0.0060520321 -0.041035473 ;
	setAttr ".uvtk[499]" -type "float2" 0.0047322363 -0.038119733 ;
	setAttr ".uvtk[500]" -type "float2" 0.0050733984 -0.039994597 ;
	setAttr ".uvtk[501]" -type "float2" 0.0066251755 -0.042955875 ;
	setAttr ".uvtk[502]" -type "float2" 0.005823046 -0.041593015 ;
	setAttr ".uvtk[503]" -type "float2" 0.0053444058 -0.039701104 ;
	setAttr ".uvtk[504]" -type "float2" 0.0037492365 -0.035583496 ;
	setAttr ".uvtk[505]" -type "float2" 0.0042315125 -0.036581039 ;
	setAttr ".uvtk[506]" -type "float2" 0.0053975731 -0.03940016 ;
	setAttr ".uvtk[507]" -type "float2" 0.00480555 -0.03810668 ;
	setAttr ".uvtk[508]" -type "float2" 0.0041346103 -0.037040353 ;
	setAttr ".uvtk[511]" -type "float2" 0.0007288456 -0.019403398 ;
	setAttr ".uvtk[512]" -type "float2" 0.00050295144 -0.012315929 ;
	setAttr ".uvtk[514]" -type "float2" 0.00076484308 -0.015768886 ;
	setAttr ".uvtk[515]" -type "float2" 0.00034913141 -0.006278336 ;
	setAttr ".uvtk[517]" -type "float2" 0.0003905734 -0.0061655045 ;
	setAttr ".uvtk[518]" -type "float2" 0.00087277591 -0.0160833 ;
	setAttr ".uvtk[519]" -type "float2" 0.0006776154 -0.011476696 ;
	setAttr ".uvtk[520]" -type "float2" 0.00060979463 -0.011425376 ;
	setAttr ".uvtk[521]" -type "float2" 0.00023030117 -0.0049821138 ;
	setAttr ".uvtk[523]" -type "float2" 0.00029803719 -0.0059351921 ;
	setAttr ".uvtk[524]" -type "float2" 0.00064533576 -0.014599979 ;
	setAttr ".uvtk[525]" -type "float2" 0.00051218271 -0.010674179 ;
	setAttr ".uvtk[526]" -type "float2" 0.00040671229 -0.008995235 ;
	setAttr ".uvtk[527]" -type "float2" 0.001074262 -0.025265992 ;
	setAttr ".uvtk[528]" -type "float2" 0.0008379519 -0.019457459 ;
	setAttr ".uvtk[529]" -type "float2" 0.00097760931 -0.020056129 ;
	setAttr ".uvtk[530]" -type "float2" 0.0012438074 -0.026383758 ;
	setAttr ".uvtk[531]" -type "float2" 0.0010389835 -0.023453057 ;
	setAttr ".uvtk[532]" -type "float2" 0.00089667737 -0.022598922 ;
	setAttr ".uvtk[533]" -type "float2" 0.00054655597 -0.015112579 ;
	setAttr ".uvtk[534]" -type "float2" 0.00069005787 -0.017897487 ;
	setAttr ".uvtk[535]" -type "float2" 0.00089792162 -0.023027956 ;
	setAttr ".uvtk[536]" -type "float2" 0.00074308366 -0.020679951 ;
	setAttr ".uvtk[537]" -type "float2" 0.00057950616 -0.017439723 ;
	setAttr ".uvtk[540]" -type "float2" 0.00022619218 -0.0059913397 ;
	setAttr ".uvtk[541]" -type "float2" 0.00010691583 -0.0022366643 ;
	setAttr ".uvtk[543]" -type "float2" 0.00017608609 -0.0036395192 ;
	setAttr ".uvtk[544]" -type "float2" 0.00036499649 -0.0092684031 ;
	setAttr ".uvtk[545]" -type "float2" 0.00028375722 -0.0067020655 ;
	setAttr ".uvtk[546]" -type "float2" 0.000190394 -0.0042405725 ;
	setAttr ".uvtk[549]" -type "float2" 5.3885393e-05 -0.00095546246 ;
	setAttr ".uvtk[550]" -type "float2" 0.00011718273 -0.0027784705 ;
	setAttr ".uvtk[551]" -type "float2" 9.1053545e-05 -0.0019116998 ;
	setAttr ".uvtk[553]" -type "float2" 0.0003650263 -0.0097025633 ;
	setAttr ".uvtk[554]" -type "float2" 0.00025738403 -0.0074521899 ;
	setAttr ".uvtk[555]" -type "float2" 0.00039538741 -0.011441767 ;
	setAttr ".uvtk[556]" -type "float2" 0.00053761154 -0.014760971 ;
	setAttr ".uvtk[557]" -type "float2" 0.00043280423 -0.013241827 ;
	setAttr ".uvtk[558]" -type "float2" 0.00028257817 -0.0086758137 ;
	setAttr ".uvtk[560]" -type "float2" 0.00011880696 -0.0035207868 ;
	setAttr ".uvtk[561]" -type "float2" 0.00018405914 -0.0046566725 ;
	setAttr ".uvtk[562]" -type "float2" 0.00014467537 -0.0041416883 ;
	setAttr ".uvtk[564]" -type "float2" 0.0014683455 -0.026396215 ;
	setAttr ".uvtk[565]" -type "float2" 0.0014014542 -0.023715794 ;
	setAttr ".uvtk[566]" -type "float2" 0.0022113174 -0.03126061 ;
	setAttr ".uvtk[567]" -type "float2" 0.0013882518 -0.027556837 ;
	setAttr ".uvtk[568]" -type "float2" 0.0016558841 -0.02888602 ;
	setAttr ".uvtk[569]" -type "float2" 0.002709657 -0.032930434 ;
	setAttr ".uvtk[570]" -type "float2" 0.0020986572 -0.031055212 ;
	setAttr ".uvtk[571]" -type "float2" 0.0018187538 -0.029549241 ;
	setAttr ".uvtk[572]" -type "float2" 0.0009368211 -0.021084428 ;
	setAttr ".uvtk[573]" -type "float2" 0.0011758432 -0.025057435 ;
	setAttr ".uvtk[574]" -type "float2" 0.0018208623 -0.02826798 ;
	setAttr ".uvtk[575]" -type "float2" 0.0014745668 -0.026791751 ;
	setAttr ".uvtk[576]" -type "float2" 0.0011868104 -0.022498667 ;
	setAttr ".uvtk[577]" -type "float2" 0.0029909015 -0.034966528 ;
	setAttr ".uvtk[578]" -type "float2" 0.0025915504 -0.032718599 ;
	setAttr ".uvtk[579]" -type "float2" 0.0031511486 -0.03454572 ;
	setAttr ".uvtk[580]" -type "float2" 0.0038489252 -0.037028372 ;
	setAttr ".uvtk[581]" -type "float2" 0.0034508109 -0.035895705 ;
	setAttr ".uvtk[582]" -type "float2" 0.0027540624 -0.033941627 ;
	setAttr ".uvtk[583]" -type "float2" 0.0015150607 -0.024755836 ;
	setAttr ".uvtk[584]" -type "float2" 0.0020128936 -0.029543281 ;
	setAttr ".uvtk[585]" -type "float2" 0.0021766871 -0.031511009 ;
	setAttr ".uvtk[586]" -type "float2" 0.002080068 -0.030611873 ;
	setAttr ".uvtk[587]" -type "float2" 0.0014793575 -0.025647104 ;
	setAttr ".uvtk[589]" -type "float2" 0.00066557527 -0.011863947 ;
	setAttr ".uvtk[590]" -type "float2" 0.00047452748 -0.01055795 ;
	setAttr ".uvtk[591]" -type "float2" 0.00071880221 -0.016034544 ;
	setAttr ".uvtk[592]" -type "float2" 0.0010270476 -0.0180161 ;
	setAttr ".uvtk[593]" -type "float2" 0.0008725822 -0.017096579 ;
	setAttr ".uvtk[594]" -type "float2" 0.00058892369 -0.011266291 ;
	setAttr ".uvtk[596]" -type "float2" 0.00024340302 -0.0050822496 ;
	setAttr ".uvtk[597]" -type "float2" 0.00032575428 -0.0057287216 ;
	setAttr ".uvtk[598]" -type "float2" 0.00029668212 -0.0054389238 ;
	setAttr ".uvtk[600]" -type "float2" 0.00043551624 -0.013238311 ;
	setAttr ".uvtk[601]" -type "float2" 0.00066515803 -0.012399018 ;
	setAttr ".uvtk[602]" -type "float2" 0.0010650009 -0.018809497 ;
	setAttr ".uvtk[603]" -type "float2" 0.00085698068 -0.02007091 ;
	setAttr ".uvtk[604]" -type "float2" 0.00097110868 -0.019493103 ;
	setAttr ".uvtk[605]" -type "float2" 0.00055496395 -0.012859046 ;
	setAttr ".uvtk[607]" -type "float2" 0.00031378865 -0.006002605 ;
	setAttr ".uvtk[608]" -type "float2" 0.00014702976 -0.0064095259 ;
	setAttr ".uvtk[609]" -type "float2" 0.0002464205 -0.0062214136 ;
	setAttr ".uvtk[611]" -type "float2" 0.076050401 -0.043845832 ;
	setAttr ".uvtk[612]" -type "float2" 0.027545631 -0.042631328 ;
	setAttr ".uvtk[613]" -type "float2" 0.034325123 -0.049013257 ;
	setAttr ".uvtk[614]" -type "float2" 0.012753904 -0.047126055 ;
	setAttr ".uvtk[615]" -type "float2" 0.013871923 -0.041203797 ;
	setAttr ".uvtk[616]" -type "float2" 0.0093729645 -0.039456427 ;
	setAttr ".uvtk[617]" -type "float2" 0.0095833838 -0.033282578 ;
	setAttr ".uvtk[618]" -type "float2" 0.014272839 -0.034952998 ;
	setAttr ".uvtk[619]" -type "float2" 0.0113139 -0.034181774 ;
	setAttr ".uvtk[620]" -type "float2" 0.011110082 -0.040390193 ;
	setAttr ".uvtk[621]" -type "float2" 0.008361131 -0.045272112 ;
	setAttr ".uvtk[622]" -type "float2" 0.0089834481 -0.043957531 ;
	setAttr ".uvtk[623]" -type "float2" 0.013378814 -0.045768201 ;
	setAttr ".uvtk[624]" -type "float2" 0.010598391 -0.044930279 ;
	setAttr ".uvtk[625]" -type "float2" 0.0099585205 -0.046260178 ;
	setAttr ".uvtk[626]" -type "float2" 0.035511136 -0.043004572 ;
	setAttr ".uvtk[627]" -type "float2" 0.019072443 -0.041907012 ;
	setAttr ".uvtk[628]" -type "float2" 0.019352302 -0.035633087 ;
	setAttr ".uvtk[629]" -type "float2" 0.035840303 -0.036692023 ;
	setAttr ".uvtk[630]" -type "float2" 0.026752725 -0.036209106 ;
	setAttr ".uvtk[631]" -type "float2" 0.026506737 -0.042510927 ;
	setAttr ".uvtk[632]" -type "float2" 0.017794818 -0.047873497 ;
	setAttr ".uvtk[633]" -type "float2" 0.018589199 -0.046506464 ;
	setAttr ".uvtk[634]" -type "float2" 0.035057485 -0.047627628 ;
	setAttr ".uvtk[635]" -type "float2" 0.025924876 -0.047119856 ;
	setAttr ".uvtk[636]" -type "float2" 0.025225893 -0.048508823 ;
	setAttr ".uvtk[637]" -type "float2" 0.0084907711 -0.040936947 ;
	setAttr ".uvtk[638]" -type "float2" 0.010879397 -0.044032037 ;
	setAttr ".uvtk[639]" -type "float2" 0.0070901215 -0.042192042 ;
	setAttr ".uvtk[640]" -type "float2" 0.0077821016 -0.0441221 ;
	setAttr ".uvtk[641]" -type "float2" 0.011865541 -0.045994043 ;
	setAttr ".uvtk[642]" -type "float2" 0.0091996789 -0.045131803 ;
	setAttr ".uvtk[643]" -type "float2" 0.0083740652 -0.043175638 ;
	setAttr ".uvtk[644]" -type "float2" 0.0054133981 -0.0392887 ;
	setAttr ".uvtk[645]" -type "float2" 0.0062881112 -0.04050529 ;
	setAttr ".uvtk[646]" -type "float2" 0.009806335 -0.04226172 ;
	setAttr ".uvtk[647]" -type "float2" 0.0074569732 -0.0414505 ;
	setAttr ".uvtk[648]" -type "float2" 0.006373927 -0.040172577 ;
	setAttr ".uvtk[649]" -type "float2" 0.031623453 -0.045894682 ;
	setAttr ".uvtk[650]" -type "float2" 0.015825748 -0.044770062 ;
	setAttr ".uvtk[651]" -type "float2" 0.016862243 -0.046751022 ;
	setAttr ".uvtk[652]" -type "float2" 0.0331209 -0.047892034 ;
	setAttr ".uvtk[653]" -type "float2" 0.024286404 -0.047381699 ;
	setAttr ".uvtk[654]" -type "float2" 0.022933647 -0.045396388 ;
	setAttr ".uvtk[655]" -type "float2" 0.012994602 -0.041602492 ;
	setAttr ".uvtk[656]" -type "float2" 0.014486402 -0.042981744 ;
	setAttr ".uvtk[657]" -type "float2" 0.029708326 -0.0440678 ;
	setAttr ".uvtk[658]" -type "float2" 0.021405384 -0.043581307 ;
	setAttr ".uvtk[659]" -type "float2" 0.019411847 -0.042176068 ;
	setAttr ".uvtk[660]" -type "float2" 0.085950881 -0.05041039 ;
	setAttr ".uvtk[661]" -type "float2" 0.065695077 -0.049986064 ;
	setAttr ".uvtk[662]" -type "float2" 0.067614377 -0.043985963 ;
	setAttr ".uvtk[663]" -type "float2" 0.046087131 -0.043411016 ;
	setAttr ".uvtk[664]" -type "float2" 0.046309277 -0.037081778 ;
	setAttr ".uvtk[665]" -type "float2" 0.067936063 -0.037642658 ;
	setAttr ".uvtk[666]" -type "float2" 0.057589978 -0.037398458 ;
	setAttr ".uvtk[667]" -type "float2" 0.057327867 -0.043730319 ;
	setAttr ".uvtk[668]" -type "float2" 0.044578359 -0.049419761 ;
	setAttr ".uvtk[669]" -type "float2" 0.045576766 -0.048034132 ;
	setAttr ".uvtk[670]" -type "float2" 0.066918612 -0.048609614 ;
	setAttr ".uvtk[671]" -type "float2" 0.056612313 -0.048357844 ;
	setAttr ".uvtk[672]" -type "float2" 0.055609733 -0.049736917 ;
	setAttr ".uvtk[673]" -type "float2" 0.088059127 -0.044398248 ;
	setAttr ".uvtk[674]" -type "float2" 0.076001406 -0.044183016 ;
	setAttr ".uvtk[675]" -type "float2" 0.07630676 -0.037835836 ;
	setAttr ".uvtk[676]" -type "float2" 0.088339686 -0.038039446 ;
	setAttr ".uvtk[677]" -type "float2" 0.084279656 -0.03796953 ;
	setAttr ".uvtk[678]" -type "float2" 0.083795905 -0.044326305 ;
	setAttr ".uvtk[679]" -type "float2" 0.073910236 -0.050182343 ;
	setAttr ".uvtk[680]" -type "float2" 0.075140566 -0.048812687 ;
	setAttr ".uvtk[681]" -type "float2" 0.087196052 -0.049034834 ;
	setAttr ".uvtk[682]" -type "float2" 0.083018601 -0.048956573 ;
	setAttr ".uvtk[683]" -type "float2" 0.081668079 -0.050332487 ;
	setAttr ".uvtk[684]" -type "float2" 0.05637002 -0.043475211 ;
	setAttr ".uvtk[685]" -type "float2" 0.061965227 -0.04681772 ;
	setAttr ".uvtk[686]" -type "float2" 0.04160662 -0.046286702 ;
	setAttr ".uvtk[687]" -type "float2" 0.043269977 -0.048289537 ;
	setAttr ".uvtk[688]" -type "float2" 0.064134628 -0.048836827 ;
	setAttr ".uvtk[689]" -type "float2" 0.054179251 -0.048599541 ;
	setAttr ".uvtk[690]" -type "float2" 0.052301049 -0.046583891 ;
	setAttr ".uvtk[691]" -type "float2" 0.036793247 -0.042991817 ;
	setAttr ".uvtk[692]" -type "float2" 0.03954713 -0.044442713 ;
	setAttr ".uvtk[693]" -type "float2" 0.059419543 -0.044953763 ;
	setAttr ".uvtk[694]" -type "float2" 0.049768597 -0.044729948 ;
	setAttr ".uvtk[695]" -type "float2" 0.046966553 -0.043263316 ;
	setAttr ".uvtk[696]" -type "float2" 0.081915915 -0.047221661 ;
	setAttr ".uvtk[697]" -type "float2" 0.069945902 -0.047005296 ;
	setAttr ".uvtk[698]" -type "float2" 0.072128892 -0.049033701 ;
	setAttr ".uvtk[699]" -type "float2" 0.084200621 -0.049257338 ;
	setAttr ".uvtk[700]" -type "float2" 0.079946965 -0.049179137 ;
	setAttr ".uvtk[701]" -type "float2" 0.077661812 -0.047148168 ;
	setAttr ".uvtk[702]" -type "float2" 0.063930362 -0.043649316 ;
	setAttr ".uvtk[703]" -type "float2" 0.067202866 -0.045138955 ;
	setAttr ".uvtk[704]" -type "float2" 0.07919839 -0.045342803 ;
	setAttr ".uvtk[705]" -type "float2" 0.074833751 -0.04527241 ;
	setAttr ".uvtk[706]" -type "float2" 0.071622491 -0.043780982 ;
	setAttr ".uvtk[708]" -type "float2" 0.014959991 -0.029000282 ;
	setAttr ".uvtk[709]" -type "float2" 0.0023051947 -0.028011203 ;
	setAttr ".uvtk[710]" -type "float2" 0.0055089742 -0.037170172 ;
	setAttr ".uvtk[711]" -type "float2" 0.0033963025 -0.035832524 ;
	setAttr ".uvtk[712]" -type "float2" 0.0044146329 -0.037980556 ;
	setAttr ".uvtk[713]" -type "float2" 0.0071266592 -0.039476097 ;
	setAttr ".uvtk[714]" -type "float2" 0.0051599741 -0.038785875 ;
	setAttr ".uvtk[715]" -type "float2" 0.0039767921 -0.036546528 ;
	setAttr ".uvtk[716]" -type "float2" 0.0014712811 -0.027026355 ;
	setAttr ".uvtk[717]" -type "float2" 0.0024312735 -0.032265604 ;
	setAttr ".uvtk[718]" -type "float2" 0.003932476 -0.033434093 ;
	setAttr ".uvtk[719]" -type "float2" 0.0026798099 -0.0328933 ;
	setAttr ".uvtk[720]" -type "float2" 0.0015449971 -0.027554274 ;
	setAttr ".uvtk[721]" -type "float2" 0.02184388 -0.038552523 ;
	setAttr ".uvtk[722]" -type "float2" 0.0091763288 -0.03771764 ;
	setAttr ".uvtk[723]" -type "float2" 0.011094868 -0.040094793 ;
	setAttr ".uvtk[724]" -type "float2" 0.024802625 -0.0410375 ;
	setAttr ".uvtk[725]" -type "float2" 0.017269224 -0.040616632 ;
	setAttr ".uvtk[726]" -type "float2" 0.014706254 -0.038182318 ;
	setAttr ".uvtk[727]" -type "float2" 0.0048775524 -0.028411031 ;
	setAttr ".uvtk[728]" -type "float2" 0.0069899708 -0.033915818 ;
	setAttr ".uvtk[729]" -type "float2" 0.018558919 -0.034629762 ;
	setAttr ".uvtk[730]" -type "float2" 0.012041718 -0.034309983 ;
	setAttr ".uvtk[731]" -type "float2" 0.0092095733 -0.028744817 ;
	setAttr ".uvtk[733]" -type "float2" -0.00016874075 -0.014140069 ;
	setAttr ".uvtk[734]" -type "float2" 0.00023049116 -0.013581216 ;
	setAttr ".uvtk[735]" -type "float2" 0.00076778233 -0.020553529 ;
	setAttr ".uvtk[736]" -type "float2" 0.00094628334 -0.021344781 ;
	setAttr ".uvtk[737]" -type "float2" 0.00052478909 -0.020983338 ;
	setAttr ".uvtk[738]" -type "float2" -0.00013247132 -0.013885081 ;
	setAttr ".uvtk[740]" -type "float2" -1.2040138e-05 -0.0065940619 ;
	setAttr ".uvtk[741]" -type "float2" -0.00060999393 -0.006881237 ;
	setAttr ".uvtk[742]" -type "float2" -0.00041036308 -0.0067525506 ;
	setAttr ".uvtk[744]" -type "float2" 0.0072797239 -0.014656603 ;
	setAttr ".uvtk[745]" -type "float2" 0.0010972321 -0.014343202 ;
	setAttr ".uvtk[746]" -type "float2" 0.0028165281 -0.021651208 ;
	setAttr ".uvtk[747]" -type "float2" 0.011222869 -0.022108018 ;
	setAttr ".uvtk[748]" -type "float2" 0.0063917041 -0.021907806 ;
	setAttr ".uvtk[749]" -type "float2" 0.0037434697 -0.014514685 ;
	setAttr ".uvtk[751]" -type "float2" -4.3272972e-05 -0.0069844127 ;
	setAttr ".uvtk[752]" -type "float2" 0.0034498274 -0.0071420074 ;
	setAttr ".uvtk[753]" -type "float2" 0.0014310181 -0.0070695877 ;
	setAttr ".uvtk[755]" -type "float2" 0.053627998 -0.029717565 ;
	setAttr ".uvtk[756]" -type "float2" 0.037172467 -0.029489994 ;
	setAttr ".uvtk[757]" -type "float2" 0.048303336 -0.039261997 ;
	setAttr ".uvtk[758]" -type "float2" 0.030378446 -0.03885293 ;
	setAttr ".uvtk[759]" -type "float2" 0.033805683 -0.041367233 ;
	setAttr ".uvtk[760]" -type "float2" 0.052636206 -0.041820347 ;
	setAttr ".uvtk[761]" -type "float2" 0.043656588 -0.041625679 ;
	setAttr ".uvtk[762]" -type "float2" 0.039649934 -0.039084494 ;
	setAttr ".uvtk[763]" -type "float2" 0.021994278 -0.029203057 ;
	setAttr ".uvtk[764]" -type "float2" 0.026402429 -0.034883618 ;
	setAttr ".uvtk[765]" -type "float2" 0.0432657 -0.035236061 ;
	setAttr ".uvtk[766]" -type "float2" 0.035042614 -0.035083354 ;
	setAttr ".uvtk[767]" -type "float2" 0.029748678 -0.029362679 ;
	setAttr ".uvtk[768]" -type "float2" 0.06703797 -0.039577544 ;
	setAttr ".uvtk[769]" -type "float2" 0.055695325 -0.039403319 ;
	setAttr ".uvtk[770]" -type "float2" 0.060183346 -0.041981399 ;
	setAttr ".uvtk[771]" -type "float2" 0.071926087 -0.042168856 ;
	setAttr ".uvtk[772]" -type "float2" 0.067841321 -0.042103589 ;
	setAttr ".uvtk[773]" -type "float2" 0.062853545 -0.039515555 ;
	setAttr ".uvtk[774]" -type "float2" 0.043235004 -0.029592812 ;
	setAttr ".uvtk[775]" -type "float2" 0.049988002 -0.035361469 ;
	setAttr ".uvtk[776]" -type "float2" 0.061044514 -0.03551203 ;
	setAttr ".uvtk[777]" -type "float2" 0.056961685 -0.035457551 ;
	setAttr ".uvtk[778]" -type "float2" 0.049695611 -0.029673815 ;
	setAttr ".uvtk[780]" -type "float2" 0.021469742 -0.014890611 ;
	setAttr ".uvtk[781]" -type "float2" 0.011639789 -0.014757872 ;
	setAttr ".uvtk[782]" -type "float2" 0.017027244 -0.022257924 ;
	setAttr ".uvtk[783]" -type "float2" 0.030029267 -0.022467315 ;
	setAttr ".uvtk[784]" -type "float2" 0.023673743 -0.022373617 ;
	setAttr ".uvtk[785]" -type "float2" 0.016810149 -0.014827967 ;
	setAttr ".uvtk[787]" -type "float2" 0.0059393793 -0.0071936846 ;
	setAttr ".uvtk[788]" -type "float2" 0.011644751 -0.0072588325 ;
	setAttr ".uvtk[789]" -type "float2" 0.0089119375 -0.0072287917 ;
	setAttr ".uvtk[791]" -type "float2" 0.032791674 -0.015010417 ;
	setAttr ".uvtk[792]" -type "float2" 0.025233835 -0.014948368 ;
	setAttr ".uvtk[793]" -type "float2" 0.034912676 -0.022550642 ;
	setAttr ".uvtk[794]" -type "float2" 0.044218689 -0.022644758 ;
	setAttr ".uvtk[795]" -type "float2" 0.040807605 -0.022612393 ;
	setAttr ".uvtk[796]" -type "float2" 0.029639453 -0.014991343 ;
	setAttr ".uvtk[798]" -type "float2" 0.013506591 -0.0072872639 ;
	setAttr ".uvtk[799]" -type "float2" 0.018420905 -0.00731951 ;
	setAttr ".uvtk[800]" -type "float2" 0.016363233 -0.0073082447 ;
	setAttr ".uvtk[804]" -type "float2" -0.0081555843 -0.027962744 ;
	setAttr ".uvtk[805]" -type "float2" -0.01203841 -0.0011809468 ;
	setAttr ".uvtk[807]" -type "float2" 0.0084550381 -0.0026038289 ;
	setAttr ".uvtk[808]" -type "float2" -0.0015434623 -0.00049269199 ;
	setAttr ".uvtk[810]" -type "float2" 0.028184295 -0.0006249994 ;
	setAttr ".uvtk[811]" -type "float2" 0.011070073 -0.00043253973 ;
	setAttr ".uvtk[813]" -type "float2" 0.019980013 -0.00043263659 ;
	setAttr ".uvtk[815]" -type "float2" 0.017720461 -0.00043498725 ;
	setAttr ".uvtk[816]" -type "float2" 0.045932531 -0.00063422322 ;
	setAttr ".uvtk[817]" -type "float2" 0.034690052 -0.00060943514 ;
	setAttr ".uvtk[818]" -type "float2" 0.020359337 -0.0006069541 ;
	setAttr ".uvtk[819]" -type "float2" 0.030632198 -0.00061193109 ;
	setAttr ".uvtk[820]" -type "float2" 0.040954381 -0.0006339848 ;
	setAttr ".uvtk[821]" -type "float2" -0.0036477745 -0.00039311126 ;
	setAttr ".uvtk[823]" -type "float2" 0.0059043467 -0.00042361766 ;
	setAttr ".uvtk[825]" -type "float2" 0.00077077746 -0.00041215122 ;
	setAttr ".uvtk[826]" -type "float2" 0.017559052 -0.00059971213 ;
	setAttr ".uvtk[827]" -type "float2" 0.011953861 -0.00059081614 ;
	setAttr ".uvtk[828]" -type "float2" -0.0032276213 -0.00052122772 ;
	setAttr ".uvtk[829]" -type "float2" 0.0035640895 -0.00056361407 ;
	setAttr ".uvtk[830]" -type "float2" 0.0072634816 -0.0005556196 ;
	setAttr ".uvtk[831]" -type "float2" 0.049788386 -0.0029449761 ;
	setAttr ".uvtk[832]" -type "float2" 0.040903628 -0.00074733794 ;
	setAttr ".uvtk[833]" -type "float2" 0.062369496 -0.00078879297 ;
	setAttr ".uvtk[834]" -type "float2" 0.055012405 -0.0006596446 ;
	setAttr ".uvtk[835]" -type "float2" 0.035212845 -0.0006377548 ;
	setAttr ".uvtk[836]" -type "float2" 0.049037844 -0.00065355003 ;
	setAttr ".uvtk[837]" -type "float2" 0.055642426 -0.00077325106 ;
	setAttr ".uvtk[838]" -type "float2" 0.072950214 -0.0030213743 ;
	setAttr ".uvtk[839]" -type "float2" 0.068148613 -0.0013953596 ;
	setAttr ".uvtk[840]" -type "float2" 0.045661509 -0.0013343543 ;
	setAttr ".uvtk[841]" -type "float2" 0.061191708 -0.0013735592 ;
	setAttr ".uvtk[842]" -type "float2" 0.065688342 -0.0029971749 ;
	setAttr ".uvtk[843]" -type "float2" 0.0036154389 -0.00051677227 ;
	setAttr ".uvtk[844]" -type "float2" 0.027192205 -0.00069977343 ;
	setAttr ".uvtk[845]" -type "float2" 0.022588432 -0.00060121715 ;
	setAttr ".uvtk[846]" -type "float2" 0.0010663569 -0.00045631826 ;
	setAttr ".uvtk[847]" -type "float2" 0.01068002 -0.00053961575 ;
	setAttr ".uvtk[848]" -type "float2" 0.014127254 -0.00061716139 ;
	setAttr ".uvtk[849]" -type "float2" 0.034782857 -0.0028615594 ;
	setAttr ".uvtk[850]" -type "float2" 0.031257987 -0.0012684315 ;
	setAttr ".uvtk[851]" -type "float2" 0.0061553717 -0.0010491759 ;
	setAttr ".uvtk[852]" -type "float2" 0.017346263 -0.0011718571 ;
	setAttr ".uvtk[853]" -type "float2" 0.020253569 -0.0027426183 ;
	setAttr ".uvtk[854]" -type "float2" -0.011217356 7.2561204e-05 ;
	setAttr ".uvtk[856]" -type "float2" 0.0050590038 -0.00023953617 ;
	setAttr ".uvtk[857]" -type "float2" 0.0030187368 -0.00029350817 ;
	setAttr ".uvtk[859]" -type "float2" -0.00029301643 -0.00035785884 ;
	setAttr ".uvtk[861]" -type "float2" 0.0027115345 -0.00031942874 ;
	setAttr ".uvtk[862]" -type "float2" 0.00210917 -0.00041352957 ;
	setAttr ".uvtk[863]" -type "float2" 0.00033700466 -0.00046376884 ;
	setAttr ".uvtk[864]" -type "float2" 0.0041647553 -0.00033706427 ;
	setAttr ".uvtk[865]" -type "float2" 0.0044795871 -0.00039759278 ;
	setAttr ".uvtk[866]" -type "float2" 0.0053703189 -0.00033067912 ;
	setAttr ".uvtk[867]" -type "float2" -0.0055558681 -0.00020455196 ;
	setAttr ".uvtk[869]" -type "float2" 0.00033420324 -0.00026671588 ;
	setAttr ".uvtk[871]" -type "float2" -0.0023237467 -0.00023014471 ;
	setAttr ".uvtk[872]" -type "float2" 0.00016146898 -0.00014390796 ;
	setAttr ".uvtk[873]" -type "float2" 0.00017005205 -0.00027567148 ;
	setAttr ".uvtk[874]" -type "float2" -0.0090715289 -0.00014719367 ;
	setAttr ".uvtk[875]" -type "float2" -0.0040606856 -0.00021054596 ;
	setAttr ".uvtk[876]" -type "float2" -0.0052016973 -3.606081e-05 ;
	setAttr ".uvtk[877]" -type "float2" 0.0089028478 -0.0020409971 ;
	setAttr ".uvtk[878]" -type "float2" 0.0068676472 -0.00013296306 ;
	setAttr ".uvtk[879]" -type "float2" 0.0055121779 -0.00040343404 ;
	setAttr ".uvtk[880]" -type "float2" 0.0037291646 -0.00036200881 ;
	setAttr ".uvtk[881]" -type "float2" 0.0059386492 -0.00014421344 ;
	setAttr ".uvtk[882]" -type "float2" 0.0066292286 -0.00025714934 ;
	setAttr ".uvtk[883]" -type "float2" 0.0081576109 -0.00027827919 ;
	setAttr ".uvtk[884]" -type "float2" 0.0093181729 -0.0024404526 ;
	setAttr ".uvtk[885]" -type "float2" 0.0074160099 -0.0009162724 ;
	setAttr ".uvtk[886]" -type "float2" 0.0078857541 -0.00058464706 ;
	setAttr ".uvtk[887]" -type "float2" 0.0096107721 -0.00075888634 ;
	setAttr ".uvtk[888]" -type "float2" 0.011004746 -0.0022544712 ;
	setAttr ".uvtk[889]" -type "float2" -0.012667239 0.00043292344 ;
	setAttr ".uvtk[890]" -type "float2" 0.00095784664 2.9921532e-05 ;
	setAttr ".uvtk[891]" -type "float2" 0.00043666363 -1.0386109e-05 ;
	setAttr ".uvtk[892]" -type "float2" -0.01243341 0.00029741228 ;
	setAttr ".uvtk[893]" -type "float2" -0.0056325793 0.00013546646 ;
	setAttr ".uvtk[894]" -type "float2" -0.0055269599 0.00021889806 ;
	setAttr ".uvtk[895]" -type "float2" 0.0027879477 -0.0018025637 ;
	setAttr ".uvtk[896]" -type "float2" 0.0017639399 -0.00038652122 ;
	setAttr ".uvtk[897]" -type "float2" -0.012477338 0.00012783706 ;
	setAttr ".uvtk[898]" -type "float2" -0.0050800443 -0.00015294552 ;
	setAttr ".uvtk[899]" -type "float2" -0.0043437481 -0.0015127808 ;
	setAttr ".uvtk[900]" -type "float2" 0.018275082 -0.031577885 ;
	setAttr ".uvtk[901]" -type "float2" 0.015506268 -0.016571522 ;
	setAttr ".uvtk[902]" -type "float2" 0.060526103 -0.017098337 ;
	setAttr ".uvtk[903]" -type "float2" 0.056210876 -0.009547323 ;
	setAttr ".uvtk[904]" -type "float2" 0.080152273 -0.0096499026 ;
	setAttr ".uvtk[905]" -type "float2" 0.076804072 -0.0059670508 ;
	setAttr ".uvtk[906]" -type "float2" 0.053133667 -0.0058779418 ;
	setAttr ".uvtk[907]" -type "float2" 0.069507778 -0.0059379637 ;
	setAttr ".uvtk[908]" -type "float2" 0.072482169 -0.0096186697 ;
	setAttr ".uvtk[909]" -type "float2" 0.085022032 -0.017227471 ;
	setAttr ".uvtk[910]" -type "float2" 0.082825512 -0.013392657 ;
	setAttr ".uvtk[911]" -type "float2" 0.058627605 -0.013276696 ;
	setAttr ".uvtk[912]" -type "float2" 0.075096637 -0.013354748 ;
	setAttr ".uvtk[913]" -type "float2" 0.077173799 -0.01718536 ;
	setAttr ".uvtk[914]" -type "float2" 0.012406349 -0.0091017485 ;
	setAttr ".uvtk[915]" -type "float2" 0.040468067 -0.0094360411 ;
	setAttr ".uvtk[916]" -type "float2" 0.037965298 -0.0057760477 ;
	setAttr ".uvtk[917]" -type "float2" 0.010501087 -0.0054771602 ;
	setAttr ".uvtk[918]" -type "float2" 0.02280727 -0.0056422949 ;
	setAttr ".uvtk[919]" -type "float2" 0.025269568 -0.0092844069 ;
	setAttr ".uvtk[920]" -type "float2" 0.044582069 -0.016970575 ;
	setAttr ".uvtk[921]" -type "float2" 0.042806625 -0.013154477 ;
	setAttr ".uvtk[922]" -type "float2" 0.014188766 -0.012787282 ;
	setAttr ".uvtk[923]" -type "float2" 0.027177066 -0.012994677 ;
	setAttr ".uvtk[924]" -type "float2" 0.02882129 -0.016795069 ;
	setAttr ".uvtk[925]" -type "float2" 0.063663453 -0.032289088 ;
	setAttr ".uvtk[926]" -type "float2" 0.062847435 -0.024537295 ;
	setAttr ".uvtk[927]" -type "float2" 0.087775499 -0.024690241 ;
	setAttr ".uvtk[928]" -type "float2" 0.086630434 -0.021064848 ;
	setAttr ".uvtk[929]" -type "float2" 0.061897337 -0.020926625 ;
	setAttr ".uvtk[930]" -type "float2" 0.078791022 -0.021018445 ;
	setAttr ".uvtk[931]" -type "float2" 0.079832733 -0.02463907 ;
	setAttr ".uvtk[932]" -type "float2" 0.088866204 -0.032478333 ;
	setAttr ".uvtk[933]" -type "float2" 0.088506669 -0.028291225 ;
	setAttr ".uvtk[934]" -type "float2" 0.063401461 -0.028122663 ;
	setAttr ".uvtk[935]" -type "float2" 0.080397576 -0.02823326 ;
	setAttr ".uvtk[936]" -type "float2" 0.080646127 -0.032415867 ;
	setAttr ".uvtk[937]" -type "float2" 0.017515242 -0.023919523 ;
	setAttr ".uvtk[938]" -type "float2" 0.046752423 -0.024387389 ;
	setAttr ".uvtk[939]" -type "float2" 0.045854747 -0.020785362 ;
	setAttr ".uvtk[940]" -type "float2" 0.016693115 -0.020353407 ;
	setAttr ".uvtk[941]" -type "float2" 0.029989004 -0.020598024 ;
	setAttr ".uvtk[942]" -type "float2" 0.030826539 -0.024180859 ;
	setAttr ".uvtk[943]" -type "float2" 0.047412097 -0.032110572 ;
	setAttr ".uvtk[944]" -type "float2" 0.047252029 -0.027955592 ;
	setAttr ".uvtk[945]" -type "float2" 0.01803118 -0.027452201 ;
	setAttr ".uvtk[946]" -type "float2" 0.031372607 -0.027735263 ;
	setAttr ".uvtk[947]" -type "float2" 0.031583548 -0.031874895 ;
	setAttr ".uvtk[948]" -type "float2" -0.0095512867 -0.01399821 ;
	setAttr ".uvtk[949]" -type "float2" 0.012585223 -0.015565395 ;
	setAttr ".uvtk[950]" -type "float2" 0.011193752 -0.0083191693 ;
	setAttr ".uvtk[951]" -type "float2" 0.012610018 -0.0088739991 ;
	setAttr ".uvtk[952]" -type "float2" 0.011055171 -0.0052858293 ;
	setAttr ".uvtk[953]" -type "float2" 0.010150492 -0.004812181 ;
	setAttr ".uvtk[954]" -type "float2" 0.012340248 -0.0050621927 ;
	setAttr ".uvtk[955]" -type "float2" 0.013525665 -0.0086148679 ;
	setAttr ".uvtk[956]" -type "float2" 0.015115082 -0.016291589 ;
	setAttr ".uvtk[957]" -type "float2" 0.013924599 -0.012535423 ;
	setAttr ".uvtk[958]" -type "float2" 0.011779785 -0.011894286 ;
	setAttr ".uvtk[959]" -type "float2" 0.014698625 -0.0122343 ;
	setAttr ".uvtk[960]" -type "float2" 0.015606701 -0.015961796 ;
	setAttr ".uvtk[961]" -type "float2" -0.01055038 -0.0071282387 ;
	setAttr ".uvtk[962]" -type "float2" 0.0042401552 -0.0079743564 ;
	setAttr ".uvtk[963]" -type "float2" 0.00362885 -0.0045188665 ;
	setAttr ".uvtk[964]" -type "float2" -0.01105684 -0.0037875175 ;
	setAttr ".uvtk[965]" -type "float2" -0.0036792159 -0.0041783452 ;
	setAttr ".uvtk[966]" -type "float2" -0.002846837 -0.0075685382 ;
	setAttr ".uvtk[967]" -type "float2" 0.0055244565 -0.01510945 ;
	setAttr ".uvtk[968]" -type "float2" 0.004953444 -0.011487722 ;
	setAttr ".uvtk[969]" -type "float2" -0.010040641 -0.010515273 ;
	setAttr ".uvtk[970]" -type "float2" -0.0023682117 -0.011026293 ;
	setAttr ".uvtk[971]" -type "float2" -0.0018740892 -0.014577925 ;
	setAttr ".uvtk[972]" -type "float2" 0.014799535 -0.030188859 ;
	setAttr ".uvtk[973]" -type "float2" 0.013933659 -0.022712916 ;
	setAttr ".uvtk[974]" -type "float2" 0.016933143 -0.023590326 ;
	setAttr ".uvtk[975]" -type "float2" 0.016050816 -0.020053446 ;
	setAttr ".uvtk[976]" -type "float2" 0.013301432 -0.019251466 ;
	setAttr ".uvtk[977]" -type "float2" 0.016459346 -0.019685417 ;
	setAttr ".uvtk[978]" -type "float2" 0.017139733 -0.023196638 ;
	setAttr ".uvtk[979]" -type "float2" 0.017747164 -0.031200588 ;
	setAttr ".uvtk[980]" -type "float2" 0.017483354 -0.027104586 ;
	setAttr ".uvtk[981]" -type "float2" 0.014442325 -0.026156008 ;
	setAttr ".uvtk[982]" -type "float2" 0.017736077 -0.026669532 ;
	setAttr ".uvtk[983]" -type "float2" 0.01808244 -0.030743003 ;
	setAttr ".uvtk[984]" -type "float2" -0.0086397529 -0.020774215 ;
	setAttr ".uvtk[985]" -type "float2" 0.0066177249 -0.022154003 ;
	setAttr ".uvtk[986]" -type "float2" 0.0061542392 -0.018731803 ;
	setAttr ".uvtk[987]" -type "float2" -0.0090370774 -0.01748094 ;
	setAttr ".uvtk[988]" -type "float2" -0.0014141798 -0.018149346 ;
	setAttr ".uvtk[989]" -type "float2" -0.0010039806 -0.021508038 ;
	setAttr ".uvtk[990]" -type "float2" 0.007355392 -0.02955538 ;
	setAttr ".uvtk[991]" -type "float2" 0.0070344806 -0.025548786 ;
	setAttr ".uvtk[992]" -type "float2" -0.0084059834 -0.024058282 ;
	setAttr ".uvtk[993]" -type "float2" -0.0006121397 -0.024860173 ;
	setAttr ".uvtk[994]" -type "float2" -0.00036245584 -0.028814197 ;
	setAttr ".uvtk[997]" -type "float2" 0.043385565 0.0011494458 ;
	setAttr ".uvtk[998]" -type "float2" 0.029445291 0.00070634484 ;
	setAttr ".uvtk[1000]" -type "float2" -0.0085801482 0.0004036203 ;
	setAttr ".uvtk[1001]" -type "float2" -0.0054456592 -0.00013809651 ;
	setAttr ".uvtk[1003]" -type "float2" -0.009154439 -0.00017797202 ;
	setAttr ".uvtk[1005]" -type "float2" -0.0094638467 -0.00015520677 ;
	setAttr ".uvtk[1006]" -type "float2" -0.016920269 0.000172019 ;
	setAttr ".uvtk[1007]" -type "float2" -0.014180005 -8.8468194e-05 ;
	setAttr ".uvtk[1008]" -type "float2" -0.0077744126 2.143532e-05 ;
	setAttr ".uvtk[1009]" -type "float2" -0.014312446 -3.2283366e-05 ;
	setAttr ".uvtk[1010]" -type "float2" -0.016569853 0.00028286129 ;
	setAttr ".uvtk[1011]" -type "float2" 0.013429284 -6.1001629e-05 ;
	setAttr ".uvtk[1013]" -type "float2" 0.0005659461 -0.00011239946 ;
	setAttr ".uvtk[1015]" -type "float2" 0.0066869855 -9.7688287e-05 ;
	setAttr ".uvtk[1016]" -type "float2" 0.0036163926 0.0005043596 ;
	setAttr ".uvtk[1017]" -type "float2" 0.0020255446 7.7702105e-05 ;
	setAttr ".uvtk[1018]" -type "float2" 0.02267319 0.00020427257 ;
	setAttr ".uvtk[1019]" -type "float2" 0.011979878 0.00013326108 ;
	setAttr ".uvtk[1020]" -type "float2" 0.016478956 0.00061471015 ;
	setAttr ".uvtk[1021]" -type "float2" -0.0062065125 -3.6671758e-05 ;
	setAttr ".uvtk[1022]" -type "float2" -0.0079032779 0.0011921823 ;
	setAttr ".uvtk[1023]" -type "float2" -0.018618226 0.00068846345 ;
	setAttr ".uvtk[1024]" -type "float2" -0.018254876 0.00046506524 ;
	setAttr ".uvtk[1025]" -type "float2" -0.0085806251 0.00083808601 ;
	setAttr ".uvtk[1026]" -type "float2" -0.017470062 0.0006531328 ;
	setAttr ".uvtk[1027]" -type "float2" -0.017712116 0.00094507635 ;
	setAttr ".uvtk[1028]" -type "float2" -0.017797887 -0.00081767142 ;
	setAttr ".uvtk[1029]" -type "float2" -0.018397033 0.00043845177 ;
	setAttr ".uvtk[1030]" -type "float2" -0.0071499348 0.0010790378 ;
	setAttr ".uvtk[1031]" -type "float2" -0.017115235 0.00076261163 ;
	setAttr ".uvtk[1032]" -type "float2" -0.016572297 -0.00043742359 ;
	setAttr ".uvtk[1033]" -type "float2" 0.038405716 0.0018580556 ;
	setAttr ".uvtk[1034]" -type "float2" 0.006949544 0.001435861 ;
	setAttr ".uvtk[1035]" -type "float2" 0.0052658319 0.0010019243 ;
	setAttr ".uvtk[1036]" -type "float2" 0.034674764 0.0013029426 ;
	setAttr ".uvtk[1037]" -type "float2" 0.020012021 0.0011659563 ;
	setAttr ".uvtk[1038]" -type "float2" 0.0228163 0.0016621947 ;
	setAttr ".uvtk[1039]" -type "float2" 0.0098320246 0.00036050379 ;
	setAttr ".uvtk[1040]" -type "float2" 0.0084553957 0.0014061332 ;
	setAttr ".uvtk[1041]" -type "float2" 0.041257739 0.0019851625 ;
	setAttr ".uvtk[1042]" -type "float2" 0.025077879 0.0017061383 ;
	setAttr ".uvtk[1043]" -type "float2" 0.026952624 0.00076746941 ;
	setAttr ".uvtk[1046]" -type "float2" 0.027555108 0.00079610199 ;
	setAttr ".uvtk[1047]" -type "float2" 0.011517763 9.033829e-06 ;
	setAttr ".uvtk[1049]" -type "float2" 0.018830717 -2.1569431e-05 ;
	setAttr ".uvtk[1051]" -type "float2" 0.017494977 -1.0631979e-05 ;
	setAttr ".uvtk[1052]" -type "float2" 0.039184272 0.00077791512 ;
	setAttr ".uvtk[1053]" -type "float2" 0.030950963 0.0002553761 ;
	setAttr ".uvtk[1054]" -type "float2" 0.020542979 0.00029646605 ;
	setAttr ".uvtk[1055]" -type "float2" 0.029234827 0.00029115379 ;
	setAttr ".uvtk[1056]" -type "float2" 0.037471414 0.000819318 ;
	setAttr ".uvtk[1058]" -type "float2" 0.012379825 0.00054132938 ;
	setAttr ".uvtk[1059]" -type "float2" 0.004653275 -2.5387853e-05 ;
	setAttr ".uvtk[1061]" -type "float2" 0.0075354576 -5.6624413e-06 ;
	setAttr ".uvtk[1062]" -type "float2" 0.0089809895 0.00018888712 ;
	setAttr ".uvtk[1063]" -type "float2" 0.014043927 0.0002643913 ;
	setAttr ".uvtk[1064]" -type "float2" 0.019568324 0.00071017444 ;
	setAttr ".uvtk[1067]" -type "float2" 0.0022166371 -3.7655234e-05 ;
	setAttr ".uvtk[1069]" -type "float2" 0.0042198896 8.5264444e-05 ;
	setAttr ".uvtk[1070]" -type "float2" 0.0058602095 0.00029044598 ;
	setAttr ".uvtk[1071]" -type "float2" 0.041754961 0.0019624531 ;
	setAttr ".uvtk[1072]" -type "float2" 0.036778569 0.0020434111 ;
	setAttr ".uvtk[1073]" -type "float2" 0.049538672 0.0020048618 ;
	setAttr ".uvtk[1074]" -type "float2" 0.045160592 0.001398325 ;
	setAttr ".uvtk[1075]" -type "float2" 0.03290844 0.001393646 ;
	setAttr ".uvtk[1076]" -type "float2" 0.043550193 0.0014396012 ;
	setAttr ".uvtk[1077]" -type "float2" 0.047903597 0.0020682067 ;
	setAttr ".uvtk[1078]" -type "float2" 0.055012286 0.0014844984 ;
	setAttr ".uvtk[1079]" -type "float2" 0.052665889 0.002209425 ;
	setAttr ".uvtk[1080]" -type "float2" 0.039522588 0.0023983717 ;
	setAttr ".uvtk[1081]" -type "float2" 0.051054597 0.0023556948 ;
	setAttr ".uvtk[1082]" -type "float2" 0.053446889 0.0017776489 ;
	setAttr ".uvtk[1084]" -type "float2" 0.017193973 0.0014915317 ;
	setAttr ".uvtk[1085]" -type "float2" 0.015102565 0.00096030533 ;
	setAttr ".uvtk[1086]" -type "float2" 0.023563385 0.0012459904 ;
	setAttr ".uvtk[1087]" -type "float2" 0.026537478 0.0018630624 ;
	setAttr ".uvtk[1089]" -type "float2" 0.0073027611 0.00054235756 ;
	setAttr ".uvtk[1090]" -type "float2" 0.0083085299 0.00090348721 ;
	setAttr ".uvtk[1091]" -type "float2" 0.019996345 0.0017932206 ;
	setAttr ".uvtk[1092]" -type "float2" 0.01882571 0.0019187182 ;
	setAttr ".uvtk[1093]" -type "float2" 0.028848529 0.0022601485 ;
	setAttr ".uvtk[1094]" -type "float2" 0.030410588 0.0019854456 ;
	setAttr ".uvtk[1096]" -type "float2" 0.0090758801 0.0012286156 ;
	setAttr ".uvtk[1097]" -type "float2" 0.0098237395 0.0012129396 ;
	setAttr ".uvtk[1098]" -type "float2" 0.049590349 -0.019541383 ;
	setAttr ".uvtk[1099]" -type "float2" 0.048257828 -0.0088646114 ;
	setAttr ".uvtk[1100]" -type "float2" -0.0027110577 -0.011804163 ;
	setAttr ".uvtk[1101]" -type "float2" -0.0042563677 -0.0055500567 ;
	setAttr ".uvtk[1102]" -type "float2" -0.016461134 -0.0066444576 ;
	setAttr ".uvtk[1103]" -type "float2" -0.017294288 -0.0033767819 ;
	setAttr ".uvtk[1104]" -type "float2" -0.0052411556 -0.0024527609 ;
	setAttr ".uvtk[1105]" -type "float2" -0.015803933 -0.0029264092 ;
	setAttr ".uvtk[1106]" -type "float2" -0.014939308 -0.006123364 ;
	setAttr ".uvtk[1107]" -type "float2" -0.015263438 -0.01334092 ;
	setAttr ".uvtk[1108]" -type "float2" -0.015790403 -0.0099566579 ;
	setAttr ".uvtk[1109]" -type "float2" -0.0034171343 -0.0086542368 ;
	setAttr ".uvtk[1110]" -type "float2" -0.014163733 -0.0093272924 ;
	setAttr ".uvtk[1111]" -type "float2" -0.013609052 -0.012607843 ;
	setAttr ".uvtk[1112]" -type "float2" 0.046327591 -0.0036228299 ;
	setAttr ".uvtk[1113]" -type "float2" 0.012152493 -0.004948765 ;
	setAttr ".uvtk[1114]" -type "float2" 0.011052907 -0.0019578934 ;
	setAttr ".uvtk[1115]" -type "float2" 0.045011759 -0.00092381239 ;
	setAttr ".uvtk[1116]" -type "float2" 0.028455257 -0.0014415383 ;
	setAttr ".uvtk[1117]" -type "float2" 0.029558063 -0.0043046474 ;
	setAttr ".uvtk[1118]" -type "float2" 0.013846695 -0.010928214 ;
	setAttr ".uvtk[1119]" -type "float2" 0.013073087 -0.0079204738 ;
	setAttr ".uvtk[1120]" -type "float2" 0.04735148 -0.0062342882 ;
	setAttr ".uvtk[1121]" -type "float2" 0.030602217 -0.0071212649 ;
	setAttr ".uvtk[1122]" -type "float2" 0.031401932 -0.0099409223 ;
	setAttr ".uvtk[1123]" -type "float2" -0.00117594 -0.024554849 ;
	setAttr ".uvtk[1124]" -type "float2" -0.0016076565 -0.017916769 ;
	setAttr ".uvtk[1125]" -type "float2" -0.014532864 -0.019927949 ;
	setAttr ".uvtk[1126]" -type "float2" -0.014880121 -0.016733795 ;
	setAttr ".uvtk[1127]" -type "float2" -0.0020577908 -0.014954776 ;
	setAttr ".uvtk[1128]" -type "float2" -0.013104737 -0.01588884 ;
	setAttr ".uvtk[1129]" -type "float2" -0.012584507 -0.018984526 ;
	setAttr ".uvtk[1130]" -type "float2" -0.01394397 -0.026978731 ;
	setAttr ".uvtk[1131]" -type "float2" -0.014156938 -0.023141414 ;
	setAttr ".uvtk[1132]" -type "float2" -0.0013952255 -0.020912319 ;
	setAttr ".uvtk[1133]" -type "float2" -0.01227802 -0.022096157 ;
	setAttr ".uvtk[1134]" -type "float2" -0.012268901 -0.025856733 ;
	setAttr ".uvtk[1135]" -type "float2" 0.049138546 -0.013930708 ;
	setAttr ".uvtk[1136]" -type "float2" 0.014901698 -0.016719431 ;
	setAttr ".uvtk[1137]" -type "float2" 0.01448828 -0.013911158 ;
	setAttr ".uvtk[1138]" -type "float2" 0.048941672 -0.011486292 ;
	setAttr ".uvtk[1139]" -type "float2" 0.031938195 -0.012752891 ;
	setAttr ".uvtk[1140]" -type "float2" 0.032541573 -0.015383005 ;
	setAttr ".uvtk[1141]" -type "float2" 0.015398383 -0.023071885 ;
	setAttr ".uvtk[1142]" -type "float2" 0.01528883 -0.019554943 ;
	setAttr ".uvtk[1143]" -type "float2" 0.049436212 -0.016405344 ;
	setAttr ".uvtk[1144]" -type "float2" 0.03275013 -0.01806137 ;
	setAttr ".uvtk[1145]" -type "float2" 0.032848239 -0.021397531 ;
	setAttr ".uvtk[1147]" -type "float2" 0.046499252 -0.0052978396 ;
	setAttr ".uvtk[1148]" -type "float2" 0.044523418 -0.0015245974 ;
	setAttr ".uvtk[1149]" -type "float2" 0.058119893 -0.0028943717 ;
	setAttr ".uvtk[1150]" -type "float2" 0.056776047 -0.00042125583 ;
	setAttr ".uvtk[1151]" -type "float2" 0.043273747 0.00046944618 ;
	setAttr ".uvtk[1152]" -type "float2" 0.055223525 7.2747469e-05 ;
	setAttr ".uvtk[1153]" -type "float2" 0.056480467 -0.0021930039 ;
	setAttr ".uvtk[1154]" -type "float2" 0.060091734 -0.0077355504 ;
	setAttr ".uvtk[1155]" -type "float2" 0.05915308 -0.0053084195 ;
	setAttr ".uvtk[1156]" -type "float2" 0.04564172 -0.0034192801 ;
	setAttr ".uvtk[1157]" -type "float2" 0.05766362 -0.0043723583 ;
	setAttr ".uvtk[1158]" -type "float2" 0.05846256 -0.0065456927 ;
	setAttr ".uvtk[1160]" -type "float2" 0.021729827 -0.00047644973 ;
	setAttr ".uvtk[1161]" -type "float2" 0.020804524 0.00083601475 ;
	setAttr ".uvtk[1162]" -type "float2" 0.03178376 0.00075316429 ;
	setAttr ".uvtk[1163]" -type "float2" 0.032947659 -0.00093874335 ;
	setAttr ".uvtk[1165]" -type "float2" 0.010336161 0.00064817071 ;
	setAttr ".uvtk[1166]" -type "float2" 0.010671854 -0.00014537573 ;
	setAttr ".uvtk[1167]" -type "float2" 0.023127913 -0.0027271211 ;
	setAttr ".uvtk[1168]" -type "float2" 0.022586346 -0.0016225874 ;
	setAttr ".uvtk[1169]" -type "float2" 0.03392905 -0.0024936795 ;
	setAttr ".uvtk[1170]" -type "float2" 0.034701526 -0.0040359199 ;
	setAttr ".uvtk[1172]" -type "float2" 0.01112771 -0.00079548359 ;
	setAttr ".uvtk[1173]" -type "float2" 0.011464715 -0.0014156103 ;
	setAttr ".uvtk[1174]" -type "float2" 0.047369838 -0.012415528 ;
	setAttr ".uvtk[1175]" -type "float2" 0.047285438 -0.0087099075 ;
	setAttr ".uvtk[1176]" -type "float2" 0.061054885 -0.012331873 ;
	setAttr ".uvtk[1177]" -type "float2" 0.060698569 -0.010135561 ;
	setAttr ".uvtk[1178]" -type "float2" 0.04700768 -0.0071241558 ;
	setAttr ".uvtk[1179]" -type "float2" 0.05898267 -0.008671999 ;
	setAttr ".uvtk[1180]" -type "float2" 0.059362233 -0.010591596 ;
	setAttr ".uvtk[1181]" -type "float2" 0.061248183 -0.017428875 ;
	setAttr ".uvtk[1182]" -type "float2" 0.061132193 -0.01455608 ;
	setAttr ".uvtk[1183]" -type "float2" 0.047424793 -0.010258466 ;
	setAttr ".uvtk[1184]" -type "float2" 0.059540212 -0.012502253 ;
	setAttr ".uvtk[1185]" -type "float2" 0.059474826 -0.015057743 ;
	setAttr ".uvtk[1187]" -type "float2" 0.023674488 -0.0045771003 ;
	setAttr ".uvtk[1188]" -type "float2" 0.02348578 -0.0037835538 ;
	setAttr ".uvtk[1189]" -type "float2" 0.035194039 -0.0054950416 ;
	setAttr ".uvtk[1190]" -type "float2" 0.035511971 -0.0066837072 ;
	setAttr ".uvtk[1192]" -type "float2" 0.011579812 -0.0019882321 ;
	setAttr ".uvtk[1193]" -type "float2" 0.011764228 -0.0023389459 ;
	setAttr ".uvtk[1194]" -type "float2" 0.023684442 -0.0062792301 ;
	setAttr ".uvtk[1195]" -type "float2" 0.023787796 -0.0052194595 ;
	setAttr ".uvtk[1196]" -type "float2" 0.035513222 -0.0078159571 ;
	setAttr ".uvtk[1197]" -type "float2" 0.035437465 -0.0094938278 ;
	setAttr ".uvtk[1199]" -type "float2" 0.0118047 -0.0025311112 ;
	setAttr ".uvtk[1200]" -type "float2" 0.011770904 -0.002995193 ;
	setAttr ".uvtk[1202]" -type "float2" -0.01105684 -0.0392887 ;
	setAttr ".uvtk[1203]" -type "float2" 0.010501087 -0.042991817 ;
	setAttr ".uvtk[1204]" -type "float2" 0.016693115 -0.049419761 ;
	setAttr ".uvtk[1205]" -type "float2" 0.061897337 -0.050182343 ;
	setAttr ".uvtk[1206]" -type "float2" 0.063401461 -0.044183016 ;
	setAttr ".uvtk[1207]" -type "float2" 0.088506669 -0.044398248 ;
	setAttr ".uvtk[1208]" -type "float2" 0.088866204 -0.038039446 ;
	setAttr ".uvtk[1209]" -type "float2" 0.063663453 -0.037835836 ;
	setAttr ".uvtk[1210]" -type "float2" 0.080646127 -0.03796953 ;
	setAttr ".uvtk[1211]" -type "float2" 0.080397576 -0.044326305 ;
	setAttr ".uvtk[1212]" -type "float2" 0.086630434 -0.05041039 ;
	setAttr ".uvtk[1213]" -type "float2" 0.087775499 -0.049034834 ;
	setAttr ".uvtk[1214]" -type "float2" 0.062847435 -0.048812687 ;
	setAttr ".uvtk[1215]" -type "float2" 0.079832733 -0.048956573 ;
	setAttr ".uvtk[1216]" -type "float2" 0.078791022 -0.050332487 ;
	setAttr ".uvtk[1217]" -type "float2" 0.01803118 -0.043411016 ;
	setAttr ".uvtk[1218]" -type "float2" 0.047252029 -0.043985963 ;
	setAttr ".uvtk[1219]" -type "float2" 0.047412097 -0.037642658 ;
	setAttr ".uvtk[1220]" -type "float2" 0.018275082 -0.037081778 ;
	setAttr ".uvtk[1221]" -type "float2" 0.031583548 -0.037398458 ;
	setAttr ".uvtk[1222]" -type "float2" 0.031372607 -0.043730319 ;
	setAttr ".uvtk[1223]" -type "float2" 0.045854747 -0.049986064 ;
	setAttr ".uvtk[1224]" -type "float2" 0.046752423 -0.048609614 ;
	setAttr ".uvtk[1225]" -type "float2" 0.017515242 -0.048034132 ;
	setAttr ".uvtk[1226]" -type "float2" 0.030826539 -0.048357844 ;
	setAttr ".uvtk[1227]" -type "float2" 0.029989004 -0.049736917 ;
	setAttr ".uvtk[1228]" -type "float2" 0.053133667 -0.043649316 ;
	setAttr ".uvtk[1229]" -type "float2" 0.058627605 -0.047005296 ;
	setAttr ".uvtk[1230]" -type "float2" 0.082825512 -0.047221661 ;
	setAttr ".uvtk[1231]" -type "float2" 0.085022032 -0.049257338 ;
	setAttr ".uvtk[1232]" -type "float2" 0.060526103 -0.049033701 ;
	setAttr ".uvtk[1233]" -type "float2" 0.077173799 -0.049179137 ;
	setAttr ".uvtk[1234]" -type "float2" 0.075096637 -0.047148168 ;
	setAttr ".uvtk[1235]" -type "float2" 0.076804072 -0.043845832 ;
	setAttr ".uvtk[1236]" -type "float2" 0.080152273 -0.045342803 ;
	setAttr ".uvtk[1237]" -type "float2" 0.056210876 -0.045138955 ;
	setAttr ".uvtk[1238]" -type "float2" 0.072482169 -0.04527241 ;
	setAttr ".uvtk[1239]" -type "float2" 0.069507778 -0.043780982 ;
	setAttr ".uvtk[1240]" -type "float2" 0.014188766 -0.046286702 ;
	setAttr ".uvtk[1241]" -type "float2" 0.042806625 -0.04681772 ;
	setAttr ".uvtk[1242]" -type "float2" 0.044582069 -0.048836827 ;
	setAttr ".uvtk[1243]" -type "float2" 0.015506268 -0.048289537 ;
	setAttr ".uvtk[1244]" -type "float2" 0.02882129 -0.048599541 ;
	setAttr ".uvtk[1245]" -type "float2" 0.027177066 -0.046583891 ;
	setAttr ".uvtk[1246]" -type "float2" 0.037965298 -0.043475211 ;
	setAttr ".uvtk[1247]" -type "float2" 0.040468067 -0.044953763 ;
	setAttr ".uvtk[1248]" -type "float2" 0.012406349 -0.044442713 ;
	setAttr ".uvtk[1249]" -type "float2" 0.025269568 -0.044729948 ;
	setAttr ".uvtk[1250]" -type "float2" 0.02280727 -0.043263316 ;
	setAttr ".uvtk[1251]" -type "float2" -0.0090370774 -0.045272112 ;
	setAttr ".uvtk[1252]" -type "float2" 0.013301432 -0.047873497 ;
	setAttr ".uvtk[1253]" -type "float2" 0.014442325 -0.041907012 ;
	setAttr ".uvtk[1254]" -type "float2" 0.017483354 -0.043004572 ;
	setAttr ".uvtk[1255]" -type "float2" 0.017747164 -0.036692023 ;
	setAttr ".uvtk[1256]" -type "float2" 0.014799535 -0.035633087 ;
	setAttr ".uvtk[1257]" -type "float2" 0.01808244 -0.036209106 ;
	setAttr ".uvtk[1258]" -type "float2" 0.017736077 -0.042510927 ;
	setAttr ".uvtk[1259]" -type "float2" 0.016050816 -0.049013257 ;
	setAttr ".uvtk[1260]" -type "float2" 0.016933143 -0.047627628 ;
	setAttr ".uvtk[1261]" -type "float2" 0.013933659 -0.046506464 ;
	setAttr ".uvtk[1262]" -type "float2" 0.017139733 -0.047119856 ;
	setAttr ".uvtk[1263]" -type "float2" 0.016459346 -0.048508823 ;
	setAttr ".uvtk[1264]" -type "float2" -0.0084059834 -0.039456427 ;
	setAttr ".uvtk[1265]" -type "float2" 0.0070344806 -0.041203797 ;
	setAttr ".uvtk[1266]" -type "float2" 0.007355392 -0.034952998 ;
	setAttr ".uvtk[1267]" -type "float2" -0.0081555843 -0.033282578 ;
	setAttr ".uvtk[1268]" -type "float2" -0.00036245584 -0.034181774 ;
	setAttr ".uvtk[1269]" -type "float2" -0.0006121397 -0.040390193 ;
	setAttr ".uvtk[1270]" -type "float2" 0.0061542392 -0.047126055 ;
	setAttr ".uvtk[1271]" -type "float2" 0.0066177249 -0.045768201 ;
	setAttr ".uvtk[1272]" -type "float2" -0.0086397529 -0.043957531 ;
	setAttr ".uvtk[1273]" -type "float2" -0.0010039806 -0.044930279 ;
	setAttr ".uvtk[1274]" -type "float2" -0.0014141798 -0.046260178 ;
	setAttr ".uvtk[1275]" -type "float2" 0.010150492 -0.041602492 ;
	setAttr ".uvtk[1276]" -type "float2" 0.011779785 -0.044770062 ;
	setAttr ".uvtk[1277]" -type "float2" 0.013924599 -0.045894682 ;
	setAttr ".uvtk[1278]" -type "float2" 0.015115082 -0.047892034 ;
	setAttr ".uvtk[1279]" -type "float2" 0.012585223 -0.046751022 ;
	setAttr ".uvtk[1280]" -type "float2" 0.015606701 -0.047381699 ;
	setAttr ".uvtk[1281]" -type "float2" 0.014698625 -0.045396388 ;
	setAttr ".uvtk[1282]" -type "float2" 0.011055171 -0.042631328 ;
	setAttr ".uvtk[1283]" -type "float2" 0.012610018 -0.0440678 ;
	setAttr ".uvtk[1284]" -type "float2" 0.011193752 -0.042981744 ;
	setAttr ".uvtk[1285]" -type "float2" 0.013525665 -0.043581307 ;
	setAttr ".uvtk[1286]" -type "float2" 0.012340248 -0.042176068 ;
	setAttr ".uvtk[1287]" -type "float2" -0.010040641 -0.042192042 ;
	setAttr ".uvtk[1288]" -type "float2" 0.004953444 -0.044032037 ;
	setAttr ".uvtk[1289]" -type "float2" 0.0055244565 -0.045994043 ;
	setAttr ".uvtk[1290]" -type "float2" -0.0095512867 -0.0441221 ;
	setAttr ".uvtk[1291]" -type "float2" -0.0018740892 -0.045131803 ;
	setAttr ".uvtk[1292]" -type "float2" -0.0023682117 -0.043175638 ;
	setAttr ".uvtk[1293]" -type "float2" 0.00362885 -0.040936947 ;
	setAttr ".uvtk[1294]" -type "float2" 0.0042401552 -0.04226172 ;
	setAttr ".uvtk[1295]" -type "float2" -0.01055038 -0.04050529 ;
	setAttr ".uvtk[1296]" -type "float2" -0.002846837 -0.0414505 ;
	setAttr ".uvtk[1297]" -type "float2" -0.0036792159 -0.040172577 ;
	setAttr ".uvtk[1299]" -type "float2" 0.0010663569 -0.029203057 ;
	setAttr ".uvtk[1300]" -type "float2" 0.035212845 -0.029592812 ;
	setAttr ".uvtk[1301]" -type "float2" 0.045661509 -0.039403319 ;
	setAttr ".uvtk[1302]" -type "float2" 0.068148613 -0.039577544 ;
	setAttr ".uvtk[1303]" -type "float2" 0.072950214 -0.042168856 ;
	setAttr ".uvtk[1304]" -type "float2" 0.049788386 -0.041981399 ;
	setAttr ".uvtk[1305]" -type "float2" 0.065688342 -0.042103589 ;
	setAttr ".uvtk[1306]" -type "float2" 0.061191708 -0.039515555 ;
	setAttr ".uvtk[1307]" -type "float2" 0.055012405 -0.029717565 ;
	setAttr ".uvtk[1308]" -type "float2" 0.062369496 -0.03551203 ;
	setAttr ".uvtk[1309]" -type "float2" 0.040903628 -0.035361469 ;
	setAttr ".uvtk[1310]" -type "float2" 0.055642426 -0.035457551 ;
	setAttr ".uvtk[1311]" -type "float2" 0.049037844 -0.029673815 ;
	setAttr ".uvtk[1312]" -type "float2" 0.0061553717 -0.03885293 ;
	setAttr ".uvtk[1313]" -type "float2" 0.031257987 -0.039261997 ;
	setAttr ".uvtk[1314]" -type "float2" 0.034782857 -0.041820347 ;
	setAttr ".uvtk[1315]" -type "float2" 0.0084550381 -0.041367233 ;
	setAttr ".uvtk[1316]" -type "float2" 0.020253569 -0.041625679 ;
	setAttr ".uvtk[1317]" -type "float2" 0.017346263 -0.039084494 ;
	setAttr ".uvtk[1318]" -type "float2" 0.022588432 -0.029489994 ;
	setAttr ".uvtk[1319]" -type "float2" 0.027192205 -0.035236061 ;
	setAttr ".uvtk[1320]" -type "float2" 0.0036154389 -0.034883618 ;
	setAttr ".uvtk[1321]" -type "float2" 0.014127254 -0.035083354 ;
	setAttr ".uvtk[1322]" -type "float2" 0.01068002 -0.029362679 ;
	setAttr ".uvtk[1324]" -type "float2" 0.020359337 -0.014948368 ;
	setAttr ".uvtk[1325]" -type "float2" 0.034690052 -0.015010417 ;
	setAttr ".uvtk[1326]" -type "float2" 0.045932531 -0.022644758 ;
	setAttr ".uvtk[1327]" -type "float2" 0.028184295 -0.022550642 ;
	setAttr ".uvtk[1328]" -type "float2" 0.040954381 -0.022612393 ;
	setAttr ".uvtk[1329]" -type "float2" 0.030632198 -0.014991343 ;
	setAttr ".uvtk[1331]" -type "float2" 0.019980013 -0.00731951 ;
	setAttr ".uvtk[1332]" -type "float2" 0.011070073 -0.0072872639 ;
	setAttr ".uvtk[1333]" -type "float2" 0.017720461 -0.0073082447 ;
	setAttr ".uvtk[1335]" -type "float2" -0.0032276213 -0.014757872 ;
	setAttr ".uvtk[1336]" -type "float2" 0.011953861 -0.014890611 ;
	setAttr ".uvtk[1337]" -type "float2" 0.017559052 -0.022467315 ;
	setAttr ".uvtk[1338]" -type "float2" -0.0015434623 -0.022257924 ;
	setAttr ".uvtk[1339]" -type "float2" 0.0072634816 -0.022373617 ;
	setAttr ".uvtk[1340]" -type "float2" 0.0035640895 -0.014827967 ;
	setAttr ".uvtk[1342]" -type "float2" 0.0059043467 -0.0072588325 ;
	setAttr ".uvtk[1343]" -type "float2" -0.0036477745 -0.0071936846 ;
	setAttr ".uvtk[1344]" -type "float2" 0.00077077746 -0.0072287917 ;
	setAttr ".uvtk[1346]" -type "float2" -0.01243341 -0.027026355 ;
	setAttr ".uvtk[1347]" -type "float2" 0.0059386492 -0.028411031 ;
	setAttr ".uvtk[1348]" -type "float2" 0.0078857541 -0.03771764 ;
	setAttr ".uvtk[1349]" -type "float2" 0.0074160099 -0.038552523 ;
	setAttr ".uvtk[1350]" -type "float2" 0.0093181729 -0.0410375 ;
	setAttr ".uvtk[1351]" -type "float2" 0.0089028478 -0.040094793 ;
	setAttr ".uvtk[1352]" -type "float2" 0.011004746 -0.040616632 ;
	setAttr ".uvtk[1353]" -type "float2" 0.0096107721 -0.038182318 ;
	setAttr ".uvtk[1354]" -type "float2" 0.0037291646 -0.029000282 ;
	setAttr ".uvtk[1355]" -type "float2" 0.0055121779 -0.034629762 ;
	setAttr ".uvtk[1356]" -type "float2" 0.0068676472 -0.033915818 ;
	setAttr ".uvtk[1357]" -type "float2" 0.0081576109 -0.034309983 ;
	setAttr ".uvtk[1358]" -type "float2" 0.0066292286 -0.028744817 ;
	setAttr ".uvtk[1359]" -type "float2" -0.012477338 -0.035832524 ;
	setAttr ".uvtk[1360]" -type "float2" 0.0017639399 -0.037170172 ;
	setAttr ".uvtk[1361]" -type "float2" 0.0027879477 -0.039476097 ;
	setAttr ".uvtk[1362]" -type "float2" -0.01203841 -0.037980556 ;
	setAttr ".uvtk[1363]" -type "float2" -0.0043437481 -0.038785875 ;
	setAttr ".uvtk[1364]" -type "float2" -0.0050800443 -0.036546528 ;
	setAttr ".uvtk[1365]" -type "float2" 0.00043666363 -0.028011203 ;
	setAttr ".uvtk[1366]" -type "float2" 0.00095784664 -0.033434093 ;
	setAttr ".uvtk[1367]" -type "float2" -0.012667239 -0.032265604 ;
	setAttr ".uvtk[1368]" -type "float2" -0.0055269599 -0.0328933 ;
	setAttr ".uvtk[1369]" -type "float2" -0.0056325793 -0.027554274 ;
	setAttr ".uvtk[1371]" -type "float2" 0.0041647553 -0.014343202 ;
	setAttr ".uvtk[1372]" -type "float2" 0.00033700466 -0.014656603 ;
	setAttr ".uvtk[1373]" -type "float2" 0.00210917 -0.022108018 ;
	setAttr ".uvtk[1374]" -type "float2" 0.0050590038 -0.021651208 ;
	setAttr ".uvtk[1375]" -type "float2" 0.0053703189 -0.021907806 ;
	setAttr ".uvtk[1376]" -type "float2" 0.0044795871 -0.014514685 ;
	setAttr ".uvtk[1378]" -type "float2" -0.00029301643 -0.0071420074 ;
	setAttr ".uvtk[1379]" -type "float2" 0.0030187368 -0.0069844127 ;
	setAttr ".uvtk[1380]" -type "float2" 0.0027115345 -0.0070695877 ;
	setAttr ".uvtk[1382]" -type "float2" -0.0090715289 -0.013581216 ;
	setAttr ".uvtk[1383]" -type "float2" 0.00017005205 -0.014140069 ;
	setAttr ".uvtk[1384]" -type "float2" 0.00016146898 -0.021344781 ;
	setAttr ".uvtk[1385]" -type "float2" -0.011217356 -0.020553529 ;
	setAttr ".uvtk[1386]" -type "float2" -0.0052016973 -0.020983338 ;
	setAttr ".uvtk[1387]" -type "float2" -0.0040606856 -0.013885081 ;
	setAttr ".uvtk[1389]" -type "float2" 0.00033420324 -0.006881237 ;
	setAttr ".uvtk[1390]" -type "float2" -0.0055558681 -0.0065940619 ;
	setAttr ".uvtk[1391]" -type "float2" -0.0023237467 -0.0067525506 ;
	setAttr ".uvtk[1394]" -type "float2" 0.045011759 -0.029585004 ;
	setAttr ".uvtk[1395]" -type "float2" 0.048941672 -0.034708977 ;
	setAttr ".uvtk[1396]" -type "float2" -0.0020577908 -0.041153669 ;
	setAttr ".uvtk[1397]" -type "float2" -0.0013952255 -0.035611033 ;
	setAttr ".uvtk[1398]" -type "float2" -0.014156938 -0.038361251 ;
	setAttr ".uvtk[1399]" -type "float2" -0.01394397 -0.032246053 ;
	setAttr ".uvtk[1400]" -type "float2" -0.00117594 -0.029649317 ;
	setAttr ".uvtk[1401]" -type "float2" -0.012268901 -0.03104192 ;
	setAttr ".uvtk[1402]" -type "float2" -0.01227802 -0.037098348 ;
	setAttr ".uvtk[1403]" -type "float2" -0.014880121 -0.044105649 ;
	setAttr ".uvtk[1404]" -type "float2" -0.014532864 -0.042828918 ;
	setAttr ".uvtk[1405]" -type "float2" -0.0016076565 -0.039952099 ;
	setAttr ".uvtk[1406]" -type "float2" -0.012584507 -0.041497707 ;
	setAttr ".uvtk[1407]" -type "float2" -0.013104737 -0.042745948 ;
	setAttr ".uvtk[1408]" -type "float2" 0.049436212 -0.029685795 ;
	setAttr ".uvtk[1409]" -type "float2" 0.01528883 -0.03389883 ;
	setAttr ".uvtk[1410]" -type "float2" 0.015398383 -0.028034687 ;
	setAttr ".uvtk[1411]" -type "float2" 0.049590349 -0.024137795 ;
	setAttr ".uvtk[1412]" -type "float2" 0.032848239 -0.026218355 ;
	setAttr ".uvtk[1413]" -type "float2" 0.03275013 -0.03193444 ;
	setAttr ".uvtk[1414]" -type "float2" 0.01448828 -0.039309621 ;
	setAttr ".uvtk[1415]" -type "float2" 0.014901698 -0.038148105 ;
	setAttr ".uvtk[1416]" -type "float2" 0.049138546 -0.033701062 ;
	setAttr ".uvtk[1417]" -type "float2" 0.032541573 -0.036081314 ;
	setAttr ".uvtk[1418]" -type "float2" 0.031938195 -0.037171185 ;
	setAttr ".uvtk[1419]" -type "float2" -0.0052411556 -0.035583496 ;
	setAttr ".uvtk[1420]" -type "float2" -0.0034171343 -0.038119733 ;
	setAttr ".uvtk[1421]" -type "float2" -0.015790403 -0.041035473 ;
	setAttr ".uvtk[1422]" -type "float2" -0.015263438 -0.042955875 ;
	setAttr ".uvtk[1423]" -type "float2" -0.0027110577 -0.039994597 ;
	setAttr ".uvtk[1424]" -type "float2" -0.013609052 -0.041593015 ;
	setAttr ".uvtk[1425]" -type "float2" -0.014163733 -0.039701104 ;
	setAttr ".uvtk[1426]" -type "float2" -0.017294288 -0.038254023 ;
	setAttr ".uvtk[1427]" -type "float2" -0.016461134 -0.03940016 ;
	setAttr ".uvtk[1428]" -type "float2" -0.0042563677 -0.036581039 ;
	setAttr ".uvtk[1429]" -type "float2" -0.014939308 -0.03810668 ;
	setAttr ".uvtk[1430]" -type "float2" -0.015803933 -0.037040353 ;
	setAttr ".uvtk[1431]" -type "float2" 0.04735148 -0.031692088 ;
	setAttr ".uvtk[1432]" -type "float2" 0.013073087 -0.036269248 ;
	setAttr ".uvtk[1433]" -type "float2" 0.013846695 -0.038119793 ;
	setAttr ".uvtk[1434]" -type "float2" 0.048257828 -0.033491611 ;
	setAttr ".uvtk[1435]" -type "float2" 0.031401932 -0.035974741 ;
	setAttr ".uvtk[1436]" -type "float2" 0.030602217 -0.034142554 ;
	setAttr ".uvtk[1437]" -type "float2" 0.011052907 -0.033855975 ;
	setAttr ".uvtk[1438]" -type "float2" 0.012152493 -0.034772217 ;
	setAttr ".uvtk[1439]" -type "float2" 0.046327591 -0.030321121 ;
	setAttr ".uvtk[1440]" -type "float2" 0.029558063 -0.032706082 ;
	setAttr ".uvtk[1441]" -type "float2" 0.028455257 -0.031854868 ;
	setAttr ".uvtk[1443]" -type "float2" 0.04700768 -0.024707198 ;
	setAttr ".uvtk[1444]" -type "float2" 0.047424793 -0.020702839 ;
	setAttr ".uvtk[1445]" -type "float2" 0.061132193 -0.027091801 ;
	setAttr ".uvtk[1446]" -type "float2" 0.061248183 -0.021779358 ;
	setAttr ".uvtk[1447]" -type "float2" 0.047369838 -0.016042352 ;
	setAttr ".uvtk[1448]" -type "float2" 0.059474826 -0.019092023 ;
	setAttr ".uvtk[1449]" -type "float2" 0.059540212 -0.024130344 ;
	setAttr ".uvtk[1450]" -type "float2" 0.060698569 -0.031863987 ;
	setAttr ".uvtk[1451]" -type "float2" 0.061054885 -0.030946791 ;
	setAttr ".uvtk[1452]" -type "float2" 0.047285438 -0.024060488 ;
	setAttr ".uvtk[1453]" -type "float2" 0.059362233 -0.027769566 ;
	setAttr ".uvtk[1454]" -type "float2" 0.05898267 -0.02856499 ;
	setAttr ".uvtk[1456]" -type "float2" 0.023787796 -0.012095869 ;
	setAttr ".uvtk[1457]" -type "float2" 0.023684442 -0.0086597204 ;
	setAttr ".uvtk[1458]" -type "float2" 0.035437465 -0.012573361 ;
	setAttr ".uvtk[1459]" -type "float2" 0.035513222 -0.01672256 ;
	setAttr ".uvtk[1461]" -type "float2" 0.011770904 -0.0044124722 ;
	setAttr ".uvtk[1462]" -type "float2" 0.0118047 -0.0066409111 ;
	setAttr ".uvtk[1463]" -type "float2" 0.02348578 -0.014755309 ;
	setAttr ".uvtk[1464]" -type "float2" 0.023674488 -0.014544427 ;
	setAttr ".uvtk[1465]" -type "float2" 0.035511971 -0.019682884 ;
	setAttr ".uvtk[1466]" -type "float2" 0.035194039 -0.020137668 ;
	setAttr ".uvtk[1468]" -type "float2" 0.011764228 -0.0082095265 ;
	setAttr ".uvtk[1469]" -type "float2" 0.011579812 -0.0082490444 ;
	setAttr ".uvtk[1470]" -type "float2" 0.043273747 -0.020201921 ;
	setAttr ".uvtk[1471]" -type "float2" 0.04564172 -0.021682203 ;
	setAttr ".uvtk[1472]" -type "float2" 0.05915308 -0.028834224 ;
	setAttr ".uvtk[1473]" -type "float2" 0.060091734 -0.030615151 ;
	setAttr ".uvtk[1474]" -type "float2" 0.046499252 -0.023380876 ;
	setAttr ".uvtk[1475]" -type "float2" 0.05846256 -0.027274132 ;
	setAttr ".uvtk[1476]" -type "float2" 0.05766362 -0.025530279 ;
	setAttr ".uvtk[1477]" -type "float2" 0.056776047 -0.026902556 ;
	setAttr ".uvtk[1478]" -type "float2" 0.058119893 -0.027535141 ;
	setAttr ".uvtk[1479]" -type "float2" 0.044523418 -0.020565927 ;
	setAttr ".uvtk[1480]" -type "float2" 0.056480467 -0.0243029 ;
	setAttr ".uvtk[1481]" -type "float2" 0.055223525 -0.023791075 ;
	setAttr ".uvtk[1483]" -type "float2" 0.022586346 -0.01207298 ;
	setAttr ".uvtk[1484]" -type "float2" 0.023127913 -0.01347363 ;
	setAttr ".uvtk[1485]" -type "float2" 0.034701526 -0.018816173 ;
	setAttr ".uvtk[1486]" -type "float2" 0.03392905 -0.017226636 ;
	setAttr ".uvtk[1488]" -type "float2" 0.011464715 -0.0072749257 ;
	setAttr ".uvtk[1489]" -type "float2" 0.01112771 -0.0062920451 ;
	setAttr ".uvtk[1490]" -type "float2" 0.020804524 -0.011255562 ;
	setAttr ".uvtk[1491]" -type "float2" 0.021729827 -0.011304379 ;
	setAttr ".uvtk[1492]" -type "float2" 0.032947659 -0.016233861 ;
	setAttr ".uvtk[1493]" -type "float2" 0.03178376 -0.016024411 ;
	setAttr ".uvtk[1495]" -type "float2" 0.010671854 -0.0058117509 ;
	setAttr ".uvtk[1496]" -type "float2" 0.010336161 -0.0058831573 ;
	setAttr ".uvtk[1498]" -type "float2" 0.034674764 -0.021084428 ;
	setAttr ".uvtk[1499]" -type "float2" -0.0085806251 -0.024755836 ;
	setAttr ".uvtk[1500]" -type "float2" -0.0071499348 -0.032718599 ;
	setAttr ".uvtk[1501]" -type "float2" -0.018397033 -0.034966528 ;
	setAttr ".uvtk[1502]" -type "float2" -0.017797887 -0.037028372 ;
	setAttr ".uvtk[1503]" -type "float2" -0.0062065125 -0.03454572 ;
	setAttr ".uvtk[1504]" -type "float2" -0.016572297 -0.035895705 ;
	setAttr ".uvtk[1505]" -type "float2" -0.017115235 -0.033941627 ;
	setAttr ".uvtk[1506]" -type "float2" -0.018254876 -0.026396215 ;
	setAttr ".uvtk[1507]" -type "float2" -0.018618226 -0.031511009 ;
	setAttr ".uvtk[1508]" -type "float2" -0.0079032779 -0.029543281 ;
	setAttr ".uvtk[1509]" -type "float2" -0.017712116 -0.030611873 ;
	setAttr ".uvtk[1510]" -type "float2" -0.017470062 -0.025647104 ;
	setAttr ".uvtk[1511]" -type "float2" 0.041257739 -0.027556837 ;
	setAttr ".uvtk[1512]" -type "float2" 0.0084553957 -0.03126061 ;
	setAttr ".uvtk[1513]" -type "float2" 0.0098320246 -0.032930434 ;
	setAttr ".uvtk[1514]" -type "float2" 0.043385565 -0.02888602 ;
	setAttr ".uvtk[1515]" -type "float2" 0.026952624 -0.031055212 ;
	setAttr ".uvtk[1516]" -type "float2" 0.025077879 -0.029549241 ;
	setAttr ".uvtk[1517]" -type "float2" 0.0052658319 -0.023715794 ;
	setAttr ".uvtk[1518]" -type "float2" 0.006949544 -0.02826798 ;
	setAttr ".uvtk[1519]" -type "float2" 0.038405716 -0.025057435 ;
	setAttr ".uvtk[1520]" -type "float2" 0.0228163 -0.026791751 ;
	setAttr ".uvtk[1521]" -type "float2" 0.020012021 -0.022498667 ;
	setAttr ".uvtk[1523]" -type "float2" -0.0077744126 -0.012399018 ;
	setAttr ".uvtk[1524]" -type "float2" -0.014180005 -0.013238311 ;
	setAttr ".uvtk[1525]" -type "float2" -0.016920269 -0.02007091 ;
	setAttr ".uvtk[1526]" -type "float2" -0.0085801482 -0.018809497 ;
	setAttr ".uvtk[1527]" -type "float2" -0.016569853 -0.019493103 ;
	setAttr ".uvtk[1528]" -type "float2" -0.014312446 -0.012859046 ;
	setAttr ".uvtk[1530]" -type "float2" -0.009154439 -0.0064095259 ;
	setAttr ".uvtk[1531]" -type "float2" -0.0054456592 -0.006002605 ;
	setAttr ".uvtk[1532]" -type "float2" -0.0094638467 -0.0062214136 ;
	setAttr ".uvtk[1534]" -type "float2" 0.02267319 -0.01055795 ;
	setAttr ".uvtk[1535]" -type "float2" 0.0020255446 -0.011863947 ;
	setAttr ".uvtk[1536]" -type "float2" 0.0036163926 -0.0180161 ;
	setAttr ".uvtk[1537]" -type "float2" 0.029445291 -0.016034544 ;
	setAttr ".uvtk[1538]" -type "float2" 0.016478956 -0.017096579 ;
	setAttr ".uvtk[1539]" -type "float2" 0.011979878 -0.011266291 ;
	setAttr ".uvtk[1541]" -type "float2" 0.0005659461 -0.0057287216 ;
	setAttr ".uvtk[1542]" -type "float2" 0.013429284 -0.0050822496 ;
	setAttr ".uvtk[1543]" -type "float2" 0.0066869855 -0.0054389238 ;
	setAttr ".uvtk[1546]" -type "float2" 0.03290844 -0.015112579 ;
	setAttr ".uvtk[1547]" -type "float2" 0.039522588 -0.019457459 ;
	setAttr ".uvtk[1548]" -type "float2" 0.052665889 -0.025265992 ;
	setAttr ".uvtk[1549]" -type "float2" 0.055012286 -0.026383758 ;
	setAttr ".uvtk[1550]" -type "float2" 0.041754961 -0.020056129 ;
	setAttr ".uvtk[1551]" -type "float2" 0.053446889 -0.023453057 ;
	setAttr ".uvtk[1552]" -type "float2" 0.051054597 -0.022598922 ;
	setAttr ".uvtk[1553]" -type "float2" 0.045160592 -0.019403398 ;
	setAttr ".uvtk[1554]" -type "float2" 0.049538672 -0.023027956 ;
	setAttr ".uvtk[1555]" -type "float2" 0.036778569 -0.017897487 ;
	setAttr ".uvtk[1556]" -type "float2" 0.047903597 -0.020679951 ;
	setAttr ".uvtk[1557]" -type "float2" 0.043550193 -0.017439723 ;
	setAttr ".uvtk[1559]" -type "float2" 0.01882571 -0.011425376 ;
	setAttr ".uvtk[1560]" -type "float2" 0.019996345 -0.011476696 ;
	setAttr ".uvtk[1561]" -type "float2" 0.030410588 -0.0160833 ;
	setAttr ".uvtk[1562]" -type "float2" 0.028848529 -0.015768886 ;
	setAttr ".uvtk[1564]" -type "float2" 0.0098237395 -0.0061655045 ;
	setAttr ".uvtk[1565]" -type "float2" 0.0090758801 -0.006278336 ;
	setAttr ".uvtk[1566]" -type "float2" 0.015102565 -0.008995235 ;
	setAttr ".uvtk[1567]" -type "float2" 0.017193973 -0.010674179 ;
	setAttr ".uvtk[1568]" -type "float2" 0.026537478 -0.014599979 ;
	setAttr ".uvtk[1569]" -type "float2" 0.023563385 -0.012315929 ;
	setAttr ".uvtk[1571]" -type "float2" 0.0083085299 -0.0059351921 ;
	setAttr ".uvtk[1572]" -type "float2" 0.0073027611 -0.0049821138 ;
	setAttr ".uvtk[1574]" -type "float2" 0.020542979 -0.0074521899 ;
	setAttr ".uvtk[1575]" -type "float2" 0.030950963 -0.0097025633 ;
	setAttr ".uvtk[1576]" -type "float2" 0.039184272 -0.014760971 ;
	setAttr ".uvtk[1577]" -type "float2" 0.027555108 -0.011441767 ;
	setAttr ".uvtk[1578]" -type "float2" 0.037471414 -0.013241827 ;
	setAttr ".uvtk[1579]" -type "float2" 0.029234827 -0.0086758137 ;
	setAttr ".uvtk[1581]" -type "float2" 0.018830717 -0.0046566725 ;
	setAttr ".uvtk[1582]" -type "float2" 0.011517763 -0.0035207868 ;
	setAttr ".uvtk[1583]" -type "float2" 0.017494977 -0.0041416883 ;
	setAttr ".uvtk[1586]" -type "float2" 0.0089809895 -0.0042405725 ;
	setAttr ".uvtk[1587]" -type "float2" 0.012379825 -0.0067020655 ;
	setAttr ".uvtk[1588]" -type "float2" 0.019568324 -0.0092684031 ;
	setAttr ".uvtk[1589]" -type "float2" 0.014043927 -0.0059913397 ;
	setAttr ".uvtk[1591]" -type "float2" 0.0058602095 -0.0036395192 ;
	setAttr ".uvtk[1592]" -type "float2" 0.0042198896 -0.0022366643 ;
	setAttr ".uvtk[1594]" -type "float2" 0.004653275 -0.0019116998 ;
	setAttr ".uvtk[1595]" -type "float2" 0.0075354576 -0.0027784705 ;
	setAttr ".uvtk[1598]" -type "float2" 0.0022166371 -0.00095546246 ;
	setAttr ".uvtk[1603]" -type "float2" 0.0022166371 -0.00095546246 ;
	setAttr ".uvtk[1604]" -type "float2" 0.018420905 -0.00731951 ;
	setAttr ".uvtk[1607]" -type "float2" 0.016363233 -0.0073082447 ;
	setAttr ".uvtk[1608]" -type "float2" 0.088059127 -0.028291225 ;
	setAttr ".uvtk[1609]" -type "float2" 0.088339686 -0.032478333 ;
	setAttr ".uvtk[1610]" -type "float2" 0.084279656 -0.032415867 ;
	setAttr ".uvtk[1611]" -type "float2" 0.083795905 -0.02823326 ;
	setAttr ".uvtk[1612]" -type "float2" 0.0078765005 -0.023141414 ;
	setAttr ".uvtk[1613]" -type "float2" 0.0080622286 -0.026978731 ;
	setAttr ".uvtk[1614]" -type "float2" 0.0067503899 -0.025856733 ;
	setAttr ".uvtk[1615]" -type "float2" 0.0066321343 -0.022096157 ;
	setAttr ".uvtk[1616]" -type "float2" 0.0029909015 0.00043845177 ;
	setAttr ".uvtk[1617]" -type "float2" 0.0038489252 -0.00081767142 ;
	setAttr ".uvtk[1618]" -type "float2" 0.0034508109 -0.00043742359 ;
	setAttr ".uvtk[1619]" -type "float2" 0.0027540624 0.00076261163 ;
	setAttr ".uvtk[1620]" -type "float2" 0.001074262 0.002209425 ;
	setAttr ".uvtk[1621]" -type "float2" 0.0012438074 0.0014844984 ;
	setAttr ".uvtk[1622]" -type "float2" 0.0010389835 0.0017776489 ;
	setAttr ".uvtk[1623]" -type "float2" 0.00089667737 0.0023556948 ;
	setAttr ".uvtk[1624]" -type "float2" 0.0003650263 0.0002553761 ;
	setAttr ".uvtk[1625]" -type "float2" 0.00053761154 0.00077791512 ;
	setAttr ".uvtk[1626]" -type "float2" 0.00043280423 0.000819318 ;
	setAttr ".uvtk[1627]" -type "float2" 0.00028257817 0.00029115379 ;
	setAttr ".uvtk[1628]" -type "float2" 0.00022619218 0.0002643913 ;
	setAttr ".uvtk[1629]" -type "float2" 0.00036499649 0.00071017444 ;
	setAttr ".uvtk[1630]" -type "float2" 0.00028375722 0.00054132938 ;
	setAttr ".uvtk[1631]" -type "float2" 0.000190394 0.00018888712 ;
	setAttr ".uvtk[1634]" -type "float2" 0.00011718273 -5.6624413e-06 ;
	setAttr ".uvtk[1635]" -type "float2" 9.1053545e-05 -2.5387853e-05 ;
	setAttr ".uvtk[1638]" -type "float2" 5.3885393e-05 -3.7655234e-05 ;
	setAttr ".uvtk[1640]" -type "float2" 0.00017608609 0.00029044598 ;
	setAttr ".uvtk[1643]" -type "float2" 0.00010691583 8.5264444e-05 ;
	setAttr ".uvtk[1646]" -type "float2" 0.00018405914 -2.1569431e-05 ;
	setAttr ".uvtk[1647]" -type "float2" 0.00014467537 -1.0631979e-05 ;
	setAttr ".uvtk[1649]" -type "float2" 0.00011880696 9.033829e-06 ;
	setAttr ".uvtk[1650]" -type "float2" 0.00039538741 0.00079610199 ;
	setAttr ".uvtk[1651]" -type "float2" 0.00025738403 0.00029646605 ;
	setAttr ".uvtk[1652]" -type "float2" 0.00087277591 0.0019854456 ;
	setAttr ".uvtk[1653]" -type "float2" 0.0006776154 0.0017932206 ;
	setAttr ".uvtk[1654]" -type "float2" 0.00060979463 0.0019187182 ;
	setAttr ".uvtk[1655]" -type "float2" 0.00076484308 0.0022601485 ;
	setAttr ".uvtk[1656]" -type "float2" 0.00050295144 0.0012459904 ;
	setAttr ".uvtk[1657]" -type "float2" 0.00064533576 0.0018630624 ;
	setAttr ".uvtk[1658]" -type "float2" 0.00051218271 0.0014915317 ;
	setAttr ".uvtk[1659]" -type "float2" 0.00040671229 0.00096030533 ;
	setAttr ".uvtk[1662]" -type "float2" 0.00023030117 0.00054235756 ;
	setAttr ".uvtk[1663]" -type "float2" 0.00029803719 0.00090348721 ;
	setAttr ".uvtk[1664]" -type "float2" 0.0003905734 0.0012129396 ;
	setAttr ".uvtk[1667]" -type "float2" 0.00034913141 0.0012286156 ;
	setAttr ".uvtk[1668]" -type "float2" 0.0007288456 0.001398325 ;
	setAttr ".uvtk[1669]" -type "float2" 0.00089792162 0.0020048618 ;
	setAttr ".uvtk[1670]" -type "float2" 0.00074308366 0.0020682067 ;
	setAttr ".uvtk[1671]" -type "float2" 0.00057950616 0.0014396012 ;
	setAttr ".uvtk[1672]" -type "float2" 0.00069005787 0.0020434111 ;
	setAttr ".uvtk[1673]" -type "float2" 0.00054655597 0.001393646 ;
	setAttr ".uvtk[1674]" -type "float2" 0.00097760931 0.0019624531 ;
	setAttr ".uvtk[1675]" -type "float2" 0.0008379519 0.0023983717 ;
	setAttr ".uvtk[1676]" -type "float2" 0.00043551624 -8.8468194e-05 ;
	setAttr ".uvtk[1677]" -type "float2" 0.00085698068 0.000172019 ;
	setAttr ".uvtk[1678]" -type "float2" 0.00097110868 0.00028286129 ;
	setAttr ".uvtk[1679]" -type "float2" 0.00055496395 -3.2283366e-05 ;
	setAttr ".uvtk[1680]" -type "float2" 0.00066557527 7.7702105e-05 ;
	setAttr ".uvtk[1681]" -type "float2" 0.0010270476 0.0005043596 ;
	setAttr ".uvtk[1682]" -type "float2" 0.0008725822 0.00061471015 ;
	setAttr ".uvtk[1683]" -type "float2" 0.00058892369 0.00013326108 ;
	setAttr ".uvtk[1686]" -type "float2" 0.00032575428 -0.00011239946 ;
	setAttr ".uvtk[1687]" -type "float2" 0.00029668212 -9.7688287e-05 ;
	setAttr ".uvtk[1689]" -type "float2" 0.00024340302 -6.1001629e-05 ;
	setAttr ".uvtk[1690]" -type "float2" 0.00071880221 0.00070634484 ;
	setAttr ".uvtk[1691]" -type "float2" 0.00047452748 0.00020427257 ;
	setAttr ".uvtk[1694]" -type "float2" 0.00014702976 -0.00017797202 ;
	setAttr ".uvtk[1695]" -type "float2" 0.0002464205 -0.00015520677 ;
	setAttr ".uvtk[1697]" -type "float2" 0.00031378865 -0.00013809651 ;
	setAttr ".uvtk[1698]" -type "float2" 0.0010650009 0.0004036203 ;
	setAttr ".uvtk[1699]" -type "float2" 0.00066515803 2.143532e-05 ;
	setAttr ".uvtk[1700]" -type "float2" 0.002709657 0.00036050379 ;
	setAttr ".uvtk[1701]" -type "float2" 0.0020986572 0.00076746941 ;
	setAttr ".uvtk[1702]" -type "float2" 0.0018187538 0.0017061383 ;
	setAttr ".uvtk[1703]" -type "float2" 0.0022113174 0.0014061332 ;
	setAttr ".uvtk[1704]" -type "float2" 0.0014014542 0.0010019243 ;
	setAttr ".uvtk[1705]" -type "float2" 0.0018208623 0.001435861 ;
	setAttr ".uvtk[1706]" -type "float2" 0.0014745668 0.0016621947 ;
	setAttr ".uvtk[1707]" -type "float2" 0.0011868104 0.0011659563 ;
	setAttr ".uvtk[1708]" -type "float2" 0.0009368211 0.0013029426 ;
	setAttr ".uvtk[1709]" -type "float2" 0.0011758432 0.0018580556 ;
	setAttr ".uvtk[1710]" -type "float2" 0.0016558841 0.0011494458 ;
	setAttr ".uvtk[1711]" -type "float2" 0.0013882518 0.0019851625 ;
	setAttr ".uvtk[1712]" -type "float2" 0.0014683455 0.00046506524 ;
	setAttr ".uvtk[1713]" -type "float2" 0.0021766871 0.00068846345 ;
	setAttr ".uvtk[1714]" -type "float2" 0.002080068 0.00094507635 ;
	setAttr ".uvtk[1715]" -type "float2" 0.0014793575 0.0006531328 ;
	setAttr ".uvtk[1716]" -type "float2" 0.0020128936 0.0011921823 ;
	setAttr ".uvtk[1717]" -type "float2" 0.0015150607 0.00083808601 ;
	setAttr ".uvtk[1718]" -type "float2" 0.0031511486 -3.6671758e-05 ;
	setAttr ".uvtk[1719]" -type "float2" 0.0025915504 0.0010790378 ;
	setAttr ".uvtk[1720]" -type "float2" 0.0025462806 -0.017428875 ;
	setAttr ".uvtk[1721]" -type "float2" 0.0021363646 -0.015057743 ;
	setAttr ".uvtk[1722]" -type "float2" 0.0020884201 -0.012502253 ;
	setAttr ".uvtk[1723]" -type "float2" 0.0025023147 -0.01455608 ;
	setAttr ".uvtk[1724]" -type "float2" 0.0018752441 -0.0053084195 ;
	setAttr ".uvtk[1725]" -type "float2" 0.0020987466 -0.0077355504 ;
	setAttr ".uvtk[1726]" -type "float2" 0.0016999319 -0.0065456927 ;
	setAttr ".uvtk[1727]" -type "float2" 0.0015293434 -0.0043723583 ;
	setAttr ".uvtk[1728]" -type "float2" 0.0010745712 -0.0024936795 ;
	setAttr ".uvtk[1729]" -type "float2" 0.0011466555 -0.0040359199 ;
	setAttr ".uvtk[1730]" -type "float2" 0.00076494738 -0.0027271211 ;
	setAttr ".uvtk[1731]" -type "float2" 0.00077654421 -0.0016225874 ;
	setAttr ".uvtk[1732]" -type "float2" 0.00095874071 0.00075316429 ;
	setAttr ".uvtk[1733]" -type "float2" 0.0010258108 -0.00093874335 ;
	setAttr ".uvtk[1734]" -type "float2" 0.00075664185 -0.00047644973 ;
	setAttr ".uvtk[1735]" -type "float2" 0.00073384494 0.00083601475 ;
	setAttr ".uvtk[1738]" -type "float2" 0.00041157473 0.00064817071 ;
	setAttr ".uvtk[1739]" -type "float2" 0.00042335875 -0.00014537573 ;
	setAttr ".uvtk[1742]" -type "float2" 0.00040728878 -0.00079548359 ;
	setAttr ".uvtk[1743]" -type "float2" 0.00040456094 -0.0014156103 ;
	setAttr ".uvtk[1744]" -type "float2" 0.0014627352 -0.00042125583 ;
	setAttr ".uvtk[1745]" -type "float2" 0.001654692 -0.0028943717 ;
	setAttr ".uvtk[1746]" -type "float2" 0.0013623163 -0.0021930039 ;
	setAttr ".uvtk[1747]" -type "float2" 0.0011956319 7.2747469e-05 ;
	setAttr ".uvtk[1748]" -type "float2" 0.0010852143 0.00046944618 ;
	setAttr ".uvtk[1749]" -type "float2" 0.001201313 -0.0015245974 ;
	setAttr ".uvtk[1750]" -type "float2" 0.0014272444 -0.0052978396 ;
	setAttr ".uvtk[1751]" -type "float2" 0.0013271719 -0.0034192801 ;
	setAttr ".uvtk[1752]" -type "float2" 0.0012355447 -0.0094938278 ;
	setAttr ".uvtk[1753]" -type "float2" 0.00076255947 -0.0062792301 ;
	setAttr ".uvtk[1754]" -type "float2" 0.00077883154 -0.0052194595 ;
	setAttr ".uvtk[1755]" -type "float2" 0.0012140609 -0.0078159571 ;
	setAttr ".uvtk[1756]" -type "float2" 0.0011680312 -0.0054950416 ;
	setAttr ".uvtk[1757]" -type "float2" 0.0012196079 -0.0066837072 ;
	setAttr ".uvtk[1758]" -type "float2" 0.00075806491 -0.0045771003 ;
	setAttr ".uvtk[1759]" -type "float2" 0.00078514591 -0.0037835538 ;
	setAttr ".uvtk[1762]" -type "float2" 0.00037998985 -0.0019882321 ;
	setAttr ".uvtk[1763]" -type "float2" 0.00038758945 -0.0023389459 ;
	setAttr ".uvtk[1764]" -type "float2" 0.00038132537 -0.002995193 ;
	setAttr ".uvtk[1767]" -type "float2" 0.0003718771 -0.0025311112 ;
	setAttr ".uvtk[1768]" -type "float2" 0.0022629052 -0.010135561 ;
	setAttr ".uvtk[1769]" -type "float2" 0.0024179518 -0.012331873 ;
	setAttr ".uvtk[1770]" -type "float2" 0.0019899309 -0.010591596 ;
	setAttr ".uvtk[1771]" -type "float2" 0.0018530414 -0.008671999 ;
	setAttr ".uvtk[1772]" -type "float2" 0.0015522242 -0.0071241558 ;
	setAttr ".uvtk[1773]" -type "float2" 0.0016203262 -0.0087099075 ;
	setAttr ".uvtk[1774]" -type "float2" 0.001722984 -0.012415528 ;
	setAttr ".uvtk[1775]" -type "float2" 0.0016903281 -0.010258466 ;
	setAttr ".uvtk[1776]" -type "float2" 0.0060520321 -0.0099566579 ;
	setAttr ".uvtk[1777]" -type "float2" 0.0066251755 -0.01334092 ;
	setAttr ".uvtk[1778]" -type "float2" 0.005823046 -0.012607843 ;
	setAttr ".uvtk[1779]" -type "float2" 0.0053444058 -0.0093272924 ;
	setAttr ".uvtk[1780]" -type "float2" 0.0041833073 -0.010928214 ;
	setAttr ".uvtk[1781]" -type "float2" 0.003297925 -0.0099409223 ;
	setAttr ".uvtk[1782]" -type "float2" 0.0030870885 -0.0071212649 ;
	setAttr ".uvtk[1783]" -type "float2" 0.0038592666 -0.0079204738 ;
	setAttr ".uvtk[1784]" -type "float2" 0.0031222701 -0.0019578934 ;
	setAttr ".uvtk[1785]" -type "float2" 0.0035503209 -0.004948765 ;
	setAttr ".uvtk[1786]" -type "float2" 0.002763249 -0.0043046474 ;
	setAttr ".uvtk[1787]" -type "float2" 0.0024669915 -0.0014415383 ;
	setAttr ".uvtk[1788]" -type "float2" 0.0018620864 -0.00092381239 ;
	setAttr ".uvtk[1789]" -type "float2" 0.0021584183 -0.0036228299 ;
	setAttr ".uvtk[1790]" -type "float2" 0.002641663 -0.0088646114 ;
	setAttr ".uvtk[1791]" -type "float2" 0.0024116337 -0.0062342882 ;
	setAttr ".uvtk[1792]" -type "float2" 0.0046433806 -0.0033767819 ;
	setAttr ".uvtk[1793]" -type "float2" 0.0053975731 -0.0066444576 ;
	setAttr ".uvtk[1794]" -type "float2" 0.00480555 -0.006123364 ;
	setAttr ".uvtk[1795]" -type "float2" 0.0041346103 -0.0029264092 ;
	setAttr ".uvtk[1796]" -type "float2" 0.0042315125 -0.0055500567 ;
	setAttr ".uvtk[1797]" -type "float2" 0.0037492365 -0.0024527609 ;
	setAttr ".uvtk[1798]" -type "float2" 0.0050733984 -0.011804163 ;
	setAttr ".uvtk[1799]" -type "float2" 0.0047322363 -0.0086542368 ;
	setAttr ".uvtk[1800]" -type "float2" 0.0045852065 -0.023071885 ;
	setAttr ".uvtk[1801]" -type "float2" 0.0037094578 -0.021397531 ;
	setAttr ".uvtk[1802]" -type "float2" 0.0036727414 -0.01806137 ;
	setAttr ".uvtk[1803]" -type "float2" 0.0045509189 -0.019554943 ;
	setAttr ".uvtk[1804]" -type "float2" 0.0043606013 -0.013911158 ;
	setAttr ".uvtk[1805]" -type "float2" 0.0044725984 -0.016719431 ;
	setAttr ".uvtk[1806]" -type "float2" 0.0036415011 -0.015383005 ;
	setAttr ".uvtk[1807]" -type "float2" 0.0034896582 -0.012752891 ;
	setAttr ".uvtk[1808]" -type "float2" 0.0028386489 -0.011486292 ;
	setAttr ".uvtk[1809]" -type "float2" 0.0029460266 -0.013930708 ;
	setAttr ".uvtk[1810]" -type "float2" 0.0030510202 -0.019541383 ;
	setAttr ".uvtk[1811]" -type "float2" 0.0030424744 -0.016405344 ;
	setAttr ".uvtk[1812]" -type "float2" 0.0071892142 -0.016733795 ;
	setAttr ".uvtk[1813]" -type "float2" 0.0075222254 -0.019927949 ;
	setAttr ".uvtk[1814]" -type "float2" 0.0065123737 -0.018984526 ;
	setAttr ".uvtk[1815]" -type "float2" 0.0061675757 -0.01588884 ;
	setAttr ".uvtk[1816]" -type "float2" 0.0054635108 -0.017916769 ;
	setAttr ".uvtk[1817]" -type "float2" 0.0053338557 -0.014954776 ;
	setAttr ".uvtk[1818]" -type "float2" 0.005627349 -0.024554849 ;
	setAttr ".uvtk[1819]" -type "float2" 0.0056089461 -0.020912319 ;
	setAttr ".uvtk[1820]" -type "float2" 0.06703797 -0.0013953596 ;
	setAttr ".uvtk[1821]" -type "float2" 0.071926087 -0.0030213743 ;
	setAttr ".uvtk[1822]" -type "float2" 0.067841321 -0.0029971749 ;
	setAttr ".uvtk[1823]" -type "float2" 0.062853545 -0.0013735592 ;
	setAttr ".uvtk[1824]" -type "float2" 0.02184388 -0.0009162724 ;
	setAttr ".uvtk[1825]" -type "float2" 0.024802625 -0.0024404526 ;
	setAttr ".uvtk[1826]" -type "float2" 0.017269224 -0.0022544712 ;
	setAttr ".uvtk[1827]" -type "float2" 0.014706254 -0.00075888634 ;
	setAttr ".uvtk[1828]" -type "float2" 0.0072797239 -0.00046376884 ;
	setAttr ".uvtk[1829]" -type "float2" 0.011222869 -0.00041352957 ;
	setAttr ".uvtk[1830]" -type "float2" 0.0063917041 -0.00033067912 ;
	setAttr ".uvtk[1831]" -type "float2" 0.0037434697 -0.00039759278 ;
	setAttr ".uvtk[1832]" -type "float2" -0.00016874075 -0.00027567148 ;
	setAttr ".uvtk[1833]" -type "float2" 0.00094628334 -0.00014390796 ;
	setAttr ".uvtk[1834]" -type "float2" 0.00052478909 -3.606081e-05 ;
	setAttr ".uvtk[1835]" -type "float2" -0.00013247132 -0.00021054596 ;
	setAttr ".uvtk[1838]" -type "float2" -0.00060999393 -0.00026671588 ;
	setAttr ".uvtk[1839]" -type "float2" -0.00041036308 -0.00023014471 ;
	setAttr ".uvtk[1841]" -type "float2" -1.2040138e-05 -0.00020455196 ;
	setAttr ".uvtk[1842]" -type "float2" 0.00076778233 7.2561204e-05 ;
	setAttr ".uvtk[1843]" -type "float2" 0.00023049116 -0.00014719367 ;
	setAttr ".uvtk[1846]" -type "float2" 0.0034498274 -0.00035785884 ;
	setAttr ".uvtk[1847]" -type "float2" 0.0014310181 -0.00031942874 ;
	setAttr ".uvtk[1849]" -type "float2" -4.3272972e-05 -0.00029350817 ;
	setAttr ".uvtk[1850]" -type "float2" 0.0028165281 -0.00023953617 ;
	setAttr ".uvtk[1851]" -type "float2" 0.0010972321 -0.00033706427 ;
	setAttr ".uvtk[1852]" -type "float2" 0.0071266592 -0.0018025637 ;
	setAttr ".uvtk[1853]" -type "float2" 0.0051599741 -0.0015127808 ;
	setAttr ".uvtk[1854]" -type "float2" 0.0039767921 -0.00015294552 ;
	setAttr ".uvtk[1855]" -type "float2" 0.0055089742 -0.00038652122 ;
	setAttr ".uvtk[1856]" -type "float2" 0.0023051947 -1.0386109e-05 ;
	setAttr ".uvtk[1857]" -type "float2" 0.003932476 2.9921532e-05 ;
	setAttr ".uvtk[1858]" -type "float2" 0.0026798099 0.00021889806 ;
	setAttr ".uvtk[1859]" -type "float2" 0.0015449971 0.00013546646 ;
	setAttr ".uvtk[1860]" -type "float2" 0.0014712811 0.00029741228 ;
	setAttr ".uvtk[1861]" -type "float2" 0.0024312735 0.00043292344 ;
	setAttr ".uvtk[1862]" -type "float2" 0.0044146329 -0.0011809468 ;
	setAttr ".uvtk[1863]" -type "float2" 0.0033963025 0.00012783706 ;
	setAttr ".uvtk[1864]" -type "float2" 0.014959991 -0.00036200881 ;
	setAttr ".uvtk[1865]" -type "float2" 0.018558919 -0.00040343404 ;
	setAttr ".uvtk[1866]" -type "float2" 0.012041718 -0.00027827919 ;
	setAttr ".uvtk[1867]" -type "float2" 0.0092095733 -0.00025714934 ;
	setAttr ".uvtk[1868]" -type "float2" 0.0069899708 -0.00013296306 ;
	setAttr ".uvtk[1869]" -type "float2" 0.0048775524 -0.00014421344 ;
	setAttr ".uvtk[1870]" -type "float2" 0.011094868 -0.0020409971 ;
	setAttr ".uvtk[1871]" -type "float2" 0.0091763288 -0.00058464706 ;
	setAttr ".uvtk[1872]" -type "float2" 0.032791674 -0.00060943514 ;
	setAttr ".uvtk[1873]" -type "float2" 0.044218689 -0.00063422322 ;
	setAttr ".uvtk[1874]" -type "float2" 0.040807605 -0.0006339848 ;
	setAttr ".uvtk[1875]" -type "float2" 0.029639453 -0.00061193109 ;
	setAttr ".uvtk[1876]" -type "float2" 0.021469742 -0.00059081614 ;
	setAttr ".uvtk[1877]" -type "float2" 0.030029267 -0.00059971213 ;
	setAttr ".uvtk[1878]" -type "float2" 0.023673743 -0.0005556196 ;
	setAttr ".uvtk[1879]" -type "float2" 0.016810149 -0.00056361407 ;
	setAttr ".uvtk[1882]" -type "float2" 0.011644751 -0.00042361766 ;
	setAttr ".uvtk[1883]" -type "float2" 0.0089119375 -0.00041215122 ;
	setAttr ".uvtk[1885]" -type "float2" 0.0059393793 -0.00039311126 ;
	setAttr ".uvtk[1886]" -type "float2" 0.017027244 -0.00049269199 ;
	setAttr ".uvtk[1887]" -type "float2" 0.011639789 -0.00052122772 ;
	setAttr ".uvtk[1890]" -type "float2" 0.018420905 -0.00043263659 ;
	setAttr ".uvtk[1891]" -type "float2" 0.016363233 -0.00043498725 ;
	setAttr ".uvtk[1893]" -type "float2" 0.013506591 -0.00043253973 ;
	setAttr ".uvtk[1894]" -type "float2" 0.034912676 -0.0006249994 ;
	setAttr ".uvtk[1895]" -type "float2" 0.025233835 -0.0006069541 ;
	setAttr ".uvtk[1896]" -type "float2" 0.052636206 -0.0028615594 ;
	setAttr ".uvtk[1897]" -type "float2" 0.043656588 -0.0027426183 ;
	setAttr ".uvtk[1898]" -type "float2" 0.039649934 -0.0011718571 ;
	setAttr ".uvtk[1899]" -type "float2" 0.048303336 -0.0012684315 ;
	setAttr ".uvtk[1900]" -type "float2" 0.037172467 -0.00060121715 ;
	setAttr ".uvtk[1901]" -type "float2" 0.0432657 -0.00069977343 ;
	setAttr ".uvtk[1902]" -type "float2" 0.035042614 -0.00061716139 ;
	setAttr ".uvtk[1903]" -type "float2" 0.029748678 -0.00053961575 ;
	setAttr ".uvtk[1904]" -type "float2" 0.021994278 -0.00045631826 ;
	setAttr ".uvtk[1905]" -type "float2" 0.026402429 -0.00051677227 ;
	setAttr ".uvtk[1906]" -type "float2" 0.033805683 -0.0026038289 ;
	setAttr ".uvtk[1907]" -type "float2" 0.030378446 -0.0010491759 ;
	setAttr ".uvtk[1908]" -type "float2" 0.053627998 -0.0006596446 ;
	setAttr ".uvtk[1909]" -type "float2" 0.061044514 -0.00078879297 ;
	setAttr ".uvtk[1910]" -type "float2" 0.056961685 -0.00077325106 ;
	setAttr ".uvtk[1911]" -type "float2" 0.049695611 -0.00065355003 ;
	setAttr ".uvtk[1912]" -type "float2" 0.049988002 -0.00074733794 ;
	setAttr ".uvtk[1913]" -type "float2" 0.043235004 -0.0006377548 ;
	setAttr ".uvtk[1914]" -type "float2" 0.060183346 -0.0029449761 ;
	setAttr ".uvtk[1915]" -type "float2" 0.055695325 -0.0013343543 ;
	setAttr ".uvtk[1916]" -type "float2" 0.035840303 -0.031200588 ;
	setAttr ".uvtk[1917]" -type "float2" 0.026752725 -0.030743003 ;
	setAttr ".uvtk[1918]" -type "float2" 0.026506737 -0.026669532 ;
	setAttr ".uvtk[1919]" -type "float2" 0.035511136 -0.027104586 ;
	setAttr ".uvtk[1920]" -type "float2" 0.031623453 -0.012535423 ;
	setAttr ".uvtk[1921]" -type "float2" 0.0331209 -0.016291589 ;
	setAttr ".uvtk[1922]" -type "float2" 0.024286404 -0.015961796 ;
	setAttr ".uvtk[1923]" -type "float2" 0.022933647 -0.0122343 ;
	setAttr ".uvtk[1924]" -type "float2" 0.010879397 -0.011487722 ;
	setAttr ".uvtk[1925]" -type "float2" 0.011865541 -0.01510945 ;
	setAttr ".uvtk[1926]" -type "float2" 0.0091996789 -0.014577925 ;
	setAttr ".uvtk[1927]" -type "float2" 0.0083740652 -0.011026293 ;
	setAttr ".uvtk[1928]" -type "float2" 0.0084907711 -0.0045188665 ;
	setAttr ".uvtk[1929]" -type "float2" 0.009806335 -0.0079743564 ;
	setAttr ".uvtk[1930]" -type "float2" 0.0074569732 -0.0075685382 ;
	setAttr ".uvtk[1931]" -type "float2" 0.006373927 -0.0041783452 ;
	setAttr ".uvtk[1932]" -type "float2" 0.0054133981 -0.0037875175 ;
	setAttr ".uvtk[1933]" -type "float2" 0.0062881112 -0.0071282387 ;
	setAttr ".uvtk[1934]" -type "float2" 0.0070901215 -0.010515273 ;
	setAttr ".uvtk[1935]" -type "float2" 0.0077821016 -0.01399821 ;
	setAttr ".uvtk[1936]" -type "float2" 0.027545631 -0.0052858293 ;
	setAttr ".uvtk[1937]" -type "float2" 0.029708326 -0.0088739991 ;
	setAttr ".uvtk[1938]" -type "float2" 0.021405384 -0.0086148679 ;
	setAttr ".uvtk[1939]" -type "float2" 0.019411847 -0.0050621927 ;
	setAttr ".uvtk[1940]" -type "float2" 0.012994602 -0.004812181 ;
	setAttr ".uvtk[1941]" -type "float2" 0.014486402 -0.0083191693 ;
	setAttr ".uvtk[1942]" -type "float2" 0.016862243 -0.015565395 ;
	setAttr ".uvtk[1943]" -type "float2" 0.015825748 -0.011894286 ;
	setAttr ".uvtk[1944]" -type "float2" 0.014272839 -0.02955538 ;
	setAttr ".uvtk[1945]" -type "float2" 0.0113139 -0.028814197 ;
	setAttr ".uvtk[1946]" -type "float2" 0.011110082 -0.024860173 ;
	setAttr ".uvtk[1947]" -type "float2" 0.013871923 -0.025548786 ;
	setAttr ".uvtk[1948]" -type "float2" 0.012753904 -0.018731803 ;
	setAttr ".uvtk[1949]" -type "float2" 0.013378814 -0.022154003 ;
	setAttr ".uvtk[1950]" -type "float2" 0.010598391 -0.021508038 ;
	setAttr ".uvtk[1951]" -type "float2" 0.0099585205 -0.018149346 ;
	setAttr ".uvtk[1952]" -type "float2" 0.008361131 -0.01748094 ;
	setAttr ".uvtk[1953]" -type "float2" 0.0089834481 -0.020774215 ;
	setAttr ".uvtk[1954]" -type "float2" 0.0095833838 -0.027962744 ;
	setAttr ".uvtk[1955]" -type "float2" 0.0093729645 -0.024058282 ;
	setAttr ".uvtk[1956]" -type "float2" 0.034325123 -0.020053446 ;
	setAttr ".uvtk[1957]" -type "float2" 0.035057485 -0.023590326 ;
	setAttr ".uvtk[1958]" -type "float2" 0.025924876 -0.023196638 ;
	setAttr ".uvtk[1959]" -type "float2" 0.025225893 -0.019685417 ;
	setAttr ".uvtk[1960]" -type "float2" 0.017794818 -0.019251466 ;
	setAttr ".uvtk[1961]" -type "float2" 0.018589199 -0.022712916 ;
	setAttr ".uvtk[1962]" -type "float2" 0.019352302 -0.030188859 ;
	setAttr ".uvtk[1963]" -type "float2" 0.019072443 -0.026156008 ;
	setAttr ".uvtk[1964]" -type "float2" 0.081915915 -0.013392657 ;
	setAttr ".uvtk[1965]" -type "float2" 0.084200621 -0.017227471 ;
	setAttr ".uvtk[1966]" -type "float2" 0.079946965 -0.01718536 ;
	setAttr ".uvtk[1967]" -type "float2" 0.077661812 -0.013354748 ;
	setAttr ".uvtk[1968]" -type "float2" 0.064134628 -0.016970575 ;
	setAttr ".uvtk[1969]" -type "float2" 0.054179251 -0.016795069 ;
	setAttr ".uvtk[1970]" -type "float2" 0.052301049 -0.012994677 ;
	setAttr ".uvtk[1971]" -type "float2" 0.061965227 -0.013154477 ;
	setAttr ".uvtk[1972]" -type "float2" 0.05637002 -0.0057760477 ;
	setAttr ".uvtk[1973]" -type "float2" 0.059419543 -0.0094360411 ;
	setAttr ".uvtk[1974]" -type "float2" 0.049768597 -0.0092844069 ;
	setAttr ".uvtk[1975]" -type "float2" 0.046966553 -0.0056422949 ;
	setAttr ".uvtk[1976]" -type "float2" 0.036793247 -0.0054771602 ;
	setAttr ".uvtk[1977]" -type "float2" 0.03954713 -0.0091017485 ;
	setAttr ".uvtk[1978]" -type "float2" 0.043269977 -0.016571522 ;
	setAttr ".uvtk[1979]" -type "float2" 0.04160662 -0.012787282 ;
	setAttr ".uvtk[1980]" -type "float2" 0.076050401 -0.0059670508 ;
	setAttr ".uvtk[1981]" -type "float2" 0.07919839 -0.0096499026 ;
	setAttr ".uvtk[1982]" -type "float2" 0.074833751 -0.0096186697 ;
	setAttr ".uvtk[1983]" -type "float2" 0.071622491 -0.0059379637 ;
	setAttr ".uvtk[1984]" -type "float2" 0.067202866 -0.009547323 ;
	setAttr ".uvtk[1985]" -type "float2" 0.063930362 -0.0058779418 ;
	setAttr ".uvtk[1986]" -type "float2" 0.072128892 -0.017098337 ;
	setAttr ".uvtk[1987]" -type "float2" 0.069945902 -0.013276696 ;
	setAttr ".uvtk[1988]" -type "float2" 0.067936063 -0.032110572 ;
	setAttr ".uvtk[1989]" -type "float2" 0.057589978 -0.031874895 ;
	setAttr ".uvtk[1990]" -type "float2" 0.057327867 -0.027735263 ;
	setAttr ".uvtk[1991]" -type "float2" 0.067614377 -0.027955592 ;
	setAttr ".uvtk[1992]" -type "float2" 0.065695077 -0.020785362 ;
	setAttr ".uvtk[1993]" -type "float2" 0.066918612 -0.024387389 ;
	setAttr ".uvtk[1994]" -type "float2" 0.056612313 -0.024180859 ;
	setAttr ".uvtk[1995]" -type "float2" 0.055609733 -0.020598024 ;
	setAttr ".uvtk[1996]" -type "float2" 0.044578359 -0.020353407 ;
	setAttr ".uvtk[1997]" -type "float2" 0.045576766 -0.023919523 ;
	setAttr ".uvtk[1998]" -type "float2" 0.046309277 -0.031577885 ;
	setAttr ".uvtk[1999]" -type "float2" 0.046087131 -0.027452201 ;
	setAttr ".uvtk[2000]" -type "float2" 0.085950881 -0.021064848 ;
	setAttr ".uvtk[2001]" -type "float2" 0.087196052 -0.024690241 ;
	setAttr ".uvtk[2002]" -type "float2" 0.083018601 -0.02463907 ;
	setAttr ".uvtk[2003]" -type "float2" 0.081668079 -0.021018445 ;
	setAttr ".uvtk[2004]" -type "float2" 0.075140566 -0.024537295 ;
	setAttr ".uvtk[2005]" -type "float2" 0.073910236 -0.020926625 ;
	setAttr ".uvtk[2006]" -type "float2" 0.07630676 -0.032289088 ;
	setAttr ".uvtk[2007]" -type "float2" 0.076001406 -0.028122663 ;
	setAttr ".uvtk[2010]" -type "float2" 0.0002464205 -0.0062214136 ;
	setAttr ".uvtk[2011]" -type "float2" 0.00014702976 -0.0064095259 ;
	setAttr ".uvtk[2012]" -type "float2" 0.0053975731 -0.03940016 ;
	setAttr ".uvtk[2013]" -type "float2" 0.0046433806 -0.038254023 ;
	setAttr ".uvtk[2014]" -type "float2" 0.0041346103 -0.037040353 ;
	setAttr ".uvtk[2015]" -type "float2" 0.00480555 -0.03810668 ;
	setAttr ".uvtk[2016]" -type "float2" 0.001654692 -0.027535141 ;
	setAttr ".uvtk[2017]" -type "float2" 0.0014627352 -0.026902556 ;
	setAttr ".uvtk[2018]" -type "float2" 0.0011956319 -0.023791075 ;
	setAttr ".uvtk[2019]" -type "float2" 0.0013623163 -0.0243029 ;
	setAttr ".uvtk[2020]" -type "float2" 0.0024179518 -0.030946791 ;
	setAttr ".uvtk[2021]" -type "float2" 0.0022629052 -0.031863987 ;
	setAttr ".uvtk[2022]" -type "float2" 0.0018530414 -0.02856499 ;
	setAttr ".uvtk[2023]" -type "float2" 0.0019899309 -0.027769566 ;
	setAttr ".uvtk[2024]" -type "float2" 0.0012196079 -0.019682884 ;
	setAttr ".uvtk[2025]" -type "float2" 0.0011680312 -0.020137668 ;
	setAttr ".uvtk[2026]" -type "float2" 0.00078514591 -0.014755309 ;
	setAttr ".uvtk[2027]" -type "float2" 0.00075806491 -0.014544427 ;
	setAttr ".uvtk[2028]" -type "float2" 0.0012355447 -0.012573361 ;
	setAttr ".uvtk[2029]" -type "float2" 0.0012140609 -0.01672256 ;
	setAttr ".uvtk[2030]" -type "float2" 0.00077883154 -0.012095869 ;
	setAttr ".uvtk[2031]" -type "float2" 0.00076255947 -0.0086597204 ;
	setAttr ".uvtk[2034]" -type "float2" 0.00038132537 -0.0044124722 ;
	setAttr ".uvtk[2035]" -type "float2" 0.0003718771 -0.0066409111 ;
	setAttr ".uvtk[2038]" -type "float2" 0.00038758945 -0.0082095265 ;
	setAttr ".uvtk[2039]" -type "float2" 0.00037998985 -0.0082490444 ;
	setAttr ".uvtk[2040]" -type "float2" 0.0025462806 -0.021779358 ;
	setAttr ".uvtk[2041]" -type "float2" 0.0025023147 -0.027091801 ;
	setAttr ".uvtk[2042]" -type "float2" 0.0020884201 -0.024130344 ;
	setAttr ".uvtk[2043]" -type "float2" 0.0021363646 -0.019092023 ;
	setAttr ".uvtk[2044]" -type "float2" 0.001722984 -0.016042352 ;
	setAttr ".uvtk[2045]" -type "float2" 0.0016903281 -0.020702839 ;
	setAttr ".uvtk[2046]" -type "float2" 0.0015522242 -0.024707198 ;
	setAttr ".uvtk[2047]" -type "float2" 0.0016203262 -0.024060488 ;
	setAttr ".uvtk[2048]" -type "float2" 0.0010258108 -0.016233861 ;
	setAttr ".uvtk[2049]" -type "float2" 0.00095874071 -0.016024411 ;
	setAttr ".uvtk[2050]" -type "float2" 0.00073384494 -0.011255562 ;
	setAttr ".uvtk[2051]" -type "float2" 0.00075664185 -0.011304379 ;
	setAttr ".uvtk[2052]" -type "float2" 0.0011466555 -0.018816173 ;
	setAttr ".uvtk[2053]" -type "float2" 0.0010745712 -0.017226636 ;
	setAttr ".uvtk[2054]" -type "float2" 0.00077654421 -0.01207298 ;
	setAttr ".uvtk[2055]" -type "float2" 0.00076494738 -0.01347363 ;
	setAttr ".uvtk[2058]" -type "float2" 0.00040456094 -0.0072749257 ;
	setAttr ".uvtk[2059]" -type "float2" 0.00040728878 -0.0062920451 ;
	setAttr ".uvtk[2062]" -type "float2" 0.00042335875 -0.0058117509 ;
	setAttr ".uvtk[2063]" -type "float2" 0.00041157473 -0.0058831573 ;
	setAttr ".uvtk[2064]" -type "float2" 0.0020987466 -0.030615151 ;
	setAttr ".uvtk[2065]" -type "float2" 0.0018752441 -0.028834224 ;
	setAttr ".uvtk[2066]" -type "float2" 0.0015293434 -0.025530279 ;
	setAttr ".uvtk[2067]" -type "float2" 0.0016999319 -0.027274132 ;
	setAttr ".uvtk[2068]" -type "float2" 0.0014272444 -0.023380876 ;
	setAttr ".uvtk[2069]" -type "float2" 0.0013271719 -0.021682203 ;
	setAttr ".uvtk[2070]" -type "float2" 0.0010852143 -0.020201921 ;
	setAttr ".uvtk[2071]" -type "float2" 0.001201313 -0.020565927 ;
	setAttr ".uvtk[2072]" -type "float2" 0.0075222254 -0.042828918 ;
	setAttr ".uvtk[2073]" -type "float2" 0.0071892142 -0.044105649 ;
	setAttr ".uvtk[2074]" -type "float2" 0.0061675757 -0.042745948 ;
	setAttr ".uvtk[2075]" -type "float2" 0.0065123737 -0.041497707 ;
	setAttr ".uvtk[2076]" -type "float2" 0.0044725984 -0.038148105 ;
	setAttr ".uvtk[2077]" -type "float2" 0.0043606013 -0.039309621 ;
	setAttr ".uvtk[2078]" -type "float2" 0.0034896582 -0.037171185 ;
	setAttr ".uvtk[2079]" -type "float2" 0.0036415011 -0.036081314 ;
	setAttr ".uvtk[2080]" -type "float2" 0.0045852065 -0.028034687 ;
	setAttr ".uvtk[2081]" -type "float2" 0.0045509189 -0.03389883 ;
	setAttr ".uvtk[2082]" -type "float2" 0.0036727414 -0.03193444 ;
	setAttr ".uvtk[2083]" -type "float2" 0.0037094578 -0.026218355 ;
	setAttr ".uvtk[2084]" -type "float2" 0.0030510202 -0.024137795 ;
	setAttr ".uvtk[2085]" -type "float2" 0.0030424744 -0.029685795 ;
	setAttr ".uvtk[2086]" -type "float2" 0.0028386489 -0.034708977 ;
	setAttr ".uvtk[2087]" -type "float2" 0.0029460266 -0.033701062 ;
	setAttr ".uvtk[2088]" -type "float2" 0.0080622286 -0.032246053 ;
	setAttr ".uvtk[2089]" -type "float2" 0.0078765005 -0.038361251 ;
	setAttr ".uvtk[2090]" -type "float2" 0.0066321343 -0.037098348 ;
	setAttr ".uvtk[2091]" -type "float2" 0.0067503899 -0.03104192 ;
	setAttr ".uvtk[2092]" -type "float2" 0.005627349 -0.029649317 ;
	setAttr ".uvtk[2093]" -type "float2" 0.0056089461 -0.035611033 ;
	setAttr ".uvtk[2094]" -type "float2" 0.0053338557 -0.041153669 ;
	setAttr ".uvtk[2095]" -type "float2" 0.0054635108 -0.039952099 ;
	setAttr ".uvtk[2096]" -type "float2" 0.0031222701 -0.033855975 ;
	setAttr ".uvtk[2097]" -type "float2" 0.0024669915 -0.031854868 ;
	setAttr ".uvtk[2098]" -type "float2" 0.002763249 -0.032706082 ;
	setAttr ".uvtk[2099]" -type "float2" 0.0035503209 -0.034772217 ;
	setAttr ".uvtk[2100]" -type "float2" 0.0041833073 -0.038119793 ;
	setAttr ".uvtk[2101]" -type "float2" 0.0038592666 -0.036269248 ;
	setAttr ".uvtk[2102]" -type "float2" 0.0030870885 -0.034142554 ;
	setAttr ".uvtk[2103]" -type "float2" 0.003297925 -0.035974741 ;
	setAttr ".uvtk[2104]" -type "float2" 0.002641663 -0.033491611 ;
	setAttr ".uvtk[2105]" -type "float2" 0.0024116337 -0.031692088 ;
	setAttr ".uvtk[2106]" -type "float2" 0.0018620864 -0.029585004 ;
	setAttr ".uvtk[2107]" -type "float2" 0.0021584183 -0.030321121 ;
	setAttr ".uvtk[2108]" -type "float2" 0.0066251755 -0.042955875 ;
	setAttr ".uvtk[2109]" -type "float2" 0.0060520321 -0.041035473 ;
	setAttr ".uvtk[2110]" -type "float2" 0.0053444058 -0.039701104 ;
	setAttr ".uvtk[2111]" -type "float2" 0.005823046 -0.041593015 ;
	setAttr ".uvtk[2112]" -type "float2" 0.0047322363 -0.038119733 ;
	setAttr ".uvtk[2113]" -type "float2" 0.0050733984 -0.039994597 ;
	setAttr ".uvtk[2114]" -type "float2" 0.0037492365 -0.035583496 ;
	setAttr ".uvtk[2115]" -type "float2" 0.0042315125 -0.036581039 ;
	setAttr ".uvtk[2118]" -type "float2" 0.00014467537 -0.0041416883 ;
	setAttr ".uvtk[2119]" -type "float2" 0.00018405914 -0.0046566725 ;
	setAttr ".uvtk[2120]" -type "float2" 0.00089792162 -0.023027956 ;
	setAttr ".uvtk[2121]" -type "float2" 0.0007288456 -0.019403398 ;
	setAttr ".uvtk[2122]" -type "float2" 0.00057950616 -0.017439723 ;
	setAttr ".uvtk[2123]" -type "float2" 0.00074308366 -0.020679951 ;
	setAttr ".uvtk[2124]" -type "float2" 0.00064533576 -0.014599979 ;
	setAttr ".uvtk[2125]" -type "float2" 0.00050295144 -0.012315929 ;
	setAttr ".uvtk[2126]" -type "float2" 0.00040671229 -0.008995235 ;
	setAttr ".uvtk[2127]" -type "float2" 0.00051218271 -0.010674179 ;
	setAttr ".uvtk[2128]" -type "float2" 0.00087277591 -0.0160833 ;
	setAttr ".uvtk[2129]" -type "float2" 0.00076484308 -0.015768886 ;
	setAttr ".uvtk[2130]" -type "float2" 0.00060979463 -0.011425376 ;
	setAttr ".uvtk[2131]" -type "float2" 0.0006776154 -0.011476696 ;
	setAttr ".uvtk[2134]" -type "float2" 0.0003905734 -0.0061655045 ;
	setAttr ".uvtk[2135]" -type "float2" 0.00034913141 -0.006278336 ;
	setAttr ".uvtk[2138]" -type "float2" 0.00029803719 -0.0059351921 ;
	setAttr ".uvtk[2139]" -type "float2" 0.00023030117 -0.0049821138 ;
	setAttr ".uvtk[2140]" -type "float2" 0.0012438074 -0.026383758 ;
	setAttr ".uvtk[2141]" -type "float2" 0.001074262 -0.025265992 ;
	setAttr ".uvtk[2142]" -type "float2" 0.00089667737 -0.022598922 ;
	setAttr ".uvtk[2143]" -type "float2" 0.0010389835 -0.023453057 ;
	setAttr ".uvtk[2144]" -type "float2" 0.00097760931 -0.020056129 ;
	setAttr ".uvtk[2145]" -type "float2" 0.0008379519 -0.019457459 ;
	setAttr ".uvtk[2146]" -type "float2" 0.00054655597 -0.015112579 ;
	setAttr ".uvtk[2147]" -type "float2" 0.00069005787 -0.017897487 ;
	setAttr ".uvtk[2150]" -type "float2" 9.1053545e-05 -0.0019116998 ;
	setAttr ".uvtk[2151]" -type "float2" 0.00011718273 -0.0027784705 ;
	setAttr ".uvtk[2152]" -type "float2" 0.00036499649 -0.0092684031 ;
	setAttr ".uvtk[2153]" -type "float2" 0.00022619218 -0.0059913397 ;
	setAttr ".uvtk[2154]" -type "float2" 0.000190394 -0.0042405725 ;
	setAttr ".uvtk[2155]" -type "float2" 0.00028375722 -0.0067020655 ;
	setAttr ".uvtk[2158]" -type "float2" 0.00017608609 -0.0036395192 ;
	setAttr ".uvtk[2159]" -type "float2" 0.00010691583 -0.0022366643 ;
	setAttr ".uvtk[2163]" -type "float2" 5.3885393e-05 -0.00095546246 ;
	setAttr ".uvtk[2164]" -type "float2" 0.00053761154 -0.014760971 ;
	setAttr ".uvtk[2165]" -type "float2" 0.0003650263 -0.0097025633 ;
	setAttr ".uvtk[2166]" -type "float2" 0.00028257817 -0.0086758137 ;
	setAttr ".uvtk[2167]" -type "float2" 0.00043280423 -0.013241827 ;
	setAttr ".uvtk[2168]" -type "float2" 0.00039538741 -0.011441767 ;
	setAttr ".uvtk[2169]" -type "float2" 0.00025738403 -0.0074521899 ;
	setAttr ".uvtk[2171]" -type "float2" 0.00011880696 -0.0035207868 ;
	setAttr ".uvtk[2172]" -type "float2" 0.0021766871 -0.031511009 ;
	setAttr ".uvtk[2173]" -type "float2" 0.0014683455 -0.026396215 ;
	setAttr ".uvtk[2174]" -type "float2" 0.0014793575 -0.025647104 ;
	setAttr ".uvtk[2175]" -type "float2" 0.002080068 -0.030611873 ;
	setAttr ".uvtk[2176]" -type "float2" 0.0018208623 -0.02826798 ;
	setAttr ".uvtk[2177]" -type "float2" 0.0014014542 -0.023715794 ;
	setAttr ".uvtk[2178]" -type "float2" 0.0011868104 -0.022498667 ;
	setAttr ".uvtk[2179]" -type "float2" 0.0014745668 -0.026791751 ;
	setAttr ".uvtk[2180]" -type "float2" 0.002709657 -0.032930434 ;
	setAttr ".uvtk[2181]" -type "float2" 0.0022113174 -0.03126061 ;
	setAttr ".uvtk[2182]" -type "float2" 0.0018187538 -0.029549241 ;
	setAttr ".uvtk[2183]" -type "float2" 0.0020986572 -0.031055212 ;
	setAttr ".uvtk[2184]" -type "float2" 0.0016558841 -0.02888602 ;
	setAttr ".uvtk[2185]" -type "float2" 0.0013882518 -0.027556837 ;
	setAttr ".uvtk[2186]" -type "float2" 0.0011758432 -0.025057435 ;
	setAttr ".uvtk[2187]" -type "float2" 0.0009368211 -0.021084428 ;
	setAttr ".uvtk[2188]" -type "float2" 0.0038489252 -0.037028372 ;
	setAttr ".uvtk[2189]" -type "float2" 0.0029909015 -0.034966528 ;
	setAttr ".uvtk[2190]" -type "float2" 0.0027540624 -0.033941627 ;
	setAttr ".uvtk[2191]" -type "float2" 0.0034508109 -0.035895705 ;
	setAttr ".uvtk[2192]" -type "float2" 0.0031511486 -0.03454572 ;
	setAttr ".uvtk[2193]" -type "float2" 0.0025915504 -0.032718599 ;
	setAttr ".uvtk[2194]" -type "float2" 0.0015150607 -0.024755836 ;
	setAttr ".uvtk[2195]" -type "float2" 0.0020128936 -0.029543281 ;
	setAttr ".uvtk[2198]" -type "float2" 0.00029668212 -0.0054389238 ;
	setAttr ".uvtk[2199]" -type "float2" 0.00032575428 -0.0057287216 ;
	setAttr ".uvtk[2200]" -type "float2" 0.0010270476 -0.0180161 ;
	setAttr ".uvtk[2201]" -type "float2" 0.00066557527 -0.011863947 ;
	setAttr ".uvtk[2202]" -type "float2" 0.00058892369 -0.011266291 ;
	setAttr ".uvtk[2203]" -type "float2" 0.0008725822 -0.017096579 ;
	setAttr ".uvtk[2204]" -type "float2" 0.00071880221 -0.016034544 ;
	setAttr ".uvtk[2205]" -type "float2" 0.00047452748 -0.01055795 ;
	setAttr ".uvtk[2207]" -type "float2" 0.00024340302 -0.0050822496 ;
	setAttr ".uvtk[2208]" -type "float2" 0.00085698068 -0.02007091 ;
	setAttr ".uvtk[2209]" -type "float2" 0.00043551624 -0.013238311 ;
	setAttr ".uvtk[2210]" -type "float2" 0.00055496395 -0.012859046 ;
	setAttr ".uvtk[2211]" -type "float2" 0.00097110868 -0.019493103 ;
	setAttr ".uvtk[2212]" -type "float2" 0.0010650009 -0.018809497 ;
	setAttr ".uvtk[2213]" -type "float2" 0.00066515803 -0.012399018 ;
	setAttr ".uvtk[2215]" -type "float2" 0.00031378865 -0.006002605 ;
	setAttr ".uvtk[2216]" -type "float2" 0.07919839 -0.045342803 ;
	setAttr ".uvtk[2217]" -type "float2" 0.076050401 -0.043845832 ;
	setAttr ".uvtk[2218]" -type "float2" 0.071622491 -0.043780982 ;
	setAttr ".uvtk[2219]" -type "float2" 0.074833751 -0.04527241 ;
	setAttr ".uvtk[2220]" -type "float2" 0.027545631 -0.042631328 ;
	setAttr ".uvtk[2221]" -type "float2" 0.019411847 -0.042176068 ;
	setAttr ".uvtk[2222]" -type "float2" 0.021405384 -0.043581307 ;
	setAttr ".uvtk[2223]" -type "float2" 0.029708326 -0.0440678 ;
	setAttr ".uvtk[2224]" -type "float2" 0.035057485 -0.047627628 ;
	setAttr ".uvtk[2225]" -type "float2" 0.034325123 -0.049013257 ;
	setAttr ".uvtk[2226]" -type "float2" 0.025225893 -0.048508823 ;
	setAttr ".uvtk[2227]" -type "float2" 0.025924876 -0.047119856 ;
	setAttr ".uvtk[2228]" -type "float2" 0.013378814 -0.045768201 ;
	setAttr ".uvtk[2229]" -type "float2" 0.012753904 -0.047126055 ;
	setAttr ".uvtk[2230]" -type "float2" 0.0099585205 -0.046260178 ;
	setAttr ".uvtk[2231]" -type "float2" 0.010598391 -0.044930279 ;
	setAttr ".uvtk[2232]" -type "float2" 0.014272839 -0.034952998 ;
	setAttr ".uvtk[2233]" -type "float2" 0.013871923 -0.041203797 ;
	setAttr ".uvtk[2234]" -type "float2" 0.011110082 -0.040390193 ;
	setAttr ".uvtk[2235]" -type "float2" 0.0113139 -0.034181774 ;
	setAttr ".uvtk[2236]" -type "float2" 0.0095833838 -0.033282578 ;
	setAttr ".uvtk[2237]" -type "float2" 0.0093729645 -0.039456427 ;
	setAttr ".uvtk[2238]" -type "float2" 0.0089834481 -0.043957531 ;
	setAttr ".uvtk[2239]" -type "float2" 0.008361131 -0.045272112 ;
	setAttr ".uvtk[2240]" -type "float2" 0.035840303 -0.036692023 ;
	setAttr ".uvtk[2241]" -type "float2" 0.035511136 -0.043004572 ;
	setAttr ".uvtk[2242]" -type "float2" 0.026506737 -0.042510927 ;
	setAttr ".uvtk[2243]" -type "float2" 0.026752725 -0.036209106 ;
	setAttr ".uvtk[2244]" -type "float2" 0.019352302 -0.035633087 ;
	setAttr ".uvtk[2245]" -type "float2" 0.019072443 -0.041907012 ;
	setAttr ".uvtk[2246]" -type "float2" 0.017794818 -0.047873497 ;
	setAttr ".uvtk[2247]" -type "float2" 0.018589199 -0.046506464 ;
	setAttr ".uvtk[2248]" -type "float2" 0.0084907711 -0.040936947 ;
	setAttr ".uvtk[2249]" -type "float2" 0.006373927 -0.040172577 ;
	setAttr ".uvtk[2250]" -type "float2" 0.0074569732 -0.0414505 ;
	setAttr ".uvtk[2251]" -type "float2" 0.009806335 -0.04226172 ;
	setAttr ".uvtk[2252]" -type "float2" 0.011865541 -0.045994043 ;
	setAttr ".uvtk[2253]" -type "float2" 0.010879397 -0.044032037 ;
	setAttr ".uvtk[2254]" -type "float2" 0.0083740652 -0.043175638 ;
	setAttr ".uvtk[2255]" -type "float2" 0.0091996789 -0.045131803 ;
	setAttr ".uvtk[2256]" -type "float2" 0.0077821016 -0.0441221 ;
	setAttr ".uvtk[2257]" -type "float2" 0.0070901215 -0.042192042 ;
	setAttr ".uvtk[2258]" -type "float2" 0.0054133981 -0.0392887 ;
	setAttr ".uvtk[2259]" -type "float2" 0.0062881112 -0.04050529 ;
	setAttr ".uvtk[2260]" -type "float2" 0.0331209 -0.047892034 ;
	setAttr ".uvtk[2261]" -type "float2" 0.031623453 -0.045894682 ;
	setAttr ".uvtk[2262]" -type "float2" 0.022933647 -0.045396388 ;
	setAttr ".uvtk[2263]" -type "float2" 0.024286404 -0.047381699 ;
	setAttr ".uvtk[2264]" -type "float2" 0.016862243 -0.046751022 ;
	setAttr ".uvtk[2265]" -type "float2" 0.015825748 -0.044770062 ;
	setAttr ".uvtk[2266]" -type "float2" 0.012994602 -0.041602492 ;
	setAttr ".uvtk[2267]" -type "float2" 0.014486402 -0.042981744 ;
	setAttr ".uvtk[2268]" -type "float2" 0.087196052 -0.049034834 ;
	setAttr ".uvtk[2269]" -type "float2" 0.085950881 -0.05041039 ;
	setAttr ".uvtk[2270]" -type "float2" 0.081668079 -0.050332487 ;
	setAttr ".uvtk[2271]" -type "float2" 0.083018601 -0.048956573 ;
	setAttr ".uvtk[2272]" -type "float2" 0.065695077 -0.049986064 ;
	setAttr ".uvtk[2273]" -type "float2" 0.055609733 -0.049736917 ;
	setAttr ".uvtk[2274]" -type "float2" 0.056612313 -0.048357844 ;
	setAttr ".uvtk[2275]" -type "float2" 0.066918612 -0.048609614 ;
	setAttr ".uvtk[2276]" -type "float2" 0.067936063 -0.037642658 ;
	setAttr ".uvtk[2277]" -type "float2" 0.067614377 -0.043985963 ;
	setAttr ".uvtk[2278]" -type "float2" 0.057327867 -0.043730319 ;
	setAttr ".uvtk[2279]" -type "float2" 0.057589978 -0.037398458 ;
	setAttr ".uvtk[2280]" -type "float2" 0.046309277 -0.037081778 ;
	setAttr ".uvtk[2281]" -type "float2" 0.046087131 -0.043411016 ;
	setAttr ".uvtk[2282]" -type "float2" 0.044578359 -0.049419761 ;
	setAttr ".uvtk[2283]" -type "float2" 0.045576766 -0.048034132 ;
	setAttr ".uvtk[2284]" -type "float2" 0.088339686 -0.038039446 ;
	setAttr ".uvtk[2285]" -type "float2" 0.088059127 -0.044398248 ;
	setAttr ".uvtk[2286]" -type "float2" 0.083795905 -0.044326305 ;
	setAttr ".uvtk[2287]" -type "float2" 0.084279656 -0.03796953 ;
	setAttr ".uvtk[2288]" -type "float2" 0.076001406 -0.044183016 ;
	setAttr ".uvtk[2289]" -type "float2" 0.07630676 -0.037835836 ;
	setAttr ".uvtk[2290]" -type "float2" 0.073910236 -0.050182343 ;
	setAttr ".uvtk[2291]" -type "float2" 0.075140566 -0.048812687 ;
	setAttr ".uvtk[2292]" -type "float2" 0.05637002 -0.043475211 ;
	setAttr ".uvtk[2293]" -type "float2" 0.046966553 -0.043263316 ;
	setAttr ".uvtk[2294]" -type "float2" 0.049768597 -0.044729948 ;
	setAttr ".uvtk[2295]" -type "float2" 0.059419543 -0.044953763 ;
	setAttr ".uvtk[2296]" -type "float2" 0.064134628 -0.048836827 ;
	setAttr ".uvtk[2297]" -type "float2" 0.061965227 -0.04681772 ;
	setAttr ".uvtk[2298]" -type "float2" 0.052301049 -0.046583891 ;
	setAttr ".uvtk[2299]" -type "float2" 0.054179251 -0.048599541 ;
	setAttr ".uvtk[2300]" -type "float2" 0.043269977 -0.048289537 ;
	setAttr ".uvtk[2301]" -type "float2" 0.04160662 -0.046286702 ;
	setAttr ".uvtk[2302]" -type "float2" 0.036793247 -0.042991817 ;
	setAttr ".uvtk[2303]" -type "float2" 0.03954713 -0.044442713 ;
	setAttr ".uvtk[2304]" -type "float2" 0.084200621 -0.049257338 ;
	setAttr ".uvtk[2305]" -type "float2" 0.081915915 -0.047221661 ;
	setAttr ".uvtk[2306]" -type "float2" 0.077661812 -0.047148168 ;
	setAttr ".uvtk[2307]" -type "float2" 0.079946965 -0.049179137 ;
	setAttr ".uvtk[2308]" -type "float2" 0.069945902 -0.047005296 ;
	setAttr ".uvtk[2309]" -type "float2" 0.072128892 -0.049033701 ;
	setAttr ".uvtk[2310]" -type "float2" 0.063930362 -0.043649316 ;
	setAttr ".uvtk[2311]" -type "float2" 0.067202866 -0.045138955 ;
	setAttr ".uvtk[2314]" -type "float2" 0.0014310181 -0.0070695877 ;
	setAttr ".uvtk[2315]" -type "float2" 0.0034498274 -0.0071420074 ;
	setAttr ".uvtk[2316]" -type "float2" 0.018558919 -0.034629762 ;
	setAttr ".uvtk[2317]" -type "float2" 0.014959991 -0.029000282 ;
	setAttr ".uvtk[2318]" -type "float2" 0.0092095733 -0.028744817 ;
	setAttr ".uvtk[2319]" -type "float2" 0.012041718 -0.034309983 ;
	setAttr ".uvtk[2320]" -type "float2" 0.003932476 -0.033434093 ;
	setAttr ".uvtk[2321]" -type "float2" 0.0023051947 -0.028011203 ;
	setAttr ".uvtk[2322]" -type "float2" 0.0015449971 -0.027554274 ;
	setAttr ".uvtk[2323]" -type "float2" 0.0026798099 -0.0328933 ;
	setAttr ".uvtk[2324]" -type "float2" 0.0071266592 -0.039476097 ;
	setAttr ".uvtk[2325]" -type "float2" 0.0055089742 -0.037170172 ;
	setAttr ".uvtk[2326]" -type "float2" 0.0039767921 -0.036546528 ;
	setAttr ".uvtk[2327]" -type "float2" 0.0051599741 -0.038785875 ;
	setAttr ".uvtk[2328]" -type "float2" 0.0044146329 -0.037980556 ;
	setAttr ".uvtk[2329]" -type "float2" 0.0033963025 -0.035832524 ;
	setAttr ".uvtk[2330]" -type "float2" 0.0024312735 -0.032265604 ;
	setAttr ".uvtk[2331]" -type "float2" 0.0014712811 -0.027026355 ;
	setAttr ".uvtk[2332]" -type "float2" 0.024802625 -0.0410375 ;
	setAttr ".uvtk[2333]" -type "float2" 0.02184388 -0.038552523 ;
	setAttr ".uvtk[2334]" -type "float2" 0.014706254 -0.038182318 ;
	setAttr ".uvtk[2335]" -type "float2" 0.017269224 -0.040616632 ;
	setAttr ".uvtk[2336]" -type "float2" 0.011094868 -0.040094793 ;
	setAttr ".uvtk[2337]" -type "float2" 0.0091763288 -0.03771764 ;
	setAttr ".uvtk[2338]" -type "float2" 0.0048775524 -0.028411031 ;
	setAttr ".uvtk[2339]" -type "float2" 0.0069899708 -0.033915818 ;
	setAttr ".uvtk[2342]" -type "float2" -0.00041036308 -0.0067525506 ;
	setAttr ".uvtk[2343]" -type "float2" -0.00060999393 -0.006881237 ;
	setAttr ".uvtk[2344]" -type "float2" 0.00094628334 -0.021344781 ;
	setAttr ".uvtk[2345]" -type "float2" -0.00016874075 -0.014140069 ;
	setAttr ".uvtk[2346]" -type "float2" -0.00013247132 -0.013885081 ;
	setAttr ".uvtk[2347]" -type "float2" 0.00052478909 -0.020983338 ;
	setAttr ".uvtk[2348]" -type "float2" 0.00076778233 -0.020553529 ;
	setAttr ".uvtk[2349]" -type "float2" 0.00023049116 -0.013581216 ;
	setAttr ".uvtk[2351]" -type "float2" -1.2040138e-05 -0.0065940619 ;
	setAttr ".uvtk[2352]" -type "float2" 0.011222869 -0.022108018 ;
	setAttr ".uvtk[2353]" -type "float2" 0.0072797239 -0.014656603 ;
	setAttr ".uvtk[2354]" -type "float2" 0.0037434697 -0.014514685 ;
	setAttr ".uvtk[2355]" -type "float2" 0.0063917041 -0.021907806 ;
	setAttr ".uvtk[2356]" -type "float2" 0.0028165281 -0.021651208 ;
	setAttr ".uvtk[2357]" -type "float2" 0.0010972321 -0.014343202 ;
	setAttr ".uvtk[2359]" -type "float2" -4.3272972e-05 -0.0069844127 ;
	setAttr ".uvtk[2360]" -type "float2" 0.061044514 -0.03551203 ;
	setAttr ".uvtk[2361]" -type "float2" 0.053627998 -0.029717565 ;
	setAttr ".uvtk[2362]" -type "float2" 0.049695611 -0.029673815 ;
	setAttr ".uvtk[2363]" -type "float2" 0.056961685 -0.035457551 ;
	setAttr ".uvtk[2364]" -type "float2" 0.037172467 -0.029489994 ;
	setAttr ".uvtk[2365]" -type "float2" 0.029748678 -0.029362679 ;
	setAttr ".uvtk[2366]" -type "float2" 0.035042614 -0.035083354 ;
	setAttr ".uvtk[2367]" -type "float2" 0.0432657 -0.035236061 ;
	setAttr ".uvtk[2368]" -type "float2" 0.052636206 -0.041820347 ;
	setAttr ".uvtk[2369]" -type "float2" 0.048303336 -0.039261997 ;
	setAttr ".uvtk[2370]" -type "float2" 0.039649934 -0.039084494 ;
	setAttr ".uvtk[2371]" -type "float2" 0.043656588 -0.041625679 ;
	setAttr ".uvtk[2372]" -type "float2" 0.033805683 -0.041367233 ;
	setAttr ".uvtk[2373]" -type "float2" 0.030378446 -0.03885293 ;
	setAttr ".uvtk[2374]" -type "float2" 0.021994278 -0.029203057 ;
	setAttr ".uvtk[2375]" -type "float2" 0.026402429 -0.034883618 ;
	setAttr ".uvtk[2376]" -type "float2" 0.071926087 -0.042168856 ;
	setAttr ".uvtk[2377]" -type "float2" 0.06703797 -0.039577544 ;
	setAttr ".uvtk[2378]" -type "float2" 0.062853545 -0.039515555 ;
	setAttr ".uvtk[2379]" -type "float2" 0.067841321 -0.042103589 ;
	setAttr ".uvtk[2380]" -type "float2" 0.055695325 -0.039403319 ;
	setAttr ".uvtk[2381]" -type "float2" 0.060183346 -0.041981399 ;
	setAttr ".uvtk[2382]" -type "float2" 0.043235004 -0.029592812 ;
	setAttr ".uvtk[2383]" -type "float2" 0.049988002 -0.035361469 ;
	setAttr ".uvtk[2386]" -type "float2" 0.0089119375 -0.0072287917 ;
	setAttr ".uvtk[2387]" -type "float2" 0.011644751 -0.0072588325 ;
	setAttr ".uvtk[2388]" -type "float2" 0.030029267 -0.022467315 ;
	setAttr ".uvtk[2389]" -type "float2" 0.021469742 -0.014890611 ;
	setAttr ".uvtk[2390]" -type "float2" 0.016810149 -0.014827967 ;
	setAttr ".uvtk[2391]" -type "float2" 0.023673743 -0.022373617 ;
	setAttr ".uvtk[2392]" -type "float2" 0.017027244 -0.022257924 ;
	setAttr ".uvtk[2393]" -type "float2" 0.011639789 -0.014757872 ;
	setAttr ".uvtk[2395]" -type "float2" 0.0059393793 -0.0071936846 ;
	setAttr ".uvtk[2396]" -type "float2" 0.044218689 -0.022644758 ;
	setAttr ".uvtk[2397]" -type "float2" 0.032791674 -0.015010417 ;
	setAttr ".uvtk[2398]" -type "float2" 0.029639453 -0.014991343 ;
	setAttr ".uvtk[2399]" -type "float2" 0.040807605 -0.022612393 ;
	setAttr ".uvtk[2400]" -type "float2" 0.025233835 -0.014948368 ;
	setAttr ".uvtk[2401]" -type "float2" 0.034912676 -0.022550642 ;
	setAttr ".uvtk[2403]" -type "float2" 0.013506591 -0.0072872639 ;
	setAttr ".uvtk[2406]" -type "float2" 0.011770904 -0.002995193 ;
	setAttr ".uvtk[2407]" -type "float2" 0.0118047 -0.0025311112 ;
	setAttr ".uvtk[2408]" -type "float2" -0.0084059834 -0.024058282 ;
	setAttr ".uvtk[2409]" -type "float2" -0.0081555843 -0.027962744 ;
	setAttr ".uvtk[2410]" -type "float2" -0.00036245584 -0.028814197 ;
	setAttr ".uvtk[2411]" -type "float2" -0.0006121397 -0.024860173 ;
	setAttr ".uvtk[2412]" -type "float2" -0.012477338 0.00012783706 ;
	setAttr ".uvtk[2413]" -type "float2" -0.01203841 -0.0011809468 ;
	setAttr ".uvtk[2414]" -type "float2" -0.0043437481 -0.0015127808 ;
	setAttr ".uvtk[2415]" -type "float2" -0.0050800443 -0.00015294552 ;
	setAttr ".uvtk[2416]" -type "float2" 0.0061553717 -0.0010491759 ;
	setAttr ".uvtk[2417]" -type "float2" 0.0084550381 -0.0026038289 ;
	setAttr ".uvtk[2418]" -type "float2" 0.020253569 -0.0027426183 ;
	setAttr ".uvtk[2419]" -type "float2" 0.017346263 -0.0011718571 ;
	setAttr ".uvtk[2420]" -type "float2" -0.0032276213 -0.00052122772 ;
	setAttr ".uvtk[2421]" -type "float2" -0.0015434623 -0.00049269199 ;
	setAttr ".uvtk[2422]" -type "float2" 0.0072634816 -0.0005556196 ;
	setAttr ".uvtk[2423]" -type "float2" 0.0035640895 -0.00056361407 ;
	setAttr ".uvtk[2424]" -type "float2" 0.020359337 -0.0006069541 ;
	setAttr ".uvtk[2425]" -type "float2" 0.028184295 -0.0006249994 ;
	setAttr ".uvtk[2426]" -type "float2" 0.040954381 -0.0006339848 ;
	setAttr ".uvtk[2427]" -type "float2" 0.030632198 -0.00061193109 ;
	setAttr ".uvtk[2430]" -type "float2" 0.011070073 -0.00043253973 ;
	setAttr ".uvtk[2431]" -type "float2" 0.017720461 -0.00043498725 ;
	setAttr ".uvtk[2433]" -type "float2" 0.019980013 -0.00043263659 ;
	setAttr ".uvtk[2434]" -type "float2" 0.045932531 -0.00063422322 ;
	setAttr ".uvtk[2435]" -type "float2" 0.034690052 -0.00060943514 ;
	setAttr ".uvtk[2438]" -type "float2" -0.0036477745 -0.00039311126 ;
	setAttr ".uvtk[2439]" -type "float2" 0.00077077746 -0.00041215122 ;
	setAttr ".uvtk[2441]" -type "float2" 0.0059043467 -0.00042361766 ;
	setAttr ".uvtk[2442]" -type "float2" 0.017559052 -0.00059971213 ;
	setAttr ".uvtk[2443]" -type "float2" 0.011953861 -0.00059081614 ;
	setAttr ".uvtk[2444]" -type "float2" 0.049788386 -0.0029449761 ;
	setAttr ".uvtk[2445]" -type "float2" 0.065688342 -0.0029971749 ;
	setAttr ".uvtk[2446]" -type "float2" 0.061191708 -0.0013735592 ;
	setAttr ".uvtk[2447]" -type "float2" 0.045661509 -0.0013343543 ;
	setAttr ".uvtk[2448]" -type "float2" 0.035212845 -0.0006377548 ;
	setAttr ".uvtk[2449]" -type "float2" 0.040903628 -0.00074733794 ;
	setAttr ".uvtk[2450]" -type "float2" 0.055642426 -0.00077325106 ;
	setAttr ".uvtk[2451]" -type "float2" 0.049037844 -0.00065355003 ;
	setAttr ".uvtk[2452]" -type "float2" 0.055012405 -0.0006596446 ;
	setAttr ".uvtk[2453]" -type "float2" 0.062369496 -0.00078879297 ;
	setAttr ".uvtk[2454]" -type "float2" 0.072950214 -0.0030213743 ;
	setAttr ".uvtk[2455]" -type "float2" 0.068148613 -0.0013953596 ;
	setAttr ".uvtk[2456]" -type "float2" 0.0010663569 -0.00045631826 ;
	setAttr ".uvtk[2457]" -type "float2" 0.0036154389 -0.00051677227 ;
	setAttr ".uvtk[2458]" -type "float2" 0.014127254 -0.00061716139 ;
	setAttr ".uvtk[2459]" -type "float2" 0.01068002 -0.00053961575 ;
	setAttr ".uvtk[2460]" -type "float2" 0.027192205 -0.00069977343 ;
	setAttr ".uvtk[2461]" -type "float2" 0.022588432 -0.00060121715 ;
	setAttr ".uvtk[2462]" -type "float2" 0.034782857 -0.0028615594 ;
	setAttr ".uvtk[2463]" -type "float2" 0.031257987 -0.0012684315 ;
	setAttr ".uvtk[2464]" -type "float2" -0.0090715289 -0.00014719367 ;
	setAttr ".uvtk[2465]" -type "float2" -0.011217356 7.2561204e-05 ;
	setAttr ".uvtk[2466]" -type "float2" -0.0052016973 -3.606081e-05 ;
	setAttr ".uvtk[2467]" -type "float2" -0.0040606856 -0.00021054596 ;
	setAttr ".uvtk[2468]" -type "float2" 0.0041647553 -0.00033706427 ;
	setAttr ".uvtk[2469]" -type "float2" 0.0050590038 -0.00023953617 ;
	setAttr ".uvtk[2470]" -type "float2" 0.0053703189 -0.00033067912 ;
	setAttr ".uvtk[2471]" -type "float2" 0.0044795871 -0.00039759278 ;
	setAttr ".uvtk[2474]" -type "float2" 0.0030187368 -0.00029350817 ;
	setAttr ".uvtk[2475]" -type "float2" 0.0027115345 -0.00031942874 ;
	setAttr ".uvtk[2477]" -type "float2" -0.00029301643 -0.00035785884 ;
	setAttr ".uvtk[2478]" -type "float2" 0.00210917 -0.00041352957 ;
	setAttr ".uvtk[2479]" -type "float2" 0.00033700466 -0.00046376884 ;
	setAttr ".uvtk[2482]" -type "float2" -0.0055558681 -0.00020455196 ;
	setAttr ".uvtk[2483]" -type "float2" -0.0023237467 -0.00023014471 ;
	setAttr ".uvtk[2485]" -type "float2" 0.00033420324 -0.00026671588 ;
	setAttr ".uvtk[2486]" -type "float2" 0.00016146898 -0.00014390796 ;
	setAttr ".uvtk[2487]" -type "float2" 0.00017005205 -0.00027567148 ;
	setAttr ".uvtk[2488]" -type "float2" 0.0089028478 -0.0020409971 ;
	setAttr ".uvtk[2489]" -type "float2" 0.011004746 -0.0022544712 ;
	setAttr ".uvtk[2490]" -type "float2" 0.0096107721 -0.00075888634 ;
	setAttr ".uvtk[2491]" -type "float2" 0.0078857541 -0.00058464706 ;
	setAttr ".uvtk[2492]" -type "float2" 0.0059386492 -0.00014421344 ;
	setAttr ".uvtk[2493]" -type "float2" 0.0068676472 -0.00013296306 ;
	setAttr ".uvtk[2494]" -type "float2" 0.0081576109 -0.00027827919 ;
	setAttr ".uvtk[2495]" -type "float2" 0.0066292286 -0.00025714934 ;
	setAttr ".uvtk[2496]" -type "float2" 0.0037291646 -0.00036200881 ;
	setAttr ".uvtk[2497]" -type "float2" 0.0055121779 -0.00040343404 ;
	setAttr ".uvtk[2498]" -type "float2" 0.0093181729 -0.0024404526 ;
	setAttr ".uvtk[2499]" -type "float2" 0.0074160099 -0.0009162724 ;
	setAttr ".uvtk[2500]" -type "float2" -0.01243341 0.00029741228 ;
	setAttr ".uvtk[2501]" -type "float2" -0.012667239 0.00043292344 ;
	setAttr ".uvtk[2502]" -type "float2" -0.0055269599 0.00021889806 ;
	setAttr ".uvtk[2503]" -type "float2" -0.0056325793 0.00013546646 ;
	setAttr ".uvtk[2504]" -type "float2" 0.00095784664 2.9921532e-05 ;
	setAttr ".uvtk[2505]" -type "float2" 0.00043666363 -1.0386109e-05 ;
	setAttr ".uvtk[2506]" -type "float2" 0.0027879477 -0.0018025637 ;
	setAttr ".uvtk[2507]" -type "float2" 0.0017639399 -0.00038652122 ;
	setAttr ".uvtk[2508]" -type "float2" 0.018275082 -0.031577885 ;
	setAttr ".uvtk[2509]" -type "float2" 0.031583548 -0.031874895 ;
	setAttr ".uvtk[2510]" -type "float2" 0.031372607 -0.027735263 ;
	setAttr ".uvtk[2511]" -type "float2" 0.01803118 -0.027452201 ;
	setAttr ".uvtk[2512]" -type "float2" 0.014188766 -0.012787282 ;
	setAttr ".uvtk[2513]" -type "float2" 0.015506268 -0.016571522 ;
	setAttr ".uvtk[2514]" -type "float2" 0.02882129 -0.016795069 ;
	setAttr ".uvtk[2515]" -type "float2" 0.027177066 -0.012994677 ;
	setAttr ".uvtk[2516]" -type "float2" 0.058627605 -0.013276696 ;
	setAttr ".uvtk[2517]" -type "float2" 0.060526103 -0.017098337 ;
	setAttr ".uvtk[2518]" -type "float2" 0.077173799 -0.01718536 ;
	setAttr ".uvtk[2519]" -type "float2" 0.075096637 -0.013354748 ;
	setAttr ".uvtk[2520]" -type "float2" 0.053133667 -0.0058779418 ;
	setAttr ".uvtk[2521]" -type "float2" 0.056210876 -0.009547323 ;
	setAttr ".uvtk[2522]" -type "float2" 0.072482169 -0.0096186697 ;
	setAttr ".uvtk[2523]" -type "float2" 0.069507778 -0.0059379637 ;
	setAttr ".uvtk[2524]" -type "float2" 0.076804072 -0.0059670508 ;
	setAttr ".uvtk[2525]" -type "float2" 0.080152273 -0.0096499026 ;
	setAttr ".uvtk[2526]" -type "float2" 0.082825512 -0.013392657 ;
	setAttr ".uvtk[2527]" -type "float2" 0.085022032 -0.017227471 ;
	setAttr ".uvtk[2528]" -type "float2" 0.010501087 -0.0054771602 ;
	setAttr ".uvtk[2529]" -type "float2" 0.012406349 -0.0091017485 ;
	setAttr ".uvtk[2530]" -type "float2" 0.025269568 -0.0092844069 ;
	setAttr ".uvtk[2531]" -type "float2" 0.02280727 -0.0056422949 ;
	setAttr ".uvtk[2532]" -type "float2" 0.037965298 -0.0057760477 ;
	setAttr ".uvtk[2533]" -type "float2" 0.040468067 -0.0094360411 ;
	setAttr ".uvtk[2534]" -type "float2" 0.044582069 -0.016970575 ;
	setAttr ".uvtk[2535]" -type "float2" 0.042806625 -0.013154477 ;
	setAttr ".uvtk[2536]" -type "float2" 0.063663453 -0.032289088 ;
	setAttr ".uvtk[2537]" -type "float2" 0.080646127 -0.032415867 ;
	setAttr ".uvtk[2538]" -type "float2" 0.080397576 -0.02823326 ;
	setAttr ".uvtk[2539]" -type "float2" 0.063401461 -0.028122663 ;
	setAttr ".uvtk[2540]" -type "float2" 0.061897337 -0.020926625 ;
	setAttr ".uvtk[2541]" -type "float2" 0.062847435 -0.024537295 ;
	setAttr ".uvtk[2542]" -type "float2" 0.079832733 -0.02463907 ;
	setAttr ".uvtk[2543]" -type "float2" 0.078791022 -0.021018445 ;
	setAttr ".uvtk[2544]" -type "float2" 0.086630434 -0.021064848 ;
	setAttr ".uvtk[2545]" -type "float2" 0.087775499 -0.024690241 ;
	setAttr ".uvtk[2546]" -type "float2" 0.088866204 -0.032478333 ;
	setAttr ".uvtk[2547]" -type "float2" 0.088506669 -0.028291225 ;
	setAttr ".uvtk[2548]" -type "float2" 0.016693115 -0.020353407 ;
	setAttr ".uvtk[2549]" -type "float2" 0.017515242 -0.023919523 ;
	setAttr ".uvtk[2550]" -type "float2" 0.030826539 -0.024180859 ;
	setAttr ".uvtk[2551]" -type "float2" 0.029989004 -0.020598024 ;
	setAttr ".uvtk[2552]" -type "float2" 0.045854747 -0.020785362 ;
	setAttr ".uvtk[2553]" -type "float2" 0.046752423 -0.024387389 ;
	setAttr ".uvtk[2554]" -type "float2" 0.047412097 -0.032110572 ;
	setAttr ".uvtk[2555]" -type "float2" 0.047252029 -0.027955592 ;
	setAttr ".uvtk[2556]" -type "float2" -0.010040641 -0.010515273 ;
	setAttr ".uvtk[2557]" -type "float2" -0.0095512867 -0.01399821 ;
	setAttr ".uvtk[2558]" -type "float2" -0.0018740892 -0.014577925 ;
	setAttr ".uvtk[2559]" -type "float2" -0.0023682117 -0.011026293 ;
	setAttr ".uvtk[2560]" -type "float2" 0.012585223 -0.015565395 ;
	setAttr ".uvtk[2561]" -type "float2" 0.015606701 -0.015961796 ;
	setAttr ".uvtk[2562]" -type "float2" 0.014698625 -0.0122343 ;
	setAttr ".uvtk[2563]" -type "float2" 0.011779785 -0.011894286 ;
	setAttr ".uvtk[2564]" -type "float2" 0.010150492 -0.004812181 ;
	setAttr ".uvtk[2565]" -type "float2" 0.011193752 -0.0083191693 ;
	setAttr ".uvtk[2566]" -type "float2" 0.013525665 -0.0086148679 ;
	setAttr ".uvtk[2567]" -type "float2" 0.012340248 -0.0050621927 ;
	setAttr ".uvtk[2568]" -type "float2" 0.011055171 -0.0052858293 ;
	setAttr ".uvtk[2569]" -type "float2" 0.012610018 -0.0088739991 ;
	setAttr ".uvtk[2570]" -type "float2" 0.015115082 -0.016291589 ;
	setAttr ".uvtk[2571]" -type "float2" 0.013924599 -0.012535423 ;
	setAttr ".uvtk[2572]" -type "float2" -0.01105684 -0.0037875175 ;
	setAttr ".uvtk[2573]" -type "float2" -0.01055038 -0.0071282387 ;
	setAttr ".uvtk[2574]" -type "float2" -0.002846837 -0.0075685382 ;
	setAttr ".uvtk[2575]" -type "float2" -0.0036792159 -0.0041783452 ;
	setAttr ".uvtk[2576]" -type "float2" 0.0042401552 -0.0079743564 ;
	setAttr ".uvtk[2577]" -type "float2" 0.00362885 -0.0045188665 ;
	setAttr ".uvtk[2578]" -type "float2" 0.0055244565 -0.01510945 ;
	setAttr ".uvtk[2579]" -type "float2" 0.004953444 -0.011487722 ;
	setAttr ".uvtk[2580]" -type "float2" 0.014799535 -0.030188859 ;
	setAttr ".uvtk[2581]" -type "float2" 0.01808244 -0.030743003 ;
	setAttr ".uvtk[2582]" -type "float2" 0.017736077 -0.026669532 ;
	setAttr ".uvtk[2583]" -type "float2" 0.014442325 -0.026156008 ;
	setAttr ".uvtk[2584]" -type "float2" 0.013301432 -0.019251466 ;
	setAttr ".uvtk[2585]" -type "float2" 0.013933659 -0.022712916 ;
	setAttr ".uvtk[2586]" -type "float2" 0.017139733 -0.023196638 ;
	setAttr ".uvtk[2587]" -type "float2" 0.016459346 -0.019685417 ;
	setAttr ".uvtk[2588]" -type "float2" 0.016050816 -0.020053446 ;
	setAttr ".uvtk[2589]" -type "float2" 0.016933143 -0.023590326 ;
	setAttr ".uvtk[2590]" -type "float2" 0.017747164 -0.031200588 ;
	setAttr ".uvtk[2591]" -type "float2" 0.017483354 -0.027104586 ;
	setAttr ".uvtk[2592]" -type "float2" -0.0090370774 -0.01748094 ;
	setAttr ".uvtk[2593]" -type "float2" -0.0086397529 -0.020774215 ;
	setAttr ".uvtk[2594]" -type "float2" -0.0010039806 -0.021508038 ;
	setAttr ".uvtk[2595]" -type "float2" -0.0014141798 -0.018149346 ;
	setAttr ".uvtk[2596]" -type "float2" 0.0066177249 -0.022154003 ;
	setAttr ".uvtk[2597]" -type "float2" 0.0061542392 -0.018731803 ;
	setAttr ".uvtk[2598]" -type "float2" 0.007355392 -0.02955538 ;
	setAttr ".uvtk[2599]" -type "float2" 0.0070344806 -0.025548786 ;
	setAttr ".uvtk[2602]" -type "float2" 0.0098237395 0.0012129396 ;
	setAttr ".uvtk[2603]" -type "float2" 0.0090758801 0.0012286156 ;
	setAttr ".uvtk[2604]" -type "float2" 0.041257739 0.0019851625 ;
	setAttr ".uvtk[2605]" -type "float2" 0.043385565 0.0011494458 ;
	setAttr ".uvtk[2606]" -type "float2" 0.026952624 0.00076746941 ;
	setAttr ".uvtk[2607]" -type "float2" 0.025077879 0.0017061383 ;
	setAttr ".uvtk[2608]" -type "float2" 0.02267319 0.00020427257 ;
	setAttr ".uvtk[2609]" -type "float2" 0.029445291 0.00070634484 ;
	setAttr ".uvtk[2610]" -type "float2" 0.016478956 0.00061471015 ;
	setAttr ".uvtk[2611]" -type "float2" 0.011979878 0.00013326108 ;
	setAttr ".uvtk[2612]" -type "float2" -0.0077744126 2.143532e-05 ;
	setAttr ".uvtk[2613]" -type "float2" -0.0085801482 0.0004036203 ;
	setAttr ".uvtk[2614]" -type "float2" -0.016569853 0.00028286129 ;
	setAttr ".uvtk[2615]" -type "float2" -0.014312446 -3.2283366e-05 ;
	setAttr ".uvtk[2618]" -type "float2" -0.0054456592 -0.00013809651 ;
	setAttr ".uvtk[2619]" -type "float2" -0.0094638467 -0.00015520677 ;
	setAttr ".uvtk[2621]" -type "float2" -0.009154439 -0.00017797202 ;
	setAttr ".uvtk[2622]" -type "float2" -0.016920269 0.000172019 ;
	setAttr ".uvtk[2623]" -type "float2" -0.014180005 -8.8468194e-05 ;
	setAttr ".uvtk[2626]" -type "float2" 0.013429284 -6.1001629e-05 ;
	setAttr ".uvtk[2627]" -type "float2" 0.0066869855 -9.7688287e-05 ;
	setAttr ".uvtk[2629]" -type "float2" 0.0005659461 -0.00011239946 ;
	setAttr ".uvtk[2630]" -type "float2" 0.0036163926 0.0005043596 ;
	setAttr ".uvtk[2631]" -type "float2" 0.0020255446 7.7702105e-05 ;
	setAttr ".uvtk[2632]" -type "float2" -0.0062065125 -3.6671758e-05 ;
	setAttr ".uvtk[2633]" -type "float2" -0.016572297 -0.00043742359 ;
	setAttr ".uvtk[2634]" -type "float2" -0.017115235 0.00076261163 ;
	setAttr ".uvtk[2635]" -type "float2" -0.0071499348 0.0010790378 ;
	setAttr ".uvtk[2636]" -type "float2" -0.0085806251 0.00083808601 ;
	setAttr ".uvtk[2637]" -type "float2" -0.0079032779 0.0011921823 ;
	setAttr ".uvtk[2638]" -type "float2" -0.017712116 0.00094507635 ;
	setAttr ".uvtk[2639]" -type "float2" -0.017470062 0.0006531328 ;
	setAttr ".uvtk[2640]" -type "float2" -0.018254876 0.00046506524 ;
	setAttr ".uvtk[2641]" -type "float2" -0.018618226 0.00068846345 ;
	setAttr ".uvtk[2642]" -type "float2" -0.017797887 -0.00081767142 ;
	setAttr ".uvtk[2643]" -type "float2" -0.018397033 0.00043845177 ;
	setAttr ".uvtk[2644]" -type "float2" 0.034674764 0.0013029426 ;
	setAttr ".uvtk[2645]" -type "float2" 0.038405716 0.0018580556 ;
	setAttr ".uvtk[2646]" -type "float2" 0.0228163 0.0016621947 ;
	setAttr ".uvtk[2647]" -type "float2" 0.020012021 0.0011659563 ;
	setAttr ".uvtk[2648]" -type "float2" 0.006949544 0.001435861 ;
	setAttr ".uvtk[2649]" -type "float2" 0.0052658319 0.0010019243 ;
	setAttr ".uvtk[2650]" -type "float2" 0.0098320246 0.00036050379 ;
	setAttr ".uvtk[2651]" -type "float2" 0.0084553957 0.0014061332 ;
	setAttr ".uvtk[2654]" -type "float2" 0.0058602095 0.00029044598 ;
	setAttr ".uvtk[2655]" -type "float2" 0.0042198896 8.5264444e-05 ;
	setAttr ".uvtk[2656]" -type "float2" 0.020542979 0.00029646605 ;
	setAttr ".uvtk[2657]" -type "float2" 0.027555108 0.00079610199 ;
	setAttr ".uvtk[2658]" -type "float2" 0.037471414 0.000819318 ;
	setAttr ".uvtk[2659]" -type "float2" 0.029234827 0.00029115379 ;
	setAttr ".uvtk[2662]" -type "float2" 0.011517763 9.033829e-06 ;
	setAttr ".uvtk[2663]" -type "float2" 0.017494977 -1.0631979e-05 ;
	setAttr ".uvtk[2665]" -type "float2" 0.018830717 -2.1569431e-05 ;
	setAttr ".uvtk[2666]" -type "float2" 0.039184272 0.00077791512 ;
	setAttr ".uvtk[2667]" -type "float2" 0.030950963 0.0002553761 ;
	setAttr ".uvtk[2668]" -type "float2" 0.0089809895 0.00018888712 ;
	setAttr ".uvtk[2669]" -type "float2" 0.012379825 0.00054132938 ;
	setAttr ".uvtk[2670]" -type "float2" 0.019568324 0.00071017444 ;
	setAttr ".uvtk[2671]" -type "float2" 0.014043927 0.0002643913 ;
	setAttr ".uvtk[2674]" -type "float2" 0.004653275 -2.5387853e-05 ;
	setAttr ".uvtk[2675]" -type "float2" 0.0075354576 -5.6624413e-06 ;
	setAttr ".uvtk[2679]" -type "float2" 0.0022166371 -3.7655234e-05 ;
	setAttr ".uvtk[2680]" -type "float2" 0.041754961 0.0019624531 ;
	setAttr ".uvtk[2681]" -type "float2" 0.053446889 0.0017776489 ;
	setAttr ".uvtk[2682]" -type "float2" 0.051054597 0.0023556948 ;
	setAttr ".uvtk[2683]" -type "float2" 0.039522588 0.0023983717 ;
	setAttr ".uvtk[2684]" -type "float2" 0.03290844 0.001393646 ;
	setAttr ".uvtk[2685]" -type "float2" 0.036778569 0.0020434111 ;
	setAttr ".uvtk[2686]" -type "float2" 0.047903597 0.0020682067 ;
	setAttr ".uvtk[2687]" -type "float2" 0.043550193 0.0014396012 ;
	setAttr ".uvtk[2688]" -type "float2" 0.045160592 0.001398325 ;
	setAttr ".uvtk[2689]" -type "float2" 0.049538672 0.0020048618 ;
	setAttr ".uvtk[2690]" -type "float2" 0.055012286 0.0014844984 ;
	setAttr ".uvtk[2691]" -type "float2" 0.052665889 0.002209425 ;
	setAttr ".uvtk[2694]" -type "float2" 0.0083085299 0.00090348721 ;
	setAttr ".uvtk[2695]" -type "float2" 0.0073027611 0.00054235756 ;
	setAttr ".uvtk[2696]" -type "float2" 0.017193973 0.0014915317 ;
	setAttr ".uvtk[2697]" -type "float2" 0.026537478 0.0018630624 ;
	setAttr ".uvtk[2698]" -type "float2" 0.023563385 0.0012459904 ;
	setAttr ".uvtk[2699]" -type "float2" 0.015102565 0.00096030533 ;
	setAttr ".uvtk[2700]" -type "float2" 0.019996345 0.0017932206 ;
	setAttr ".uvtk[2701]" -type "float2" 0.030410588 0.0019854456 ;
	setAttr ".uvtk[2702]" -type "float2" 0.028848529 0.0022601485 ;
	setAttr ".uvtk[2703]" -type "float2" 0.01882571 0.0019187182 ;
	setAttr ".uvtk[2704]" -type "float2" 0.049590349 -0.019541383 ;
	setAttr ".uvtk[2705]" -type "float2" 0.032848239 -0.021397531 ;
	setAttr ".uvtk[2706]" -type "float2" 0.03275013 -0.01806137 ;
	setAttr ".uvtk[2707]" -type "float2" 0.049436212 -0.016405344 ;
	setAttr ".uvtk[2708]" -type "float2" 0.04735148 -0.0062342882 ;
	setAttr ".uvtk[2709]" -type "float2" 0.048257828 -0.0088646114 ;
	setAttr ".uvtk[2710]" -type "float2" 0.031401932 -0.0099409223 ;
	setAttr ".uvtk[2711]" -type "float2" 0.030602217 -0.0071212649 ;
	setAttr ".uvtk[2712]" -type "float2" -0.0034171343 -0.0086542368 ;
	setAttr ".uvtk[2713]" -type "float2" -0.0027110577 -0.011804163 ;
	setAttr ".uvtk[2714]" -type "float2" -0.013609052 -0.012607843 ;
	setAttr ".uvtk[2715]" -type "float2" -0.014163733 -0.0093272924 ;
	setAttr ".uvtk[2716]" -type "float2" -0.0052411556 -0.0024527609 ;
	setAttr ".uvtk[2717]" -type "float2" -0.0042563677 -0.0055500567 ;
	setAttr ".uvtk[2718]" -type "float2" -0.014939308 -0.006123364 ;
	setAttr ".uvtk[2719]" -type "float2" -0.015803933 -0.0029264092 ;
	setAttr ".uvtk[2720]" -type "float2" -0.017294288 -0.0033767819 ;
	setAttr ".uvtk[2721]" -type "float2" -0.016461134 -0.0066444576 ;
	setAttr ".uvtk[2722]" -type "float2" -0.015790403 -0.0099566579 ;
	setAttr ".uvtk[2723]" -type "float2" -0.015263438 -0.01334092 ;
	setAttr ".uvtk[2724]" -type "float2" 0.045011759 -0.00092381239 ;
	setAttr ".uvtk[2725]" -type "float2" 0.046327591 -0.0036228299 ;
	setAttr ".uvtk[2726]" -type "float2" 0.029558063 -0.0043046474 ;
	setAttr ".uvtk[2727]" -type "float2" 0.028455257 -0.0014415383 ;
	setAttr ".uvtk[2728]" -type "float2" 0.011052907 -0.0019578934 ;
	setAttr ".uvtk[2729]" -type "float2" 0.012152493 -0.004948765 ;
	setAttr ".uvtk[2730]" -type "float2" 0.013846695 -0.010928214 ;
	setAttr ".uvtk[2731]" -type "float2" 0.013073087 -0.0079204738 ;
	setAttr ".uvtk[2732]" -type "float2" -0.00117594 -0.024554849 ;
	setAttr ".uvtk[2733]" -type "float2" -0.012268901 -0.025856733 ;
	setAttr ".uvtk[2734]" -type "float2" -0.01227802 -0.022096157 ;
	setAttr ".uvtk[2735]" -type "float2" -0.0013952255 -0.020912319 ;
	setAttr ".uvtk[2736]" -type "float2" -0.0020577908 -0.014954776 ;
	setAttr ".uvtk[2737]" -type "float2" -0.0016076565 -0.017916769 ;
	setAttr ".uvtk[2738]" -type "float2" -0.012584507 -0.018984526 ;
	setAttr ".uvtk[2739]" -type "float2" -0.013104737 -0.01588884 ;
	setAttr ".uvtk[2740]" -type "float2" -0.014880121 -0.016733795 ;
	setAttr ".uvtk[2741]" -type "float2" -0.014532864 -0.019927949 ;
	setAttr ".uvtk[2742]" -type "float2" -0.01394397 -0.026978731 ;
	setAttr ".uvtk[2743]" -type "float2" -0.014156938 -0.023141414 ;
	setAttr ".uvtk[2744]" -type "float2" 0.048941672 -0.011486292 ;
	setAttr ".uvtk[2745]" -type "float2" 0.049138546 -0.013930708 ;
	setAttr ".uvtk[2746]" -type "float2" 0.032541573 -0.015383005 ;
	setAttr ".uvtk[2747]" -type "float2" 0.031938195 -0.012752891 ;
	setAttr ".uvtk[2748]" -type "float2" 0.01448828 -0.013911158 ;
	setAttr ".uvtk[2749]" -type "float2" 0.014901698 -0.016719431 ;
	setAttr ".uvtk[2750]" -type "float2" 0.015398383 -0.023071885 ;
	setAttr ".uvtk[2751]" -type "float2" 0.01528883 -0.019554943 ;
	setAttr ".uvtk[2754]" -type "float2" 0.011464715 -0.0014156103 ;
	setAttr ".uvtk[2755]" -type "float2" 0.01112771 -0.00079548359 ;
	setAttr ".uvtk[2756]" -type "float2" 0.046499252 -0.0052978396 ;
	setAttr ".uvtk[2757]" -type "float2" 0.05846256 -0.0065456927 ;
	setAttr ".uvtk[2758]" -type "float2" 0.05766362 -0.0043723583 ;
	setAttr ".uvtk[2759]" -type "float2" 0.04564172 -0.0034192801 ;
	setAttr ".uvtk[2760]" -type "float2" 0.043273747 0.00046944618 ;
	setAttr ".uvtk[2761]" -type "float2" 0.044523418 -0.0015245974 ;
	setAttr ".uvtk[2762]" -type "float2" 0.056480467 -0.0021930039 ;
	setAttr ".uvtk[2763]" -type "float2" 0.055223525 7.2747469e-05 ;
	setAttr ".uvtk[2764]" -type "float2" 0.056776047 -0.00042125583 ;
	setAttr ".uvtk[2765]" -type "float2" 0.058119893 -0.0028943717 ;
	setAttr ".uvtk[2766]" -type "float2" 0.060091734 -0.0077355504 ;
	setAttr ".uvtk[2767]" -type "float2" 0.05915308 -0.0053084195 ;
	setAttr ".uvtk[2770]" -type "float2" 0.010671854 -0.00014537573 ;
	setAttr ".uvtk[2771]" -type "float2" 0.010336161 0.00064817071 ;
	setAttr ".uvtk[2772]" -type "float2" 0.021729827 -0.00047644973 ;
	setAttr ".uvtk[2773]" -type "float2" 0.032947659 -0.00093874335 ;
	setAttr ".uvtk[2774]" -type "float2" 0.03178376 0.00075316429 ;
	setAttr ".uvtk[2775]" -type "float2" 0.020804524 0.00083601475 ;
	setAttr ".uvtk[2776]" -type "float2" 0.023127913 -0.0027271211 ;
	setAttr ".uvtk[2777]" -type "float2" 0.034701526 -0.0040359199 ;
	setAttr ".uvtk[2778]" -type "float2" 0.03392905 -0.0024936795 ;
	setAttr ".uvtk[2779]" -type "float2" 0.022586346 -0.0016225874 ;
	setAttr ".uvtk[2780]" -type "float2" 0.047369838 -0.012415528 ;
	setAttr ".uvtk[2781]" -type "float2" 0.059474826 -0.015057743 ;
	setAttr ".uvtk[2782]" -type "float2" 0.059540212 -0.012502253 ;
	setAttr ".uvtk[2783]" -type "float2" 0.047424793 -0.010258466 ;
	setAttr ".uvtk[2784]" -type "float2" 0.04700768 -0.0071241558 ;
	setAttr ".uvtk[2785]" -type "float2" 0.047285438 -0.0087099075 ;
	setAttr ".uvtk[2786]" -type "float2" 0.059362233 -0.010591596 ;
	setAttr ".uvtk[2787]" -type "float2" 0.05898267 -0.008671999 ;
	setAttr ".uvtk[2788]" -type "float2" 0.060698569 -0.010135561 ;
	setAttr ".uvtk[2789]" -type "float2" 0.061054885 -0.012331873 ;
	setAttr ".uvtk[2790]" -type "float2" 0.061248183 -0.017428875 ;
	setAttr ".uvtk[2791]" -type "float2" 0.061132193 -0.01455608 ;
	setAttr ".uvtk[2794]" -type "float2" 0.011764228 -0.0023389459 ;
	setAttr ".uvtk[2795]" -type "float2" 0.011579812 -0.0019882321 ;
	setAttr ".uvtk[2796]" -type "float2" 0.023674488 -0.0045771003 ;
	setAttr ".uvtk[2797]" -type "float2" 0.035511971 -0.0066837072 ;
	setAttr ".uvtk[2798]" -type "float2" 0.035194039 -0.0054950416 ;
	setAttr ".uvtk[2799]" -type "float2" 0.02348578 -0.0037835538 ;
	setAttr ".uvtk[2800]" -type "float2" 0.023684442 -0.0062792301 ;
	setAttr ".uvtk[2801]" -type "float2" 0.035437465 -0.0094938278 ;
	setAttr ".uvtk[2802]" -type "float2" 0.035513222 -0.0078159571 ;
	setAttr ".uvtk[2803]" -type "float2" 0.023787796 -0.0052194595 ;
	setAttr ".uvtk[2806]" -type "float2" -0.0023237467 -0.0067525506 ;
	setAttr ".uvtk[2807]" -type "float2" -0.0055558681 -0.0065940619 ;
	setAttr ".uvtk[2808]" -type "float2" -0.01055038 -0.04050529 ;
	setAttr ".uvtk[2809]" -type "float2" -0.01105684 -0.0392887 ;
	setAttr ".uvtk[2810]" -type "float2" -0.0036792159 -0.040172577 ;
	setAttr ".uvtk[2811]" -type "float2" -0.002846837 -0.0414505 ;
	setAttr ".uvtk[2812]" -type "float2" 0.012406349 -0.044442713 ;
	setAttr ".uvtk[2813]" -type "float2" 0.010501087 -0.042991817 ;
	setAttr ".uvtk[2814]" -type "float2" 0.02280727 -0.043263316 ;
	setAttr ".uvtk[2815]" -type "float2" 0.025269568 -0.044729948 ;
	setAttr ".uvtk[2816]" -type "float2" 0.017515242 -0.048034132 ;
	setAttr ".uvtk[2817]" -type "float2" 0.016693115 -0.049419761 ;
	setAttr ".uvtk[2818]" -type "float2" 0.029989004 -0.049736917 ;
	setAttr ".uvtk[2819]" -type "float2" 0.030826539 -0.048357844 ;
	setAttr ".uvtk[2820]" -type "float2" 0.062847435 -0.048812687 ;
	setAttr ".uvtk[2821]" -type "float2" 0.061897337 -0.050182343 ;
	setAttr ".uvtk[2822]" -type "float2" 0.078791022 -0.050332487 ;
	setAttr ".uvtk[2823]" -type "float2" 0.079832733 -0.048956573 ;
	setAttr ".uvtk[2824]" -type "float2" 0.063663453 -0.037835836 ;
	setAttr ".uvtk[2825]" -type "float2" 0.063401461 -0.044183016 ;
	setAttr ".uvtk[2826]" -type "float2" 0.080397576 -0.044326305 ;
	setAttr ".uvtk[2827]" -type "float2" 0.080646127 -0.03796953 ;
	setAttr ".uvtk[2828]" -type "float2" 0.088866204 -0.038039446 ;
	setAttr ".uvtk[2829]" -type "float2" 0.088506669 -0.044398248 ;
	setAttr ".uvtk[2830]" -type "float2" 0.087775499 -0.049034834 ;
	setAttr ".uvtk[2831]" -type "float2" 0.086630434 -0.05041039 ;
	setAttr ".uvtk[2832]" -type "float2" 0.018275082 -0.037081778 ;
	setAttr ".uvtk[2833]" -type "float2" 0.01803118 -0.043411016 ;
	setAttr ".uvtk[2834]" -type "float2" 0.031372607 -0.043730319 ;
	setAttr ".uvtk[2835]" -type "float2" 0.031583548 -0.037398458 ;
	setAttr ".uvtk[2836]" -type "float2" 0.047412097 -0.037642658 ;
	setAttr ".uvtk[2837]" -type "float2" 0.047252029 -0.043985963 ;
	setAttr ".uvtk[2838]" -type "float2" 0.045854747 -0.049986064 ;
	setAttr ".uvtk[2839]" -type "float2" 0.046752423 -0.048609614 ;
	setAttr ".uvtk[2840]" -type "float2" 0.056210876 -0.045138955 ;
	setAttr ".uvtk[2841]" -type "float2" 0.053133667 -0.043649316 ;
	setAttr ".uvtk[2842]" -type "float2" 0.069507778 -0.043780982 ;
	setAttr ".uvtk[2843]" -type "float2" 0.072482169 -0.04527241 ;
	setAttr ".uvtk[2844]" -type "float2" 0.060526103 -0.049033701 ;
	setAttr ".uvtk[2845]" -type "float2" 0.058627605 -0.047005296 ;
	setAttr ".uvtk[2846]" -type "float2" 0.075096637 -0.047148168 ;
	setAttr ".uvtk[2847]" -type "float2" 0.077173799 -0.049179137 ;
	setAttr ".uvtk[2848]" -type "float2" 0.085022032 -0.049257338 ;
	setAttr ".uvtk[2849]" -type "float2" 0.082825512 -0.047221661 ;
	setAttr ".uvtk[2850]" -type "float2" 0.080152273 -0.045342803 ;
	setAttr ".uvtk[2851]" -type "float2" 0.076804072 -0.043845832 ;
	setAttr ".uvtk[2852]" -type "float2" 0.015506268 -0.048289537 ;
	setAttr ".uvtk[2853]" -type "float2" 0.014188766 -0.046286702 ;
	setAttr ".uvtk[2854]" -type "float2" 0.027177066 -0.046583891 ;
	setAttr ".uvtk[2855]" -type "float2" 0.02882129 -0.048599541 ;
	setAttr ".uvtk[2856]" -type "float2" 0.044582069 -0.048836827 ;
	setAttr ".uvtk[2857]" -type "float2" 0.042806625 -0.04681772 ;
	setAttr ".uvtk[2858]" -type "float2" 0.037965298 -0.043475211 ;
	setAttr ".uvtk[2859]" -type "float2" 0.040468067 -0.044953763 ;
	setAttr ".uvtk[2860]" -type "float2" -0.0086397529 -0.043957531 ;
	setAttr ".uvtk[2861]" -type "float2" -0.0090370774 -0.045272112 ;
	setAttr ".uvtk[2862]" -type "float2" -0.0014141798 -0.046260178 ;
	setAttr ".uvtk[2863]" -type "float2" -0.0010039806 -0.044930279 ;
	setAttr ".uvtk[2864]" -type "float2" 0.013933659 -0.046506464 ;
	setAttr ".uvtk[2865]" -type "float2" 0.013301432 -0.047873497 ;
	setAttr ".uvtk[2866]" -type "float2" 0.016459346 -0.048508823 ;
	setAttr ".uvtk[2867]" -type "float2" 0.017139733 -0.047119856 ;
	setAttr ".uvtk[2868]" -type "float2" 0.014799535 -0.035633087 ;
	setAttr ".uvtk[2869]" -type "float2" 0.014442325 -0.041907012 ;
	setAttr ".uvtk[2870]" -type "float2" 0.017736077 -0.042510927 ;
	setAttr ".uvtk[2871]" -type "float2" 0.01808244 -0.036209106 ;
	setAttr ".uvtk[2872]" -type "float2" 0.017747164 -0.036692023 ;
	setAttr ".uvtk[2873]" -type "float2" 0.017483354 -0.043004572 ;
	setAttr ".uvtk[2874]" -type "float2" 0.016050816 -0.049013257 ;
	setAttr ".uvtk[2875]" -type "float2" 0.016933143 -0.047627628 ;
	setAttr ".uvtk[2876]" -type "float2" -0.0081555843 -0.033282578 ;
	setAttr ".uvtk[2877]" -type "float2" -0.0084059834 -0.039456427 ;
	setAttr ".uvtk[2878]" -type "float2" -0.0006121397 -0.040390193 ;
	setAttr ".uvtk[2879]" -type "float2" -0.00036245584 -0.034181774 ;
	setAttr ".uvtk[2880]" -type "float2" 0.007355392 -0.034952998 ;
	setAttr ".uvtk[2881]" -type "float2" 0.0070344806 -0.041203797 ;
	setAttr ".uvtk[2882]" -type "float2" 0.0061542392 -0.047126055 ;
	setAttr ".uvtk[2883]" -type "float2" 0.0066177249 -0.045768201 ;
	setAttr ".uvtk[2884]" -type "float2" 0.010150492 -0.041602492 ;
	setAttr ".uvtk[2885]" -type "float2" 0.012340248 -0.042176068 ;
	setAttr ".uvtk[2886]" -type "float2" 0.013525665 -0.043581307 ;
	setAttr ".uvtk[2887]" -type "float2" 0.011193752 -0.042981744 ;
	setAttr ".uvtk[2888]" -type "float2" 0.012585223 -0.046751022 ;
	setAttr ".uvtk[2889]" -type "float2" 0.011779785 -0.044770062 ;
	setAttr ".uvtk[2890]" -type "float2" 0.014698625 -0.045396388 ;
	setAttr ".uvtk[2891]" -type "float2" 0.015606701 -0.047381699 ;
	setAttr ".uvtk[2892]" -type "float2" 0.015115082 -0.047892034 ;
	setAttr ".uvtk[2893]" -type "float2" 0.013924599 -0.045894682 ;
	setAttr ".uvtk[2894]" -type "float2" 0.011055171 -0.042631328 ;
	setAttr ".uvtk[2895]" -type "float2" 0.012610018 -0.0440678 ;
	setAttr ".uvtk[2896]" -type "float2" -0.0095512867 -0.0441221 ;
	setAttr ".uvtk[2897]" -type "float2" -0.010040641 -0.042192042 ;
	setAttr ".uvtk[2898]" -type "float2" -0.0023682117 -0.043175638 ;
	setAttr ".uvtk[2899]" -type "float2" -0.0018740892 -0.045131803 ;
	setAttr ".uvtk[2900]" -type "float2" 0.004953444 -0.044032037 ;
	setAttr ".uvtk[2901]" -type "float2" 0.0055244565 -0.045994043 ;
	setAttr ".uvtk[2902]" -type "float2" 0.00362885 -0.040936947 ;
	setAttr ".uvtk[2903]" -type "float2" 0.0042401552 -0.04226172 ;
	setAttr ".uvtk[2906]" -type "float2" 0.00077077746 -0.0072287917 ;
	setAttr ".uvtk[2907]" -type "float2" -0.0036477745 -0.0071936846 ;
	setAttr ".uvtk[2908]" -type "float2" 0.0036154389 -0.034883618 ;
	setAttr ".uvtk[2909]" -type "float2" 0.0010663569 -0.029203057 ;
	setAttr ".uvtk[2910]" -type "float2" 0.01068002 -0.029362679 ;
	setAttr ".uvtk[2911]" -type "float2" 0.014127254 -0.035083354 ;
	setAttr ".uvtk[2912]" -type "float2" 0.040903628 -0.035361469 ;
	setAttr ".uvtk[2913]" -type "float2" 0.035212845 -0.029592812 ;
	setAttr ".uvtk[2914]" -type "float2" 0.049037844 -0.029673815 ;
	setAttr ".uvtk[2915]" -type "float2" 0.055642426 -0.035457551 ;
	setAttr ".uvtk[2916]" -type "float2" 0.049788386 -0.041981399 ;
	setAttr ".uvtk[2917]" -type "float2" 0.045661509 -0.039403319 ;
	setAttr ".uvtk[2918]" -type "float2" 0.061191708 -0.039515555 ;
	setAttr ".uvtk[2919]" -type "float2" 0.065688342 -0.042103589 ;
	setAttr ".uvtk[2920]" -type "float2" 0.072950214 -0.042168856 ;
	setAttr ".uvtk[2921]" -type "float2" 0.068148613 -0.039577544 ;
	setAttr ".uvtk[2922]" -type "float2" 0.062369496 -0.03551203 ;
	setAttr ".uvtk[2923]" -type "float2" 0.055012405 -0.029717565 ;
	setAttr ".uvtk[2924]" -type "float2" 0.0084550381 -0.041367233 ;
	setAttr ".uvtk[2925]" -type "float2" 0.0061553717 -0.03885293 ;
	setAttr ".uvtk[2926]" -type "float2" 0.017346263 -0.039084494 ;
	setAttr ".uvtk[2927]" -type "float2" 0.020253569 -0.041625679 ;
	setAttr ".uvtk[2928]" -type "float2" 0.034782857 -0.041820347 ;
	setAttr ".uvtk[2929]" -type "float2" 0.031257987 -0.039261997 ;
	setAttr ".uvtk[2930]" -type "float2" 0.022588432 -0.029489994 ;
	setAttr ".uvtk[2931]" -type "float2" 0.027192205 -0.035236061 ;
	setAttr ".uvtk[2934]" -type "float2" 0.017720461 -0.0073082447 ;
	setAttr ".uvtk[2935]" -type "float2" 0.011070073 -0.0072872639 ;
	setAttr ".uvtk[2936]" -type "float2" 0.028184295 -0.022550642 ;
	setAttr ".uvtk[2937]" -type "float2" 0.020359337 -0.014948368 ;
	setAttr ".uvtk[2938]" -type "float2" 0.030632198 -0.014991343 ;
	setAttr ".uvtk[2939]" -type "float2" 0.040954381 -0.022612393 ;
	setAttr ".uvtk[2940]" -type "float2" 0.045932531 -0.022644758 ;
	setAttr ".uvtk[2941]" -type "float2" 0.034690052 -0.015010417 ;
	setAttr ".uvtk[2943]" -type "float2" 0.019980013 -0.00731951 ;
	setAttr ".uvtk[2944]" -type "float2" -0.0015434623 -0.022257924 ;
	setAttr ".uvtk[2945]" -type "float2" -0.0032276213 -0.014757872 ;
	setAttr ".uvtk[2946]" -type "float2" 0.0035640895 -0.014827967 ;
	setAttr ".uvtk[2947]" -type "float2" 0.0072634816 -0.022373617 ;
	setAttr ".uvtk[2948]" -type "float2" 0.017559052 -0.022467315 ;
	setAttr ".uvtk[2949]" -type "float2" 0.011953861 -0.014890611 ;
	setAttr ".uvtk[2951]" -type "float2" 0.0059043467 -0.0072588325 ;
	setAttr ".uvtk[2952]" -type "float2" -0.012667239 -0.032265604 ;
	setAttr ".uvtk[2953]" -type "float2" -0.01243341 -0.027026355 ;
	setAttr ".uvtk[2954]" -type "float2" -0.0056325793 -0.027554274 ;
	setAttr ".uvtk[2955]" -type "float2" -0.0055269599 -0.0328933 ;
	setAttr ".uvtk[2956]" -type "float2" 0.0068676472 -0.033915818 ;
	setAttr ".uvtk[2957]" -type "float2" 0.0059386492 -0.028411031 ;
	setAttr ".uvtk[2958]" -type "float2" 0.0066292286 -0.028744817 ;
	setAttr ".uvtk[2959]" -type "float2" 0.0081576109 -0.034309983 ;
	setAttr ".uvtk[2960]" -type "float2" 0.0089028478 -0.040094793 ;
	setAttr ".uvtk[2961]" -type "float2" 0.0078857541 -0.03771764 ;
	setAttr ".uvtk[2962]" -type "float2" 0.0096107721 -0.038182318 ;
	setAttr ".uvtk[2963]" -type "float2" 0.011004746 -0.040616632 ;
	setAttr ".uvtk[2964]" -type "float2" 0.0093181729 -0.0410375 ;
	setAttr ".uvtk[2965]" -type "float2" 0.0074160099 -0.038552523 ;
	setAttr ".uvtk[2966]" -type "float2" 0.0055121779 -0.034629762 ;
	setAttr ".uvtk[2967]" -type "float2" 0.0037291646 -0.029000282 ;
	setAttr ".uvtk[2968]" -type "float2" -0.01203841 -0.037980556 ;
	setAttr ".uvtk[2969]" -type "float2" -0.012477338 -0.035832524 ;
	setAttr ".uvtk[2970]" -type "float2" -0.0050800443 -0.036546528 ;
	setAttr ".uvtk[2971]" -type "float2" -0.0043437481 -0.038785875 ;
	setAttr ".uvtk[2972]" -type "float2" 0.0027879477 -0.039476097 ;
	setAttr ".uvtk[2973]" -type "float2" 0.0017639399 -0.037170172 ;
	setAttr ".uvtk[2974]" -type "float2" 0.00043666363 -0.028011203 ;
	setAttr ".uvtk[2975]" -type "float2" 0.00095784664 -0.033434093 ;
	setAttr ".uvtk[2978]" -type "float2" 0.0027115345 -0.0070695877 ;
	setAttr ".uvtk[2979]" -type "float2" 0.0030187368 -0.0069844127 ;
	setAttr ".uvtk[2980]" -type "float2" 0.0050590038 -0.021651208 ;
	setAttr ".uvtk[2981]" -type "float2" 0.0041647553 -0.014343202 ;
	setAttr ".uvtk[2982]" -type "float2" 0.0044795871 -0.014514685 ;
	setAttr ".uvtk[2983]" -type "float2" 0.0053703189 -0.021907806 ;
	setAttr ".uvtk[2984]" -type "float2" 0.00210917 -0.022108018 ;
	setAttr ".uvtk[2985]" -type "float2" 0.00033700466 -0.014656603 ;
	setAttr ".uvtk[2987]" -type "float2" -0.00029301643 -0.0071420074 ;
	setAttr ".uvtk[2988]" -type "float2" -0.011217356 -0.020553529 ;
	setAttr ".uvtk[2989]" -type "float2" -0.0090715289 -0.013581216 ;
	setAttr ".uvtk[2990]" -type "float2" -0.0040606856 -0.013885081 ;
	setAttr ".uvtk[2991]" -type "float2" -0.0052016973 -0.020983338 ;
	setAttr ".uvtk[2992]" -type "float2" 0.00016146898 -0.021344781 ;
	setAttr ".uvtk[2993]" -type "float2" 0.00017005205 -0.014140069 ;
	setAttr ".uvtk[2995]" -type "float2" 0.00033420324 -0.006881237 ;
	setAttr ".uvtk[2998]" -type "float2" 0.010336161 -0.0058831573 ;
	setAttr ".uvtk[2999]" -type "float2" 0.010671854 -0.0058117509 ;
	setAttr ".uvtk[3000]" -type "float2" 0.045011759 -0.029585004 ;
	setAttr ".uvtk[3001]" -type "float2" 0.028455257 -0.031854868 ;
	setAttr ".uvtk[3002]" -type "float2" 0.029558063 -0.032706082 ;
	setAttr ".uvtk[3003]" -type "float2" 0.046327591 -0.030321121 ;
	setAttr ".uvtk[3004]" -type "float2" 0.049138546 -0.033701062 ;
	setAttr ".uvtk[3005]" -type "float2" 0.048941672 -0.034708977 ;
	setAttr ".uvtk[3006]" -type "float2" 0.031938195 -0.037171185 ;
	setAttr ".uvtk[3007]" -type "float2" 0.032541573 -0.036081314 ;
	setAttr ".uvtk[3008]" -type "float2" -0.0016076565 -0.039952099 ;
	setAttr ".uvtk[3009]" -type "float2" -0.0020577908 -0.041153669 ;
	setAttr ".uvtk[3010]" -type "float2" -0.013104737 -0.042745948 ;
	setAttr ".uvtk[3011]" -type "float2" -0.012584507 -0.041497707 ;
	setAttr ".uvtk[3012]" -type "float2" -0.00117594 -0.029649317 ;
	setAttr ".uvtk[3013]" -type "float2" -0.0013952255 -0.035611033 ;
	setAttr ".uvtk[3014]" -type "float2" -0.01227802 -0.037098348 ;
	setAttr ".uvtk[3015]" -type "float2" -0.012268901 -0.03104192 ;
	setAttr ".uvtk[3016]" -type "float2" -0.01394397 -0.032246053 ;
	setAttr ".uvtk[3017]" -type "float2" -0.014156938 -0.038361251 ;
	setAttr ".uvtk[3018]" -type "float2" -0.014532864 -0.042828918 ;
	setAttr ".uvtk[3019]" -type "float2" -0.014880121 -0.044105649 ;
	setAttr ".uvtk[3020]" -type "float2" 0.049590349 -0.024137795 ;
	setAttr ".uvtk[3021]" -type "float2" 0.049436212 -0.029685795 ;
	setAttr ".uvtk[3022]" -type "float2" 0.03275013 -0.03193444 ;
	setAttr ".uvtk[3023]" -type "float2" 0.032848239 -0.026218355 ;
	setAttr ".uvtk[3024]" -type "float2" 0.015398383 -0.028034687 ;
	setAttr ".uvtk[3025]" -type "float2" 0.01528883 -0.03389883 ;
	setAttr ".uvtk[3026]" -type "float2" 0.01448828 -0.039309621 ;
	setAttr ".uvtk[3027]" -type "float2" 0.014901698 -0.038148105 ;
	setAttr ".uvtk[3028]" -type "float2" -0.0052411556 -0.035583496 ;
	setAttr ".uvtk[3029]" -type "float2" -0.015803933 -0.037040353 ;
	setAttr ".uvtk[3030]" -type "float2" -0.014939308 -0.03810668 ;
	setAttr ".uvtk[3031]" -type "float2" -0.0042563677 -0.036581039 ;
	setAttr ".uvtk[3032]" -type "float2" -0.0027110577 -0.039994597 ;
	setAttr ".uvtk[3033]" -type "float2" -0.0034171343 -0.038119733 ;
	setAttr ".uvtk[3034]" -type "float2" -0.014163733 -0.039701104 ;
	setAttr ".uvtk[3035]" -type "float2" -0.013609052 -0.041593015 ;
	setAttr ".uvtk[3036]" -type "float2" -0.015263438 -0.042955875 ;
	setAttr ".uvtk[3037]" -type "float2" -0.015790403 -0.041035473 ;
	setAttr ".uvtk[3038]" -type "float2" -0.017294288 -0.038254023 ;
	setAttr ".uvtk[3039]" -type "float2" -0.016461134 -0.03940016 ;
	setAttr ".uvtk[3040]" -type "float2" 0.048257828 -0.033491611 ;
	setAttr ".uvtk[3041]" -type "float2" 0.04735148 -0.031692088 ;
	setAttr ".uvtk[3042]" -type "float2" 0.030602217 -0.034142554 ;
	setAttr ".uvtk[3043]" -type "float2" 0.031401932 -0.035974741 ;
	setAttr ".uvtk[3044]" -type "float2" 0.013846695 -0.038119793 ;
	setAttr ".uvtk[3045]" -type "float2" 0.013073087 -0.036269248 ;
	setAttr ".uvtk[3046]" -type "float2" 0.011052907 -0.033855975 ;
	setAttr ".uvtk[3047]" -type "float2" 0.012152493 -0.034772217 ;
	setAttr ".uvtk[3050]" -type "float2" 0.011579812 -0.0082490444 ;
	setAttr ".uvtk[3051]" -type "float2" 0.011764228 -0.0082095265 ;
	setAttr ".uvtk[3052]" -type "float2" 0.04700768 -0.024707198 ;
	setAttr ".uvtk[3053]" -type "float2" 0.05898267 -0.02856499 ;
	setAttr ".uvtk[3054]" -type "float2" 0.059362233 -0.027769566 ;
	setAttr ".uvtk[3055]" -type "float2" 0.047285438 -0.024060488 ;
	setAttr ".uvtk[3056]" -type "float2" 0.047369838 -0.016042352 ;
	setAttr ".uvtk[3057]" -type "float2" 0.047424793 -0.020702839 ;
	setAttr ".uvtk[3058]" -type "float2" 0.059540212 -0.024130344 ;
	setAttr ".uvtk[3059]" -type "float2" 0.059474826 -0.019092023 ;
	setAttr ".uvtk[3060]" -type "float2" 0.061248183 -0.021779358 ;
	setAttr ".uvtk[3061]" -type "float2" 0.061132193 -0.027091801 ;
	setAttr ".uvtk[3062]" -type "float2" 0.060698569 -0.031863987 ;
	setAttr ".uvtk[3063]" -type "float2" 0.061054885 -0.030946791 ;
	setAttr ".uvtk[3066]" -type "float2" 0.0118047 -0.0066409111 ;
	setAttr ".uvtk[3067]" -type "float2" 0.011770904 -0.0044124722 ;
	setAttr ".uvtk[3068]" -type "float2" 0.023787796 -0.012095869 ;
	setAttr ".uvtk[3069]" -type "float2" 0.035513222 -0.01672256 ;
	setAttr ".uvtk[3070]" -type "float2" 0.035437465 -0.012573361 ;
	setAttr ".uvtk[3071]" -type "float2" 0.023684442 -0.0086597204 ;
	setAttr ".uvtk[3072]" -type "float2" 0.02348578 -0.014755309 ;
	setAttr ".uvtk[3073]" -type "float2" 0.035194039 -0.020137668 ;
	setAttr ".uvtk[3074]" -type "float2" 0.035511971 -0.019682884 ;
	setAttr ".uvtk[3075]" -type "float2" 0.023674488 -0.014544427 ;
	setAttr ".uvtk[3076]" -type "float2" 0.043273747 -0.020201921 ;
	setAttr ".uvtk[3077]" -type "float2" 0.055223525 -0.023791075 ;
	setAttr ".uvtk[3078]" -type "float2" 0.056480467 -0.0243029 ;
	setAttr ".uvtk[3079]" -type "float2" 0.044523418 -0.020565927 ;
	setAttr ".uvtk[3080]" -type "float2" 0.046499252 -0.023380876 ;
	setAttr ".uvtk[3081]" -type "float2" 0.04564172 -0.021682203 ;
	setAttr ".uvtk[3082]" -type "float2" 0.05766362 -0.025530279 ;
	setAttr ".uvtk[3083]" -type "float2" 0.05846256 -0.027274132 ;
	setAttr ".uvtk[3084]" -type "float2" 0.060091734 -0.030615151 ;
	setAttr ".uvtk[3085]" -type "float2" 0.05915308 -0.028834224 ;
	setAttr ".uvtk[3086]" -type "float2" 0.056776047 -0.026902556 ;
	setAttr ".uvtk[3087]" -type "float2" 0.058119893 -0.027535141 ;
	setAttr ".uvtk[3090]" -type "float2" 0.01112771 -0.0062920451 ;
	setAttr ".uvtk[3091]" -type "float2" 0.011464715 -0.0072749257 ;
	setAttr ".uvtk[3092]" -type "float2" 0.022586346 -0.01207298 ;
	setAttr ".uvtk[3093]" -type "float2" 0.03392905 -0.017226636 ;
	setAttr ".uvtk[3094]" -type "float2" 0.034701526 -0.018816173 ;
	setAttr ".uvtk[3095]" -type "float2" 0.023127913 -0.01347363 ;
	setAttr ".uvtk[3096]" -type "float2" 0.020804524 -0.011255562 ;
	setAttr ".uvtk[3097]" -type "float2" 0.03178376 -0.016024411 ;
	setAttr ".uvtk[3098]" -type "float2" 0.032947659 -0.016233861 ;
	setAttr ".uvtk[3099]" -type "float2" 0.021729827 -0.011304379 ;
	setAttr ".uvtk[3102]" -type "float2" 0.0066869855 -0.0054389238 ;
	setAttr ".uvtk[3103]" -type "float2" 0.013429284 -0.0050822496 ;
	setAttr ".uvtk[3104]" -type "float2" 0.038405716 -0.025057435 ;
	setAttr ".uvtk[3105]" -type "float2" 0.034674764 -0.021084428 ;
	setAttr ".uvtk[3106]" -type "float2" 0.020012021 -0.022498667 ;
	setAttr ".uvtk[3107]" -type "float2" 0.0228163 -0.026791751 ;
	setAttr ".uvtk[3108]" -type "float2" -0.0079032779 -0.029543281 ;
	setAttr ".uvtk[3109]" -type "float2" -0.0085806251 -0.024755836 ;
	setAttr ".uvtk[3110]" -type "float2" -0.017470062 -0.025647104 ;
	setAttr ".uvtk[3111]" -type "float2" -0.017712116 -0.030611873 ;
	setAttr ".uvtk[3112]" -type "float2" -0.0062065125 -0.03454572 ;
	setAttr ".uvtk[3113]" -type "float2" -0.0071499348 -0.032718599 ;
	setAttr ".uvtk[3114]" -type "float2" -0.017115235 -0.033941627 ;
	setAttr ".uvtk[3115]" -type "float2" -0.016572297 -0.035895705 ;
	setAttr ".uvtk[3116]" -type "float2" -0.017797887 -0.037028372 ;
	setAttr ".uvtk[3117]" -type "float2" -0.018397033 -0.034966528 ;
	setAttr ".uvtk[3118]" -type "float2" -0.018618226 -0.031511009 ;
	setAttr ".uvtk[3119]" -type "float2" -0.018254876 -0.026396215 ;
	setAttr ".uvtk[3120]" -type "float2" 0.043385565 -0.02888602 ;
	setAttr ".uvtk[3121]" -type "float2" 0.041257739 -0.027556837 ;
	setAttr ".uvtk[3122]" -type "float2" 0.025077879 -0.029549241 ;
	setAttr ".uvtk[3123]" -type "float2" 0.026952624 -0.031055212 ;
	setAttr ".uvtk[3124]" -type "float2" 0.0098320246 -0.032930434 ;
	setAttr ".uvtk[3125]" -type "float2" 0.0084553957 -0.03126061 ;
	setAttr ".uvtk[3126]" -type "float2" 0.0052658319 -0.023715794 ;
	setAttr ".uvtk[3127]" -type "float2" 0.006949544 -0.02826798 ;
	setAttr ".uvtk[3130]" -type "float2" -0.0094638467 -0.0062214136 ;
	setAttr ".uvtk[3131]" -type "float2" -0.0054456592 -0.006002605 ;
	setAttr ".uvtk[3132]" -type "float2" -0.0085801482 -0.018809497 ;
	setAttr ".uvtk[3133]" -type "float2" -0.0077744126 -0.012399018 ;
	setAttr ".uvtk[3134]" -type "float2" -0.014312446 -0.012859046 ;
	setAttr ".uvtk[3135]" -type "float2" -0.016569853 -0.019493103 ;
	setAttr ".uvtk[3136]" -type "float2" -0.016920269 -0.02007091 ;
	setAttr ".uvtk[3137]" -type "float2" -0.014180005 -0.013238311 ;
	setAttr ".uvtk[3139]" -type "float2" -0.009154439 -0.0064095259 ;
	setAttr ".uvtk[3140]" -type "float2" 0.029445291 -0.016034544 ;
	setAttr ".uvtk[3141]" -type "float2" 0.02267319 -0.01055795 ;
	setAttr ".uvtk[3142]" -type "float2" 0.011979878 -0.011266291 ;
	setAttr ".uvtk[3143]" -type "float2" 0.016478956 -0.017096579 ;
	setAttr ".uvtk[3144]" -type "float2" 0.0036163926 -0.0180161 ;
	setAttr ".uvtk[3145]" -type "float2" 0.0020255446 -0.011863947 ;
	setAttr ".uvtk[3147]" -type "float2" 0.0005659461 -0.0057287216 ;
	setAttr ".uvtk[3150]" -type "float2" 0.0073027611 -0.0049821138 ;
	setAttr ".uvtk[3151]" -type "float2" 0.0083085299 -0.0059351921 ;
	setAttr ".uvtk[3152]" -type "float2" 0.03290844 -0.015112579 ;
	setAttr ".uvtk[3153]" -type "float2" 0.043550193 -0.017439723 ;
	setAttr ".uvtk[3154]" -type "float2" 0.047903597 -0.020679951 ;
	setAttr ".uvtk[3155]" -type "float2" 0.036778569 -0.017897487 ;
	setAttr ".uvtk[3156]" -type "float2" 0.041754961 -0.020056129 ;
	setAttr ".uvtk[3157]" -type "float2" 0.039522588 -0.019457459 ;
	setAttr ".uvtk[3158]" -type "float2" 0.051054597 -0.022598922 ;
	setAttr ".uvtk[3159]" -type "float2" 0.053446889 -0.023453057 ;
	setAttr ".uvtk[3160]" -type "float2" 0.055012286 -0.026383758 ;
	setAttr ".uvtk[3161]" -type "float2" 0.052665889 -0.025265992 ;
	setAttr ".uvtk[3162]" -type "float2" 0.045160592 -0.019403398 ;
	setAttr ".uvtk[3163]" -type "float2" 0.049538672 -0.023027956 ;
	setAttr ".uvtk[3166]" -type "float2" 0.0090758801 -0.006278336 ;
	setAttr ".uvtk[3167]" -type "float2" 0.0098237395 -0.0061655045 ;
	setAttr ".uvtk[3168]" -type "float2" 0.01882571 -0.011425376 ;
	setAttr ".uvtk[3169]" -type "float2" 0.028848529 -0.015768886 ;
	setAttr ".uvtk[3170]" -type "float2" 0.030410588 -0.0160833 ;
	setAttr ".uvtk[3171]" -type "float2" 0.019996345 -0.011476696 ;
	setAttr ".uvtk[3172]" -type "float2" 0.015102565 -0.008995235 ;
	setAttr ".uvtk[3173]" -type "float2" 0.023563385 -0.012315929 ;
	setAttr ".uvtk[3174]" -type "float2" 0.026537478 -0.014599979 ;
	setAttr ".uvtk[3175]" -type "float2" 0.017193973 -0.010674179 ;
	setAttr ".uvtk[3178]" -type "float2" 0.017494977 -0.0041416883 ;
	setAttr ".uvtk[3179]" -type "float2" 0.011517763 -0.0035207868 ;
	setAttr ".uvtk[3180]" -type "float2" 0.027555108 -0.011441767 ;
	setAttr ".uvtk[3181]" -type "float2" 0.020542979 -0.0074521899 ;
	setAttr ".uvtk[3182]" -type "float2" 0.029234827 -0.0086758137 ;
	setAttr ".uvtk[3183]" -type "float2" 0.037471414 -0.013241827 ;
	setAttr ".uvtk[3184]" -type "float2" 0.039184272 -0.014760971 ;
	setAttr ".uvtk[3185]" -type "float2" 0.030950963 -0.0097025633 ;
	setAttr ".uvtk[3187]" -type "float2" 0.018830717 -0.0046566725 ;
	setAttr ".uvtk[3190]" -type "float2" 0.0042198896 -0.0022366643 ;
	setAttr ".uvtk[3191]" -type "float2" 0.0058602095 -0.0036395192 ;
	setAttr ".uvtk[3192]" -type "float2" 0.0089809895 -0.0042405725 ;
	setAttr ".uvtk[3193]" -type "float2" 0.014043927 -0.0059913397 ;
	setAttr ".uvtk[3194]" -type "float2" 0.019568324 -0.0092684031 ;
	setAttr ".uvtk[3195]" -type "float2" 0.012379825 -0.0067020655 ;
	setAttr ".uvtk[3198]" -type "float2" 0.0075354576 -0.0027784705 ;
	setAttr ".uvtk[3199]" -type "float2" 0.004653275 -0.0019116998 ;
createNode expression -n "expression1";
	rename -uid "E7A853C8-47EC-E163-14D8-ABB4646DA09F";
	setAttr -k on ".nds";
	setAttr -s 12 ".out";
	setAttr ".ixp" -type "string" "$offset = .I[0];\r\n.O[0] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle.ty`;\r\n.O[1] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle.tz`;\r\n\r\n.O[2] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle1.ty`;\r\n.O[3] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle1.tz`;\r\n\r\n.O[4] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle2.ty`;\r\n.O[5] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle2.tz`;\r\n\r\n.O[6] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle3.ty`;\r\n.O[7] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle3.tz`;\r\n\r\n.O[8] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle4.ty`;\r\n.O[9] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle4.tz`;\r\n\r\n.O[10] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle5.ty`;\r\n.O[11] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle5.tz`;";
createNode makeThreePointCircularArc -n "threePointCircularArc2_waveGRP_10";
	rename -uid "45ECE97B-496A-A3E7-2942-02A4B82F8F20";
	setAttr ".s" 12;
createNode makeThreePointCircularArc -n "threePointCircularArc1_waveGRP_10";
	rename -uid "79BF4FAD-4DC3-8B05-F206-52B63D61F8C5";
	setAttr ".s" 12;
createNode expression -n "expression2";
	rename -uid "D1CE2F06-4925-3990-AD62-25A4FB34771E";
	setAttr -k on ".nds";
	setAttr -s 12 ".out";
	setAttr ".ixp" -type "string" "$offset = .I[0];\r\n.O[0] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle.ty`;\r\n.O[1] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle.tz`;\r\n\r\n.O[2] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle1.ty`;\r\n.O[3] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle1.tz`;\r\n\r\n.O[4] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle2.ty`;\r\n.O[5] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle2.tz`;\r\n\r\n.O[6] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle3.ty`;\r\n.O[7] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle3.tz`;\r\n\r\n.O[8] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle4.ty`;\r\n.O[9] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle4.tz`;\r\n\r\n.O[10] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle5.ty`;\r\n.O[11] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle5.tz`;";
createNode expression -n "expression3";
	rename -uid "344233B3-4BC7-9A0D-5E5D-DDB1DC211EE5";
	setAttr -k on ".nds";
	setAttr -s 12 ".out";
	setAttr ".ixp" -type "string" "$offset = .I[0];\r\n.O[0] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle.ty`;\r\n.O[1] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle.tz`;\r\n\r\n.O[2] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle1.ty`;\r\n.O[3] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle1.tz`;\r\n\r\n.O[4] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle2.ty`;\r\n.O[5] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle2.tz`;\r\n\r\n.O[6] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle3.ty`;\r\n.O[7] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle3.tz`;\r\n\r\n.O[8] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle4.ty`;\r\n.O[9] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle4.tz`;\r\n\r\n.O[10] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle5.ty`;\r\n.O[11] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle5.tz`;";
createNode expression -n "expression4";
	rename -uid "CFBE19DD-4BDF-D1C8-FDFD-4B9880FF5B02";
	setAttr -k on ".nds";
	setAttr -s 12 ".out";
	setAttr ".ixp" -type "string" "$offset = .I[0];\r\n.O[0] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle.ty`;\r\n.O[1] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle.tz`;\r\n\r\n.O[2] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle1.ty`;\r\n.O[3] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle1.tz`;\r\n\r\n.O[4] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle2.ty`;\r\n.O[5] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle2.tz`;\r\n\r\n.O[6] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle3.ty`;\r\n.O[7] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle3.tz`;\r\n\r\n.O[8] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle4.ty`;\r\n.O[9] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle4.tz`;\r\n\r\n.O[10] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle5.ty`;\r\n.O[11] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle5.tz`;";
createNode expression -n "expression5";
	rename -uid "3D86720E-48DD-3D35-33C0-DAA9C038D8C6";
	setAttr -k on ".nds";
	setAttr -s 12 ".out";
	setAttr ".ixp" -type "string" "$offset = .I[0];\r\n.O[0] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle.ty`;\r\n.O[1] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle.tz`;\r\n\r\n.O[2] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle1.ty`;\r\n.O[3] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle1.tz`;\r\n\r\n.O[4] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle2.ty`;\r\n.O[5] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle2.tz`;\r\n\r\n.O[6] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle3.ty`;\r\n.O[7] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle3.tz`;\r\n\r\n.O[8] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle4.ty`;\r\n.O[9] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle4.tz`;\r\n\r\n.O[10] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle5.ty`;\r\n.O[11] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle5.tz`;";
createNode expression -n "expression6";
	rename -uid "92649A8E-48D5-ABDD-335F-79AFE2AB5183";
	setAttr -k on ".nds";
	setAttr -s 12 ".out";
	setAttr ".ixp" -type "string" "$offset = .I[0];\r\n.O[0] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle.ty`;\r\n.O[1] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle.tz`;\r\n\r\n.O[2] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle1.ty`;\r\n.O[3] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle1.tz`;\r\n\r\n.O[4] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle2.ty`;\r\n.O[5] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle2.tz`;\r\n\r\n.O[6] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle3.ty`;\r\n.O[7] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle3.tz`;\r\n\r\n.O[8] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle4.ty`;\r\n.O[9] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle4.tz`;\r\n\r\n.O[10] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle5.ty`;\r\n.O[11] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle5.tz`;";
createNode expression -n "expression7";
	rename -uid "C8CE5993-4406-17F1-D602-3F99747D2D38";
	setAttr -k on ".nds";
	setAttr -s 12 ".out";
	setAttr ".ixp" -type "string" "$offset = .I[0];\r\n.O[0] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle.ty`;\r\n.O[1] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle.tz`;\r\n\r\n.O[2] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle1.ty`;\r\n.O[3] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle1.tz`;\r\n\r\n.O[4] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle2.ty`;\r\n.O[5] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle2.tz`;\r\n\r\n.O[6] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle3.ty`;\r\n.O[7] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle3.tz`;\r\n\r\n.O[8] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle4.ty`;\r\n.O[9] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle4.tz`;\r\n\r\n.O[10] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle5.ty`;\r\n.O[11] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle5.tz`;";
createNode expression -n "expression8";
	rename -uid "F1B870C2-4436-9444-8FB3-1098E38D816E";
	setAttr -k on ".nds";
	setAttr -s 12 ".out";
	setAttr ".ixp" -type "string" "$offset = .I[0];\r\n.O[0] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle.ty`;\r\n.O[1] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle.tz`;\r\n\r\n.O[2] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle1.ty`;\r\n.O[3] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle1.tz`;\r\n\r\n.O[4] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle2.ty`;\r\n.O[5] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle2.tz`;\r\n\r\n.O[6] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle3.ty`;\r\n.O[7] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle3.tz`;\r\n\r\n.O[8] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle4.ty`;\r\n.O[9] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle4.tz`;\r\n\r\n.O[10] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle5.ty`;\r\n.O[11] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle5.tz`;";
createNode expression -n "expression9";
	rename -uid "9B698FC4-4D09-F949-A42C-15BCACF281EB";
	setAttr -k on ".nds";
	setAttr -s 12 ".out";
	setAttr ".ixp" -type "string" "$offset = .I[0];\r\n.O[0] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle.ty`;\r\n.O[1] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle.tz`;\r\n\r\n.O[2] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle1.ty`;\r\n.O[3] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle1.tz`;\r\n\r\n.O[4] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle2.ty`;\r\n.O[5] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle2.tz`;\r\n\r\n.O[6] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle3.ty`;\r\n.O[7] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle3.tz`;\r\n\r\n.O[8] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle4.ty`;\r\n.O[9] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle4.tz`;\r\n\r\n.O[10] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle5.ty`;\r\n.O[11] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle5.tz`;";
createNode animCurveTL -n "waveTest_waveHandle_translateY";
	rename -uid "E157442F-4FA2-735A-FC89-C09741D20EDC";
	setAttr ".tan" 9;
	setAttr -s 12 ".ktv[0:11]"  1 0.44533595399892845 45 1.5861975442562612
		 85 7.2061623876623333 107 5.8939687288825739 117 2.714353948651544 124 0.74886209310399132
		 148 0.7805151652488167 156 1.3718397623342908 166 0.54315666261445616 178 0.89980792304004475
		 232 0.44533595399892845 248 0.44533595399892845;
	setAttr -s 12 ".kit[2:11]"  3 1 10 9 9 9 9 9 
		3 3;
	setAttr -s 12 ".kot[2:11]"  1 1 10 9 9 9 9 9 
		3 3;
	setAttr -s 12 ".ktl[0:11]" no yes no no yes yes yes yes yes yes no 
		no;
	setAttr -s 12 ".kwl[1:11]" yes no no yes yes yes yes yes yes no no;
	setAttr -s 12 ".kix[3:11]"  0.95138150453567505 0.41666650772094727 
		0.29166650772094727 1 0.33333349227905273 0.41666650772094727 0.5 2.2500004768371582 
		0.66666603088378906;
	setAttr -s 12 ".kiy[3:11]"  -3.406257152557373 -3.0265336036682129 
		-0.43667307496070862 0.46723321080207825 -0.1054927185177803 -0.2145598828792572 
		-0.017785580828785896 0 0;
	setAttr -s 12 ".kox[2:11]"  0.71219903230667114 0.81409639120101929 
		0.29166650772094727 1 0.33333349227905273 0.41666650772094727 0.5 2.2500004768371582 
		0.66666603088378906 0.66666603088378906;
	setAttr -s 12 ".koy[2:11]"  0 -3.3241145610809326 -2.1185731887817383 
		-1.4971656799316406 0.15574447810649872 -0.13186578452587128 -0.25747194886207581 
		-0.080035127699375153 0 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "waveTest_waveHandle_translateZ";
	rename -uid "744CE765-4BDF-594D-6B72-E99E32D6ADF4";
	setAttr ".tan" 9;
	setAttr -s 11 ".ktv[0:10]"  1 -8 45 -7.8703866809749394 107 3.9231598768422629
		 117 7.1544525654640907 124 8.8745166494698431 148 10.469467607200341 156 6.7024878757741719
		 166 -0.18876055986092299 178 -3.1115506887139368 232 -8 248 -8;
	setAttr -s 11 ".kit[1:10]"  1 1 10 9 9 9 9 9 
		3 3;
	setAttr -s 11 ".kot[1:10]"  1 2 10 9 9 9 9 9 
		3 3;
	setAttr -s 11 ".ktl[0:10]" no yes no yes yes yes yes yes yes no no;
	setAttr -s 11 ".kwl[1:10]" yes no yes yes yes yes yes yes no no;
	setAttr -s 11 ".kix[1:10]"  0.67732477188110352 4.6693267822265625 
		0.41666650772094727 0.29166650772094727 1 0.33333349227905273 0.41666650772094727 
		0.5 2.2500004768371582 0.66666603088378906;
	setAttr -s 11 ".kiy[1:10]"  1.3377916812896729 34.824344635009766 2.9125630855560303 
		0.74855148792266846 -1.6290214061737061 -4.736992359161377 -4.4609255790710449 -1.4202251434326172 
		0 0;
	setAttr -s 11 ".kox[1:10]"  2.6246349811553955 0.41666650772094727 
		0.29166650772094727 1 0.33333349227905273 0.41666650772094727 0.5 2.2500004768371582 
		0.66666603088378906 0.66666603088378906;
	setAttr -s 11 ".koy[1:10]"  5.1839408874511719 3.231292724609375 2.0387938022613525 
		2.5664634704589844 -0.54300737380981445 -5.9212355613708496 -5.3531126976013184 -6.3910140991210938 
		0 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "waveTest_waveHandle1_translateY";
	rename -uid "EB3E300B-43D9-BBFA-C7D7-9C861CF637EA";
	setAttr ".tan" 9;
	setAttr -s 9 ".ktv[0:8]"  1 0.44533595399892845 107 7.3604653063224985
		 124 6.5301496995465342 148 3.3587335322974594 156 2.1250110017806891 166 0.67763800457518752
		 178 0.77536146222242153 232 0.44533595399892845 248 0.44533595399892845;
	setAttr -s 9 ".kit[0:8]"  2 3 9 9 9 9 9 3 
		3;
	setAttr -s 9 ".kot[0:8]"  1 3 9 9 9 9 9 3 
		3;
	setAttr -s 9 ".ktl[0:8]" no yes yes yes yes yes yes no no;
	setAttr -s 9 ".kwl[1:8]" yes yes yes yes yes yes no no;
	setAttr -s 9 ".kox[0:8]"  4.1368808746337891 0.70833301544189453 
		1 0.33333349227905273 0.41666650772094727 0.5 2.2500004768371582 0.66666603088378906 
		0.66666603088378906;
	setAttr -s 9 ".koy[0:8]"  1.6084121465682983 0 -2.342477560043335 
		-1.1012850999832153 -1.4894969463348389 -0.73617261648178101 -0.19006532430648804 
		0 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "waveTest_waveHandle1_translateZ";
	rename -uid "0BCE36FC-43F7-0829-52E4-68AE2DC229FD";
	setAttr ".tan" 9;
	setAttr -s 10 ".ktv[0:9]"  1 -11 55 -11.49076488366101 85 -8.6519540163589905
		 124 4.5813660466209152 148 7.7291478867956362 156 4.6478288581995475 166 -3.1565152573053266
		 177 -7.4006122297863017 232 -11 248 -11;
	setAttr -s 10 ".kit[0:9]"  2 2 1 1 9 9 9 9 
		3 3;
	setAttr -s 10 ".kot[0:9]"  2 1 1 1 9 9 9 9 
		3 3;
	setAttr -s 10 ".ktl[0:9]" no no no yes no yes yes yes no no;
	setAttr -s 10 ".kwl[3:9]" yes no yes yes yes no no;
	setAttr -s 10 ".kix[2:9]"  1.0557798147201538 0.68265062570571899 
		1 0.33333349227905273 0.41666650772094727 0.45833349227905273 2.2916669845581055 
		0.66666603088378906;
	setAttr -s 10 ".kiy[2:9]"  4.0641570091247559 4.6785697937011719 0.049847103655338287 
		-4.8380746841430664 -5.7373509407043457 -1.3072476387023926 0 0;
	setAttr -s 10 ".kox[1:9]"  1.0775530338287354 0.64264810085296631 
		0.96374219655990601 0.33333349227905273 0.41666650772094727 0.45833349227905273 2.2916669845581055 
		0.66666603088378906 0.66666603088378906;
	setAttr -s 10 ".koy[1:9]"  -0.028543993830680847 3.976534366607666 
		6.605039119720459 0.016615709289908409 -6.0475883483886719 -6.3110904693603516 -6.5362372398376465 
		0 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "waveTest_waveHandle2_translateY";
	rename -uid "A42FDD4B-4841-8813-21DD-CEA7618C1314";
	setAttr ".tan" 9;
	setAttr -s 10 ".ktv[0:9]"  1 0.40100378652987034 55 0.63195196707622925
		 85 2.9907350018243788 124 4.0126509991750616 148 4.394360660012512 156 2.870181255155503
		 166 1.0911794186593395 178 0.72897026072752702 232 0.40100378652987034 248 0.40100378652987034;
	setAttr -s 10 ".kit[0:9]"  2 2 1 9 9 9 9 9 
		3 3;
	setAttr -s 10 ".kot[0:9]"  2 1 1 9 9 9 9 9 
		3 3;
	setAttr -s 10 ".ktl[0:9]" no no no no yes yes yes yes no no;
	setAttr -s 10 ".kwl[4:9]" yes yes yes yes no no;
	setAttr -s 10 ".kix[2:9]"  1.388070821762085 1.6249997615814209 1 
		0.33333349227905273 0.41666650772094727 0.5 2.2500004768371582 0.66666603088378906;
	setAttr -s 10 ".kiy[2:9]"  3.0677680969238281 0.86891108751296997 
		-0.85685223340988159 -1.4680812358856201 -0.9732775092124939 -0.12548646330833435 
		0 0;
	setAttr -s 10 ".kox[1:9]"  1.5941011905670166 1.3150429725646973 1 
		0.33333349227905273 0.41666650772094727 0.5 2.2500004768371582 0.66666603088378906 
		0.66666603088378906;
	setAttr -s 10 ".koy[1:9]"  0.54391586780548096 3.3172571659088135 
		0.53471457958221436 -0.28561753034591675 -1.8350999355316162 -1.167933464050293 -0.56468915939331055 
		0 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "waveTest_waveHandle2_translateZ";
	rename -uid "A8430062-4A6C-F9ED-C2B1-1B839F6F1A22";
	setAttr ".tan" 9;
	setAttr -s 9 ".ktv[0:8]"  1 -14 68 -13.40411228913811 124 -6.695788468470127
		 148 -3.1429523945215365 156 -0.8041998786472444 166 -6.7613818448369321 178 -11.314419064487144
		 232 -14 248 -14;
	setAttr -s 9 ".kit[0:8]"  2 1 9 9 9 9 10 3 
		1;
	setAttr -s 9 ".kot[0:8]"  2 1 9 9 9 9 10 3 
		1;
	setAttr -s 9 ".ktl[0:8]" no no no yes yes yes yes no no;
	setAttr -s 9 ".kwl[3:8]" yes yes yes yes no no;
	setAttr -s 9 ".kix[1:8]"  1.7066148519515991 2.3333332538604736 1 
		0.33333349227905273 0.41666650772094727 0.5 2.2500004768371582 1;
	setAttr -s 9 ".kiy[1:8]"  2.9134757518768311 7.1828117370605469 4.4186906814575195 
		-1.6081916093826294 -4.7773714065551758 -1.3161121606826782 0 0;
	setAttr -s 9 ".kox[1:8]"  2.3823027610778809 1 0.33333349227905273 
		0.41666650772094727 0.5 2.2500004768371582 0.66666603088378906 2.7916665077209473;
	setAttr -s 9 ".koy[1:8]"  5.7150726318359375 3.0783481597900391 1.4728976488113403 
		-2.0102379322052002 -5.7328476905822754 -5.9225058555603027 0 0.59588772058486938;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "waveTest_waveHandle3_translateY";
	rename -uid "2C09A77A-4744-9D26-267B-ADA99D484327";
	setAttr ".tan" 9;
	setAttr -s 13 ".ktv[0:12]"  1 0.44533595399892845 68 1.9060120689512488
		 85 2.9780438563524005 107 3.9032325193538053 114 3.3215310189448948 124 4.4272886525978734
		 148 2.0407172494453665 152 -0.055033027058809303 157 0.46752861481976815 166 0.47661207859848442
		 178 0.82732526853202293 232 0.44533595399892845 248 0.44533595399892845;
	setAttr -s 13 ".kit[0:12]"  2 9 1 9 10 9 9 9 
		9 9 9 3 3;
	setAttr -s 13 ".kot[0:12]"  2 9 1 9 10 9 9 9 
		9 9 9 3 3;
	setAttr -s 13 ".ktl[0:12]" no yes yes yes yes yes yes yes yes yes yes 
		no no;
	setAttr -s 13 ".kwl[1:12]" yes yes yes yes yes yes yes yes yes yes 
		no no;
	setAttr -s 13 ".kix[2:12]"  0.69367349147796631 0.91666674613952637 
		0.29166650772094727 0.41666650772094727 1 0.16666698455810547 0.20833301544189453 
		0.375 0.5 2.2500004768371582 0.66666603088378906;
	setAttr -s 13 ".kiy[2:12]"  1.2011528015136719 0.2605765163898468 0.21578779816627502 
		-0.37670981884002686 -3.8419890403747559 -0.69919627904891968 0.18987306952476501 
		0.15419857203960419 -0.0056865671649575233 0 0;
	setAttr -s 13 ".kox[2:12]"  1.5907626152038574 0.29166650772094727 
		0.41666650772094727 1 0.16666698455810547 0.20833301544189453 0.375 0.5 2.2500004768371582 
		0.66666603088378906 0.66666603088378906;
	setAttr -s 13 ".koy[2:12]"  2.8003411293029785 0.082910656929016113 
		0.30826833844184875 -0.90410393476486206 -0.64033269882202148 -0.87399232387542725 
		0.34177204966545105 0.20559808611869812 -0.025589557364583015 0 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "waveTest_waveHandle3_translateZ";
	rename -uid "4825D1C3-4B8D-5811-54E7-8A81C1B0D5D1";
	setAttr ".tan" 9;
	setAttr -s 13 ".ktv[0:12]"  1 -1.9999999999999991 68 -1.8891834287533973
		 85 -2.2935651592370605 107 -2.6420679886592597 114 -1.6189334602937917 124 1.3568296133845286
		 148 5.8224130168173884 152 7.4829555435713031 157 7.9896809744409802 166 4.3550011788237768
		 178 2.0274517109342631 232 -1.9999999999999991 248 -1.9999999999999991;
	setAttr -s 13 ".kit[0:12]"  2 2 1 9 10 9 9 9 
		9 9 9 3 3;
	setAttr -s 13 ".kot[0:12]"  2 2 1 9 10 9 9 9 
		9 9 9 3 3;
	setAttr -s 13 ".ktl[0:12]" no yes yes yes yes yes yes yes yes yes yes 
		no no;
	setAttr -s 13 ".kwl[1:12]" yes yes yes yes yes yes yes yes yes yes 
		no no;
	setAttr -s 13 ".kix[2:12]"  2.3258447647094727 0.91666674613952637 
		0.29166650772094727 0.41666650772094727 1 0.16666698455810547 0.20833301544189453 
		0.375 0.5 2.2500004768371582 0.66666603088378906;
	setAttr -s 13 ".kiy[2:12]"  -1.6265275478363037 0.51178961992263794 
		1.6466047763824463 2.1886308193206787 5.2509636878967285 0.96323204040527344 -1.1171255111694336 
		-2.5552411079406738 -1.1554545164108276 0 0;
	setAttr -s 13 ".kox[2:12]"  0.91372472047805786 0.29166650772094727 
		0.41666650772094727 1 0.16666698455810547 0.20833301544189453 0.375 0.5 2.2500004768371582 
		0.66666603088378906 0.66666603088378906;
	setAttr -s 13 ".koy[2:12]"  -0.63899284601211548 0.1628420501947403 
		2.3522927761077881 5.2527155876159668 0.87516230344772339 1.2040358781814575 -2.010828971862793 
		-3.4069881439208984 -5.1995468139648438 0 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "waveTest_waveHandle4_translateY";
	rename -uid "E1F5DA8C-4D1F-B95D-4368-86B4CFD31B65";
	setAttr ".tan" 9;
	setAttr -s 10 ".ktv[0:9]"  1 0.40100378652987034 42 0.54534639937134433
		 85 0.57502002017859644 124 -2.7218400223110617 148 -0.70310802670822903 157 0.19132648602989749
		 166 0.25957860264303345 178 0.42613217126758829 232 0.40100378652987034 248 0.40100378652987034;
	setAttr -s 10 ".kit[0:9]"  2 2 2 9 9 9 9 9 
		3 3;
	setAttr -s 10 ".kot[0:9]"  2 2 1 9 9 9 9 9 
		3 3;
	setAttr -s 10 ".ktl[2:9]" no yes yes yes yes yes yes yes;
	setAttr -s 10 ".kwl[0:9]" yes yes no yes yes yes yes yes yes yes;
	setAttr -s 10 ".kox[2:9]"  2.1261515617370605 1 0.375 0.375 0.5 2.2500004768371582 
		0.66666603088378906 0.66666603088378906;
	setAttr -s 10 ".koy[2:9]"  -0.36788618564605713 -0.48690596222877502 
		0.79449993371963501 0.48134332895278931 0.13417467474937439 0.1157115176320076 0 
		0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "waveTest_waveHandle4_translateZ";
	rename -uid "A656954C-4884-8E8A-F07F-FFA8B38F23D2";
	setAttr ".tan" 9;
	setAttr -s 7 ".ktv[0:6]"  1 1.0000000000000009 85 1.1483681040362566
		 109 2.3341694202003658 157 9.192598767053898 178 5.8079129916005936 232 1.0000000000000009
		 248 1.0000000000000009;
	setAttr -s 7 ".kit[0:6]"  2 2 1 9 9 3 3;
	setAttr -s 7 ".kot[0:6]"  2 1 1 9 9 3 3;
	setAttr -s 7 ".ktl[0:6]" no no no yes no no no;
	setAttr -s 7 ".kwl[3:6]" yes no no no;
	setAttr -s 7 ".kix[2:6]"  1.2560138702392578 2 0.875 2.2500004768371582 
		0.66666603088378906;
	setAttr -s 7 ".kiy[2:6]"  2.9973986148834229 2.4165172576904297 -2.2939271926879883 
		0 0;
	setAttr -s 7 ".kox[1:6]"  1.0768041610717773 1.8391022682189941 0.875 
		2.2500004768371582 0.66666603088378906 0.66666603088378906;
	setAttr -s 7 ".koy[1:6]"  0.32940191030502319 4.8505825996398926 
		1.057226300239563 -5.8986716270446777 0 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "waveTest_waveHandle5_translateY";
	rename -uid "D36CBE68-49E8-F7C7-A141-999125845C95";
	setAttr ".tan" 9;
	setAttr -s 11 ".ktv[0:10]"  1 0.44533595399892845 45 1.4283228259549461
		 85 5.9050521544648644 107 4.7198074969768227 124 0.41349548475062781 148 0.96090811845765889
		 157 0.96275066197566961 166 0.84669118223778761 178 0.78101059187261712 232 0.44533595399892845
		 248 0.44533595399892845;
	setAttr -s 11 ".kit[2:10]"  3 1 9 9 9 9 9 3 
		3;
	setAttr -s 11 ".kot[2:10]"  3 2 9 9 9 9 9 3 
		3;
	setAttr -s 11 ".ktl[0:10]" no yes no no yes yes yes yes yes no no;
	setAttr -s 11 ".kwl[1:10]" yes no yes yes yes yes yes yes no no;
	setAttr -s 11 ".kix[3:10]"  0.83445614576339722 0.70833301544189453 
		1 0.375 0.375 0.5 2.2500004768371582 0.66666603088378906;
	setAttr -s 11 ".kiy[3:10]"  -2.1998939514160156 -1.5585676431655884 
		0.39945831894874573 -0.057108469307422638 -0.077888600528240204 -0.072973668575286865 
		0 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "waveTest_waveHandle5_translateZ";
	rename -uid "A23B3F49-47BD-04D1-0ABF-83858243F6B4";
	setAttr ".tan" 9;
	setAttr -s 11 ".ktv[0:10]"  1 -5.0000000000000009 45 -5.0100770181651173
		 68 -3.955087706081458 107 3.1538049360824103 124 7.7145223170926958 148 8.5494065525210079
		 157 6.9560401473590687 166 2.508067107843011 178 0.56283398435263488 232 -5.0000000000000009
		 248 -5.0000000000000009;
	setAttr -s 11 ".kit[0:10]"  2 2 9 1 9 9 9 9 
		9 3 3;
	setAttr -s 11 ".kot[0:10]"  2 2 1 2 9 9 9 9 
		9 3 3;
	setAttr -s 11 ".ktl[0:10]" no yes no yes yes yes yes yes yes no no;
	setAttr -s 11 ".kwl[1:10]" yes no yes yes yes yes yes yes no no;
	setAttr -s 11 ".kix[3:10]"  1.673969030380249 0.70833301544189453 1 
		0.375 0.375 0.5 2.2500004768371582 0.66666603088378906;
	setAttr -s 11 ".kiy[3:10]"  9.3583049774169922 2.2372000217437744 -0.55162340402603149 
		-3.02066969871521 -2.7399454116821289 -1.3651028871536255 0 0;
	setAttr -s 11 ".kox[2:10]"  1.771906852722168 0.70833301544189453 1 
		0.375 0.375 0.5 2.2500004768371582 0.66666603088378906 0.66666603088378906;
	setAttr -s 11 ".koy[2:10]"  3.692730188369751 4.5607175827026367 3.1584014892578125 
		-0.20685876905918121 -3.02066969871521 -3.6532607078552246 -6.1429643630981445 0 
		0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode makeNurbCircle -n "waveTest_makeNurbCircle1";
	rename -uid "D925C514-42B3-D075-AF50-E79860F03440";
	setAttr ".nr" -type "double3" 1 0 2.2204460492503131e-16 ;
	setAttr ".r" 0.27847107727465586;
createNode makeThreePointCircularArc -n "waveTest_makeThreePointCircularArc3";
	rename -uid "35C2A7B4-4E83-E5BC-4DC3-08B31FD68112";
	setAttr ".s" 12;
	setAttr ".pt1" -type "double3" 0 0.31649962882933713 3.4738164764866752 ;
	setAttr ".pt2" -type "double3" 0 0.35082020593628394 8.0384532317106761 ;
	setAttr ".pt3" -type "double3" 0 0.31649962882933713 12.122601907437414 ;
createNode attachCurve -n "waveTest_attachCurve3";
	rename -uid "2C046484-4539-545D-5114-1CA463E5C608";
	setAttr ".rv1" yes;
	setAttr ".rv2" yes;
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode makeThreePointCircularArc -n "threePointCircularArc1_waveMasterGRP";
	rename -uid "9FFCF1AA-48B7-1CD6-30D8-8F993632E1E2";
	setAttr ".s" 12;
createNode makeThreePointCircularArc -n "threePointCircularArc2_waveMasterGRP";
	rename -uid "B9B626A7-4802-CBA8-5E62-B5A54E602B55";
	setAttr ".s" 12;
createNode attachCurve -n "waveTest_attachCurve1";
	rename -uid "E28CDAFC-45D3-76E7-92A6-D4BA460D1782";
	setAttr ".rv1" yes;
	setAttr ".rv2" yes;
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode attachCurve -n "waveTest_attachCurve2";
	rename -uid "7A222E2E-4601-72B2-965F-198CF89E219E";
	setAttr ".m" 1;
	setAttr ".kmk" no;
	setAttr ".bb" 0.52;
createNode expression -n "expression10";
	rename -uid "37CCB254-4D55-BD3E-41B4-7AB755D86DCA";
	setAttr -k on ".nds";
	setAttr -s 12 ".out";
	setAttr ".ixp" -type "string" "$offset = .I[0];\r\n.O[0] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle.ty`;\r\n.O[1] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle.tz`;\r\n\r\n.O[2] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle1.ty`;\r\n.O[3] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle1.tz`;\r\n\r\n.O[4] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle2.ty`;\r\n.O[5] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle2.tz`;\r\n\r\n.O[6] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle3.ty`;\r\n.O[7] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle3.tz`;\r\n\r\n.O[8] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle4.ty`;\r\n.O[9] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle4.tz`;\r\n\r\n.O[10] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle5.ty`;\r\n.O[11] = `getAttr -t (frame - $offset) waveMasterGRP|waveHandle5.tz`;";
createNode displayLayer -n "mesh";
	rename -uid "8DB2A0A9-4463-BCB7-F465-9BB1197F3CAA";
	setAttr ".dt" 2;
	setAttr ".do" 2;
createNode animCurveTL -n "waveHandle_translateX";
	rename -uid "3141E356-4AAD-D59B-6570-738473B5B6E1";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  195 55 248 55;
createNode animCurveTL -n "waveHandle1_translateX";
	rename -uid "9A30BD2B-4A0D-16D5-48BD-6795460D0080";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  195 55 248 55;
createNode animCurveTL -n "waveHandle2_translateX";
	rename -uid "F8DF5940-4CE5-36B5-3B2E-33A45B5BDB72";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  195 55 248 55;
createNode animCurveTL -n "waveHandle3_translateX";
	rename -uid "0D287399-4E04-B1C1-988A-D99343CED9A9";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  114 54.993080016103633 195 55 248 55;
createNode animCurveTL -n "waveHandle4_translateX";
	rename -uid "99BEB9CB-4EC9-CBB5-4CFB-F099C0A7A9B8";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  195 55 248 55;
createNode animCurveTL -n "waveHandle5_translateX";
	rename -uid "761F7BE7-4289-94B3-AD26-05B9432CC88F";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  195 55 248 55;
createNode animCurveTU -n "waveHandle_visibility";
	rename -uid "DCC50B17-4089-53CA-A1F7-04A6438EBECF";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTA -n "waveHandle_rotateX";
	rename -uid "2C4E9500-4524-0A95-3F09-9CB7D89DB60D";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 0 248 0;
	setAttr ".pst" 3;
createNode animCurveTA -n "waveHandle_rotateY";
	rename -uid "10A4B810-443D-D429-B843-CDA4880FE162";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 0 248 0;
	setAttr ".pst" 3;
createNode animCurveTA -n "waveHandle_rotateZ";
	rename -uid "7D5122F3-4F23-2DBA-346D-128A8A573C98";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 0 248 0;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle_scaleX";
	rename -uid "333DA6F2-4FEE-0435-6633-C49C66E84C05";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle_scaleY";
	rename -uid "23796275-4D1B-9C6C-AC21-AB9E2C4E0856";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle_scaleZ";
	rename -uid "12293B30-4E2D-8128-EC68-3F830D51BA96";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle1_visibility";
	rename -uid "73741362-44F6-B3B4-A586-CEAA02593402";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTA -n "waveHandle1_rotateX";
	rename -uid "7D6ABC1A-459D-F81B-6AD6-AC97FFF33568";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 0 248 0;
	setAttr ".pst" 3;
createNode animCurveTA -n "waveHandle1_rotateY";
	rename -uid "CF162870-491C-ACC9-42F0-FC9D2B3BCED7";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 0 248 0;
	setAttr ".pst" 3;
createNode animCurveTA -n "waveHandle1_rotateZ";
	rename -uid "78F8278C-4E21-E9AB-8783-FD8C57EA7F9C";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 0 248 0;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle1_scaleX";
	rename -uid "A900A0E9-4A1A-DEBA-87A0-BDB8F9FCBDD1";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle1_scaleY";
	rename -uid "733EB239-4EDC-5CCC-DDFB-E798DFFA6010";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle1_scaleZ";
	rename -uid "7D1CF311-4AFF-4359-379C-48B0DBF6ABC2";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle2_visibility";
	rename -uid "0C2045FF-4375-C9C3-6966-968A8862AA3D";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTA -n "waveHandle2_rotateX";
	rename -uid "1466DDA8-4C02-6F5E-D314-7D88E2943BAB";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 0 248 0;
	setAttr ".pst" 3;
createNode animCurveTA -n "waveHandle2_rotateY";
	rename -uid "264C1B9D-4697-B55E-2ADC-46AA01E17A7A";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 0 248 0;
	setAttr ".pst" 3;
createNode animCurveTA -n "waveHandle2_rotateZ";
	rename -uid "35F07685-453E-AC69-7AD8-048CDB24736F";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 0 248 0;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle2_scaleX";
	rename -uid "6A5A8F80-4036-5C7A-C502-F0AE80FC320A";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle2_scaleY";
	rename -uid "99F9EC4C-4357-29D7-0BB4-168CD2EC4FED";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle2_scaleZ";
	rename -uid "5C4C628F-4868-C99A-6761-40A6937B0D13";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle3_visibility";
	rename -uid "31AE80F6-4D1D-F2B0-2ED3-0489CE9A5AE2";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTA -n "waveHandle3_rotateX";
	rename -uid "CB67E9A1-4BB0-1019-D035-F5A8B8D05372";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 0 248 0;
	setAttr ".pst" 3;
createNode animCurveTA -n "waveHandle3_rotateY";
	rename -uid "B2DEE77B-4908-15E4-F2FB-16B54FDE1D7A";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 0 248 0;
	setAttr ".pst" 3;
createNode animCurveTA -n "waveHandle3_rotateZ";
	rename -uid "8DC48D87-4366-25F7-D015-D3A287E283E1";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 0 248 0;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle3_scaleX";
	rename -uid "D6DB601A-4566-5CF9-1127-34954A2626E2";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle3_scaleY";
	rename -uid "72F39CD7-4942-7C21-63D8-BC8034C8C7A5";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle3_scaleZ";
	rename -uid "E9FE8F6C-4A91-943E-A8C3-CB9204E2781D";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle4_visibility";
	rename -uid "FF68699C-4BD9-297B-55B5-4585D0F1B6B0";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTA -n "waveHandle4_rotateX";
	rename -uid "E99FDE83-444C-8B02-D788-F6989FCEF299";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 0 248 0;
	setAttr ".pst" 3;
createNode animCurveTA -n "waveHandle4_rotateY";
	rename -uid "AE0796BF-469A-1D6E-9092-0992792CD237";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 0 248 0;
	setAttr ".pst" 3;
createNode animCurveTA -n "waveHandle4_rotateZ";
	rename -uid "30C6EF1F-4A66-6046-44D9-129925BF5AA9";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 0 248 0;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle4_scaleX";
	rename -uid "DF76C30F-485E-5966-CBE4-3885DDB48FBE";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle4_scaleY";
	rename -uid "CF9B799C-4843-7D6C-FB65-7D8F3E4CE4E1";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle4_scaleZ";
	rename -uid "DB43E6F3-42BF-3273-4DE0-9EBC6F6C4B42";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle5_visibility";
	rename -uid "F12DDB50-460B-851B-13DB-5D9DED09785D";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTA -n "waveHandle5_rotateX";
	rename -uid "FA8BA807-41B2-A7E2-0505-839F36C0362F";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 0 248 0;
	setAttr ".pst" 3;
createNode animCurveTA -n "waveHandle5_rotateY";
	rename -uid "017DF7AB-495F-4ACD-BE8C-CCB5EC51B68F";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 0 248 0;
	setAttr ".pst" 3;
createNode animCurveTA -n "waveHandle5_rotateZ";
	rename -uid "1BED40A8-4208-C038-2233-9FA9752A0397";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 0 248 0;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle5_scaleX";
	rename -uid "BD9FD769-4412-D396-7A6B-979A9188BB9C";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle5_scaleY";
	rename -uid "6C264A88-4DE0-7F0C-7909-B89258530133";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode animCurveTU -n "waveHandle5_scaleZ";
	rename -uid "B89F4904-4F75-0BAC-869A-E9BBD5FFEC78";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  232 1 248 1;
	setAttr ".pst" 3;
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "C5CCF7D8-4346-A7F9-DCDE-259D81F72C7C";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "D6EE21BF-4CD3-C38A-5D41-C2A7E21C3A45";
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "DB2C094F-4761-2523-7DEE-8E82E6A19E4B";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -347.5932983480962 587.34129636940986 ;
	setAttr ".tgi[0].vh" -type "double2" 168.08957564580734 648.4555253148186 ;
select -ne :time1;
	setAttr ".o" 161;
	setAttr ".unw" 161;
select -ne :hardwareRenderingGlobals;
	setAttr ".vac" 2;
	setAttr ".etmr" no;
	setAttr ".tmr" 4096;
select -ne :renderPartition;
	setAttr -s 3 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
select -ne :defaultRenderingList1;
select -ne :defaultTextureList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ep" 1;
select -ne :defaultResolution;
	setAttr ".w" 640;
	setAttr ".h" 480;
	setAttr ".dar" 1.3333332538604736;
select -ne :defaultColorMgtGlobals;
	setAttr ".cme" no;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "loftCurves.di" "|waveGRP_01|CRV.do";
connectAttr "attachCurve3.oc" "|waveGRP_01|CRV|CRVShape.cr";
connectAttr "BaseArcs.di" "|waveGRP_01|guts_GRP.do";
connectAttr "BaseArcs.di" "|waveGRP_01|guts_GRP|curve2.do";
connectAttr "threePointCircularArc1_waveGRP_01.oc" "|waveGRP_01|guts_GRP|curve2|curveShape2.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_01|guts_GRP|curve4.do";
connectAttr "threePointCircularArc2_waveGRP_01.oc" "|waveGRP_01|guts_GRP|curve4|curveShape4.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_01|guts_GRP|curve2attachedCurve1.do";
connectAttr "attachCurve1.oc" "|waveGRP_01|guts_GRP|curve2attachedCurve1|curve2attachedCurveShape1.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_01|guts_GRP|curve2attachedCurve1attachedCurve1.do"
		;
connectAttr "attachCurve2.oc" "|waveGRP_01|guts_GRP|curve2attachedCurve1attachedCurve1|curve2attachedCurve1attachedCurveShape1.cr"
		;
connectAttr "waveCtrlGRP_01Buf_parentConstraint1.ctx" "waveCtrlGRP_01Buf.tx";
connectAttr "waveCtrlGRP_01Buf_parentConstraint1.cty" "waveCtrlGRP_01Buf.ty";
connectAttr "waveCtrlGRP_01Buf_parentConstraint1.ctz" "waveCtrlGRP_01Buf.tz";
connectAttr "waveCtrlGRP_01Buf_parentConstraint1.crx" "waveCtrlGRP_01Buf.rx";
connectAttr "waveCtrlGRP_01Buf_parentConstraint1.cry" "waveCtrlGRP_01Buf.ry";
connectAttr "waveCtrlGRP_01Buf_parentConstraint1.crz" "waveCtrlGRP_01Buf.rz";
connectAttr "waveGRP_01.s" "waveCtrlGRP_01Buf.s";
connectAttr "waveCtrlGRP_01Buf.ro" "waveCtrlGRP_01Buf_parentConstraint1.cro";
connectAttr "waveCtrlGRP_01Buf.pim" "waveCtrlGRP_01Buf_parentConstraint1.cpim";
connectAttr "waveCtrlGRP_01Buf.rp" "waveCtrlGRP_01Buf_parentConstraint1.crp";
connectAttr "waveCtrlGRP_01Buf.rpt" "waveCtrlGRP_01Buf_parentConstraint1.crt";
connectAttr "wave01_Ctrl.t" "waveCtrlGRP_01Buf_parentConstraint1.tg[0].tt";
connectAttr "wave01_Ctrl.rp" "waveCtrlGRP_01Buf_parentConstraint1.tg[0].trp";
connectAttr "wave01_Ctrl.rpt" "waveCtrlGRP_01Buf_parentConstraint1.tg[0].trt";
connectAttr "wave01_Ctrl.r" "waveCtrlGRP_01Buf_parentConstraint1.tg[0].tr";
connectAttr "wave01_Ctrl.ro" "waveCtrlGRP_01Buf_parentConstraint1.tg[0].tro";
connectAttr "wave01_Ctrl.s" "waveCtrlGRP_01Buf_parentConstraint1.tg[0].ts";
connectAttr "wave01_Ctrl.pm" "waveCtrlGRP_01Buf_parentConstraint1.tg[0].tpm";
connectAttr "waveCtrlGRP_01Buf_parentConstraint1.w0" "waveCtrlGRP_01Buf_parentConstraint1.tg[0].tw"
		;
connectAttr "expression10.out[0]" "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandleBuf.ty"
		;
connectAttr "expression10.out[1]" "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandleBuf.tz"
		;
connectAttr "expression10.out[2]" "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle1Buf.ty"
		;
connectAttr "expression10.out[3]" "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle1Buf.tz"
		;
connectAttr "expression10.out[4]" "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle2Buf.ty"
		;
connectAttr "expression10.out[5]" "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle2Buf.tz"
		;
connectAttr "expression10.out[6]" "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle3Buf.ty"
		;
connectAttr "expression10.out[7]" "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle3Buf.tz"
		;
connectAttr "expression10.out[8]" "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle4Buf.ty"
		;
connectAttr "expression10.out[9]" "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle4Buf.tz"
		;
connectAttr "expression10.out[10]" "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle5Buf.ty"
		;
connectAttr "expression10.out[11]" "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle5Buf.tz"
		;
connectAttr "BaseArcs.di" "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|Anchorcrv1.do"
		;
connectAttr "makeThreePointCircularArc3.oc" "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|Anchorcrv1|AnchorcrvShape1.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|Anchorcrv2.do"
		;
connectAttr "loftCurves.di" "|waveGRP_02|CRV.do";
connectAttr "attachCurve4.oc" "|waveGRP_02|CRV|CRVShape.cr";
connectAttr "BaseArcs.di" "|waveGRP_02|guts_GRP.do";
connectAttr "BaseArcs.di" "|waveGRP_02|guts_GRP|curve2.do";
connectAttr "threePointCircularArc1_waveGRP_02.oc" "|waveGRP_02|guts_GRP|curve2|curveShape2.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_02|guts_GRP|curve4.do";
connectAttr "threePointCircularArc2_waveGRP_02.oc" "|waveGRP_02|guts_GRP|curve4|curveShape4.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_02|guts_GRP|curve2attachedCurve1.do";
connectAttr "attachCurve6.oc" "|waveGRP_02|guts_GRP|curve2attachedCurve1|curve2attachedCurveShape1.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_02|guts_GRP|curve2attachedCurve1attachedCurve1.do"
		;
connectAttr "attachCurve5.oc" "|waveGRP_02|guts_GRP|curve2attachedCurve1attachedCurve1|curve2attachedCurve1attachedCurveShape1.cr"
		;
connectAttr "waveCtrlGRP_02Buf_parentConstraint1.ctx" "waveCtrlGRP_02Buf.tx";
connectAttr "waveCtrlGRP_02Buf_parentConstraint1.cty" "waveCtrlGRP_02Buf.ty";
connectAttr "waveCtrlGRP_02Buf_parentConstraint1.ctz" "waveCtrlGRP_02Buf.tz";
connectAttr "waveCtrlGRP_02Buf_parentConstraint1.crx" "waveCtrlGRP_02Buf.rx";
connectAttr "waveCtrlGRP_02Buf_parentConstraint1.cry" "waveCtrlGRP_02Buf.ry";
connectAttr "waveCtrlGRP_02Buf_parentConstraint1.crz" "waveCtrlGRP_02Buf.rz";
connectAttr "waveGRP_02.s" "waveCtrlGRP_02Buf.s";
connectAttr "waveCtrlGRP_02Buf.ro" "waveCtrlGRP_02Buf_parentConstraint1.cro";
connectAttr "waveCtrlGRP_02Buf.pim" "waveCtrlGRP_02Buf_parentConstraint1.cpim";
connectAttr "waveCtrlGRP_02Buf.rp" "waveCtrlGRP_02Buf_parentConstraint1.crp";
connectAttr "waveCtrlGRP_02Buf.rpt" "waveCtrlGRP_02Buf_parentConstraint1.crt";
connectAttr "wave02_Ctrl.t" "waveCtrlGRP_02Buf_parentConstraint1.tg[0].tt";
connectAttr "wave02_Ctrl.rp" "waveCtrlGRP_02Buf_parentConstraint1.tg[0].trp";
connectAttr "wave02_Ctrl.rpt" "waveCtrlGRP_02Buf_parentConstraint1.tg[0].trt";
connectAttr "wave02_Ctrl.r" "waveCtrlGRP_02Buf_parentConstraint1.tg[0].tr";
connectAttr "wave02_Ctrl.ro" "waveCtrlGRP_02Buf_parentConstraint1.tg[0].tro";
connectAttr "wave02_Ctrl.s" "waveCtrlGRP_02Buf_parentConstraint1.tg[0].ts";
connectAttr "wave02_Ctrl.pm" "waveCtrlGRP_02Buf_parentConstraint1.tg[0].tpm";
connectAttr "waveCtrlGRP_02Buf_parentConstraint1.w0" "waveCtrlGRP_02Buf_parentConstraint1.tg[0].tw"
		;
connectAttr "expression1.out[0]" "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandleBuf.ty"
		;
connectAttr "expression1.out[1]" "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandleBuf.tz"
		;
connectAttr "expression1.out[2]" "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle1Buf.ty"
		;
connectAttr "expression1.out[3]" "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle1Buf.tz"
		;
connectAttr "expression1.out[4]" "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle2Buf.ty"
		;
connectAttr "expression1.out[5]" "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle2Buf.tz"
		;
connectAttr "expression1.out[6]" "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle3Buf.ty"
		;
connectAttr "expression1.out[7]" "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle3Buf.tz"
		;
connectAttr "expression1.out[8]" "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle4Buf.ty"
		;
connectAttr "expression1.out[9]" "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle4Buf.tz"
		;
connectAttr "expression1.out[10]" "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle5Buf.ty"
		;
connectAttr "expression1.out[11]" "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle5Buf.tz"
		;
connectAttr "BaseArcs.di" "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|Anchorcrv1.do"
		;
connectAttr "makeThreePointCircularArc4.oc" "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|Anchorcrv1|AnchorcrvShape1.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|Anchorcrv2.do"
		;
connectAttr "loftCurves.di" "|waveGRP_03|CRV.do";
connectAttr "attachCurve7.oc" "|waveGRP_03|CRV|CRVShape.cr";
connectAttr "BaseArcs.di" "|waveGRP_03|guts_GRP.do";
connectAttr "BaseArcs.di" "|waveGRP_03|guts_GRP|curve2.do";
connectAttr "threePointCircularArc1_waveGRP_03.oc" "|waveGRP_03|guts_GRP|curve2|curveShape2.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_03|guts_GRP|curve4.do";
connectAttr "threePointCircularArc2_waveGRP_03.oc" "|waveGRP_03|guts_GRP|curve4|curveShape4.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_03|guts_GRP|curve2attachedCurve1.do";
connectAttr "attachCurve9.oc" "|waveGRP_03|guts_GRP|curve2attachedCurve1|curve2attachedCurveShape1.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_03|guts_GRP|curve2attachedCurve1attachedCurve1.do"
		;
connectAttr "attachCurve8.oc" "|waveGRP_03|guts_GRP|curve2attachedCurve1attachedCurve1|curve2attachedCurve1attachedCurveShape1.cr"
		;
connectAttr "waveCtrlGRP_03Buf_parentConstraint1.ctx" "waveCtrlGRP_03Buf.tx";
connectAttr "waveCtrlGRP_03Buf_parentConstraint1.cty" "waveCtrlGRP_03Buf.ty";
connectAttr "waveCtrlGRP_03Buf_parentConstraint1.ctz" "waveCtrlGRP_03Buf.tz";
connectAttr "waveCtrlGRP_03Buf_parentConstraint1.crx" "waveCtrlGRP_03Buf.rx";
connectAttr "waveCtrlGRP_03Buf_parentConstraint1.cry" "waveCtrlGRP_03Buf.ry";
connectAttr "waveCtrlGRP_03Buf_parentConstraint1.crz" "waveCtrlGRP_03Buf.rz";
connectAttr "waveGRP_03.s" "waveCtrlGRP_03Buf.s";
connectAttr "waveCtrlGRP_03Buf.ro" "waveCtrlGRP_03Buf_parentConstraint1.cro";
connectAttr "waveCtrlGRP_03Buf.pim" "waveCtrlGRP_03Buf_parentConstraint1.cpim";
connectAttr "waveCtrlGRP_03Buf.rp" "waveCtrlGRP_03Buf_parentConstraint1.crp";
connectAttr "waveCtrlGRP_03Buf.rpt" "waveCtrlGRP_03Buf_parentConstraint1.crt";
connectAttr "wave03_Ctrl.t" "waveCtrlGRP_03Buf_parentConstraint1.tg[0].tt";
connectAttr "wave03_Ctrl.rp" "waveCtrlGRP_03Buf_parentConstraint1.tg[0].trp";
connectAttr "wave03_Ctrl.rpt" "waveCtrlGRP_03Buf_parentConstraint1.tg[0].trt";
connectAttr "wave03_Ctrl.r" "waveCtrlGRP_03Buf_parentConstraint1.tg[0].tr";
connectAttr "wave03_Ctrl.ro" "waveCtrlGRP_03Buf_parentConstraint1.tg[0].tro";
connectAttr "wave03_Ctrl.s" "waveCtrlGRP_03Buf_parentConstraint1.tg[0].ts";
connectAttr "wave03_Ctrl.pm" "waveCtrlGRP_03Buf_parentConstraint1.tg[0].tpm";
connectAttr "waveCtrlGRP_03Buf_parentConstraint1.w0" "waveCtrlGRP_03Buf_parentConstraint1.tg[0].tw"
		;
connectAttr "expression2.out[0]" "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandleBuf.ty"
		;
connectAttr "expression2.out[1]" "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandleBuf.tz"
		;
connectAttr "expression2.out[2]" "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle1Buf.ty"
		;
connectAttr "expression2.out[3]" "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle1Buf.tz"
		;
connectAttr "expression2.out[4]" "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle2Buf.ty"
		;
connectAttr "expression2.out[5]" "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle2Buf.tz"
		;
connectAttr "expression2.out[6]" "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle3Buf.ty"
		;
connectAttr "expression2.out[7]" "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle3Buf.tz"
		;
connectAttr "expression2.out[8]" "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle4Buf.ty"
		;
connectAttr "expression2.out[9]" "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle4Buf.tz"
		;
connectAttr "expression2.out[10]" "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle5Buf.ty"
		;
connectAttr "expression2.out[11]" "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle5Buf.tz"
		;
connectAttr "BaseArcs.di" "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|Anchorcrv1.do"
		;
connectAttr "makeThreePointCircularArc7.oc" "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|Anchorcrv1|AnchorcrvShape1.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|Anchorcrv2.do"
		;
connectAttr "loftCurves.di" "|waveGRP_04|CRV.do";
connectAttr "attachCurve10.oc" "|waveGRP_04|CRV|CRVShape.cr";
connectAttr "BaseArcs.di" "|waveGRP_04|guts_GRP.do";
connectAttr "BaseArcs.di" "|waveGRP_04|guts_GRP|curve2.do";
connectAttr "threePointCircularArc1_waveGRP_04.oc" "|waveGRP_04|guts_GRP|curve2|curveShape2.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_04|guts_GRP|curve4.do";
connectAttr "threePointCircularArc2_waveGRP_04.oc" "|waveGRP_04|guts_GRP|curve4|curveShape4.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_04|guts_GRP|curve2attachedCurve1.do";
connectAttr "attachCurve12.oc" "|waveGRP_04|guts_GRP|curve2attachedCurve1|curve2attachedCurveShape1.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_04|guts_GRP|curve2attachedCurve1attachedCurve1.do"
		;
connectAttr "attachCurve11.oc" "|waveGRP_04|guts_GRP|curve2attachedCurve1attachedCurve1|curve2attachedCurve1attachedCurveShape1.cr"
		;
connectAttr "waveCtrlGRP_04Buf_parentConstraint1.ctx" "waveCtrlGRP_04Buf.tx";
connectAttr "waveCtrlGRP_04Buf_parentConstraint1.cty" "waveCtrlGRP_04Buf.ty";
connectAttr "waveCtrlGRP_04Buf_parentConstraint1.ctz" "waveCtrlGRP_04Buf.tz";
connectAttr "waveCtrlGRP_04Buf_parentConstraint1.crx" "waveCtrlGRP_04Buf.rx";
connectAttr "waveCtrlGRP_04Buf_parentConstraint1.cry" "waveCtrlGRP_04Buf.ry";
connectAttr "waveCtrlGRP_04Buf_parentConstraint1.crz" "waveCtrlGRP_04Buf.rz";
connectAttr "waveGRP_04.s" "waveCtrlGRP_04Buf.s";
connectAttr "waveCtrlGRP_04Buf.ro" "waveCtrlGRP_04Buf_parentConstraint1.cro";
connectAttr "waveCtrlGRP_04Buf.pim" "waveCtrlGRP_04Buf_parentConstraint1.cpim";
connectAttr "waveCtrlGRP_04Buf.rp" "waveCtrlGRP_04Buf_parentConstraint1.crp";
connectAttr "waveCtrlGRP_04Buf.rpt" "waveCtrlGRP_04Buf_parentConstraint1.crt";
connectAttr "wave04_Ctrl.t" "waveCtrlGRP_04Buf_parentConstraint1.tg[0].tt";
connectAttr "wave04_Ctrl.rp" "waveCtrlGRP_04Buf_parentConstraint1.tg[0].trp";
connectAttr "wave04_Ctrl.rpt" "waveCtrlGRP_04Buf_parentConstraint1.tg[0].trt";
connectAttr "wave04_Ctrl.r" "waveCtrlGRP_04Buf_parentConstraint1.tg[0].tr";
connectAttr "wave04_Ctrl.ro" "waveCtrlGRP_04Buf_parentConstraint1.tg[0].tro";
connectAttr "wave04_Ctrl.s" "waveCtrlGRP_04Buf_parentConstraint1.tg[0].ts";
connectAttr "wave04_Ctrl.pm" "waveCtrlGRP_04Buf_parentConstraint1.tg[0].tpm";
connectAttr "waveCtrlGRP_04Buf_parentConstraint1.w0" "waveCtrlGRP_04Buf_parentConstraint1.tg[0].tw"
		;
connectAttr "expression3.out[0]" "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandleBuf.ty"
		;
connectAttr "expression3.out[1]" "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandleBuf.tz"
		;
connectAttr "expression3.out[2]" "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle1Buf.ty"
		;
connectAttr "expression3.out[3]" "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle1Buf.tz"
		;
connectAttr "expression3.out[4]" "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle2Buf.ty"
		;
connectAttr "expression3.out[5]" "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle2Buf.tz"
		;
connectAttr "expression3.out[6]" "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle3Buf.ty"
		;
connectAttr "expression3.out[7]" "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle3Buf.tz"
		;
connectAttr "expression3.out[8]" "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle4Buf.ty"
		;
connectAttr "expression3.out[9]" "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle4Buf.tz"
		;
connectAttr "expression3.out[10]" "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle5Buf.ty"
		;
connectAttr "expression3.out[11]" "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle5Buf.tz"
		;
connectAttr "BaseArcs.di" "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|Anchorcrv1.do"
		;
connectAttr "makeThreePointCircularArc10.oc" "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|Anchorcrv1|AnchorcrvShape1.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|Anchorcrv2.do"
		;
connectAttr "loftCurves.di" "|waveGRP_05|CRV.do";
connectAttr "attachCurve13.oc" "|waveGRP_05|CRV|CRVShape.cr";
connectAttr "BaseArcs.di" "|waveGRP_05|guts_GRP.do";
connectAttr "BaseArcs.di" "|waveGRP_05|guts_GRP|curve2.do";
connectAttr "threePointCircularArc1_waveGRP_05.oc" "|waveGRP_05|guts_GRP|curve2|curveShape2.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_05|guts_GRP|curve4.do";
connectAttr "threePointCircularArc2_waveGRP_05.oc" "|waveGRP_05|guts_GRP|curve4|curveShape4.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_05|guts_GRP|curve2attachedCurve1.do";
connectAttr "attachCurve15.oc" "|waveGRP_05|guts_GRP|curve2attachedCurve1|curve2attachedCurveShape1.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_05|guts_GRP|curve2attachedCurve1attachedCurve1.do"
		;
connectAttr "attachCurve14.oc" "|waveGRP_05|guts_GRP|curve2attachedCurve1attachedCurve1|curve2attachedCurve1attachedCurveShape1.cr"
		;
connectAttr "waveCtrlGRP_05Buf_parentConstraint1.ctx" "waveCtrlGRP_05Buf.tx";
connectAttr "waveCtrlGRP_05Buf_parentConstraint1.cty" "waveCtrlGRP_05Buf.ty";
connectAttr "waveCtrlGRP_05Buf_parentConstraint1.ctz" "waveCtrlGRP_05Buf.tz";
connectAttr "waveCtrlGRP_05Buf_parentConstraint1.crx" "waveCtrlGRP_05Buf.rx";
connectAttr "waveCtrlGRP_05Buf_parentConstraint1.cry" "waveCtrlGRP_05Buf.ry";
connectAttr "waveCtrlGRP_05Buf_parentConstraint1.crz" "waveCtrlGRP_05Buf.rz";
connectAttr "waveGRP_05.s" "waveCtrlGRP_05Buf.s";
connectAttr "waveCtrlGRP_05Buf.ro" "waveCtrlGRP_05Buf_parentConstraint1.cro";
connectAttr "waveCtrlGRP_05Buf.pim" "waveCtrlGRP_05Buf_parentConstraint1.cpim";
connectAttr "waveCtrlGRP_05Buf.rp" "waveCtrlGRP_05Buf_parentConstraint1.crp";
connectAttr "waveCtrlGRP_05Buf.rpt" "waveCtrlGRP_05Buf_parentConstraint1.crt";
connectAttr "wave05_Ctrl.t" "waveCtrlGRP_05Buf_parentConstraint1.tg[0].tt";
connectAttr "wave05_Ctrl.rp" "waveCtrlGRP_05Buf_parentConstraint1.tg[0].trp";
connectAttr "wave05_Ctrl.rpt" "waveCtrlGRP_05Buf_parentConstraint1.tg[0].trt";
connectAttr "wave05_Ctrl.r" "waveCtrlGRP_05Buf_parentConstraint1.tg[0].tr";
connectAttr "wave05_Ctrl.ro" "waveCtrlGRP_05Buf_parentConstraint1.tg[0].tro";
connectAttr "wave05_Ctrl.s" "waveCtrlGRP_05Buf_parentConstraint1.tg[0].ts";
connectAttr "wave05_Ctrl.pm" "waveCtrlGRP_05Buf_parentConstraint1.tg[0].tpm";
connectAttr "waveCtrlGRP_05Buf_parentConstraint1.w0" "waveCtrlGRP_05Buf_parentConstraint1.tg[0].tw"
		;
connectAttr "expression4.out[0]" "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandleBuf.ty"
		;
connectAttr "expression4.out[1]" "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandleBuf.tz"
		;
connectAttr "expression4.out[2]" "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle1Buf.ty"
		;
connectAttr "expression4.out[3]" "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle1Buf.tz"
		;
connectAttr "expression4.out[4]" "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle2Buf.ty"
		;
connectAttr "expression4.out[5]" "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle2Buf.tz"
		;
connectAttr "expression4.out[6]" "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle3Buf.ty"
		;
connectAttr "expression4.out[7]" "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle3Buf.tz"
		;
connectAttr "expression4.out[8]" "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle4Buf.ty"
		;
connectAttr "expression4.out[9]" "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle4Buf.tz"
		;
connectAttr "expression4.out[10]" "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle5Buf.ty"
		;
connectAttr "expression4.out[11]" "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle5Buf.tz"
		;
connectAttr "BaseArcs.di" "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|Anchorcrv1.do"
		;
connectAttr "makeThreePointCircularArc13.oc" "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|Anchorcrv1|AnchorcrvShape1.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|Anchorcrv2.do"
		;
connectAttr "loftCurves.di" "|waveGRP_06|CRV.do";
connectAttr "attachCurve16.oc" "|waveGRP_06|CRV|CRVShape.cr";
connectAttr "BaseArcs.di" "|waveGRP_06|guts_GRP.do";
connectAttr "BaseArcs.di" "|waveGRP_06|guts_GRP|curve2.do";
connectAttr "threePointCircularArc1_waveGRP_06.oc" "|waveGRP_06|guts_GRP|curve2|curveShape2.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_06|guts_GRP|curve4.do";
connectAttr "threePointCircularArc2_waveGRP_06.oc" "|waveGRP_06|guts_GRP|curve4|curveShape4.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_06|guts_GRP|curve2attachedCurve1.do";
connectAttr "attachCurve18.oc" "|waveGRP_06|guts_GRP|curve2attachedCurve1|curve2attachedCurveShape1.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_06|guts_GRP|curve2attachedCurve1attachedCurve1.do"
		;
connectAttr "attachCurve17.oc" "|waveGRP_06|guts_GRP|curve2attachedCurve1attachedCurve1|curve2attachedCurve1attachedCurveShape1.cr"
		;
connectAttr "waveCtrlGRP_06Buf_parentConstraint1.ctx" "waveCtrlGRP_06Buf.tx";
connectAttr "waveCtrlGRP_06Buf_parentConstraint1.cty" "waveCtrlGRP_06Buf.ty";
connectAttr "waveCtrlGRP_06Buf_parentConstraint1.ctz" "waveCtrlGRP_06Buf.tz";
connectAttr "waveCtrlGRP_06Buf_parentConstraint1.crx" "waveCtrlGRP_06Buf.rx";
connectAttr "waveCtrlGRP_06Buf_parentConstraint1.cry" "waveCtrlGRP_06Buf.ry";
connectAttr "waveCtrlGRP_06Buf_parentConstraint1.crz" "waveCtrlGRP_06Buf.rz";
connectAttr "waveGRP_06.s" "waveCtrlGRP_06Buf.s";
connectAttr "waveCtrlGRP_06Buf.ro" "waveCtrlGRP_06Buf_parentConstraint1.cro";
connectAttr "waveCtrlGRP_06Buf.pim" "waveCtrlGRP_06Buf_parentConstraint1.cpim";
connectAttr "waveCtrlGRP_06Buf.rp" "waveCtrlGRP_06Buf_parentConstraint1.crp";
connectAttr "waveCtrlGRP_06Buf.rpt" "waveCtrlGRP_06Buf_parentConstraint1.crt";
connectAttr "wave06_Ctrl.t" "waveCtrlGRP_06Buf_parentConstraint1.tg[0].tt";
connectAttr "wave06_Ctrl.rp" "waveCtrlGRP_06Buf_parentConstraint1.tg[0].trp";
connectAttr "wave06_Ctrl.rpt" "waveCtrlGRP_06Buf_parentConstraint1.tg[0].trt";
connectAttr "wave06_Ctrl.r" "waveCtrlGRP_06Buf_parentConstraint1.tg[0].tr";
connectAttr "wave06_Ctrl.ro" "waveCtrlGRP_06Buf_parentConstraint1.tg[0].tro";
connectAttr "wave06_Ctrl.s" "waveCtrlGRP_06Buf_parentConstraint1.tg[0].ts";
connectAttr "wave06_Ctrl.pm" "waveCtrlGRP_06Buf_parentConstraint1.tg[0].tpm";
connectAttr "waveCtrlGRP_06Buf_parentConstraint1.w0" "waveCtrlGRP_06Buf_parentConstraint1.tg[0].tw"
		;
connectAttr "expression5.out[0]" "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandleBuf.ty"
		;
connectAttr "expression5.out[1]" "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandleBuf.tz"
		;
connectAttr "expression5.out[2]" "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle1Buf.ty"
		;
connectAttr "expression5.out[3]" "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle1Buf.tz"
		;
connectAttr "expression5.out[4]" "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle2Buf.ty"
		;
connectAttr "expression5.out[5]" "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle2Buf.tz"
		;
connectAttr "expression5.out[6]" "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle3Buf.ty"
		;
connectAttr "expression5.out[7]" "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle3Buf.tz"
		;
connectAttr "expression5.out[8]" "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle4Buf.ty"
		;
connectAttr "expression5.out[9]" "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle4Buf.tz"
		;
connectAttr "expression5.out[10]" "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle5Buf.ty"
		;
connectAttr "expression5.out[11]" "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle5Buf.tz"
		;
connectAttr "BaseArcs.di" "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|Anchorcrv1.do"
		;
connectAttr "makeThreePointCircularArc16.oc" "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|Anchorcrv1|AnchorcrvShape1.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|Anchorcrv2.do"
		;
connectAttr "loftCurves.di" "|waveGRP_07|CRV.do";
connectAttr "attachCurve19.oc" "|waveGRP_07|CRV|CRVShape.cr";
connectAttr "BaseArcs.di" "|waveGRP_07|guts_GRP.do";
connectAttr "BaseArcs.di" "|waveGRP_07|guts_GRP|curve2.do";
connectAttr "threePointCircularArc1_waveGRP_07.oc" "|waveGRP_07|guts_GRP|curve2|curveShape2.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_07|guts_GRP|curve4.do";
connectAttr "threePointCircularArc2_waveGRP_07.oc" "|waveGRP_07|guts_GRP|curve4|curveShape4.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_07|guts_GRP|curve2attachedCurve1.do";
connectAttr "attachCurve21.oc" "|waveGRP_07|guts_GRP|curve2attachedCurve1|curve2attachedCurveShape1.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_07|guts_GRP|curve2attachedCurve1attachedCurve1.do"
		;
connectAttr "attachCurve20.oc" "|waveGRP_07|guts_GRP|curve2attachedCurve1attachedCurve1|curve2attachedCurve1attachedCurveShape1.cr"
		;
connectAttr "waveCtrlGRP_07Buf_parentConstraint1.ctx" "waveCtrlGRP_07Buf.tx";
connectAttr "waveCtrlGRP_07Buf_parentConstraint1.cty" "waveCtrlGRP_07Buf.ty";
connectAttr "waveCtrlGRP_07Buf_parentConstraint1.ctz" "waveCtrlGRP_07Buf.tz";
connectAttr "waveCtrlGRP_07Buf_parentConstraint1.crx" "waveCtrlGRP_07Buf.rx";
connectAttr "waveCtrlGRP_07Buf_parentConstraint1.cry" "waveCtrlGRP_07Buf.ry";
connectAttr "waveCtrlGRP_07Buf_parentConstraint1.crz" "waveCtrlGRP_07Buf.rz";
connectAttr "waveGRP_07.s" "waveCtrlGRP_07Buf.s";
connectAttr "waveCtrlGRP_07Buf.ro" "waveCtrlGRP_07Buf_parentConstraint1.cro";
connectAttr "waveCtrlGRP_07Buf.pim" "waveCtrlGRP_07Buf_parentConstraint1.cpim";
connectAttr "waveCtrlGRP_07Buf.rp" "waveCtrlGRP_07Buf_parentConstraint1.crp";
connectAttr "waveCtrlGRP_07Buf.rpt" "waveCtrlGRP_07Buf_parentConstraint1.crt";
connectAttr "wave07_Ctrl.t" "waveCtrlGRP_07Buf_parentConstraint1.tg[0].tt";
connectAttr "wave07_Ctrl.rp" "waveCtrlGRP_07Buf_parentConstraint1.tg[0].trp";
connectAttr "wave07_Ctrl.rpt" "waveCtrlGRP_07Buf_parentConstraint1.tg[0].trt";
connectAttr "wave07_Ctrl.r" "waveCtrlGRP_07Buf_parentConstraint1.tg[0].tr";
connectAttr "wave07_Ctrl.ro" "waveCtrlGRP_07Buf_parentConstraint1.tg[0].tro";
connectAttr "wave07_Ctrl.s" "waveCtrlGRP_07Buf_parentConstraint1.tg[0].ts";
connectAttr "wave07_Ctrl.pm" "waveCtrlGRP_07Buf_parentConstraint1.tg[0].tpm";
connectAttr "waveCtrlGRP_07Buf_parentConstraint1.w0" "waveCtrlGRP_07Buf_parentConstraint1.tg[0].tw"
		;
connectAttr "expression6.out[0]" "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandleBuf.ty"
		;
connectAttr "expression6.out[1]" "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandleBuf.tz"
		;
connectAttr "expression6.out[2]" "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle1Buf.ty"
		;
connectAttr "expression6.out[3]" "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle1Buf.tz"
		;
connectAttr "expression6.out[4]" "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle2Buf.ty"
		;
connectAttr "expression6.out[5]" "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle2Buf.tz"
		;
connectAttr "expression6.out[6]" "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle3Buf.ty"
		;
connectAttr "expression6.out[7]" "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle3Buf.tz"
		;
connectAttr "expression6.out[8]" "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle4Buf.ty"
		;
connectAttr "expression6.out[9]" "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle4Buf.tz"
		;
connectAttr "expression6.out[10]" "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle5Buf.ty"
		;
connectAttr "expression6.out[11]" "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle5Buf.tz"
		;
connectAttr "BaseArcs.di" "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|Anchorcrv1.do"
		;
connectAttr "makeThreePointCircularArc19.oc" "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|Anchorcrv1|AnchorcrvShape1.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|Anchorcrv2.do"
		;
connectAttr "loftCurves.di" "|waveGRP_08|CRV.do";
connectAttr "attachCurve22.oc" "|waveGRP_08|CRV|CRVShape.cr";
connectAttr "BaseArcs.di" "|waveGRP_08|guts_GRP.do";
connectAttr "BaseArcs.di" "|waveGRP_08|guts_GRP|curve2.do";
connectAttr "threePointCircularArc1_waveGRP_08.oc" "|waveGRP_08|guts_GRP|curve2|curveShape2.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_08|guts_GRP|curve4.do";
connectAttr "threePointCircularArc2_waveGRP_08.oc" "|waveGRP_08|guts_GRP|curve4|curveShape4.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_08|guts_GRP|curve2attachedCurve1.do";
connectAttr "attachCurve24.oc" "|waveGRP_08|guts_GRP|curve2attachedCurve1|curve2attachedCurveShape1.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_08|guts_GRP|curve2attachedCurve1attachedCurve1.do"
		;
connectAttr "attachCurve23.oc" "|waveGRP_08|guts_GRP|curve2attachedCurve1attachedCurve1|curve2attachedCurve1attachedCurveShape1.cr"
		;
connectAttr "waveCtrlGRP_08Buf_parentConstraint1.ctx" "waveCtrlGRP_08Buf.tx";
connectAttr "waveCtrlGRP_08Buf_parentConstraint1.cty" "waveCtrlGRP_08Buf.ty";
connectAttr "waveCtrlGRP_08Buf_parentConstraint1.ctz" "waveCtrlGRP_08Buf.tz";
connectAttr "waveCtrlGRP_08Buf_parentConstraint1.crx" "waveCtrlGRP_08Buf.rx";
connectAttr "waveCtrlGRP_08Buf_parentConstraint1.cry" "waveCtrlGRP_08Buf.ry";
connectAttr "waveCtrlGRP_08Buf_parentConstraint1.crz" "waveCtrlGRP_08Buf.rz";
connectAttr "waveGRP_08.s" "waveCtrlGRP_08Buf.s";
connectAttr "waveCtrlGRP_08Buf.ro" "waveCtrlGRP_08Buf_parentConstraint1.cro";
connectAttr "waveCtrlGRP_08Buf.pim" "waveCtrlGRP_08Buf_parentConstraint1.cpim";
connectAttr "waveCtrlGRP_08Buf.rp" "waveCtrlGRP_08Buf_parentConstraint1.crp";
connectAttr "waveCtrlGRP_08Buf.rpt" "waveCtrlGRP_08Buf_parentConstraint1.crt";
connectAttr "wave08_Ctrl.t" "waveCtrlGRP_08Buf_parentConstraint1.tg[0].tt";
connectAttr "wave08_Ctrl.rp" "waveCtrlGRP_08Buf_parentConstraint1.tg[0].trp";
connectAttr "wave08_Ctrl.rpt" "waveCtrlGRP_08Buf_parentConstraint1.tg[0].trt";
connectAttr "wave08_Ctrl.r" "waveCtrlGRP_08Buf_parentConstraint1.tg[0].tr";
connectAttr "wave08_Ctrl.ro" "waveCtrlGRP_08Buf_parentConstraint1.tg[0].tro";
connectAttr "wave08_Ctrl.s" "waveCtrlGRP_08Buf_parentConstraint1.tg[0].ts";
connectAttr "wave08_Ctrl.pm" "waveCtrlGRP_08Buf_parentConstraint1.tg[0].tpm";
connectAttr "waveCtrlGRP_08Buf_parentConstraint1.w0" "waveCtrlGRP_08Buf_parentConstraint1.tg[0].tw"
		;
connectAttr "expression7.out[0]" "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandleBuf.ty"
		;
connectAttr "expression7.out[1]" "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandleBuf.tz"
		;
connectAttr "expression7.out[2]" "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle1Buf.ty"
		;
connectAttr "expression7.out[3]" "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle1Buf.tz"
		;
connectAttr "expression7.out[4]" "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle2Buf.ty"
		;
connectAttr "expression7.out[5]" "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle2Buf.tz"
		;
connectAttr "expression7.out[6]" "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle3Buf.ty"
		;
connectAttr "expression7.out[7]" "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle3Buf.tz"
		;
connectAttr "expression7.out[8]" "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle4Buf.ty"
		;
connectAttr "expression7.out[9]" "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle4Buf.tz"
		;
connectAttr "expression7.out[10]" "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle5Buf.ty"
		;
connectAttr "expression7.out[11]" "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle5Buf.tz"
		;
connectAttr "BaseArcs.di" "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|Anchorcrv1.do"
		;
connectAttr "makeThreePointCircularArc22.oc" "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|Anchorcrv1|AnchorcrvShape1.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|Anchorcrv2.do"
		;
connectAttr "loftCurves.di" "|waveGRP_09|CRV.do";
connectAttr "attachCurve25.oc" "|waveGRP_09|CRV|CRVShape.cr";
connectAttr "BaseArcs.di" "|waveGRP_09|guts_GRP.do";
connectAttr "BaseArcs.di" "|waveGRP_09|guts_GRP|curve2.do";
connectAttr "threePointCircularArc1_waveGRP_09.oc" "|waveGRP_09|guts_GRP|curve2|curveShape2.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_09|guts_GRP|curve4.do";
connectAttr "threePointCircularArc2_waveGRP_09.oc" "|waveGRP_09|guts_GRP|curve4|curveShape4.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_09|guts_GRP|curve2attachedCurve1.do";
connectAttr "attachCurve27.oc" "|waveGRP_09|guts_GRP|curve2attachedCurve1|curve2attachedCurveShape1.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_09|guts_GRP|curve2attachedCurve1attachedCurve1.do"
		;
connectAttr "attachCurve26.oc" "|waveGRP_09|guts_GRP|curve2attachedCurve1attachedCurve1|curve2attachedCurve1attachedCurveShape1.cr"
		;
connectAttr "waveCtrlGRP_09Buf_parentConstraint1.ctx" "waveCtrlGRP_09Buf.tx";
connectAttr "waveCtrlGRP_09Buf_parentConstraint1.cty" "waveCtrlGRP_09Buf.ty";
connectAttr "waveCtrlGRP_09Buf_parentConstraint1.ctz" "waveCtrlGRP_09Buf.tz";
connectAttr "waveCtrlGRP_09Buf_parentConstraint1.crx" "waveCtrlGRP_09Buf.rx";
connectAttr "waveCtrlGRP_09Buf_parentConstraint1.cry" "waveCtrlGRP_09Buf.ry";
connectAttr "waveCtrlGRP_09Buf_parentConstraint1.crz" "waveCtrlGRP_09Buf.rz";
connectAttr "waveGRP_09.s" "waveCtrlGRP_09Buf.s";
connectAttr "waveCtrlGRP_09Buf.ro" "waveCtrlGRP_09Buf_parentConstraint1.cro";
connectAttr "waveCtrlGRP_09Buf.pim" "waveCtrlGRP_09Buf_parentConstraint1.cpim";
connectAttr "waveCtrlGRP_09Buf.rp" "waveCtrlGRP_09Buf_parentConstraint1.crp";
connectAttr "waveCtrlGRP_09Buf.rpt" "waveCtrlGRP_09Buf_parentConstraint1.crt";
connectAttr "wave09_Ctrl.t" "waveCtrlGRP_09Buf_parentConstraint1.tg[0].tt";
connectAttr "wave09_Ctrl.rp" "waveCtrlGRP_09Buf_parentConstraint1.tg[0].trp";
connectAttr "wave09_Ctrl.rpt" "waveCtrlGRP_09Buf_parentConstraint1.tg[0].trt";
connectAttr "wave09_Ctrl.r" "waveCtrlGRP_09Buf_parentConstraint1.tg[0].tr";
connectAttr "wave09_Ctrl.ro" "waveCtrlGRP_09Buf_parentConstraint1.tg[0].tro";
connectAttr "wave09_Ctrl.s" "waveCtrlGRP_09Buf_parentConstraint1.tg[0].ts";
connectAttr "wave09_Ctrl.pm" "waveCtrlGRP_09Buf_parentConstraint1.tg[0].tpm";
connectAttr "waveCtrlGRP_09Buf_parentConstraint1.w0" "waveCtrlGRP_09Buf_parentConstraint1.tg[0].tw"
		;
connectAttr "expression8.out[0]" "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandleBuf.ty"
		;
connectAttr "expression8.out[1]" "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandleBuf.tz"
		;
connectAttr "expression8.out[2]" "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle1Buf.ty"
		;
connectAttr "expression8.out[3]" "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle1Buf.tz"
		;
connectAttr "expression8.out[4]" "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle2Buf.ty"
		;
connectAttr "expression8.out[5]" "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle2Buf.tz"
		;
connectAttr "expression8.out[6]" "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle3Buf.ty"
		;
connectAttr "expression8.out[7]" "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle3Buf.tz"
		;
connectAttr "expression8.out[8]" "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle4Buf.ty"
		;
connectAttr "expression8.out[9]" "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle4Buf.tz"
		;
connectAttr "expression8.out[10]" "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle5Buf.ty"
		;
connectAttr "expression8.out[11]" "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle5Buf.tz"
		;
connectAttr "BaseArcs.di" "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|Anchorcrv1.do"
		;
connectAttr "makeThreePointCircularArc25.oc" "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|Anchorcrv1|AnchorcrvShape1.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|Anchorcrv2.do"
		;
connectAttr "loftCurves.di" "|waveGRP_10|CRV.do";
connectAttr "attachCurve28.oc" "|waveGRP_10|CRV|CRVShape.cr";
connectAttr "BaseArcs.di" "|waveGRP_10|guts_GRP.do";
connectAttr "BaseArcs.di" "|waveGRP_10|guts_GRP|curve2.do";
connectAttr "threePointCircularArc1_waveGRP_10.oc" "|waveGRP_10|guts_GRP|curve2|curveShape2.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_10|guts_GRP|curve4.do";
connectAttr "threePointCircularArc2_waveGRP_10.oc" "|waveGRP_10|guts_GRP|curve4|curveShape4.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_10|guts_GRP|curve2attachedCurve1.do";
connectAttr "attachCurve30.oc" "|waveGRP_10|guts_GRP|curve2attachedCurve1|curve2attachedCurveShape1.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_10|guts_GRP|curve2attachedCurve1attachedCurve1.do"
		;
connectAttr "attachCurve29.oc" "|waveGRP_10|guts_GRP|curve2attachedCurve1attachedCurve1|curve2attachedCurve1attachedCurveShape1.cr"
		;
connectAttr "waveCtrlGRP_10Buf_parentConstraint1.ctx" "waveCtrlGRP_10Buf.tx";
connectAttr "waveCtrlGRP_10Buf_parentConstraint1.cty" "waveCtrlGRP_10Buf.ty";
connectAttr "waveCtrlGRP_10Buf_parentConstraint1.ctz" "waveCtrlGRP_10Buf.tz";
connectAttr "waveCtrlGRP_10Buf_parentConstraint1.crx" "waveCtrlGRP_10Buf.rx";
connectAttr "waveCtrlGRP_10Buf_parentConstraint1.cry" "waveCtrlGRP_10Buf.ry";
connectAttr "waveCtrlGRP_10Buf_parentConstraint1.crz" "waveCtrlGRP_10Buf.rz";
connectAttr "waveGRP_10.s" "waveCtrlGRP_10Buf.s";
connectAttr "waveCtrlGRP_10Buf.ro" "waveCtrlGRP_10Buf_parentConstraint1.cro";
connectAttr "waveCtrlGRP_10Buf.pim" "waveCtrlGRP_10Buf_parentConstraint1.cpim";
connectAttr "waveCtrlGRP_10Buf.rp" "waveCtrlGRP_10Buf_parentConstraint1.crp";
connectAttr "waveCtrlGRP_10Buf.rpt" "waveCtrlGRP_10Buf_parentConstraint1.crt";
connectAttr "wave10_Ctrl.t" "waveCtrlGRP_10Buf_parentConstraint1.tg[0].tt";
connectAttr "wave10_Ctrl.rp" "waveCtrlGRP_10Buf_parentConstraint1.tg[0].trp";
connectAttr "wave10_Ctrl.rpt" "waveCtrlGRP_10Buf_parentConstraint1.tg[0].trt";
connectAttr "wave10_Ctrl.r" "waveCtrlGRP_10Buf_parentConstraint1.tg[0].tr";
connectAttr "wave10_Ctrl.ro" "waveCtrlGRP_10Buf_parentConstraint1.tg[0].tro";
connectAttr "wave10_Ctrl.s" "waveCtrlGRP_10Buf_parentConstraint1.tg[0].ts";
connectAttr "wave10_Ctrl.pm" "waveCtrlGRP_10Buf_parentConstraint1.tg[0].tpm";
connectAttr "waveCtrlGRP_10Buf_parentConstraint1.w0" "waveCtrlGRP_10Buf_parentConstraint1.tg[0].tw"
		;
connectAttr "expression9.out[0]" "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandleBuf.ty"
		;
connectAttr "expression9.out[1]" "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandleBuf.tz"
		;
connectAttr "expression9.out[2]" "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle1Buf.ty"
		;
connectAttr "expression9.out[3]" "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle1Buf.tz"
		;
connectAttr "expression9.out[4]" "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle2Buf.ty"
		;
connectAttr "expression9.out[5]" "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle2Buf.tz"
		;
connectAttr "expression9.out[6]" "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle3Buf.ty"
		;
connectAttr "expression9.out[7]" "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle3Buf.tz"
		;
connectAttr "expression9.out[8]" "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle4Buf.ty"
		;
connectAttr "expression9.out[9]" "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle4Buf.tz"
		;
connectAttr "expression9.out[10]" "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle5Buf.ty"
		;
connectAttr "expression9.out[11]" "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle5Buf.tz"
		;
connectAttr "BaseArcs.di" "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|Anchorcrv1.do"
		;
connectAttr "makeThreePointCircularArc28.oc" "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|Anchorcrv1|AnchorcrvShape1.cr"
		;
connectAttr "BaseArcs.di" "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|Anchorcrv2.do"
		;
connectAttr "mesh.di" "loftedSurface1.do";
connectAttr "wave1GroupId.id" "loftedSurfaceShape1.iog.og[0].gid";
connectAttr "wave1Set.mwc" "loftedSurfaceShape1.iog.og[0].gco";
connectAttr "groupId2.id" "loftedSurfaceShape1.iog.og[1].gid";
connectAttr "tweakSet1.mwc" "loftedSurfaceShape1.iog.og[1].gco";
connectAttr "wave2GroupId.id" "loftedSurfaceShape1.iog.og[2].gid";
connectAttr "wave2Set.mwc" "loftedSurfaceShape1.iog.og[2].gco";
connectAttr "polyTweakUV1.out" "loftedSurfaceShape1.i";
connectAttr "polyTweakUV1.uvtk[0]" "loftedSurfaceShape1.uvst[0].uvtw";
connectAttr "nurbsTessellate1.op" "loftedSurfaceShape1Orig.i";
connectAttr "wave1.msg" "wave1Handle.sml";
connectAttr "wave1.amp" "wave1HandleShape.amp";
connectAttr "wave1.wav" "wave1HandleShape.wav";
connectAttr "wave1.off" "wave1HandleShape.off";
connectAttr "wave1.dr" "wave1HandleShape.dr";
connectAttr "wave1.dp" "wave1HandleShape.dp";
connectAttr "wave1.mnr" "wave1HandleShape.mnr";
connectAttr "wave1.mxr" "wave1HandleShape.mxr";
connectAttr "wave2.msg" "wave2Handle.sml";
connectAttr "wave2.amp" "wave2HandleShape.amp";
connectAttr "wave2.wav" "wave2HandleShape.wav";
connectAttr "wave2.off" "wave2HandleShape.off";
connectAttr "wave2.dr" "wave2HandleShape.dr";
connectAttr "wave2.dp" "wave2HandleShape.dp";
connectAttr "wave2.mnr" "wave2HandleShape.mnr";
connectAttr "wave2.mxr" "wave2HandleShape.mxr";
connectAttr "locator1_translateX.o" "locator1.tx";
connectAttr "locator1_translateZ.o" "locator1.tz";
connectAttr "locator1_translateY.o" "locator1.ty";
connectAttr "BaseArcs.di" "locator1.do";
connectAttr "waveTest_waveHandle_translateY.o" "|waveMasterGRP|waveHandle.ty";
connectAttr "waveTest_waveHandle_translateZ.o" "|waveMasterGRP|waveHandle.tz";
connectAttr "waveHandle_translateX.o" "|waveMasterGRP|waveHandle.tx";
connectAttr "waveHandle_visibility.o" "|waveMasterGRP|waveHandle.v";
connectAttr "waveHandle_rotateX.o" "|waveMasterGRP|waveHandle.rx";
connectAttr "waveHandle_rotateY.o" "|waveMasterGRP|waveHandle.ry";
connectAttr "waveHandle_rotateZ.o" "|waveMasterGRP|waveHandle.rz";
connectAttr "waveHandle_scaleX.o" "|waveMasterGRP|waveHandle.sx";
connectAttr "waveHandle_scaleY.o" "|waveMasterGRP|waveHandle.sy";
connectAttr "waveHandle_scaleZ.o" "|waveMasterGRP|waveHandle.sz";
connectAttr "waveTest_waveHandle1_translateY.o" "|waveMasterGRP|waveHandle1.ty";
connectAttr "waveTest_waveHandle1_translateZ.o" "|waveMasterGRP|waveHandle1.tz";
connectAttr "waveHandle1_translateX.o" "|waveMasterGRP|waveHandle1.tx";
connectAttr "waveHandle1_visibility.o" "|waveMasterGRP|waveHandle1.v";
connectAttr "waveHandle1_rotateX.o" "|waveMasterGRP|waveHandle1.rx";
connectAttr "waveHandle1_rotateY.o" "|waveMasterGRP|waveHandle1.ry";
connectAttr "waveHandle1_rotateZ.o" "|waveMasterGRP|waveHandle1.rz";
connectAttr "waveHandle1_scaleX.o" "|waveMasterGRP|waveHandle1.sx";
connectAttr "waveHandle1_scaleY.o" "|waveMasterGRP|waveHandle1.sy";
connectAttr "waveHandle1_scaleZ.o" "|waveMasterGRP|waveHandle1.sz";
connectAttr "waveTest_waveHandle2_translateY.o" "|waveMasterGRP|waveHandle2.ty";
connectAttr "waveTest_waveHandle2_translateZ.o" "|waveMasterGRP|waveHandle2.tz";
connectAttr "waveHandle2_translateX.o" "|waveMasterGRP|waveHandle2.tx";
connectAttr "waveHandle2_visibility.o" "|waveMasterGRP|waveHandle2.v";
connectAttr "waveHandle2_rotateX.o" "|waveMasterGRP|waveHandle2.rx";
connectAttr "waveHandle2_rotateY.o" "|waveMasterGRP|waveHandle2.ry";
connectAttr "waveHandle2_rotateZ.o" "|waveMasterGRP|waveHandle2.rz";
connectAttr "waveHandle2_scaleX.o" "|waveMasterGRP|waveHandle2.sx";
connectAttr "waveHandle2_scaleY.o" "|waveMasterGRP|waveHandle2.sy";
connectAttr "waveHandle2_scaleZ.o" "|waveMasterGRP|waveHandle2.sz";
connectAttr "waveTest_waveHandle3_translateY.o" "|waveMasterGRP|waveHandle3.ty";
connectAttr "waveTest_waveHandle3_translateZ.o" "|waveMasterGRP|waveHandle3.tz";
connectAttr "waveHandle3_translateX.o" "|waveMasterGRP|waveHandle3.tx";
connectAttr "waveHandle3_visibility.o" "|waveMasterGRP|waveHandle3.v";
connectAttr "waveHandle3_rotateX.o" "|waveMasterGRP|waveHandle3.rx";
connectAttr "waveHandle3_rotateY.o" "|waveMasterGRP|waveHandle3.ry";
connectAttr "waveHandle3_rotateZ.o" "|waveMasterGRP|waveHandle3.rz";
connectAttr "waveHandle3_scaleX.o" "|waveMasterGRP|waveHandle3.sx";
connectAttr "waveHandle3_scaleY.o" "|waveMasterGRP|waveHandle3.sy";
connectAttr "waveHandle3_scaleZ.o" "|waveMasterGRP|waveHandle3.sz";
connectAttr "waveTest_waveHandle4_translateY.o" "|waveMasterGRP|waveHandle4.ty";
connectAttr "waveTest_waveHandle4_translateZ.o" "|waveMasterGRP|waveHandle4.tz";
connectAttr "waveHandle4_translateX.o" "|waveMasterGRP|waveHandle4.tx";
connectAttr "waveHandle4_visibility.o" "|waveMasterGRP|waveHandle4.v";
connectAttr "waveHandle4_rotateX.o" "|waveMasterGRP|waveHandle4.rx";
connectAttr "waveHandle4_rotateY.o" "|waveMasterGRP|waveHandle4.ry";
connectAttr "waveHandle4_rotateZ.o" "|waveMasterGRP|waveHandle4.rz";
connectAttr "waveHandle4_scaleX.o" "|waveMasterGRP|waveHandle4.sx";
connectAttr "waveHandle4_scaleY.o" "|waveMasterGRP|waveHandle4.sy";
connectAttr "waveHandle4_scaleZ.o" "|waveMasterGRP|waveHandle4.sz";
connectAttr "waveTest_waveHandle5_translateY.o" "|waveMasterGRP|waveHandle5.ty";
connectAttr "waveTest_waveHandle5_translateZ.o" "|waveMasterGRP|waveHandle5.tz";
connectAttr "waveHandle5_translateX.o" "|waveMasterGRP|waveHandle5.tx";
connectAttr "waveHandle5_visibility.o" "|waveMasterGRP|waveHandle5.v";
connectAttr "waveHandle5_rotateX.o" "|waveMasterGRP|waveHandle5.rx";
connectAttr "waveHandle5_rotateY.o" "|waveMasterGRP|waveHandle5.ry";
connectAttr "waveHandle5_rotateZ.o" "|waveMasterGRP|waveHandle5.rz";
connectAttr "waveHandle5_scaleX.o" "|waveMasterGRP|waveHandle5.sx";
connectAttr "waveHandle5_scaleY.o" "|waveMasterGRP|waveHandle5.sy";
connectAttr "waveHandle5_scaleZ.o" "|waveMasterGRP|waveHandle5.sz";
connectAttr "waveTest_makeNurbCircle1.oc" "|waveMasterGRP|waveHandle5|nurbsCircleShape1.cr"
		;
connectAttr "BaseArcs.di" "|waveMasterGRP|Anchorcrv1.do";
connectAttr "waveTest_makeThreePointCircularArc3.oc" "|waveMasterGRP|Anchorcrv1|AnchorcrvShape1.cr"
		;
connectAttr "BaseArcs.di" "|waveMasterGRP|Anchorcrv2.do";
connectAttr "loftCurves.di" "|waveMasterGRP|CRV.do";
connectAttr "waveTest_attachCurve3.oc" "|waveMasterGRP|CRV|CRVShape.cr";
connectAttr "BaseArcs.di" "|waveMasterGRP|guts_GRP.do";
connectAttr "threePointCircularArc1_waveMasterGRP.oc" "|waveMasterGRP|guts_GRP|curve2|curveShape2.cr"
		;
connectAttr "threePointCircularArc2_waveMasterGRP.oc" "|waveMasterGRP|guts_GRP|curve4|curveShape4.cr"
		;
connectAttr "waveTest_attachCurve1.oc" "|waveMasterGRP|guts_GRP|curve2attachedCurve1|curve2attachedCurveShape1.cr"
		;
connectAttr "waveTest_attachCurve2.oc" "|waveMasterGRP|guts_GRP|curve2attachedCurve1attachedCurve1|curve2attachedCurve1attachedCurveShape1.cr"
		;
connectAttr "BaseArcs.di" "CRVBaseWire.do";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandleBuf|waveHandle|waveHandleLoc|waveHandleLocShape.wp" "threePointCircularArc1_waveGRP_01.pt1"
		;
connectAttr "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle1Buf|waveHandle1|waveHandle1Loc|waveHandle1LocShape.wp" "threePointCircularArc1_waveGRP_01.pt2"
		;
connectAttr "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle2Buf|waveHandle2|waveHandle2Loc|waveHandle2LocShape.wp" "threePointCircularArc1_waveGRP_01.pt3"
		;
connectAttr "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle4Buf|waveHandle4|waveHandle4Loc|waveHandle4LocShape.wp" "threePointCircularArc2_waveGRP_01.pt1"
		;
connectAttr "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle3Buf|waveHandle3|waveHandle3Loc|waveHandle3LocShape.wp" "threePointCircularArc2_waveGRP_01.pt2"
		;
connectAttr "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|waveHandle5Buf|waveHandle5|waveHandle5Loc|waveHandle5LocShape.wp" "threePointCircularArc2_waveGRP_01.pt3"
		;
connectAttr "|waveGRP_01|guts_GRP|curve2|curveShape2.ws" "attachCurve1.ic1";
connectAttr "|waveGRP_01|guts_GRP|curve4|curveShape4.ws" "attachCurve1.ic2";
connectAttr "|waveGRP_01|guts_GRP|curve2attachedCurve1|curve2attachedCurveShape1.ws" "attachCurve2.ic1"
		;
connectAttr "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|Anchorcrv1|AnchorcrvShape1.ws" "attachCurve2.ic2"
		;
connectAttr "|waveGRP_01|guts_GRP|curve2attachedCurve1attachedCurve1|curve2attachedCurve1attachedCurveShape1.ws" "attachCurve3.ic1"
		;
connectAttr "|waveGRP_01|waveCtrlGRP_01Buf|waveCtrlGRP_01|Anchorcrv2|AnchorcrvShape2.ws" "attachCurve3.ic2"
		;
connectAttr "layerManager.dli[1]" "BaseArcs.id";
connectAttr "layerManager.dli[2]" "loftCurves.id";
connectAttr "|waveGRP_02|guts_GRP|curve2attachedCurve1attachedCurve1|curve2attachedCurve1attachedCurveShape1.ws" "attachCurve4.ic1"
		;
connectAttr "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|Anchorcrv2|AnchorcrvShape2.ws" "attachCurve4.ic2"
		;
connectAttr "|waveGRP_02|guts_GRP|curve2attachedCurve1|curve2attachedCurveShape1.ws" "attachCurve5.ic1"
		;
connectAttr "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|Anchorcrv1|AnchorcrvShape1.ws" "attachCurve5.ic2"
		;
connectAttr "|waveGRP_02|guts_GRP|curve2|curveShape2.ws" "attachCurve6.ic1";
connectAttr "|waveGRP_02|guts_GRP|curve4|curveShape4.ws" "attachCurve6.ic2";
connectAttr "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandleBuf|waveHandle|waveHandleLoc|waveHandleLocShape.wp" "threePointCircularArc1_waveGRP_02.pt1"
		;
connectAttr "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle1Buf|waveHandle1|waveHandle1Loc|waveHandle1LocShape.wp" "threePointCircularArc1_waveGRP_02.pt2"
		;
connectAttr "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle2Buf|waveHandle2|waveHandle2Loc|waveHandle2LocShape.wp" "threePointCircularArc1_waveGRP_02.pt3"
		;
connectAttr "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle4Buf|waveHandle4|waveHandle4Loc|waveHandle4LocShape.wp" "threePointCircularArc2_waveGRP_02.pt1"
		;
connectAttr "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle3Buf|waveHandle3|waveHandle3Loc|waveHandle3LocShape.wp" "threePointCircularArc2_waveGRP_02.pt2"
		;
connectAttr "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandle5Buf|waveHandle5|waveHandle5Loc|waveHandle5LocShape.wp" "threePointCircularArc2_waveGRP_02.pt3"
		;
connectAttr "|waveGRP_03|guts_GRP|curve2attachedCurve1attachedCurve1|curve2attachedCurve1attachedCurveShape1.ws" "attachCurve7.ic1"
		;
connectAttr "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|Anchorcrv2|AnchorcrvShape2.ws" "attachCurve7.ic2"
		;
connectAttr "|waveGRP_03|guts_GRP|curve2attachedCurve1|curve2attachedCurveShape1.ws" "attachCurve8.ic1"
		;
connectAttr "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|Anchorcrv1|AnchorcrvShape1.ws" "attachCurve8.ic2"
		;
connectAttr "|waveGRP_03|guts_GRP|curve2|curveShape2.ws" "attachCurve9.ic1";
connectAttr "|waveGRP_03|guts_GRP|curve4|curveShape4.ws" "attachCurve9.ic2";
connectAttr "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandleBuf|waveHandle|waveHandleLoc|waveHandleLocShape.wp" "threePointCircularArc1_waveGRP_03.pt1"
		;
connectAttr "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle1Buf|waveHandle1|waveHandle1Loc|waveHandle1LocShape.wp" "threePointCircularArc1_waveGRP_03.pt2"
		;
connectAttr "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle2Buf|waveHandle2|waveHandle2Loc|waveHandle2LocShape.wp" "threePointCircularArc1_waveGRP_03.pt3"
		;
connectAttr "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle4Buf|waveHandle4|waveHandle4Loc|waveHandle4LocShape.wp" "threePointCircularArc2_waveGRP_03.pt1"
		;
connectAttr "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle3Buf|waveHandle3|waveHandle3Loc|waveHandle3LocShape.wp" "threePointCircularArc2_waveGRP_03.pt2"
		;
connectAttr "|waveGRP_03|waveCtrlGRP_03Buf|waveCtrlGRP_03|waveHandle5Buf|waveHandle5|waveHandle5Loc|waveHandle5LocShape.wp" "threePointCircularArc2_waveGRP_03.pt3"
		;
connectAttr "|waveGRP_04|guts_GRP|curve2attachedCurve1attachedCurve1|curve2attachedCurve1attachedCurveShape1.ws" "attachCurve10.ic1"
		;
connectAttr "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|Anchorcrv2|AnchorcrvShape2.ws" "attachCurve10.ic2"
		;
connectAttr "|waveGRP_04|guts_GRP|curve2attachedCurve1|curve2attachedCurveShape1.ws" "attachCurve11.ic1"
		;
connectAttr "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|Anchorcrv1|AnchorcrvShape1.ws" "attachCurve11.ic2"
		;
connectAttr "|waveGRP_04|guts_GRP|curve2|curveShape2.ws" "attachCurve12.ic1";
connectAttr "|waveGRP_04|guts_GRP|curve4|curveShape4.ws" "attachCurve12.ic2";
connectAttr "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandleBuf|waveHandle|waveHandleLoc|waveHandleLocShape.wp" "threePointCircularArc1_waveGRP_04.pt1"
		;
connectAttr "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle1Buf|waveHandle1|waveHandle1Loc|waveHandle1LocShape.wp" "threePointCircularArc1_waveGRP_04.pt2"
		;
connectAttr "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle2Buf|waveHandle2|waveHandle2Loc|waveHandle2LocShape.wp" "threePointCircularArc1_waveGRP_04.pt3"
		;
connectAttr "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle4Buf|waveHandle4|waveHandle4Loc|waveHandle4LocShape.wp" "threePointCircularArc2_waveGRP_04.pt1"
		;
connectAttr "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle3Buf|waveHandle3|waveHandle3Loc|waveHandle3LocShape.wp" "threePointCircularArc2_waveGRP_04.pt2"
		;
connectAttr "|waveGRP_04|waveCtrlGRP_04Buf|waveCtrlGRP_04|waveHandle5Buf|waveHandle5|waveHandle5Loc|waveHandle5LocShape.wp" "threePointCircularArc2_waveGRP_04.pt3"
		;
connectAttr "|waveGRP_05|guts_GRP|curve2attachedCurve1attachedCurve1|curve2attachedCurve1attachedCurveShape1.ws" "attachCurve13.ic1"
		;
connectAttr "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|Anchorcrv2|AnchorcrvShape2.ws" "attachCurve13.ic2"
		;
connectAttr "|waveGRP_05|guts_GRP|curve2attachedCurve1|curve2attachedCurveShape1.ws" "attachCurve14.ic1"
		;
connectAttr "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|Anchorcrv1|AnchorcrvShape1.ws" "attachCurve14.ic2"
		;
connectAttr "|waveGRP_05|guts_GRP|curve2|curveShape2.ws" "attachCurve15.ic1";
connectAttr "|waveGRP_05|guts_GRP|curve4|curveShape4.ws" "attachCurve15.ic2";
connectAttr "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandleBuf|waveHandle|waveHandleLoc|waveHandleLocShape.wp" "threePointCircularArc1_waveGRP_05.pt1"
		;
connectAttr "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle1Buf|waveHandle1|waveHandle1Loc|waveHandle1LocShape.wp" "threePointCircularArc1_waveGRP_05.pt2"
		;
connectAttr "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle2Buf|waveHandle2|waveHandle2Loc|waveHandle2LocShape.wp" "threePointCircularArc1_waveGRP_05.pt3"
		;
connectAttr "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle4Buf|waveHandle4|waveHandle4Loc|waveHandle4LocShape.wp" "threePointCircularArc2_waveGRP_05.pt1"
		;
connectAttr "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle3Buf|waveHandle3|waveHandle3Loc|waveHandle3LocShape.wp" "threePointCircularArc2_waveGRP_05.pt2"
		;
connectAttr "|waveGRP_05|waveCtrlGRP_05Buf|waveCtrlGRP_05|waveHandle5Buf|waveHandle5|waveHandle5Loc|waveHandle5LocShape.wp" "threePointCircularArc2_waveGRP_05.pt3"
		;
connectAttr "|waveGRP_06|guts_GRP|curve2attachedCurve1attachedCurve1|curve2attachedCurve1attachedCurveShape1.ws" "attachCurve16.ic1"
		;
connectAttr "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|Anchorcrv2|AnchorcrvShape2.ws" "attachCurve16.ic2"
		;
connectAttr "|waveGRP_06|guts_GRP|curve2attachedCurve1|curve2attachedCurveShape1.ws" "attachCurve17.ic1"
		;
connectAttr "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|Anchorcrv1|AnchorcrvShape1.ws" "attachCurve17.ic2"
		;
connectAttr "|waveGRP_06|guts_GRP|curve2|curveShape2.ws" "attachCurve18.ic1";
connectAttr "|waveGRP_06|guts_GRP|curve4|curveShape4.ws" "attachCurve18.ic2";
connectAttr "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandleBuf|waveHandle|waveHandleLoc|waveHandleLocShape.wp" "threePointCircularArc1_waveGRP_06.pt1"
		;
connectAttr "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle1Buf|waveHandle1|waveHandle1Loc|waveHandle1LocShape.wp" "threePointCircularArc1_waveGRP_06.pt2"
		;
connectAttr "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle2Buf|waveHandle2|waveHandle2Loc|waveHandle2LocShape.wp" "threePointCircularArc1_waveGRP_06.pt3"
		;
connectAttr "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle4Buf|waveHandle4|waveHandle4Loc|waveHandle4LocShape.wp" "threePointCircularArc2_waveGRP_06.pt1"
		;
connectAttr "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle3Buf|waveHandle3|waveHandle3Loc|waveHandle3LocShape.wp" "threePointCircularArc2_waveGRP_06.pt2"
		;
connectAttr "|waveGRP_06|waveCtrlGRP_06Buf|waveCtrlGRP_06|waveHandle5Buf|waveHandle5|waveHandle5Loc|waveHandle5LocShape.wp" "threePointCircularArc2_waveGRP_06.pt3"
		;
connectAttr "|waveGRP_07|guts_GRP|curve2attachedCurve1attachedCurve1|curve2attachedCurve1attachedCurveShape1.ws" "attachCurve19.ic1"
		;
connectAttr "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|Anchorcrv2|AnchorcrvShape2.ws" "attachCurve19.ic2"
		;
connectAttr "|waveGRP_07|guts_GRP|curve2attachedCurve1|curve2attachedCurveShape1.ws" "attachCurve20.ic1"
		;
connectAttr "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|Anchorcrv1|AnchorcrvShape1.ws" "attachCurve20.ic2"
		;
connectAttr "|waveGRP_07|guts_GRP|curve2|curveShape2.ws" "attachCurve21.ic1";
connectAttr "|waveGRP_07|guts_GRP|curve4|curveShape4.ws" "attachCurve21.ic2";
connectAttr "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandleBuf|waveHandle|waveHandleLoc|waveHandleLocShape.wp" "threePointCircularArc1_waveGRP_07.pt1"
		;
connectAttr "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle1Buf|waveHandle1|waveHandle1Loc|waveHandle1LocShape.wp" "threePointCircularArc1_waveGRP_07.pt2"
		;
connectAttr "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle2Buf|waveHandle2|waveHandle2Loc|waveHandle2LocShape.wp" "threePointCircularArc1_waveGRP_07.pt3"
		;
connectAttr "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle4Buf|waveHandle4|waveHandle4Loc|waveHandle4LocShape.wp" "threePointCircularArc2_waveGRP_07.pt1"
		;
connectAttr "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle3Buf|waveHandle3|waveHandle3Loc|waveHandle3LocShape.wp" "threePointCircularArc2_waveGRP_07.pt2"
		;
connectAttr "|waveGRP_07|waveCtrlGRP_07Buf|waveCtrlGRP_07|waveHandle5Buf|waveHandle5|waveHandle5Loc|waveHandle5LocShape.wp" "threePointCircularArc2_waveGRP_07.pt3"
		;
connectAttr "|waveGRP_08|guts_GRP|curve2attachedCurve1attachedCurve1|curve2attachedCurve1attachedCurveShape1.ws" "attachCurve22.ic1"
		;
connectAttr "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|Anchorcrv2|AnchorcrvShape2.ws" "attachCurve22.ic2"
		;
connectAttr "|waveGRP_08|guts_GRP|curve2attachedCurve1|curve2attachedCurveShape1.ws" "attachCurve23.ic1"
		;
connectAttr "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|Anchorcrv1|AnchorcrvShape1.ws" "attachCurve23.ic2"
		;
connectAttr "|waveGRP_08|guts_GRP|curve2|curveShape2.ws" "attachCurve24.ic1";
connectAttr "|waveGRP_08|guts_GRP|curve4|curveShape4.ws" "attachCurve24.ic2";
connectAttr "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandleBuf|waveHandle|waveHandleLoc|waveHandleLocShape.wp" "threePointCircularArc1_waveGRP_08.pt1"
		;
connectAttr "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle1Buf|waveHandle1|waveHandle1Loc|waveHandle1LocShape.wp" "threePointCircularArc1_waveGRP_08.pt2"
		;
connectAttr "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle2Buf|waveHandle2|waveHandle2Loc|waveHandle2LocShape.wp" "threePointCircularArc1_waveGRP_08.pt3"
		;
connectAttr "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle4Buf|waveHandle4|waveHandle4Loc|waveHandle4LocShape.wp" "threePointCircularArc2_waveGRP_08.pt1"
		;
connectAttr "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle3Buf|waveHandle3|waveHandle3Loc|waveHandle3LocShape.wp" "threePointCircularArc2_waveGRP_08.pt2"
		;
connectAttr "|waveGRP_08|waveCtrlGRP_08Buf|waveCtrlGRP_08|waveHandle5Buf|waveHandle5|waveHandle5Loc|waveHandle5LocShape.wp" "threePointCircularArc2_waveGRP_08.pt3"
		;
connectAttr "|waveGRP_09|guts_GRP|curve2attachedCurve1attachedCurve1|curve2attachedCurve1attachedCurveShape1.ws" "attachCurve25.ic1"
		;
connectAttr "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|Anchorcrv2|AnchorcrvShape2.ws" "attachCurve25.ic2"
		;
connectAttr "|waveGRP_09|guts_GRP|curve2attachedCurve1|curve2attachedCurveShape1.ws" "attachCurve26.ic1"
		;
connectAttr "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|Anchorcrv1|AnchorcrvShape1.ws" "attachCurve26.ic2"
		;
connectAttr "|waveGRP_09|guts_GRP|curve2|curveShape2.ws" "attachCurve27.ic1";
connectAttr "|waveGRP_09|guts_GRP|curve4|curveShape4.ws" "attachCurve27.ic2";
connectAttr "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandleBuf|waveHandle|waveHandleLoc|waveHandleLocShape.wp" "threePointCircularArc1_waveGRP_09.pt1"
		;
connectAttr "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle1Buf|waveHandle1|waveHandle1Loc|waveHandle1LocShape.wp" "threePointCircularArc1_waveGRP_09.pt2"
		;
connectAttr "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle2Buf|waveHandle2|waveHandle2Loc|waveHandle2LocShape.wp" "threePointCircularArc1_waveGRP_09.pt3"
		;
connectAttr "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle4Buf|waveHandle4|waveHandle4Loc|waveHandle4LocShape.wp" "threePointCircularArc2_waveGRP_09.pt1"
		;
connectAttr "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle3Buf|waveHandle3|waveHandle3Loc|waveHandle3LocShape.wp" "threePointCircularArc2_waveGRP_09.pt2"
		;
connectAttr "|waveGRP_09|waveCtrlGRP_09Buf|waveCtrlGRP_09|waveHandle5Buf|waveHandle5|waveHandle5Loc|waveHandle5LocShape.wp" "threePointCircularArc2_waveGRP_09.pt3"
		;
connectAttr "|waveGRP_10|guts_GRP|curve2attachedCurve1attachedCurve1|curve2attachedCurve1attachedCurveShape1.ws" "attachCurve28.ic1"
		;
connectAttr "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|Anchorcrv2|AnchorcrvShape2.ws" "attachCurve28.ic2"
		;
connectAttr "|waveGRP_10|guts_GRP|curve2attachedCurve1|curve2attachedCurveShape1.ws" "attachCurve29.ic1"
		;
connectAttr "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|Anchorcrv1|AnchorcrvShape1.ws" "attachCurve29.ic2"
		;
connectAttr "|waveGRP_10|guts_GRP|curve2|curveShape2.ws" "attachCurve30.ic1";
connectAttr "|waveGRP_10|guts_GRP|curve4|curveShape4.ws" "attachCurve30.ic2";
connectAttr "ramp1.oc" "lambert2.c";
connectAttr "lambert2.oc" "lambert2SG.ss";
connectAttr "loftedSurfaceShape1.iog" "lambert2SG.dsm" -na;
connectAttr "lambert2SG.msg" "materialInfo1.sg";
connectAttr "lambert2.msg" "materialInfo1.m";
connectAttr "ramp1.msg" "materialInfo1.t" -na;
connectAttr "place2dTexture1.o" "ramp1.uv";
connectAttr "place2dTexture1.ofs" "ramp1.fs";
connectAttr "locator1.tz" "place2dTexture1.ofu";
connectAttr "|waveGRP_01|CRV|CRVShape.ws" "loft1.ic[1]";
connectAttr "|waveGRP_02|CRV|CRVShape.ws" "loft1.ic[2]";
connectAttr "|waveGRP_03|CRV|CRVShape.ws" "loft1.ic[3]";
connectAttr "|waveGRP_04|CRV|CRVShape.ws" "loft1.ic[4]";
connectAttr "|waveGRP_05|CRV|CRVShape.ws" "loft1.ic[5]";
connectAttr "|waveGRP_06|CRV|CRVShape.ws" "loft1.ic[6]";
connectAttr "|waveGRP_07|CRV|CRVShape.ws" "loft1.ic[7]";
connectAttr "|waveGRP_08|CRV|CRVShape.ws" "loft1.ic[8]";
connectAttr "|waveGRP_09|CRV|CRVShape.ws" "loft1.ic[9]";
connectAttr "|waveGRP_10|CRV|CRVShape.ws" "loft1.ic[10]";
connectAttr "loft1.os" "nurbsTessellate1.is";
connectAttr "wave1_offset.o" "wave1.off";
connectAttr "wave1GroupParts.og" "wave1.ip[0].ig";
connectAttr "wave1GroupId.id" "wave1.ip[0].gi";
connectAttr "wave1HandleShape.dd" "wave1.dd";
connectAttr "wave1Handle.wm" "wave1.ma";
connectAttr "wave1_envelope.o" "wave1.en";
connectAttr "groupParts2.og" "tweak1.ip[0].ig";
connectAttr "groupId2.id" "tweak1.ip[0].gi";
connectAttr "wave1GroupId.msg" "wave1Set.gn" -na;
connectAttr "loftedSurfaceShape1.iog.og[0]" "wave1Set.dsm" -na;
connectAttr "wave1.msg" "wave1Set.ub[0]";
connectAttr "tweak1.og[0]" "wave1GroupParts.ig";
connectAttr "wave1GroupId.id" "wave1GroupParts.gi";
connectAttr "groupId2.msg" "tweakSet1.gn" -na;
connectAttr "loftedSurfaceShape1.iog.og[1]" "tweakSet1.dsm" -na;
connectAttr "tweak1.msg" "tweakSet1.ub[0]";
connectAttr "loftedSurfaceShape1Orig.w" "groupParts2.ig";
connectAttr "groupId2.id" "groupParts2.gi";
connectAttr "wave2_offset.o" "wave2.off";
connectAttr "wave2GroupParts.og" "wave2.ip[0].ig";
connectAttr "wave2GroupId.id" "wave2.ip[0].gi";
connectAttr "wave2HandleShape.dd" "wave2.dd";
connectAttr "wave2Handle.wm" "wave2.ma";
connectAttr "wave2_envelope.o" "wave2.en";
connectAttr "wave2GroupId.msg" "wave2Set.gn" -na;
connectAttr "loftedSurfaceShape1.iog.og[2]" "wave2Set.dsm" -na;
connectAttr "wave2.msg" "wave2Set.ub[0]";
connectAttr "wave1.og[0]" "wave2GroupParts.ig";
connectAttr "wave2GroupId.id" "wave2GroupParts.gi";
connectAttr "wave2.og[0]" "polyMirror1.ip";
connectAttr "loftedSurfaceShape1.wm" "polyMirror1.mp";
connectAttr "polyMirror1.out" "polyTweakUV1.ip";
connectAttr ":time1.o" "expression1.tim";
connectAttr "|waveGRP_02|waveCtrlGRP_02Buf|waveCtrlGRP_02|waveHandleBuf.msg" "expression1.obm"
		;
connectAttr "wave02_Ctrl.offset" "expression1.in[0]";
connectAttr "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle4Buf|waveHandle4|waveHandle4Loc|waveHandle4LocShape.wp" "threePointCircularArc2_waveGRP_10.pt1"
		;
connectAttr "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle3Buf|waveHandle3|waveHandle3Loc|waveHandle3LocShape.wp" "threePointCircularArc2_waveGRP_10.pt2"
		;
connectAttr "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle5Buf|waveHandle5|waveHandle5Loc|waveHandle5LocShape.wp" "threePointCircularArc2_waveGRP_10.pt3"
		;
connectAttr "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandleBuf|waveHandle|waveHandleLoc|waveHandleLocShape.wp" "threePointCircularArc1_waveGRP_10.pt1"
		;
connectAttr "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle1Buf|waveHandle1|waveHandle1Loc|waveHandle1LocShape.wp" "threePointCircularArc1_waveGRP_10.pt2"
		;
connectAttr "|waveGRP_10|waveCtrlGRP_10Buf|waveCtrlGRP_10|waveHandle2Buf|waveHandle2|waveHandle2Loc|waveHandle2LocShape.wp" "threePointCircularArc1_waveGRP_10.pt3"
		;
connectAttr "wave03_Ctrl.offset" "expression2.in[0]";
connectAttr ":time1.o" "expression2.tim";
connectAttr "wave04_Ctrl.offset" "expression3.in[0]";
connectAttr ":time1.o" "expression3.tim";
connectAttr "wave05_Ctrl.offset" "expression4.in[0]";
connectAttr ":time1.o" "expression4.tim";
connectAttr "wave06_Ctrl.offset" "expression5.in[0]";
connectAttr ":time1.o" "expression5.tim";
connectAttr "wave07_Ctrl.offset" "expression6.in[0]";
connectAttr ":time1.o" "expression6.tim";
connectAttr "wave04_Ctrl.msg" "expression6.obm";
connectAttr "wave08_Ctrl.offset" "expression7.in[0]";
connectAttr ":time1.o" "expression7.tim";
connectAttr "wave09_Ctrl.offset" "expression8.in[0]";
connectAttr ":time1.o" "expression8.tim";
connectAttr "wave10_Ctrl.offset" "expression9.in[0]";
connectAttr ":time1.o" "expression9.tim";
connectAttr "|waveMasterGRP|guts_GRP|curve2attachedCurve1attachedCurve1|curve2attachedCurve1attachedCurveShape1.ws" "waveTest_attachCurve3.ic1"
		;
connectAttr "|waveMasterGRP|Anchorcrv2|AnchorcrvShape2.ws" "waveTest_attachCurve3.ic2"
		;
connectAttr "|waveMasterGRP|waveHandle.tx" "threePointCircularArc1_waveMasterGRP.p1x"
		;
connectAttr "|waveMasterGRP|waveHandle.ty" "threePointCircularArc1_waveMasterGRP.p1y"
		;
connectAttr "|waveMasterGRP|waveHandle.tz" "threePointCircularArc1_waveMasterGRP.p1z"
		;
connectAttr "|waveMasterGRP|waveHandle1.tx" "threePointCircularArc1_waveMasterGRP.p2x"
		;
connectAttr "|waveMasterGRP|waveHandle1.ty" "threePointCircularArc1_waveMasterGRP.p2y"
		;
connectAttr "|waveMasterGRP|waveHandle1.tz" "threePointCircularArc1_waveMasterGRP.p2z"
		;
connectAttr "|waveMasterGRP|waveHandle2.tx" "threePointCircularArc1_waveMasterGRP.p3x"
		;
connectAttr "|waveMasterGRP|waveHandle2.ty" "threePointCircularArc1_waveMasterGRP.p3y"
		;
connectAttr "|waveMasterGRP|waveHandle2.tz" "threePointCircularArc1_waveMasterGRP.p3z"
		;
connectAttr "|waveMasterGRP|waveHandle4.t" "threePointCircularArc2_waveMasterGRP.pt1"
		;
connectAttr "|waveMasterGRP|waveHandle3.tx" "threePointCircularArc2_waveMasterGRP.p2x"
		;
connectAttr "|waveMasterGRP|waveHandle3.ty" "threePointCircularArc2_waveMasterGRP.p2y"
		;
connectAttr "|waveMasterGRP|waveHandle3.tz" "threePointCircularArc2_waveMasterGRP.p2z"
		;
connectAttr "|waveMasterGRP|waveHandle5.t" "threePointCircularArc2_waveMasterGRP.pt3"
		;
connectAttr "|waveMasterGRP|guts_GRP|curve2|curveShape2.ws" "waveTest_attachCurve1.ic1"
		;
connectAttr "|waveMasterGRP|guts_GRP|curve4|curveShape4.ws" "waveTest_attachCurve1.ic2"
		;
connectAttr "|waveMasterGRP|guts_GRP|curve2attachedCurve1|curve2attachedCurveShape1.ws" "waveTest_attachCurve2.ic1"
		;
connectAttr "|waveMasterGRP|Anchorcrv1|AnchorcrvShape1.ws" "waveTest_attachCurve2.ic2"
		;
connectAttr "wave01_Ctrl.offset" "expression10.in[0]";
connectAttr ":time1.o" "expression10.tim";
connectAttr "layerManager.dli[5]" "mesh.id";
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "lambert2.msg" ":defaultShaderList1.s" -na;
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "ramp1.msg" ":defaultTextureList1.tx" -na;
// End of Wavepro.01.5.ma
